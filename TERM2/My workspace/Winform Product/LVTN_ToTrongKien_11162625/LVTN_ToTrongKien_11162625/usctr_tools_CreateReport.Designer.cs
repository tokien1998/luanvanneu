﻿namespace LVTN_ToTrongKien_11162625
{
    partial class usctr_tools_CreateReport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.panelTitle = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panelMenu = new System.Windows.Forms.Panel();
            this.cmbReportTitle = new System.Windows.Forms.ComboBox();
            this.cmbReportGroup = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panelMainReport = new System.Windows.Forms.Panel();
            this.panelTitle.SuspendLayout();
            this.panelMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(367, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(372, 35);
            this.label1.TabIndex = 3;
            this.label1.Text = "CHỨC NĂNG BÁO CÁO";
            // 
            // panelTitle
            // 
            this.panelTitle.Controls.Add(this.flowLayoutPanel1);
            this.panelTitle.Controls.Add(this.label1);
            this.panelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitle.Location = new System.Drawing.Point(0, 0);
            this.panelTitle.Name = "panelTitle";
            this.panelTitle.Size = new System.Drawing.Size(1118, 55);
            this.panelTitle.TabIndex = 4;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 61);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(159, 680);
            this.flowLayoutPanel1.TabIndex = 5;
            // 
            // panelMenu
            // 
            this.panelMenu.Controls.Add(this.cmbReportTitle);
            this.panelMenu.Controls.Add(this.cmbReportGroup);
            this.panelMenu.Controls.Add(this.label3);
            this.panelMenu.Controls.Add(this.label2);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 55);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(165, 689);
            this.panelMenu.TabIndex = 5;
            // 
            // cmbReportTitle
            // 
            this.cmbReportTitle.FormattingEnabled = true;
            this.cmbReportTitle.Location = new System.Drawing.Point(6, 279);
            this.cmbReportTitle.Name = "cmbReportTitle";
            this.cmbReportTitle.Size = new System.Drawing.Size(153, 21);
            this.cmbReportTitle.TabIndex = 8;
            this.cmbReportTitle.SelectedIndexChanged += new System.EventHandler(this.cmbReportTitle_SelectedIndexChanged);
            // 
            // cmbReportGroup
            // 
            this.cmbReportGroup.FormattingEnabled = true;
            this.cmbReportGroup.Location = new System.Drawing.Point(6, 206);
            this.cmbReportGroup.Name = "cmbReportGroup";
            this.cmbReportGroup.Size = new System.Drawing.Size(153, 21);
            this.cmbReportGroup.TabIndex = 7;
            this.cmbReportGroup.SelectedIndexChanged += new System.EventHandler(this.cmbReportGroup_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 253);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Báo cáo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 180);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Nhóm báo cáo:";
            // 
            // panelMainReport
            // 
            this.panelMainReport.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMainReport.Location = new System.Drawing.Point(165, 55);
            this.panelMainReport.Name = "panelMainReport";
            this.panelMainReport.Size = new System.Drawing.Size(953, 689);
            this.panelMainReport.TabIndex = 6;
            // 
            // usctr_tools_CreateReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelMainReport);
            this.Controls.Add(this.panelMenu);
            this.Controls.Add(this.panelTitle);
            this.Name = "usctr_tools_CreateReport";
            this.Size = new System.Drawing.Size(1118, 744);
            this.Load += new System.EventHandler(this.usctr_tools_CreateReport_Load);
            this.panelTitle.ResumeLayout(false);
            this.panelTitle.PerformLayout();
            this.panelMenu.ResumeLayout(false);
            this.panelMenu.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelTitle;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panelMenu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panelMainReport;
        private System.Windows.Forms.ComboBox cmbReportTitle;
        private System.Windows.Forms.ComboBox cmbReportGroup;
    }
}
