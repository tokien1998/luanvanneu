﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using DevExpress.XtraEditors;
using System.Windows.Forms;

namespace LVTN_ToTrongKien_11162625
{
    //
    //--------------------------------------------------CLASS TỔNG HỢP CÁC HÀM TỰ VIẾT--------------------------------------------------
    //

    class PowerQuery
    {
        SqlConnection conn;

        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // 
        // Phương thức kiểm tra bản ghi nhập vào có trùng với DB hiện có không
        //
        public bool Sql_CheckDuplicate(string tableName, string[] columnNames, string[] columnValues)
        {
            conn = new SqlConnection(str);

            SqlDataReader dr;
            SqlCommand cmd;

            bool result = true; // Giá trị trả về mặc định là trùng (báo lỗi)

            string defaultQuery = "SELECT * FROM " + tableName + " WHERE "; //Câu lệnh mặc định: select * from [tables] where [conditions].
            string finalQuery = ""; // Câu lệnh sau khi hoàn thành

            if (!(columnNames.Count() == columnValues.Count())) //Nếu số giá trị <> số cột thì báo lỗi
            {
                XtraMessageBox.Show("Số giá trị và số cột được khai báo không khớp nhau \nLiên hệ IT support để được hỗ trợ sửa code.", "LỖI HỆ THỐNG", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    conn.Open();
                    // Nối câu query thành câu hoàn chỉnh
                    for (int i = 0; i < columnNames.Count(); i++)
                    {
                        // [conditions] = "LOWER(column) = LOWER(N'value') AND"
                        defaultQuery += "LOWER(" + columnNames[i] + ") = LOWER(N'" + columnValues[i] + "') AND ";
                    }

                    // Thêm điều kiện trung tính (1 = 1) để câu lệnh ko thay đổi
                    finalQuery = defaultQuery + "(1 = 1)";

                    // Thực thi câu lệnh
                    cmd = new SqlCommand(finalQuery, conn);
                    dr = cmd.ExecuteReader();
                    if (dr.HasRows) // Query cho ra ít nhất 1 record => Có trùng
                    {
                        result = true;
                    }
                    else
                    {
                        result = false;
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: PowerQuery.cs (Line 48 - 68).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                }
            }

            return result;
        }


        // 
        // Phương thức đổ dữ liệu vào grid
        //
        public DataTable ShowFullData(string tableName, string selectedColumns, string afterWhereStr)
        {
            conn = new SqlConnection(str);

            DataTable dt = new DataTable();
            SqlDataAdapter da;

            string finalQuery = "SELECT " + selectedColumns + " FROM " + tableName + " WHERE " + afterWhereStr;

            try
            {
                conn.Open();
                da = new SqlDataAdapter(finalQuery, conn);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n \n \nLocation: PowerQuery.cs (Line 98 - 100).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }

            return dt;
        }

        // 
        // Phương thức format caption
        //
        public void SetCaption(DevExpress.XtraGrid.Views.Grid.GridView grv, string[] columnCaptions)
        {
            for (int i = 0; i < columnCaptions.Count(); i++)
            {
                grv.Columns[i].Caption = columnCaptions[i].ToString(); // Đặt title
            }

            grv.BestFitColumns(); // fit các cột tự động
        }

        //
        // Phương thức check chuỗi là số
        //
        public bool IsNumber(string text)
        {
            ulong result; // Kiểu số interger lớn nhất trong c#
            var isNumeric = UInt64.TryParse(text, out result);
            return isNumeric;
        }

        //
        // Phương thức chuyển số thành chữ
        //

        // how to call the number
        static string[] number_name = new string[] {
                                                        "không",    //0
                                                        "một",      //1
                                                        "hai",      //2
                                                        "ba",       //3
                                                        "bốn",      //4
                                                        "năm",      //5
                                                        "sáu",      //6
                                                        "bảy",      //7
                                                        "tám",      //8
                                                        "chín"      //9
                                                    };

        //how to call the group number name, ie : 1000 call one *thous&&*
        //tỷ tỷ = 1E18
        static string[] group_number_name = new string[] {
                                                            "",   // less than 1E3
                                                            "ngàn",  //1E3
                                                            "triệu",  //1E6 
                                                            "tỷ",     //1E9
                                                            "nghìn tỷ",  //1E12
                                                            "triệu tỷ",  //1E15
                                                            "tỷ tỷ"      //1E18
                                                           };

        public string NumberToString(long number_input)
        {
            if (number_input == 0) return "không";

            bool isNegative = false;
            if (number_input < 0)
            {
                number_input = -number_input;
                isNegative = true;
            }

            int unit = -1, tenth = -1, hundreds = -1;
            int group_index = 0;
            string str_output = "";
            //loop through the } of str_number to the {ning
            while (number_input > 0)
            {
                unit = (int)(number_input % 10);
                number_input = number_input / 10;

                if (number_input > 0)
                {
                    tenth = (int)(number_input % 10);
                    number_input = number_input / 10;
                }
                else
                    tenth = -1;

                if (number_input > 0)
                {
                    hundreds = (int)(number_input % 10);
                    number_input = number_input / 10;
                }
                else
                    hundreds = -1;

                //three digits make a group
                if (unit > 0 || tenth > 0 || hundreds > 0 || group_index == 3)
                    str_output = group_number_name[group_index] + str_output;
                group_index = group_index + 1;

                if (group_index > 3)
                    group_index = 1;

                if (unit == 1 && tenth > 1)
                    str_output = "một " + str_output;
                else if (unit == 5 && tenth > 0)
                    str_output = "lăm " + str_output;
                else if (unit > 0)
                    str_output = number_name[unit] + " " + str_output;

                if (tenth < 0) break;

                if (tenth == 0 && unit > 0)
                    str_output = "lẻ " + str_output;
                else if (tenth == 1)
                    str_output = "mười " + str_output;
                else if (tenth > 1)
                    str_output = number_name[tenth] + " mươi " + str_output;

                if (hundreds < 0) break;

                if (hundreds > 0 || tenth > 0 || unit > 0)
                    str_output = " " + number_name[hundreds] + " trăm " + str_output;
                //While }
            }

            //change the call of "1" after tenth
            str_output = str_output.Replace("mươi một", "mươi mốt");

            if (isNegative)
                str_output = "Âm " + str_output;
            return str_output.Trim();
        }
    }
}
