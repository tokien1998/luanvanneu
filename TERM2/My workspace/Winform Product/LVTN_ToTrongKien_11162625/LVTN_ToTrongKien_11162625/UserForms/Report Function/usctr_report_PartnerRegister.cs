﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using DevExpress.XtraEditors;
using LVTN_ToTrongKien_11162625.Report;

namespace LVTN_ToTrongKien_11162625.UserForms.Report_Function
{
    public partial class usctr_report_PartnerRegister : UserControl
    {
        // Khai báo biến
        private static usctr_report_PartnerRegister usctr; // User control

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_report_PartnerRegister UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_report_PartnerRegister();
                }
                return usctr;
            }
        }

        public usctr_report_PartnerRegister()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void usctr_report_PartnerRegister_Load(object sender, EventArgs e)
        {
            dtpFrom.Value = DateTime.Today;
            dtpTo.Value = DateTime.Today;
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            if (dtpFrom.Value > dtpTo.Value)
            {
                XtraMessageBox.Show("Quãng thời gian chọn không hợp lệ", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                conn.Open();
                string query = "SELECT COUNT(distinct partner_id) FROM partner_register WHERE register_date BETWEEN '" + dtpFrom.Text + "' AND '" + dtpTo.Text + "'";
                cmd = new SqlCommand(query, conn);
                var rs = cmd.ExecuteScalar();
                string count = "";
                if (rs == null)
                {
                    count = "0";
                }
                else
                {
                    count = rs.ToString();
                }
                conn.Close();

                try
                {
                    conn.Open();
                    da = new SqlDataAdapter("e_report_partner_register", conn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add("@from", SqlDbType.Date).Value = dtpFrom.Value;
                    da.SelectCommand.Parameters.Add("@to", SqlDbType.Date).Value = dtpTo.Value;
                    dt = new DataTable();
                    da.Fill(dt);

                    crpt_PartnerRegister rpt = new crpt_PartnerRegister();
                    rpt.SetDataSource(dt);
                    rpt.SetParameterValue("date_From", dtpFrom.Value);
                    rpt.SetParameterValue("date_To", dtpTo.Value);
                    rpt.SetParameterValue("partner_count", count);
                    crptViewer.ReportSource = null;
                    crptViewer.ReportSource = rpt;
                }
                catch(Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: ustr_report_PartnerRegister (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                }

            }
        }
    }
}
