﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using DevExpress.XtraEditors;
using LVTN_ToTrongKien_11162625.Report;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_report_StudioInfo : UserControl
    {
        // Khai báo biến
        private static usctr_report_StudioInfo usctr; // User control

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_report_StudioInfo UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_report_StudioInfo();
                }
                return usctr;
            }
        }

        public usctr_report_StudioInfo()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void usctr_report_StudioInfo_Load(object sender, EventArgs e)
        {
            ComboboxStudio();
            radCourse.Checked = false;
            radStudio.Checked = false;
        }

        private void ComboboxStudio()
        {

            string query = "SELECT 0 as studio_id, N'--Chọn--' as tag UNION ALL SELECT studio_id, tag FROM studio ORDER BY studio_id";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbStudio.DisplayMember = "tag";
            cmbStudio.ValueMember = "studio_id";
            cmbStudio.DataSource = dt;
            cmbStudio.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbStudio.SelectedIndex = 0;
            conn.Close();
        }

        private void cmbStudio_SelectedIndexChanged(object sender, EventArgs e)
        {
            conn.Open();
            string query = "SELECT full_name FROM studio_translation WHERE language = N'vi' and studio_id = " + int.Parse(cmbStudio.SelectedValue.ToString());
            cmd = new SqlCommand(query, conn);
            var result = cmd.ExecuteScalar();
            if (result == null)
            {
                lblStudioName.Text = "";
            }
            else
            {
                lblStudioName.Text = result.ToString();
            }
            conn.Close();
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            if (cmbStudio.Text == "--Chọn--" || cmbStudio.Text == "")
            {
                XtraMessageBox.Show("Chọn một đối tác để hiện báo cáo!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (radCourse.Checked == false && radStudio.Checked == false)
                {
                    XtraMessageBox.Show("Chọn một kiểu báo cáo cụ thể!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (radStudio.Checked == true)
                    {
                        conn.Open();
                        dt = new DataTable();
                        da = new SqlDataAdapter("e_report_studio_info", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudio.SelectedValue.ToString());
                        da.Fill(dt);

                        crpt_StudioInfo rp = new crpt_StudioInfo();
                        rp.SetDataSource(dt);

                        usctr.crptViewer.ReportSource = rp;
                        usctr.Show();

                        conn.Close();
                    }
                    else if (radCourse.Checked == true) 
                    {
                        conn.Open();
                        da = new SqlDataAdapter("e_report_course", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudio.SelectedValue.ToString());
                        DataSet dsCourse = new DataSet();
                        da.Fill(dsCourse);
                        conn.Close();

                        crpt_CourseInfo rpt = new crpt_CourseInfo();
                        rpt.Database.Tables["course_overall"].SetDataSource(dsCourse.Tables[0]);
                        rpt.Database.Tables["course_info"].SetDataSource(dsCourse.Tables[1]);

                        rpt.SetParameterValue("studio_name", lblStudioName.Text);
                        rpt.SetParameterValue("studio_id", cmbStudio.SelectedValue.ToString());

                        crptViewer.ReportSource = null;
                        crptViewer.ReportSource = rpt;
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }
    }
}
