﻿namespace LVTN_ToTrongKien_11162625.UserForms.Report_Function
{
    partial class usctr_report_CategoryAmenity
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(usctr_report_CategoryAmenity));
            this.crptViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radCategory = new System.Windows.Forms.RadioButton();
            this.radAmenity = new System.Windows.Forms.RadioButton();
            this.btnShowReport = new DevExpress.XtraEditors.SimpleButton();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // crptViewer
            // 
            this.crptViewer.ActiveViewIndex = -1;
            this.crptViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crptViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.crptViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crptViewer.Location = new System.Drawing.Point(0, 83);
            this.crptViewer.Name = "crptViewer";
            this.crptViewer.Size = new System.Drawing.Size(1002, 555);
            this.crptViewer.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radCategory);
            this.panel1.Controls.Add(this.radAmenity);
            this.panel1.Controls.Add(this.btnShowReport);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1002, 83);
            this.panel1.TabIndex = 2;
            // 
            // radCategory
            // 
            this.radCategory.AutoSize = true;
            this.radCategory.Location = new System.Drawing.Point(342, 31);
            this.radCategory.Name = "radCategory";
            this.radCategory.Size = new System.Drawing.Size(108, 17);
            this.radCategory.TabIndex = 9;
            this.radCategory.TabStop = true;
            this.radCategory.Text = "Thông tin dịch vụ";
            this.radCategory.UseVisualStyleBackColor = true;
            // 
            // radAmenity
            // 
            this.radAmenity.AutoSize = true;
            this.radAmenity.Location = new System.Drawing.Point(539, 31);
            this.radAmenity.Name = "radAmenity";
            this.radAmenity.Size = new System.Drawing.Size(109, 17);
            this.radAmenity.TabIndex = 8;
            this.radAmenity.TabStop = true;
            this.radAmenity.Text = "Thông tin tiện ích";
            this.radAmenity.UseVisualStyleBackColor = true;
            // 
            // btnShowReport
            // 
            this.btnShowReport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnShowReport.ImageOptions.Image")));
            this.btnShowReport.Location = new System.Drawing.Point(755, 23);
            this.btnShowReport.Name = "btnShowReport";
            this.btnShowReport.Size = new System.Drawing.Size(99, 34);
            this.btnShowReport.TabIndex = 7;
            this.btnShowReport.Text = "HIỂN THỊ";
            this.btnShowReport.Click += new System.EventHandler(this.btnShowReport_Click);
            // 
            // usctr_report_CategoryAmenity
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.crptViewer);
            this.Controls.Add(this.panel1);
            this.Name = "usctr_report_CategoryAmenity";
            this.Size = new System.Drawing.Size(1002, 638);
            this.Load += new System.EventHandler(this.usctr_report_CategoryAmenity_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private CrystalDecisions.Windows.Forms.CrystalReportViewer crptViewer;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radCategory;
        private System.Windows.Forms.RadioButton radAmenity;
        private DevExpress.XtraEditors.SimpleButton btnShowReport;
    }
}
