﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using DevExpress.XtraEditors;
using LVTN_ToTrongKien_11162625.Report;

namespace LVTN_ToTrongKien_11162625.UserForms.Report_Function
{
    public partial class usctr_report_CategoryAmenity : UserControl
    {
        // Khai báo biến
        private static usctr_report_CategoryAmenity usctr; // User control

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_report_CategoryAmenity UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_report_CategoryAmenity();
                }
                return usctr;
            }
        }

        public usctr_report_CategoryAmenity()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void usctr_report_CategoryAmenity_Load(object sender, EventArgs e)
        {
            radCategory.Checked = false;
            radAmenity.Checked = false;
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            if (radAmenity.Checked == false && radCategory.Checked == false)
            {
                XtraMessageBox.Show("Chọn một kiểu báo cáo cụ thể!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (radCategory.Checked == true)
                {
                    try
                    {
                        conn.Open();
                        string query1 = "SELECT COUNT(category_id) FROM category WHERE service_type = 'F'";
                        cmd = new SqlCommand(query1, conn);
                        var fn = cmd.ExecuteScalar();
                        string fitness_count = fn.ToString();
                        conn.Close();

                        conn.Open();
                        string query2 = "SELECT COUNT(category_id) FROM category WHERE service_type = 'B'";
                        cmd = new SqlCommand(query2, conn);
                        var bt = cmd.ExecuteScalar();
                        string beauty_count = bt.ToString();
                        conn.Close();

                        conn.Open();
                        da = new SqlDataAdapter("e_report_category", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        dt = new DataTable();
                        da.Fill(dt);

                        crpt_Category rpt = new crpt_Category();
                        rpt.SetDataSource(dt);
                        rpt.SetParameterValue("fitness_count", fitness_count);
                        rpt.SetParameterValue("beauty_count", beauty_count);
                        crptViewer.ReportSource = null;
                        crptViewer.ReportSource = rpt;
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: ustr_report_CategoryAmenity (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
                else if (radAmenity.Checked == true)
                {
                    try
                    {
                        conn.Open();
                        string query1 = "SELECT COUNT(amenity_id) FROM amenity";
                        cmd = new SqlCommand(query1, conn);
                        var am = cmd.ExecuteScalar();
                        string amenity_count = am.ToString();
                        conn.Close();

                        conn.Open();
                        da = new SqlDataAdapter("e_report_amenity", conn);
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        dt = new DataTable();
                        da.Fill(dt);

                        crpt_Amenity rpt = new crpt_Amenity();
                        rpt.SetDataSource(dt);
                        rpt.SetParameterValue("amenity_count", amenity_count);
                        crptViewer.ReportSource = null;
                        crptViewer.ReportSource = rpt;
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: ustr_report_CategoryAmenity (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
                else
                {
                    return;
                }
            }
        }
    }
}
