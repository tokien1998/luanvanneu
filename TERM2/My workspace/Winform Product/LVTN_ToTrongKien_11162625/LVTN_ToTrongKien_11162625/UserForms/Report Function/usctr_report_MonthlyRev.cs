﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using DevExpress.XtraEditors;
using LVTN_ToTrongKien_11162625.Report;

namespace LVTN_ToTrongKien_11162625.UserForms.Report_Function
{
    public partial class usctr_report_MonthlyRev : UserControl
    {
        // Khai báo biến
        private static usctr_report_MonthlyRev usctr; // User control

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_report_MonthlyRev UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_report_MonthlyRev();
                }
                return usctr;
            }
        }

        public usctr_report_MonthlyRev()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void ComboboxStudio()
        {

            string query = "SELECT 0 as studio_id, N'--Chọn--' as tag UNION ALL SELECT studio_id, tag FROM studio ORDER BY studio_id";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbStudio.DisplayMember = "tag";
            cmbStudio.ValueMember = "studio_id";
            cmbStudio.DataSource = dt;
            cmbStudio.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbStudio.SelectedIndex = 0;
            conn.Close();
        }

        private void ComboboxYear()
        {
            cmbYear.Items.Clear();
            cmbYear.Items.Add("--Chọn--");
            cmbYear.Items.Add("2020");
            cmbYear.Items.Add("2019");
            cmbYear.Items.Add("2018");
            cmbYear.Items.Add("2017");
            cmbYear.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbYear.SelectedIndex = 0;
        }

        private void ComboboxMon()
        {
            cmbMon.Items.Clear();
            cmbMon.Items.Add("--Chọn--");
            cmbMon.Items.Add("01");
            cmbMon.Items.Add("02");
            cmbMon.Items.Add("03");
            cmbMon.Items.Add("04");
            cmbMon.Items.Add("05");
            cmbMon.Items.Add("06");
            cmbMon.Items.Add("07");
            cmbMon.Items.Add("08");
            cmbMon.Items.Add("09");
            cmbMon.Items.Add("10");
            cmbMon.Items.Add("11");
            cmbMon.Items.Add("12");
            cmbMon.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbMon.SelectedIndex = 0;
        }

        private void btnShowReport_Click(object sender, EventArgs e)
        {
            if (cmbStudio.Text == "--Chọn--" || cmbStudio.Text == "")
            {
                XtraMessageBox.Show("Chọn một đối tác để hiện báo cáo!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (cmbYear.SelectedItem.ToString() == "--Chọn--" || cmbMon.SelectedItem.ToString() == "--Chọn--")
                {
                    XtraMessageBox.Show("Chọn tháng tổng kết doanh thu cụ thể!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    conn.Open();
                    da = new SqlDataAdapter("e_report_studio_rev_report", conn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudio.SelectedValue.ToString());
                    da.SelectCommand.Parameters.Add("@year", SqlDbType.Int).Value = int.Parse(cmbYear.SelectedItem.ToString());
                    da.SelectCommand.Parameters.Add("@mon", SqlDbType.Int).Value = int.Parse(cmbMon.SelectedItem.ToString());
                    DataSet dsStudioRev = new DataSet();
                    da.Fill(dsStudioRev, "StudioRevDetail");
                    conn.Close();

                    conn.Open();
                    SqlDataAdapter da1 = new SqlDataAdapter("e_report_studio_rev_overall_report", conn);
                    da1.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da1.SelectCommand.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudio.SelectedValue.ToString());
                    da1.SelectCommand.Parameters.Add("@year", SqlDbType.Int).Value = int.Parse(cmbYear.SelectedItem.ToString());
                    da1.SelectCommand.Parameters.Add("@mon", SqlDbType.Int).Value = int.Parse(cmbMon.SelectedItem.ToString());
                   // DataSet dsStudioRev = new DataSet();
                    da1.Fill(dsStudioRev, "StudioRevOverall");
                    conn.Close();

                    conn.Open();
                    string query = "SELECT s.bank_account, s.bank_num, b.branch_name, bb.bank_name FROM studio_bank s INNER JOIN bank_branch b ON (s.branch_id = b.branch_id) INNER JOIN bank bb on (b.bank_id = bb.bank_id) WHERE s.status = N'Hoạt động' AND s.studio_id = " + cmbStudio.SelectedValue.ToString();
                    SqlDataAdapter da2 = new SqlDataAdapter(query, conn);
                    dt = new DataTable();
                    da2.Fill(dt);
                    string bankAccount = dt.Rows[0]["bank_account"].ToString();
                    string bankNum = dt.Rows[0]["bank_num"].ToString();
                    string branch_name = dt.Rows[0]["branch_name"].ToString();
                    string bank_name = dt.Rows[0]["bank_name"].ToString();
                    conn.Close();
                    crpt_MonthlyRev rpt = new crpt_MonthlyRev();
                    rpt.Database.Tables["StudioRevOverall"].SetDataSource(dsStudioRev.Tables[dsStudioRev.Tables.IndexOf("StudioRevOverall")]);
                    rpt.Database.Tables["StudioRevDetail"].SetDataSource(dsStudioRev.Tables[dsStudioRev.Tables.IndexOf("StudioRevDetail")]);

                    rpt.SetParameterValue("mon", cmbMon.Text);
                    rpt.SetParameterValue("year", cmbYear.Text);
                    rpt.SetParameterValue("studio_name", lblStudioName.Text);
                    rpt.SetParameterValue("BankAccount", bankAccount);
                    rpt.SetParameterValue("BankNum", bankNum);
                    rpt.SetParameterValue("BankName", bank_name);
                    rpt.SetParameterValue("BranchName", branch_name);

                    //rpt.SetParameterValue("studio_name", lblStudioName.Text);
                    //rpt.SetParameterValue("studio_id", cmbStudio.SelectedValue.ToString());

                    crptViewer.ReportSource = null;
                    crptViewer.ReportSource = rpt;
                }
            }
        }

        private void usctr_report_MonthlyRev_Load(object sender, EventArgs e)
        {
            ComboboxStudio();
            ComboboxYear();
            ComboboxMon();
        }

        private void cmbStudio_SelectedIndexChanged(object sender, EventArgs e)
        {
            conn.Open();
            string query = "SELECT full_name FROM studio_translation WHERE language = N'vi' and studio_id = " + int.Parse(cmbStudio.SelectedValue.ToString());
            cmd = new SqlCommand(query, conn);
            var result = cmd.ExecuteScalar();
            if (result == null)
            {
                lblStudioName.Text = "";
            }
            else
            {
                lblStudioName.Text = result.ToString();
            }
            conn.Close();
        }
    }
}
