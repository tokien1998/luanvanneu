﻿namespace LVTN_ToTrongKien_11162625
{
    partial class usctr_report_StudioInfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(usctr_report_StudioInfo));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnShowReport = new DevExpress.XtraEditors.SimpleButton();
            this.lblStudioName = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbStudio = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panelReportViewer = new System.Windows.Forms.Panel();
            this.crptViewer = new CrystalDecisions.Windows.Forms.CrystalReportViewer();
            this.radStudio = new System.Windows.Forms.RadioButton();
            this.radCourse = new System.Windows.Forms.RadioButton();
            this.panel1.SuspendLayout();
            this.panelReportViewer.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radCourse);
            this.panel1.Controls.Add(this.radStudio);
            this.panel1.Controls.Add(this.btnShowReport);
            this.panel1.Controls.Add(this.lblStudioName);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.cmbStudio);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1012, 83);
            this.panel1.TabIndex = 0;
            // 
            // btnShowReport
            // 
            this.btnShowReport.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnShowReport.ImageOptions.Image")));
            this.btnShowReport.Location = new System.Drawing.Point(758, 23);
            this.btnShowReport.Name = "btnShowReport";
            this.btnShowReport.Size = new System.Drawing.Size(99, 34);
            this.btnShowReport.TabIndex = 7;
            this.btnShowReport.Text = "HIỂN THỊ";
            this.btnShowReport.Click += new System.EventHandler(this.btnShowReport_Click);
            // 
            // lblStudioName
            // 
            this.lblStudioName.AutoSize = true;
            this.lblStudioName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStudioName.Location = new System.Drawing.Point(354, 31);
            this.lblStudioName.Name = "lblStudioName";
            this.lblStudioName.Size = new System.Drawing.Size(96, 16);
            this.lblStudioName.TabIndex = 3;
            this.lblStudioName.Text = "studio_name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(282, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên đầy đủ:";
            // 
            // cmbStudio
            // 
            this.cmbStudio.FormattingEnabled = true;
            this.cmbStudio.Location = new System.Drawing.Point(119, 29);
            this.cmbStudio.Name = "cmbStudio";
            this.cmbStudio.Size = new System.Drawing.Size(142, 21);
            this.cmbStudio.TabIndex = 1;
            this.cmbStudio.SelectedIndexChanged += new System.EventHandler(this.cmbStudio_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(69, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Đối tác:";
            // 
            // panelReportViewer
            // 
            this.panelReportViewer.Controls.Add(this.crptViewer);
            this.panelReportViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelReportViewer.Location = new System.Drawing.Point(0, 83);
            this.panelReportViewer.Name = "panelReportViewer";
            this.panelReportViewer.Size = new System.Drawing.Size(1012, 564);
            this.panelReportViewer.TabIndex = 1;
            // 
            // crptViewer
            // 
            this.crptViewer.ActiveViewIndex = -1;
            this.crptViewer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.crptViewer.Cursor = System.Windows.Forms.Cursors.Default;
            this.crptViewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.crptViewer.Location = new System.Drawing.Point(0, 0);
            this.crptViewer.Name = "crptViewer";
            this.crptViewer.Size = new System.Drawing.Size(1012, 564);
            this.crptViewer.TabIndex = 0;
            // 
            // radStudio
            // 
            this.radStudio.AutoSize = true;
            this.radStudio.Location = new System.Drawing.Point(613, 21);
            this.radStudio.Name = "radStudio";
            this.radStudio.Size = new System.Drawing.Size(106, 17);
            this.radStudio.TabIndex = 8;
            this.radStudio.TabStop = true;
            this.radStudio.Text = "Thông tin đối tác";
            this.radStudio.UseVisualStyleBackColor = true;
            // 
            // radCourse
            // 
            this.radCourse.AutoSize = true;
            this.radCourse.Location = new System.Drawing.Point(613, 44);
            this.radCourse.Name = "radCourse";
            this.radCourse.Size = new System.Drawing.Size(108, 17);
            this.radCourse.TabIndex = 9;
            this.radCourse.TabStop = true;
            this.radCourse.Text = "Thông tin dịch vụ";
            this.radCourse.UseVisualStyleBackColor = true;
            // 
            // usctr_report_StudioInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelReportViewer);
            this.Controls.Add(this.panel1);
            this.Name = "usctr_report_StudioInfo";
            this.Size = new System.Drawing.Size(1012, 647);
            this.Load += new System.EventHandler(this.usctr_report_StudioInfo_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelReportViewer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblStudioName;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbStudio;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton btnShowReport;
        private System.Windows.Forms.Panel panelReportViewer;
        private CrystalDecisions.Windows.Forms.CrystalReportViewer crptViewer;
        private System.Windows.Forms.RadioButton radCourse;
        private System.Windows.Forms.RadioButton radStudio;
    }
}
