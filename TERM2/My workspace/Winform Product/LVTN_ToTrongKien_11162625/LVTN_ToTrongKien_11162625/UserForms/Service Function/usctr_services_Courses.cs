﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_services_Courses : UserControl
    {
        // Khai báo biến
        private static usctr_services_Courses usctr; // User control
        string btn_flag = null; // Biến xác định nút đang sử dụng

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_services_Courses UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_services_Courses();
                }
                return usctr;
            }
        }

        public usctr_services_Courses()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            // none
        }

        // Dữ liệu Combobox
        private void ComboboxStudioId()
        {
            string query = "SELECT 0 as studio_id, N'--Chọn--' as studio_id_str UNION ALL SELECT studio_id, CAST(studio_id AS nvarchar) FROM studio";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbStudioId.DisplayMember = "studio_id_str";
            cmbStudioId.ValueMember = "studio_id";
            cmbStudioId.DataSource = dt;
            cmbStudioId.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void ComboboxServiceType()
        {
            string query = "SELECT 'temp' as service_type_code, N'--Chọn--' as service_type UNION ALL SELECT 'F' as service_type_code, N'Luyện tập' as service_type UNION ALL SELECT 'B' as service_type_code, N'Làm đẹp' as service_type";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbServiceType.DisplayMember = "service_type";
            cmbServiceType.ValueMember = "service_type_code";
            cmbServiceType.DataSource = dt;
            cmbServiceType.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void cmbServiceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "SELECT 0 as category_id, N'--Chọn--' as code UNION ALL SELECT category_id, code FROM category WHERE service_type = '" + cmbServiceType.SelectedValue.ToString() + "'";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbCategory.DisplayMember = "code";
            cmbCategory.ValueMember = "category_id";
            cmbCategory.DataSource = dt;
            cmbCategory.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void cmbStudioId_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cmbStudioId.Text == "--Chọn--")
            //{
            //    lblTag.Visible = false;
            //}
            //else
            //{
                conn.Open();
                // SHow Tag
                string studio_id = cmbStudioId.SelectedValue.ToString();
                string query = "SELECT tag FROM studio WHERE studio_id = " + studio_id;
                string tag;
                cmd = new SqlCommand(query, conn);
                var result = cmd.ExecuteScalar();
                if (result == null)
                {
                    tag = "";
                }
                else
                {
                    tag = result.ToString();
                }
                lblTag.Text = tag;
                lblTag.Visible = true;
                conn.Close();

                // Show data grid view
                ShowFullInfo();
                ShowFirstInfo();

            //}
            lblRowCount.Text = grvCourse.RowCount.ToString();
        }

        private void ComboboxAcceptanceLevel()
        {
            string query = "SELECT 0 as id, N'--Chọn--' as name UNION ALL SELECT 1 as id, N'Mọi trình độ' as name UNION ALL SELECT 2 as id, N'Cơ bản' as name UNION ALL SELECT 3 as id, N'Trung cấp' as name UNION ALL SELECT 4 as id, N'Nâng cao' as name";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbAcceptanceLevel.DisplayMember = "name";
            cmbAcceptanceLevel.ValueMember = "id";
            cmbAcceptanceLevel.DataSource = dt;
            cmbAcceptanceLevel.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void ComboboxApplyFor()
        {
            string query = "SELECT 0 as id, N'--Chọn--' as name UNION ALL SELECT 1 as id, N'Cho tất cả' as name UNION ALL SELECT 2 as id, N'Nam' as name UNION ALL SELECT 3 as id, N'Nữ' as name";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbApplyFor.DisplayMember = "name";
            cmbApplyFor.ValueMember = "id";
            cmbApplyFor.DataSource = dt;
            cmbApplyFor.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void ComboboxStatus()
        {
            string query = "SELECT N'--Chọn--' as stt UNION SELECT N'Mới' as stt UNION SELECT N'Hoạt động' as stt UNION SELECT N'Vô hiệu hóa' as stt";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbStatus.DisplayMember = "stt";
            cmbStatus.ValueMember = "stt";
            cmbStatus.DataSource = dt;
            cmbStatus.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        // Định dạng
        private void DefaultFormat()
        {
            cmbStudioId.Enabled = true;
            cmbCategory.Enabled = false;
            cmbServiceType.Enabled = false;
            cmbAcceptanceLevel.Enabled = false;
            cmbApplyFor.Enabled = false;
            cmbStatus.Enabled = false;
            txtCourseId.Enabled = false;
            txtCourseId.ReadOnly = true;
            txtCourseTag.Enabled = false;
            txtCourseTag.ReadOnly = true;
            nudMonthSlots.Enabled = false;
            nudSessionSlots.Enabled = false;
        }

        private void SetCaption()
        {
            string[] columnCaptions = { "Mã dịch vụ", "Mã hiệu", "Thể loại dịch vụ", "Mã loại hình dịch vụ", "Mã trình độ dịch vụ", "Mã đối tượng áp dụng", "Trạng thái", "Giới hạn tháng", "Giới hạn buổi" };
            power.SetCaption(grvCourse, columnCaptions);
        }

        // Show dữ liệu
        private void ShowFullInfo()
        {
            string studio_id = cmbStudioId.SelectedValue.ToString();
            string tableName = "course";
            string selectedColumns = "course_id, tag, service_type, category_id, acceptance_level_id, apply_for_id, status, month_slots, session_slots";
            string afterWhereStr = "studio_id = " + studio_id + " ORDER BY course_id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrCourse.DataSource = dt;
            SetCaption();
        }

        private void ShowFirstInfo()
        {
            if (grvCourse.RowCount == 0)
            {
                cmbServiceType.Text = "--Chọn--";
                cmbCategory.Text = "--Chọn--";
                cmbApplyFor.Text = "--Chọn--";
                cmbAcceptanceLevel.Text = "--Chọn--";
                cmbStatus.Text = "--Chọn--";

                txtCourseId.ResetText();
                txtCourseTag.ResetText();

                nudMonthSlots.Value = 0;
                nudSessionSlots.Value = 0;

                return;
            }
            else
            {
                txtCourseId.Text = grvCourse.GetRowCellValue(0, "course_id").ToString();
                txtCourseTag.Text = grvCourse.GetRowCellValue(0, "tag").ToString();
                cmbServiceType.SelectedValue = grvCourse.GetRowCellValue(0, "service_type").ToString();
                cmbCategory.SelectedValue = grvCourse.GetRowCellValue(0, "category_id").ToString();
                cmbAcceptanceLevel.SelectedValue = grvCourse.GetRowCellValue(0, "acceptance_level_id").ToString();
                cmbApplyFor.SelectedValue = grvCourse.GetRowCellValue(0, "apply_for_id").ToString();
                cmbStatus.SelectedValue = grvCourse.GetRowCellValue(0, "status").ToString();
                nudMonthSlots.Value = decimal.Parse(grvCourse.GetRowCellValue(0, "month_slots").ToString());
                nudSessionSlots.Value = decimal.Parse(grvCourse.GetRowCellValue(0, "session_slots").ToString());
            }
        }

        private void usctr_services_Courses_Load(object sender, EventArgs e)
        {
            btn_flag = null;

            ComboboxStudioId();
            ComboboxStatus();
            ComboboxServiceType();
            ComboboxAcceptanceLevel();
            ComboboxApplyFor();
            grctrCourse.DataSource = null;
            DefaultFormat();
            lblRowCount.Text = grvCourse.RowCount.ToString();
        }

        private void grvCourse_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvCourse.RowCount == 0)
            {
                //Nếu grid view ko có dữ liệu thì ko hiện lên textbox để tránh lỗi "Object reference not set to an instance of an object"
                cmbServiceType.SelectedIndex = 0;
                cmbCategory.SelectedIndex = 0;
                cmbApplyFor.SelectedIndex = 0;
                cmbAcceptanceLevel.SelectedIndex = 0;
                cmbStatus.SelectedIndex = 0;

                txtCourseId.ResetText();
                txtCourseTag.ResetText();

                nudMonthSlots.Value = 0;
                nudSessionSlots.Value = 0;
                return;
            }
            else
            {
                txtCourseId.Text = grvCourse.GetFocusedRowCellValue("course_id").ToString();
                txtCourseTag.Text = grvCourse.GetFocusedRowCellValue("tag").ToString();
                cmbServiceType.SelectedValue = grvCourse.GetFocusedRowCellValue("service_type").ToString();
                cmbCategory.SelectedValue = grvCourse.GetFocusedRowCellValue("category_id").ToString();
                cmbAcceptanceLevel.SelectedValue = grvCourse.GetFocusedRowCellValue("acceptance_level_id").ToString();
                cmbApplyFor.SelectedValue = grvCourse.GetFocusedRowCellValue("apply_for_id").ToString();
                cmbStatus.SelectedValue = grvCourse.GetFocusedRowCellValue("status").ToString();
                nudMonthSlots.Value = decimal.Parse(grvCourse.GetFocusedRowCellValue("month_slots").ToString());
                nudSessionSlots.Value = decimal.Parse(grvCourse.GetFocusedRowCellValue("session_slots").ToString());
            }

            btn_flag = null;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";
            cmbStudioId.Enabled = false;
            txtCourseId.Text = "Auto";
            txtCourseId.ReadOnly = true;
            txtCourseId.Enabled = false;
            txtCourseTag.Enabled = true;
            txtCourseTag.ReadOnly = false;
            txtCourseTag.ResetText();
            cmbServiceType.Enabled = true;
            cmbServiceType.Text = "--Chọn--";
            cmbCategory.Enabled = true;
            cmbCategory.Text = "--Chọn--";
            cmbAcceptanceLevel.Enabled = true;
            cmbAcceptanceLevel.Text = "--Chọn--";
            cmbApplyFor.Enabled = true;
            cmbApplyFor.Text = "--Chọn--";
            cmbStatus.Enabled = false;
            nudMonthSlots.Enabled = true;
            nudMonthSlots.ResetText();
            nudSessionSlots.Enabled = true;
            nudSessionSlots.ResetText();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            btn_flag = "edit";
            if (txtCourseId.Text == "" || txtCourseId.Text == "Auto")
            {
                XtraMessageBox.Show("Chọn một bản ghi để sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                cmbStudioId.Enabled = false;
                txtCourseId.ReadOnly = true;
                txtCourseId.Enabled = false;
                txtCourseTag.Enabled = true;
                txtCourseTag.ReadOnly = false;
                cmbServiceType.Enabled = true;
                cmbCategory.Enabled = true;
                cmbAcceptanceLevel.Enabled = true;
                cmbApplyFor.Enabled = true;
                cmbStatus.Enabled = true;
                nudMonthSlots.Enabled = true;
                nudSessionSlots.Enabled = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtCourseId.Text == "" || txtCourseId.Text == "Auto")
            {
                XtraMessageBox.Show("Chọn một bản ghi để xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa dịch vụ (mã: '" + txtCourseId.Text + "' - " + txtCourseTag.Text + ") ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM course WHERE course_id = " + int.Parse(txtCourseId.Text), conn);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_services_Courses.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        usctr_services_Courses_Load(sender, e);
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_services_Courses_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            //Creatation event
            if (btn_flag == "create")
            {
                if (cmbServiceType.Text == "--Chọn--" || cmbCategory.Text == "--Chọn--" || cmbAcceptanceLevel.Text == "--Chọn--" || cmbApplyFor.Text == "--Chọn--")
                {
                    XtraMessageBox.Show("Vui lòng điền đủ thông tin", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    { 
                        // Khai báo các cột và values cần check duplicate
                        string[] col_names = { "tag" };
                        string[] col_values = { txtCourseTag.Text };

                        conn.Open();
                        if (power.Sql_CheckDuplicate("course", col_names, col_values) == true)
                        {
                            XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        else
                        {
                            cmd = new SqlCommand("a_insert_tb_course", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudioId.SelectedValue.ToString());
                            cmd.Parameters.Add("@tag", SqlDbType.VarChar).Value = txtCourseTag.Text;
                            cmd.Parameters.Add("@service_type", SqlDbType.VarChar, 50).Value = cmbServiceType.SelectedValue.ToString();
                            cmd.Parameters.Add("@category_id", SqlDbType.Int).Value = int.Parse(cmbCategory.SelectedValue.ToString());
                            cmd.Parameters.Add("@acceptance_level_id", SqlDbType.Int).Value = int.Parse(cmbAcceptanceLevel.SelectedValue.ToString());
                            cmd.Parameters.Add("@apply_for_id", SqlDbType.Int).Value = int.Parse(cmbApplyFor.SelectedValue.ToString());
                            cmd.Parameters.Add("@month_slots", SqlDbType.Int).Value = int.Parse(nudMonthSlots.Value.ToString());
                            cmd.Parameters.Add("@session_slots", SqlDbType.Int).Value = int.Parse(nudSessionSlots.Value.ToString());
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("TẠO THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_services_Courses.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;

                        ShowFullInfo();
                        ShowFirstInfo();

                        lblRowCount.Text = grvCourse.RowCount.ToString();
                    }
                }
            }


            //Edit event
            if (btn_flag == "edit")
            {
                if (cmbServiceType.Text == "--Chọn--" || cmbCategory.Text == "--Chọn--" || cmbAcceptanceLevel.Text == "--Chọn--" || cmbApplyFor.Text == "--Chọn--" || txtCourseTag.Text == "" || cmbStatus.Text == "--Chọn--")
                {
                    XtraMessageBox.Show("Vui lòng điền đủ thông tin", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {

                        conn.Open();
                        cmd = new SqlCommand("b_update_tb_course", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@status", SqlDbType.NVarChar).Value = cmbStatus.SelectedValue.ToString();
                        cmd.Parameters.Add("@service_type", SqlDbType.VarChar).Value = cmbServiceType.SelectedValue.ToString();
                        cmd.Parameters.Add("@tag", SqlDbType.VarChar).Value = txtCourseTag.Text;
                        cmd.Parameters.Add("@category_id", SqlDbType.Int).Value = int.Parse(cmbCategory.SelectedValue.ToString());
                        cmd.Parameters.Add("@acceptance_level_id", SqlDbType.Int).Value = int.Parse(cmbAcceptanceLevel.SelectedValue.ToString());
                        cmd.Parameters.Add("@apply_for_id", SqlDbType.Int).Value = int.Parse(cmbApplyFor.SelectedValue.ToString());
                        cmd.Parameters.Add("@month_slots", SqlDbType.Int).Value = int.Parse(nudMonthSlots.Value.ToString());
                        cmd.Parameters.Add("@session_slots", SqlDbType.Int).Value = int.Parse(nudSessionSlots.Value.ToString());
                        cmd.Parameters.Add("@course_id", SqlDbType.Int).Value = int.Parse(txtCourseId.Text);
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ShowFirstInfo();

                        DefaultFormat();
                        cmbStatus.Enabled = false;
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_services_Courses.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;

                        ShowFullInfo();
                        ShowFirstInfo();

                        lblRowCount.Text = grvCourse.RowCount.ToString();
                    }
                }
            }
            
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (txtCourseId.Text == "" || txtCourseId.Text == "Auto")
            {
                XtraMessageBox.Show("Chọn một dịch vụ để xem chi tiết", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                int course_id = int.Parse(txtCourseId.Text);
                string tag = lblTag.Text;
                string courseTag = txtCourseTag.Text;
                int month_slots = int.Parse(nudMonthSlots.Value.ToString());
                int session_slots = int.Parse(nudSessionSlots.Value.ToString());

                popform_services_Course_Translation frm = new popform_services_Course_Translation(course_id, tag, courseTag, month_slots, session_slots);
                frm.ShowDialog();
            }
        }

        private void btnUnitPrice_Click(object sender, EventArgs e)
        {
            if (txtCourseId.Text == "" || txtCourseId.Text == "Auto")
            {
                XtraMessageBox.Show("Chọn một dịch vụ để xem chi tiết", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                int course_id = int.Parse(txtCourseId.Text);
                string tag = lblTag.Text;
                string courseTag = txtCourseTag.Text;

                popform_services_CoursePrice frm = new popform_services_CoursePrice(course_id, tag, courseTag);
                frm.ShowDialog();
            }
        }
    }
}
