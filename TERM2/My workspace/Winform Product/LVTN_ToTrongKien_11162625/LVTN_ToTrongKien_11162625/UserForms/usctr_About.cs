﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Diagnostics;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_About : UserControl
    {
        // Thuộc tính
        private static usctr_About usctr; // User control

        public static usctr_About UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_About();
                }
                return usctr;
            }
        }

        public usctr_About()
        {
            InitializeComponent();
        }

        private void usctr_About_Load(object sender, EventArgs e)
        {
            FormatMainLogo();
        }

        private void FormatMainLogo()
        {
            Rectangle r = new Rectangle(0, 0, MainLogo.Width, MainLogo.Height);
            GraphicsPath gp = new GraphicsPath();
            gp.AddEllipse(0, 0, MainLogo.Width, MainLogo.Height);
            Region rg = new Region(gp);
            MainLogo.Region = rg;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel1.LinkVisited = true;
            Process.Start(linkLabel1.Text);
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel2.LinkVisited = true;
            Process.Start(linkLabel2.Text);
        }
    }
}
