﻿namespace LVTN_ToTrongKien_11162625
{
    partial class usctr_lists_ContractForm
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(usctr_lists_ContractForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblRowCount = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.grctrContractForm = new DevExpress.XtraGrid.GridControl();
            this.grvContractForm = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.radForm = new System.Windows.Forms.RadioButton();
            this.radContract = new System.Windows.Forms.RadioButton();
            this.btnShowFile = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeletePDF = new DevExpress.XtraEditors.SimpleButton();
            this.btnExportContract = new DevExpress.XtraEditors.SimpleButton();
            this.btnBrowsePDF = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnRefresh = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnEdit = new DevExpress.XtraEditors.SimpleButton();
            this.btnCreate = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblContractId = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lblTag = new System.Windows.Forms.Label();
            this.txtSignedAddress = new System.Windows.Forms.TextBox();
            this.dtpSignedDate = new System.Windows.Forms.DateTimePicker();
            this.dtpExpireDate = new System.Windows.Forms.DateTimePicker();
            this.dtpValidDate = new System.Windows.Forms.DateTimePicker();
            this.txtFormLink = new System.Windows.Forms.TextBox();
            this.txtContractLink = new System.Windows.Forms.TextBox();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.cmbStudioId = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.txtAccountNum = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.cmbBankBranch = new System.Windows.Forms.ComboBox();
            this.cmbBank = new System.Windows.Forms.ComboBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtBankAccount = new System.Windows.Forms.TextBox();
            this.txtTaxNum = new System.Windows.Forms.TextBox();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.txtAddress = new System.Windows.Forms.TextBox();
            this.txtRepresentative = new System.Windows.Forms.TextBox();
            this.txtFullname = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grctrContractForm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvContractForm)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1113, 662);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.grctrContractForm);
            this.panel2.Location = new System.Drawing.Point(13, 340);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1087, 319);
            this.panel2.TabIndex = 7;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblRowCount);
            this.panel3.Controls.Add(this.label23);
            this.panel3.Location = new System.Drawing.Point(3, 277);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(166, 36);
            this.panel3.TabIndex = 1;
            // 
            // lblRowCount
            // 
            this.lblRowCount.AutoSize = true;
            this.lblRowCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRowCount.Location = new System.Drawing.Point(64, 10);
            this.lblRowCount.Name = "lblRowCount";
            this.lblRowCount.Size = new System.Drawing.Size(77, 16);
            this.lblRowCount.TabIndex = 1;
            this.lblRowCount.Text = "RowCount";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 12);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(52, 13);
            this.label23.TabIndex = 0;
            this.label23.Text = "Số lượng:";
            // 
            // grctrContractForm
            // 
            this.grctrContractForm.Location = new System.Drawing.Point(0, 0);
            this.grctrContractForm.MainView = this.grvContractForm;
            this.grctrContractForm.Name = "grctrContractForm";
            this.grctrContractForm.Size = new System.Drawing.Size(1087, 271);
            this.grctrContractForm.TabIndex = 0;
            this.grctrContractForm.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.grvContractForm});
            // 
            // grvContractForm
            // 
            this.grvContractForm.GridControl = this.grctrContractForm;
            this.grvContractForm.Name = "grvContractForm";
            this.grvContractForm.OptionsFind.AlwaysVisible = true;
            this.grvContractForm.OptionsFind.FindNullPrompt = "Nhập để tìm kiếm...";
            this.grvContractForm.FocusedRowChanged += new DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventHandler(this.grvContractForm_FocusedRowChanged);
            this.grvContractForm.FocusedRowLoaded += new DevExpress.XtraGrid.Views.Base.RowEventHandler(this.grvContractForm_FocusedRowLoaded);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label21);
            this.groupBox4.Controls.Add(this.radForm);
            this.groupBox4.Controls.Add(this.radContract);
            this.groupBox4.Controls.Add(this.btnShowFile);
            this.groupBox4.Controls.Add(this.btnDeletePDF);
            this.groupBox4.Controls.Add(this.btnExportContract);
            this.groupBox4.Controls.Add(this.btnBrowsePDF);
            this.groupBox4.Location = new System.Drawing.Point(754, 188);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(346, 143);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "BỔ TRỢ";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(54, 23);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(50, 13);
            this.label21.TabIndex = 11;
            this.label21.Text = "Áp dụng:";
            // 
            // radForm
            // 
            this.radForm.AutoSize = true;
            this.radForm.Location = new System.Drawing.Point(191, 21);
            this.radForm.Name = "radForm";
            this.radForm.Size = new System.Drawing.Size(69, 17);
            this.radForm.TabIndex = 10;
            this.radForm.TabStop = true;
            this.radForm.Text = "Biểu mẫu";
            this.radForm.UseVisualStyleBackColor = true;
            // 
            // radContract
            // 
            this.radContract.AutoSize = true;
            this.radContract.Location = new System.Drawing.Point(112, 21);
            this.radContract.Name = "radContract";
            this.radContract.Size = new System.Drawing.Size(73, 17);
            this.radContract.TabIndex = 9;
            this.radContract.TabStop = true;
            this.radContract.Text = "Hợp đồng";
            this.radContract.UseVisualStyleBackColor = true;
            // 
            // btnShowFile
            // 
            this.btnShowFile.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnShowFile.ImageOptions.Image")));
            this.btnShowFile.Location = new System.Drawing.Point(206, 91);
            this.btnShowFile.Name = "btnShowFile";
            this.btnShowFile.Size = new System.Drawing.Size(103, 35);
            this.btnShowFile.TabIndex = 8;
            this.btnShowFile.Text = "Xe&m Tài liệu";
            this.btnShowFile.Click += new System.EventHandler(this.btnShowFile_Click);
            // 
            // btnDeletePDF
            // 
            this.btnDeletePDF.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnDeletePDF.ImageOptions.SvgImage")));
            this.btnDeletePDF.Location = new System.Drawing.Point(206, 49);
            this.btnDeletePDF.Name = "btnDeletePDF";
            this.btnDeletePDF.Size = new System.Drawing.Size(103, 36);
            this.btnDeletePDF.TabIndex = 6;
            this.btnDeletePDF.Text = "Xóa T&ài liệu";
            this.btnDeletePDF.Click += new System.EventHandler(this.btnDeletePDF_Click);
            // 
            // btnExportContract
            // 
            this.btnExportContract.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnExportContract.ImageOptions.Image")));
            this.btnExportContract.Location = new System.Drawing.Point(61, 91);
            this.btnExportContract.Name = "btnExportContract";
            this.btnExportContract.Size = new System.Drawing.Size(98, 35);
            this.btnExportContract.TabIndex = 5;
            this.btnExportContract.Text = "Xuất file HĐ";
            this.btnExportContract.Click += new System.EventHandler(this.btnExportContract_Click);
            // 
            // btnBrowsePDF
            // 
            this.btnBrowsePDF.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnBrowsePDF.ImageOptions.SvgImage")));
            this.btnBrowsePDF.Location = new System.Drawing.Point(61, 50);
            this.btnBrowsePDF.Name = "btnBrowsePDF";
            this.btnBrowsePDF.Size = new System.Drawing.Size(98, 35);
            this.btnBrowsePDF.TabIndex = 4;
            this.btnBrowsePDF.Text = "Tải &Tài liệu";
            this.btnBrowsePDF.Click += new System.EventHandler(this.btnBrowsePDF_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnSave);
            this.groupBox2.Controls.Add(this.btnRefresh);
            this.groupBox2.Controls.Add(this.btnDelete);
            this.groupBox2.Controls.Add(this.btnEdit);
            this.groupBox2.Controls.Add(this.btnCreate);
            this.groupBox2.Location = new System.Drawing.Point(754, 72);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(346, 116);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "CHỨC NĂNG";
            // 
            // btnSave
            // 
            this.btnSave.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.ImageOptions.Image")));
            this.btnSave.Location = new System.Drawing.Point(135, 72);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(78, 38);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "L&ưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnRefresh.ImageOptions.SvgImage")));
            this.btnRefresh.Location = new System.Drawing.Point(256, 29);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(88, 38);
            this.btnRefresh.TabIndex = 3;
            this.btnRefresh.Text = "&Làm mới";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnDelete.ImageOptions.SvgImage")));
            this.btnDelete.Location = new System.Drawing.Point(174, 28);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(78, 38);
            this.btnDelete.TabIndex = 2;
            this.btnDelete.Text = "&Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnEdit.ImageOptions.SvgImage")));
            this.btnEdit.Location = new System.Drawing.Point(90, 29);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(78, 37);
            this.btnEdit.TabIndex = 1;
            this.btnEdit.Text = "&Sửa";
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnCreate
            // 
            this.btnCreate.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnCreate.ImageOptions.SvgImage")));
            this.btnCreate.Location = new System.Drawing.Point(6, 30);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(78, 36);
            this.btnCreate.TabIndex = 0;
            this.btnCreate.Text = "&Tạo";
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.lblContractId);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.lblTag);
            this.groupBox1.Controls.Add(this.txtSignedAddress);
            this.groupBox1.Controls.Add(this.dtpSignedDate);
            this.groupBox1.Controls.Add(this.dtpExpireDate);
            this.groupBox1.Controls.Add(this.dtpValidDate);
            this.groupBox1.Controls.Add(this.txtFormLink);
            this.groupBox1.Controls.Add(this.txtContractLink);
            this.groupBox1.Controls.Add(this.cmbStatus);
            this.groupBox1.Controls.Add(this.cmbStudioId);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(13, 72);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(735, 259);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "THÔNG TIN";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(81, 158);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(11, 13);
            this.label30.TabIndex = 9;
            this.label30.Text = "*";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(256, 162);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(11, 13);
            this.label29.TabIndex = 9;
            this.label29.Text = "*";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(76, 124);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(11, 13);
            this.label28.TabIndex = 9;
            this.label28.Text = "*";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.ForeColor = System.Drawing.Color.Red;
            this.label27.Location = new System.Drawing.Point(62, 92);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(11, 13);
            this.label27.TabIndex = 9;
            this.label27.Text = "*";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(248, 58);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(11, 13);
            this.label26.TabIndex = 9;
            this.label26.Text = "*";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(79, 57);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(11, 13);
            this.label24.TabIndex = 9;
            this.label24.Text = "*";
            // 
            // lblContractId
            // 
            this.lblContractId.AutoSize = true;
            this.lblContractId.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContractId.Location = new System.Drawing.Point(92, 58);
            this.lblContractId.Name = "lblContractId";
            this.lblContractId.Size = new System.Drawing.Size(80, 15);
            this.lblContractId.TabIndex = 19;
            this.lblContractId.Text = "Contract_Id";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(67, 27);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(11, 13);
            this.label25.TabIndex = 8;
            this.label25.Text = "*";
            // 
            // lblTag
            // 
            this.lblTag.AutoSize = true;
            this.lblTag.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTag.Location = new System.Drawing.Point(262, 25);
            this.lblTag.Name = "lblTag";
            this.lblTag.Size = new System.Drawing.Size(27, 15);
            this.lblTag.TabIndex = 18;
            this.lblTag.Text = "tag";
            // 
            // txtSignedAddress
            // 
            this.txtSignedAddress.Location = new System.Drawing.Point(95, 87);
            this.txtSignedAddress.Name = "txtSignedAddress";
            this.txtSignedAddress.Size = new System.Drawing.Size(254, 20);
            this.txtSignedAddress.TabIndex = 17;
            // 
            // dtpSignedDate
            // 
            this.dtpSignedDate.CustomFormat = "yyyy-MM-dd";
            this.dtpSignedDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSignedDate.Location = new System.Drawing.Point(95, 120);
            this.dtpSignedDate.Name = "dtpSignedDate";
            this.dtpSignedDate.Size = new System.Drawing.Size(80, 20);
            this.dtpSignedDate.TabIndex = 16;
            // 
            // dtpExpireDate
            // 
            this.dtpExpireDate.CustomFormat = "yyyy-MM-dd";
            this.dtpExpireDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpireDate.Location = new System.Drawing.Point(269, 161);
            this.dtpExpireDate.Name = "dtpExpireDate";
            this.dtpExpireDate.Size = new System.Drawing.Size(80, 20);
            this.dtpExpireDate.TabIndex = 15;
            // 
            // dtpValidDate
            // 
            this.dtpValidDate.CustomFormat = "yyyy-MM-dd";
            this.dtpValidDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpValidDate.Location = new System.Drawing.Point(95, 159);
            this.dtpValidDate.Name = "dtpValidDate";
            this.dtpValidDate.Size = new System.Drawing.Size(77, 20);
            this.dtpValidDate.TabIndex = 14;
            // 
            // txtFormLink
            // 
            this.txtFormLink.Location = new System.Drawing.Point(95, 226);
            this.txtFormLink.Name = "txtFormLink";
            this.txtFormLink.Size = new System.Drawing.Size(254, 20);
            this.txtFormLink.TabIndex = 13;
            // 
            // txtContractLink
            // 
            this.txtContractLink.Location = new System.Drawing.Point(95, 193);
            this.txtContractLink.Name = "txtContractLink";
            this.txtContractLink.Size = new System.Drawing.Size(254, 20);
            this.txtContractLink.TabIndex = 12;
            // 
            // cmbStatus
            // 
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(269, 54);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(80, 21);
            this.cmbStatus.TabIndex = 11;
            // 
            // cmbStudioId
            // 
            this.cmbStudioId.FormattingEnabled = true;
            this.cmbStudioId.Location = new System.Drawing.Point(95, 24);
            this.cmbStudioId.Name = "cmbStudioId";
            this.cmbStudioId.Size = new System.Drawing.Size(91, 21);
            this.cmbStudioId.TabIndex = 10;
            this.cmbStudioId.SelectedIndexChanged += new System.EventHandler(this.cmbStudioId_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label39);
            this.groupBox3.Controls.Add(this.label38);
            this.groupBox3.Controls.Add(this.label37);
            this.groupBox3.Controls.Add(this.label36);
            this.groupBox3.Controls.Add(this.label35);
            this.groupBox3.Controls.Add(this.label34);
            this.groupBox3.Controls.Add(this.label33);
            this.groupBox3.Controls.Add(this.label32);
            this.groupBox3.Controls.Add(this.label31);
            this.groupBox3.Controls.Add(this.txtAccountNum);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.cmbBankBranch);
            this.groupBox3.Controls.Add(this.cmbBank);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.txtBankAccount);
            this.groupBox3.Controls.Add(this.txtTaxNum);
            this.groupBox3.Controls.Add(this.txtPhone);
            this.groupBox3.Controls.Add(this.txtAddress);
            this.groupBox3.Controls.Add(this.txtRepresentative);
            this.groupBox3.Controls.Add(this.txtFullname);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Location = new System.Drawing.Point(357, 19);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(370, 225);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "BÊN B:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Location = new System.Drawing.Point(246, 186);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(11, 13);
            this.label39.TabIndex = 33;
            this.label39.Text = "*";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Location = new System.Drawing.Point(71, 195);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(11, 13);
            this.label38.TabIndex = 9;
            this.label38.Text = "*";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.ForeColor = System.Drawing.Color.Red;
            this.label37.Location = new System.Drawing.Point(229, 162);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(11, 13);
            this.label37.TabIndex = 9;
            this.label37.Text = "*";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.ForeColor = System.Drawing.Color.Red;
            this.label36.Location = new System.Drawing.Point(54, 158);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(11, 13);
            this.label36.TabIndex = 9;
            this.label36.Text = "*";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.ForeColor = System.Drawing.Color.Red;
            this.label35.Location = new System.Drawing.Point(246, 116);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(11, 13);
            this.label35.TabIndex = 9;
            this.label35.Text = "*";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.ForeColor = System.Drawing.Color.Red;
            this.label34.Location = new System.Drawing.Point(42, 122);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(11, 13);
            this.label34.TabIndex = 32;
            this.label34.Text = "*";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.ForeColor = System.Drawing.Color.Red;
            this.label33.Location = new System.Drawing.Point(50, 92);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(11, 13);
            this.label33.TabIndex = 31;
            this.label33.Text = "*";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.ForeColor = System.Drawing.Color.Red;
            this.label32.Location = new System.Drawing.Point(59, 58);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(11, 13);
            this.label32.TabIndex = 30;
            this.label32.Text = "*";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.Color.Red;
            this.label31.Location = new System.Drawing.Point(74, 28);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(11, 13);
            this.label31.TabIndex = 9;
            this.label31.Text = "*";
            // 
            // txtAccountNum
            // 
            this.txtAccountNum.Location = new System.Drawing.Point(259, 157);
            this.txtAccountNum.Name = "txtAccountNum";
            this.txtAccountNum.Size = new System.Drawing.Size(105, 20);
            this.txtAccountNum.TabIndex = 29;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(191, 164);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(40, 13);
            this.label20.TabIndex = 28;
            this.label20.Text = "Số TK:";
            // 
            // cmbBankBranch
            // 
            this.cmbBankBranch.FormattingEnabled = true;
            this.cmbBankBranch.Location = new System.Drawing.Point(259, 192);
            this.cmbBankBranch.Name = "cmbBankBranch";
            this.cmbBankBranch.Size = new System.Drawing.Size(105, 21);
            this.cmbBankBranch.TabIndex = 27;
            // 
            // cmbBank
            // 
            this.cmbBank.FormattingEnabled = true;
            this.cmbBank.Location = new System.Drawing.Point(88, 191);
            this.cmbBank.Name = "cmbBank";
            this.cmbBank.Size = new System.Drawing.Size(102, 21);
            this.cmbBank.TabIndex = 26;
            this.cmbBank.SelectedIndexChanged += new System.EventHandler(this.cmbBank_SelectedIndexChanged);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(11, 195);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(63, 13);
            this.label22.TabIndex = 25;
            this.label22.Text = "Ngân hàng:";
            // 
            // txtBankAccount
            // 
            this.txtBankAccount.Location = new System.Drawing.Point(88, 157);
            this.txtBankAccount.Name = "txtBankAccount";
            this.txtBankAccount.Size = new System.Drawing.Size(97, 20);
            this.txtBankAccount.TabIndex = 24;
            // 
            // txtTaxNum
            // 
            this.txtTaxNum.Location = new System.Drawing.Point(259, 121);
            this.txtTaxNum.Name = "txtTaxNum";
            this.txtTaxNum.Size = new System.Drawing.Size(105, 20);
            this.txtTaxNum.TabIndex = 23;
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(88, 121);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(97, 20);
            this.txtPhone.TabIndex = 22;
            // 
            // txtAddress
            // 
            this.txtAddress.Location = new System.Drawing.Point(88, 90);
            this.txtAddress.Name = "txtAddress";
            this.txtAddress.Size = new System.Drawing.Size(276, 20);
            this.txtAddress.TabIndex = 21;
            // 
            // txtRepresentative
            // 
            this.txtRepresentative.Location = new System.Drawing.Point(88, 58);
            this.txtRepresentative.Name = "txtRepresentative";
            this.txtRepresentative.Size = new System.Drawing.Size(276, 20);
            this.txtRepresentative.TabIndex = 19;
            // 
            // txtFullname
            // 
            this.txtFullname.Location = new System.Drawing.Point(88, 28);
            this.txtFullname.Name = "txtFullname";
            this.txtFullname.Size = new System.Drawing.Size(276, 20);
            this.txtFullname.TabIndex = 18;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(195, 195);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 13);
            this.label19.TabIndex = 17;
            this.label19.Text = "Chi nhánh:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(11, 31);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(68, 13);
            this.label12.TabIndex = 10;
            this.label12.Text = "Tên tổ chức:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(11, 160);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(46, 13);
            this.label18.TabIndex = 16;
            this.label18.Text = "Tên TK:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(11, 61);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 11;
            this.label13.Text = "Đại diện:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(190, 124);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(63, 13);
            this.label17.TabIndex = 15;
            this.label17.Text = "Mã số thuế:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 93);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(43, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "Địa chỉ:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 124);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(32, 13);
            this.label15.TabIndex = 13;
            this.label15.Text = "SĐT:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 93);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(58, 13);
            this.label11.TabIndex = 9;
            this.label11.Text = "Nơi ký kết:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 126);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Ngày ký kết:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 229);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Link biểu mẫu:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 196);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(79, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Link hợp đồng:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(192, 58);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(58, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Trạng thái:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(185, 165);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(74, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Ngày hết hạn:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Ngày hiệu lực:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Mã hợp đồng:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(192, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Mã hiệu:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã đối tác:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(372, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(395, 35);
            this.label1.TabIndex = 1;
            this.label1.Text = "HỢP ĐỒNG - BIỂU MẪU";
            // 
            // usctr_lists_ContractForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "usctr_lists_ContractForm";
            this.Size = new System.Drawing.Size(1113, 662);
            this.Load += new System.EventHandler(this.usctr_lists_ContractForm_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grctrContractForm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvContractForm)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpExpireDate;
        private System.Windows.Forms.DateTimePicker dtpValidDate;
        private System.Windows.Forms.TextBox txtFormLink;
        private System.Windows.Forms.TextBox txtContractLink;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.ComboBox cmbStudioId;
        private System.Windows.Forms.Label lblContractId;
        private System.Windows.Forms.Label lblTag;
        private System.Windows.Forms.TextBox txtSignedAddress;
        private System.Windows.Forms.DateTimePicker dtpSignedDate;
        private System.Windows.Forms.ComboBox cmbBankBranch;
        private System.Windows.Forms.ComboBox cmbBank;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtBankAccount;
        private System.Windows.Forms.TextBox txtTaxNum;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.TextBox txtAddress;
        private System.Windows.Forms.TextBox txtRepresentative;
        private System.Windows.Forms.TextBox txtFullname;
        private System.Windows.Forms.GroupBox groupBox4;
        private DevExpress.XtraEditors.SimpleButton btnDeletePDF;
        private DevExpress.XtraEditors.SimpleButton btnExportContract;
        private DevExpress.XtraEditors.SimpleButton btnBrowsePDF;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnRefresh;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnEdit;
        private DevExpress.XtraEditors.SimpleButton btnCreate;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraGrid.GridControl grctrContractForm;
        private DevExpress.XtraGrid.Views.Grid.GridView grvContractForm;
        private DevExpress.XtraEditors.SimpleButton btnShowFile;
        private System.Windows.Forms.Label lblRowCount;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtAccountNum;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.RadioButton radForm;
        private System.Windows.Forms.RadioButton radContract;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
    }
}
