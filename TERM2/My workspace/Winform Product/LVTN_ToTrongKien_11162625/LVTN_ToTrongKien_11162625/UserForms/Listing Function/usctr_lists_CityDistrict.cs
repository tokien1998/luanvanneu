﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_lists_CityDistrict : UserControl
    {
        // Khai báo biến
        private static usctr_lists_CityDistrict usctr; // User control
        string btn_flag = null; // Biến xác định nút đang sử dụng

        int city_id;
        string city_name;

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_lists_CityDistrict UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_lists_CityDistrict();
                }
                return usctr;
            }
        }

        public usctr_lists_CityDistrict()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        // Định dạng textbox ban đầu
        private void DefaultFormat()
        {
            txtCityId.Font = new Font(txtCityId.Font, FontStyle.Regular);
            txtCityId.Enabled = false;
            txtCityId.ReadOnly = true;
            txtCityName.Enabled = false;
            txtCityName.ReadOnly = true;
        }

        // Định dạng dữ liệu show ra bảng
        private void SetCaption()
        {
            grvCity.Columns[0].Caption = "Mã thành phố";
            grvCity.Columns[1].Caption = "Tên thành phố";

            grvCity.BestFitColumns(); //Tự động fit các columns

        }

        private void usctr_lists_CityDistrict_Load(object sender, EventArgs e)
        {
            dt.Clear();
            btn_flag = null;
            grvCity.FindFilterText = "";
            btnShowDistrict.Enabled = true;
            DefaultFormat();
            ShowFullData();
            ShowFirstInfo();

            lblCityCount.Text = grvCity.RowCount.ToString();
        }

        // Show information of the first record
        private void ShowFirstInfo()
        {
            if (grvCity.RowCount == 0)
            {
                txtCityId.ResetText();
                txtCityName.ResetText();
                return;
            }
            else
            {
                txtCityId.Text = grvCity.GetRowCellValue(0, "city_id").ToString();
                txtCityName.Text = grvCity.GetRowCellValue(0, "city_name").ToString();
            }
        }

        // Show full data
        private void ShowFullData()
        {
            try
            {
                conn.Open();
                da = new SqlDataAdapter("SELECT city_id, city_name FROM city ORDER BY city_id", conn);
                da.Fill(dt);
                grctrCity.DataSource = dt;
                grctrCity.Refresh();
                grctrCity.Update();

                SetCaption();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message, "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
            }
        }

        // Show thông tin record được chọn
        private void grvCity_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvCity.RowCount == 0)
            {
                //Nếu grid view ko có dữ liệu thì ko hiện lên textbox để tránh lỗi "Object reference not set to an instance of an object"
                txtCityId.ResetText();
                txtCityName.ResetText();
                btnShowDistrict.Enabled = false;
                return;
            }
            else
            {
                txtCityId.Font = new Font(txtCityId.Font, FontStyle.Regular);
                txtCityId.Text = grvCity.GetFocusedRowCellValue("city_id").ToString();
                txtCityName.Text = grvCity.GetFocusedRowCellValue("city_name").ToString();
                btnShowDistrict.Enabled = true;
            }

            // Đề phòng ấn nút chức năng xong lại ko thực hiện chức năng
            DefaultFormat();
            btn_flag = null;
        }

        private void grvCity_Click(object sender, EventArgs e) // Click vào vùng grid view 
        {
            // ...
        }

        private void grvCity_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e) // Click vào cột 0 - bôi đen 1 row
        {
            // ...
        }

        private void grvCity_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e) // Click vào 1 ô bất kỳ
        {
            // ...
        }

        // Nút chức năng
        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";
            txtCityName.Enabled = true;
            txtCityName.ReadOnly = false;
            txtCityName.ResetText();
            txtCityId.Text = "Auto";
            txtCityId.Font = new Font(txtCityId.Font, FontStyle.Italic);

            btnShowDistrict.Enabled = false;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (txtCityName.Text == "")
            {
                XtraMessageBox.Show("Chọn đối tượng cần sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                btn_flag = "edit";
                txtCityName.Enabled = true;
                txtCityName.ReadOnly = false;

                btnShowDistrict.Enabled = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtCityName.Text == "")
            {
                XtraMessageBox.Show("Chọn đối tượng cần xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa bản ghi '" + txtCityName.Text + "' ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM city WHERE city_id = " + int.Parse(txtCityId.Text), conn);
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message, "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btnRefresh_Click(sender, e); // F5
                        btn_flag = null;
                    }
                }

            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_lists_CityDistrict_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (txtCityName.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng nhập tên thành phố", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        // Khai báo các cột và values cần check duplicate
                        string[] col_names = { "city_name" };
                        string[] col_values = { txtCityName.Text };

                        // Check trùng
                        if (power.Sql_CheckDuplicate("city", col_names, col_values) == false)
                        {
                            conn.Open();
                            cmd = new SqlCommand("a_insert_tb_city", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@city_name", SqlDbType.NVarChar, 50).Value = txtCityName.Text;
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("THÊM THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message, "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btnRefresh_Click(sender, e); // F5
                        btn_flag = null;
                    }
                }
            }

            if (btn_flag == "edit")
            {
                if (txtCityName.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng nhập tên thành phố", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        // Khai báo các cột và values cần check duplicate
                        string[] col_names = { "city_name" };
                        string[] col_values = { txtCityName.Text };

                        // Check trùng
                        if (power.Sql_CheckDuplicate("city", col_names, col_values) == false)
                        {
                            conn.Open();
                            cmd = new SqlCommand("b_update_tb_city", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@city_name", SqlDbType.NVarChar, 50).Value = txtCityName.Text;
                            cmd.Parameters.Add("@city_id", SqlDbType.Int).Value = int.Parse(txtCityId.Text);
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message, "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btnRefresh_Click(sender, e); // F5
                        btn_flag = null;
                    }
                }
            }
        }

        private void btnShowDistrict_Click(object sender, EventArgs e)
        {
            city_id = int.Parse(txtCityId.Text);
            city_name = txtCityName.Text;

            popform_lists_CityDistrict_District frm = new popform_lists_CityDistrict_District(city_id, city_name);
            frm.ShowDialog();
        }
    }
}
