﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_lists_Category : UserControl
    {
        // Khai báo biến
        private static usctr_lists_Category usctr; // User control
        string btn_flag = null; // Biến xác định nút đang sử dụng

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_lists_Category UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_lists_Category();
                }
                return usctr;
            }
        }

        public usctr_lists_Category()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        // Dữ liệu combobox
        private void languageCollection()
        {
            dt = new DataTable();
            string query = "SELECT 'temp' as id, N'--Chọn--' as name UNION ALL SELECT 'vi' as id, N'Tiếng Việt' as name UNION ALL SELECT 'en' as id, N'English' as name";
            da = new SqlDataAdapter(query, conn);
            da.Fill(dt);
            cmbLang.DisplayMember = "name";
            cmbLang.ValueMember = "id";
            cmbLang.DataSource = dt;
            cmbLang.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void ServiceTypeCollection()
        {
            dt = new DataTable();
            string query = "SELECT 'temp' as id, N'--Chọn--' as name UNION ALL SELECT 'F' as id, N'Luyện tập' as name UNION ALL SELECT 'B' as id, N'Làm đẹp' as name";
            da = new SqlDataAdapter(query, conn);
            da.Fill(dt);
            cmbServiceType.DisplayMember = "name";
            cmbServiceType.ValueMember = "id";
            cmbServiceType.DataSource = dt;
            cmbServiceType.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void TransIdCollection()
        {
            if (cmbCateId.Text != "" || cmbCateId.Text != "--Chọn--")
            {
                dt = new DataTable();
                string query = "SELECT 0 as id, N'--Chọn--' as id_name UNION ALL SELECT id, CAST(id as NVARCHAR) as id_name FROM category_translation WHERE category_id = '" + cmbCateId.SelectedValue.ToString() + "'";
                da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                cmbTransId.DisplayMember = "id_name";
                cmbTransId.ValueMember = "id";
                cmbTransId.DataSource = dt;
                cmbTransId.DropDownStyle = ComboBoxStyle.DropDownList;
                conn.Close();
            }
        }

        private void CateIdCollection()
        {
            dt = new DataTable();
            string query = "SELECT 0 as id, N'--Chọn--' as id_name UNION ALL SELECT category_id as id, CAST(category_id as NVARCHAR) as id_name FROM category";
            da = new SqlDataAdapter(query, conn);
            da.Fill(dt);
            cmbCateId.DisplayMember = "id_name";
            cmbCateId.ValueMember = "id";
            cmbCateId.DataSource = dt;
            cmbCateId.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        // Định dạng text ban đầu
        private void DefaultFormat()
        {
            cmbCateId.Enabled = false;
            cmbTransId.Enabled = false;
            txtCateCode.Enabled = false;
            txtCateCode.ReadOnly = true;
            txtCateName.Enabled = false;
            txtCateName.ReadOnly = true;
            txtDesc.Enabled = false;
            txtDesc.ReadOnly = true;
            cmbLang.Enabled = false;
            cmbServiceType.Enabled = false;
        }

        // Định dạng dữ liệu show ra bảng
        private void SetCaption()
        {
            string[] columnCaptions = { "Mã bản ghi", "Mã dịch vụ", "Kí hiệu", "Thể loại dịch vụ", "Ngôn ngữ", "Tên dịch vụ", "Mô tả" };
            power.SetCaption(grvCate, columnCaptions);
        }

        // Show first info
        private void ShowFirstInfo()
        {
            if (grvCate.RowCount == 0)
            {
                cmbTransId.ResetText();
                cmbCateId.ResetText();
                txtCateCode.ResetText();
                txtCateName.ResetText();
                txtDesc.ResetText();
                cmbLang.ResetText();
                cmbServiceType.ResetText();
                return;
            }
            else
            {
                cmbTransId.SelectedValue = grvCate.GetRowCellValue(0, "id");
                cmbCateId.SelectedValue = grvCate.GetRowCellValue(0, "category_id");
                txtCateCode.Text = grvCate.GetRowCellValue(0, "code").ToString();
                cmbLang.SelectedValue = grvCate.GetRowCellValue(0, "language");
                txtCateName.Text = grvCate.GetRowCellValue(0, "category_name").ToString();
                txtDesc.Text = grvCate.GetRowCellValue(0, "description").ToString();
                cmbServiceType.SelectedValue = grvCate.GetRowCellValue(0, "service_type").ToString();
            }
        }

        // Show full data
        private void ShowFullData()
        {
            string tableName = @"(SELECT l.id, c.category_id, c.code, c.service_type, l.language, l.category_name, l.description
                                  FROM category c LEFT JOIN category_translation l ON (c.category_id = l.category_id)) a";
            string selectedColumns = "id, category_id, code, service_type, language, category_name, description";
            string afterWhereStr = "(1=1) ORDER BY category_id, id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrCate.DataSource = dt;
            SetCaption();
        }

        // Format of radio button Category
        private void TickCateFormat()
        {
            txtCateCode.Enabled = true;
            txtCateCode.ReadOnly = false;
            txtCateName.Enabled = true;
            txtCateName.ReadOnly = false;
            txtDesc.Enabled = true;
            txtDesc.ReadOnly = false;

            cmbCateId.Enabled = false;

            cmbTransId.Enabled = false;
            cmbServiceType.Enabled = true;

            cmbLang.Enabled = true;
        }

        // Format of checkbox Translation
        private void TickTransFormat()
        {
            txtCateCode.Enabled = true;
            txtCateCode.ReadOnly = false;
            txtCateName.Enabled = true;
            txtCateName.ReadOnly = false;
            txtDesc.Enabled = true;
            txtDesc.ReadOnly = false;

            cmbCateId.Enabled = false;

            cmbTransId.Enabled = false;
            cmbServiceType.Enabled = false;

            cmbLang.Enabled = true;
        }

        private void usctr_lists_Category_Load(object sender, EventArgs e)
        {
            btn_flag = null;
            CateIdCollection();
            languageCollection();
            ServiceTypeCollection();
            grctrCate.DataSource = null;
            grvCate.FindFilterText = "";
            ShowFullData();
            ShowFirstInfo();
            DefaultFormat();
            lblRowCount.Text = grvCate.RowCount.ToString();

            radCate.Checked = true;
            radTrans.Checked = false;
            chkFilter.Checked = false;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";

            if (radTrans.Checked == true && txtCateCode.Text == "")
            {
                XtraMessageBox.Show("Chọn dịch vụ cần thêm bản ghi", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (radCate.Checked == true)
                {
                    TickCateFormat();
                    languageCollection();
                    txtCateCode.ResetText();
                    txtCateName.ResetText();
                    txtDesc.ResetText();
                    cmbLang.SelectedIndex = 0;
                    cmbCateId.SelectedIndex = 0;
                    cmbTransId.SelectedIndex = 0;
                    cmbServiceType.SelectedIndex = 0;
                }
                else if (radTrans.Checked == true)
                {
                    TickTransFormat();
                    languageCollection();
                    cmbTransId.SelectedIndex = 0;
                    txtCateName.ResetText();
                    txtDesc.ResetText();
                }
                else
                {
                    return;
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            btn_flag = "edit";

            if (txtCateCode.Text == "" || (radCate.Checked == false && radTrans.Checked == false))
            {
                XtraMessageBox.Show("Chọn đối tượng cần sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (radCate.Checked == true)
                {
                    TickCateFormat();
                    //languageCollection();
                    txtCateName.ReadOnly = true;
                    txtDesc.ReadOnly = true;
                }
                else if (radTrans.Checked == true)
                {
                    TickTransFormat();
                    //languageCollection();
                    if ( "vi" == grvCate.GetFocusedRowCellValue("language").ToString())
                    {
                        cmbLang.Text = "Tiếng Việt";
                        cmbLang.SelectedValue = "vi";
                    }
                    if ("en" == grvCate.GetFocusedRowCellValue("language").ToString())
                    {
                        cmbLang.Text = "English";
                        cmbLang.SelectedValue = "en";
                    }
                    cmbLang.Enabled = false;

                }
                else
                {
                    return;
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtCateCode.Text == "" || (radCate.Checked == false && radTrans.Checked == false))
            {
                XtraMessageBox.Show("Chọn đối tượng cần xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (radCate.Checked == true)
                {
                    if (XtraMessageBox.Show("Xóa dịch vụ '" + txtCateCode.Text + "' và tất cả bản ghi liên quan ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        try
                        {
                            conn.Open();
                            cmd = new SqlCommand("DELETE FROM category_translation WHERE category_id = " + int.Parse(cmbCateId.SelectedValue.ToString()), conn);
                            cmd.ExecuteNonQuery();

                            cmd = new SqlCommand("DELETE FROM category WHERE category_id = " + int.Parse(cmbCateId.SelectedValue.ToString()), conn);
                            cmd.ExecuteNonQuery();

                            XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Category.cs (Line: 281 - 291).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;
                            btnRefresh_Click(sender, e);
                        }
                    }
                }
                else if (radTrans.Checked == true)
                {
                    if (XtraMessageBox.Show("Xóa mã bản ghi '" + cmbTransId.SelectedValue.ToString() + " (" + cmbLang.Text + ") của dịch vụ " + txtCateName.Text + "' ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        try
                        {
                            conn.Open();
                            cmd = new SqlCommand("DELETE FROM category_translation WHERE id = " + int.Parse(cmbTransId.SelectedValue.ToString()), conn);
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Category.cs (Line: 308 - 317).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;
                            btnRefresh_Click(sender, e);
                        }
                    }
                }
                else
                {
                    return;
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_lists_Category_Load(sender, e);
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
     
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // Editation event
            if (btn_flag == "edit")
            {
                if (radCate.Checked == true)
                {
                    if (txtCateCode.Text == "")
                    {
                        XtraMessageBox.Show("Nhập ký hiệu dịch vụ", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if (cmbServiceType.Text == "--Chọn--")
                    {
                        XtraMessageBox.Show("Chọn thể loại hình dịch vụ", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        try
                        {
                            conn.Open();
                            cmd = new SqlCommand("b_update_tb_category", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@code", SqlDbType.VarChar, 50).Value = txtCateCode.Text;
                            cmd.Parameters.Add("@category_id", SqlDbType.Int).Value = cmbCateId.SelectedValue.ToString();
                            cmd.Parameters.Add("@service_type", SqlDbType.VarChar, 50).Value = cmbServiceType.SelectedValue.ToString();
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ShowFirstInfo();

                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Category.cs (Line: 351 - 378).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;
                            btnRefresh_Click(sender, e);
                        }
                    }
                }
                else if (radTrans.Checked == true)
                {
                    if (txtCateName.Text == "" || cmbLang.Text == "")
                    {
                        XtraMessageBox.Show("Nhập tên loại dịch vụ và chọn loại ngôn ngữ", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        try
                        {
                            conn.Open();
                            cmd = new SqlCommand("b_update_tb_category_translation", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = txtDesc.Text;
                            cmd.Parameters.Add("@id", SqlDbType.Int).Value = cmbTransId.SelectedValue.ToString();
                            cmd.Parameters.Add("@language", SqlDbType.VarChar, 50).Value = cmbLang.SelectedValue.ToString();
                            cmd.Parameters.Add("@category_name", SqlDbType.NVarChar, 50).Value = txtCateName.Text;
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ShowFirstInfo();
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Category.cs (Line: 391 - 418).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;
                            btnRefresh_Click(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("Chọn đối tượng cần sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            // Creation event
            if (btn_flag == "create")
            {
                if (radCate.Checked == true)
                {
                    if (txtCateCode.Text == "")
                    {
                        XtraMessageBox.Show("Nhập ký hiệu dịch vụ", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        try
                        {
                            // Khai báo các cột và values cần check duplicate
                            string[] col_names = { "code" };
                            string[] col_values = { txtCateCode.Text };

                            conn.Open();

                            //Check trùng
                            if (power.Sql_CheckDuplicate("category", col_names, col_values) == false)
                            {
                                cmd = new SqlCommand("a_insert_tb_category", conn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@code", SqlDbType.VarChar, 50).Value = txtCateCode.Text;
                                cmd.Parameters.Add("@language", SqlDbType.VarChar, 50).Value = cmbLang.SelectedValue.ToString();
                                cmd.Parameters.Add("@category_name", SqlDbType.NVarChar, 50).Value = txtCateName.Text;
                                cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = txtDesc.Text;
                                cmd.Parameters.Add("@service_type", SqlDbType.VarChar, 50).Value = cmbServiceType.SelectedValue.ToString();
                                cmd.ExecuteNonQuery();
                                XtraMessageBox.Show("TẠO THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ShowFirstInfo();
                            }
                            else
                            {
                                XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Category.cs (Line: 439 - 467).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;
                            btnRefresh_Click(sender, e);
                        }
                    }
                }
                else if (radTrans.Checked == true)
                {
                    if (txtCateName.Text == "" || cmbLang.Text == "")
                    {
                        XtraMessageBox.Show("Nhập tên loại dịch vụ và chọn loại ngôn ngữ", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        try
                        {
                            // Khai báo các cột và values cần check duplicate
                            string[] col_names = { "language", "category_id" };
                            string[] col_values = { cmbLang.SelectedValue.ToString(), cmbCateId.SelectedValue.ToString() };

                            conn.Open();

                            //Check trùng
                            if (power.Sql_CheckDuplicate("category_translation", col_names, col_values) == false)
                            {
                                cmd = new SqlCommand("a_insert_tb_category_translation", conn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = txtDesc.Text;
                                cmd.Parameters.Add("@category_id", SqlDbType.Int).Value = cmbCateId.SelectedValue.ToString();
                                cmd.Parameters.Add("@language", SqlDbType.VarChar, 50).Value = cmbLang.SelectedValue.ToString();
                                cmd.Parameters.Add("@category_name", SqlDbType.NVarChar, 50).Value = txtCateName.Text;
                                cmd.ExecuteNonQuery();
                                XtraMessageBox.Show("TẠO THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ShowFirstInfo();
                            }
                            else
                            {
                                XtraMessageBox.Show("Ngôn ngữ đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Category.cs (Line: 480 - 507).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;
                            btnRefresh_Click(sender, e);
                        }
                    }
                }
                else
                {
                    XtraMessageBox.Show("Chọn đối tượng cần sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void chkCate_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkTrans_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void grvCate_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvCate.RowCount == 0)
            {
                //Nếu grid view ko có dữ liệu thì ko hiện lên textbox để tránh lỗi "Object reference not set to an instance of an object"
                cmbTransId.ResetText();
                cmbCateId.ResetText();
                txtCateName.ResetText();
                txtDesc.ResetText();
                txtCateCode.ResetText();
                cmbLang.ResetText();
                cmbServiceType.ResetText();
                return;
            }
            else
            {
                cmbCateId.SelectedValue = grvCate.GetFocusedRowCellValue("category_id");
                cmbTransId.SelectedValue = grvCate.GetFocusedRowCellValue("id");
                txtCateCode.Text = grvCate.GetFocusedRowCellValue("code").ToString();
                txtCateName.Text = grvCate.GetFocusedRowCellValue("category_name").ToString();
                txtDesc.Text = grvCate.GetFocusedRowCellValue("description").ToString();
                cmbLang.SelectedValue = grvCate.GetFocusedRowCellValue("language");
                cmbServiceType.SelectedValue = grvCate.GetFocusedRowCellValue("service_type");
            }
        }

        private void chkFilter_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radCate_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radTrans_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void cmbLang_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void chkFilter_CheckedChanged_1(object sender, EventArgs e)
        {
            if (chkFilter.Checked == true)
            {
                cmbTransId.Enabled = true;
                cmbTransId.Text = "--Chọn--";
                cmbCateId.Enabled = true;
                cmbCateId.Text = "--Chọn--";
                cmbLang.Enabled = true;
                cmbLang.Text = "--Chọn--";
                cmbServiceType.Enabled = true;
                cmbServiceType.Text = "--Chọn--";
                txtCateCode.Enabled = true;
                txtCateCode.ReadOnly = false;
                txtCateCode.Text = "--Tất cả--";
                txtCateName.Enabled = true;
                txtCateName.ReadOnly = false;
                txtCateName.Text = "--Tất cả--";
                txtDesc.Enabled = true;
                txtDesc.ReadOnly = false;
                txtDesc.Text = "--Tất cả--";
                radCate.Checked = false;
                radTrans.Checked = false;
            }
            else
            {
                usctr_lists_Category_Load(sender, e);
            }
        }

        private void btnFilter_Click_1(object sender, EventArgs e)
        {
            if (chkFilter.Checked == false)
            {
                XtraMessageBox.Show("Chưa chọn chế độ lọc", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string id_vl, cateid_vl, code_vl, lang_vl, catename_vl, desc_vl, service_type_vl;
                if (cmbTransId.SelectedValue.ToString() == "0" || cmbTransId.SelectedValue == null)
                {
                    id_vl = "--Tất cả--";
                }
                else
                {
                    id_vl = cmbTransId.SelectedValue.ToString();
                }

                if (cmbCateId.SelectedValue.ToString() == "0" || cmbCateId.SelectedValue == null)
                {
                    cateid_vl = "--Tất cả--";
                }
                else
                {
                    cateid_vl = cmbCateId.SelectedValue.ToString();
                }

                code_vl = txtCateCode.Text;
                catename_vl = txtCateName.Text.Replace("'", "''");
                desc_vl = txtDesc.Text.Replace("'", "''"); //Đổi dấu ' thành '' để tránh lỗi trong SQL ('' = ' trong SQL)

                if (cmbLang.SelectedValue.ToString() == "temp" || cmbLang.SelectedValue == null)
                {
                    lang_vl = "--Tất cả--";
                }
                else
                {
                    lang_vl = cmbLang.SelectedValue.ToString();
                }

                if (cmbServiceType.SelectedValue.ToString() == "temp" || cmbServiceType.SelectedValue == null)
                {
                    service_type_vl = "--Tất cả--";
                }
                else
                {
                    service_type_vl = cmbServiceType.SelectedValue.ToString();
                }

                try
                {
                    //conn.Open();
                    grctrCate.DataSource = null;
                    dt = new DataTable();
                    da = new SqlDataAdapter("d_show_filter_category", conn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add("@id", SqlDbType.NVarChar).Value = id_vl;
                    da.SelectCommand.Parameters.Add("@category_id", SqlDbType.NVarChar).Value = cateid_vl;
                    da.SelectCommand.Parameters.Add("@code", SqlDbType.NVarChar).Value = code_vl;
                    da.SelectCommand.Parameters.Add("@category_name", SqlDbType.NVarChar).Value = catename_vl;
                    da.SelectCommand.Parameters.Add("@language", SqlDbType.NVarChar).Value = lang_vl;
                    da.SelectCommand.Parameters.Add("@description", SqlDbType.NVarChar).Value = desc_vl;
                    da.SelectCommand.Parameters.Add("@service_type", SqlDbType.NVarChar).Value = service_type_vl;
                    da.Fill(dt);
                    grctrCate.DataSource = dt;
                    SetCaption();

                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Category.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                    lblRowCount.Text = grvCate.RowCount.ToString();
                }

            }
        }
        

        private void cmbCateId_SelectedIndexChanged(object sender, EventArgs e)
        {
            TransIdCollection();
        }
    }
}
