﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_lists_Studio : UserControl
    {
        // Khai báo biến
        private static usctr_lists_Studio usctr; // User control
        string btn_flag = null; // Biến xác định nút đang sử dụng

        int studioId;
        string tag, cityName, distName;
        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_lists_Studio UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_lists_Studio();
                }
                return usctr;
            }
        }

        public usctr_lists_Studio()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        // Dữ liệu combobox
        private void ComboBoxCityData()
        {
            //conn.Open();
            dt = new DataTable();
            da = new SqlDataAdapter("SELECT 0 as city_id, N'--Chọn--' as city_name UNION ALL SELECT city_id, city_name FROM city ORDER BY city_id", conn);
            da.Fill(dt);

            cmbCity.ValueMember = dt.Columns[0].ToString();
            cmbCity.DisplayMember = dt.Columns[1].ToString();
            cmbCity.DataSource = dt;
            cmbCity.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void ComboBoxDistrictData()
        {
            if (cmbCity.Text != "" || cmbCity.Text != "--Chọn--")
            {
                //conn.Open();
                dt = new DataTable();
                da = new SqlDataAdapter("SELECT 0 as dist_id, N'--Chọn--' as dist_name UNION ALL SELECT dist_id, dist_name FROM district WHERE city_id = '" + cmbCity.SelectedValue.ToString() + "' ORDER BY dist_id", conn);
                da.Fill(dt);

                cmbDist.ValueMember = dt.Columns[0].ToString();
                cmbDist.DisplayMember = dt.Columns[1].ToString();
                cmbDist.DataSource = dt;
                cmbDist.DropDownStyle = ComboBoxStyle.DropDownList;
                conn.Close();
            }
        }

        private void ComboBoxStatusData()
        {
            //conn.Open();
            string list = @"SELECT N'Chọn' as key_, N'--Chọn--' as value UNION ALL 
                            SELECT N'Mới' as key_, N'Mới' as value UNION ALL
                            SELECT N'Hoạt động' as key_, N'Hoạt động' as value UNION ALL
                            SELECT N'Vô hiệu hóa' as key_, N'Vô hiệu hóa' as value";
            dt = new DataTable();
            da = new SqlDataAdapter(list, conn);
            da.Fill(dt);

            cmbStatus.ValueMember = "key_";
            cmbStatus.DisplayMember = "value";
            cmbStatus.DataSource = dt;
            cmbStatus.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();

        }

        // Định dạng text ban đầu
        private void DefaultFormat()
        {
            txtStudioId.Enabled = false;
            txtStudioId.ReadOnly = true;
            txtStudioId.Font = new Font(txtStudioId.Font, FontStyle.Regular);

            txtHotline.Enabled = false;
            txtHotline.ReadOnly = true;

            txtMonthSlots.Enabled = false;
            txtMonthSlots.ReadOnly = true;

            txtSessionSlots.Enabled = false;
            txtSessionSlots.ReadOnly = true;

            txtFB.Enabled = false;
            txtFB.ReadOnly = true;

            txtWeb.Enabled = false;
            txtWeb.ReadOnly = true;

            txtTag.Enabled = false;
            txtTag.ReadOnly = true;

            cmbCity.Enabled = true;
            cmbDist.Enabled = true;
            cmbStatus.Enabled = true;
        }

        // Định dạng dữ liệu show ra bảng
        private void SetCaption()
        {
            string[] columnCaptions = { "Mã đối tác", "Mã hiệu", "Trạng thái", "Giới hạn tháng", "Giới hạn buổi", "Thành phố", "Quận", "Hotline", "Trang Facebook", "Trang chủ" };
            power.SetCaption(grvStudio, columnCaptions);
        }


        // Show first info
        private void ShowFirstInfo()
        {
            if (grvStudio.RowCount == 0)
            {
                txtStudioId.ResetText();
                cmbCity.SelectedIndex = 0;
                cmbDist.SelectedIndex = 0;
                cmbStatus.SelectedIndex = 0;
                txtTag.ResetText();
                txtHotline.ResetText();
                txtMonthSlots.ResetText();
                txtSessionSlots.ResetText();
                txtWeb.ResetText();
                txtFB.ResetText();
                return;
            }
            else
            {
                txtStudioId.Text = grvStudio.GetRowCellValue(0, "studio_id").ToString();
                cmbCity.SelectedItem = grvStudio.GetRowCellValue(0, "city_name");
                cmbDist.SelectedItem = grvStudio.GetRowCellValue(0, "dist_name");
                cmbStatus.SelectedValue = grvStudio.GetRowCellValue(0, "status");
                txtTag.Text = grvStudio.GetRowCellValue(0, "tag").ToString();
                txtHotline.Text = grvStudio.GetRowCellValue(0, "hotline").ToString();
                txtMonthSlots.Text = grvStudio.GetRowCellValue(0, "month_slots").ToString();
                txtSessionSlots.Text = grvStudio.GetRowCellValue(0, "session_slots").ToString();
                txtWeb.Text = grvStudio.GetRowCellValue(0, "website").ToString();
                txtFB.Text = grvStudio.GetRowCellValue(0, "facebook").ToString();
            }
        }

        // Show full data
        private void ShowFullData()
        {
            string tableName = @"(SELECT s.studio_id, s.tag, s.status, s.month_slots, s.session_slots, c.city_name, d.dist_name, s.hotline, s.facebook, s.website
                                  FROM studio s 
                                    LEFT JOIN district d ON (s.dist_id = d.dist_id)
                                    LEFT JOIN city c ON (d.city_id = c.city_id)
                                ) a";
            string selectedColumns = "studio_id, tag, status, month_slots, session_slots, city_name, dist_name, hotline, facebook, website";
            string afterWhereStr = "(1=1) ORDER BY studio_id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrStudio.DataSource = dt;
            SetCaption();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void grvStudio_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvStudio.RowCount == 0)
            {
                //Nếu grid view ko có dữ liệu thì ko hiện lên textbox để tránh lỗi "Object reference not set to an instance of an object"
                txtStudioId.ResetText();
                cmbCity.SelectedIndex = 0;
                cmbDist.SelectedIndex = 0;
                cmbStatus.SelectedIndex = 0;
                txtTag.ResetText();
                txtHotline.ResetText();
                txtMonthSlots.ResetText();
                txtSessionSlots.ResetText();
                txtFB.ResetText();
                txtWeb.ResetText();
                return;
            }
            else
            {
                txtStudioId.Font = new Font(txtStudioId.Font, FontStyle.Regular);
                txtStudioId.Text = grvStudio.GetFocusedRowCellValue("studio_id").ToString();
                txtTag.Text = grvStudio.GetFocusedRowCellValue("tag").ToString();
                cmbCity.Text = grvStudio.GetFocusedRowCellValue("city_name").ToString();
                cmbDist.Text = grvStudio.GetFocusedRowCellValue("dist_name").ToString();
                cmbStatus.SelectedValue = grvStudio.GetFocusedRowCellValue("status");
                txtHotline.Text = grvStudio.GetFocusedRowCellValue("hotline").ToString();
                txtMonthSlots.Text = grvStudio.GetFocusedRowCellValue("month_slots").ToString();
                txtSessionSlots.Text = grvStudio.GetFocusedRowCellValue("session_slots").ToString();
                txtFB.Text = grvStudio.GetFocusedRowCellValue("facebook").ToString();
                txtWeb.Text = grvStudio.GetFocusedRowCellValue("website").ToString();
            }

            //DefaultFormat();
            btn_flag = null;
        }

        private void cmbCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBoxDistrictData();
        }

        private void usctr_lists_Studio_Load(object sender, EventArgs e)
        {
            btn_flag = null;
            ComboBoxCityData();
            ComboBoxStatusData();
            grctrStudio.DataSource = null;
            grvStudio.FindFilterText = "";
            ShowFullData();
            ShowFirstInfo();
            DefaultFormat();
            lblRowCount.Text = grvStudio.RowCount.ToString();
            chkFilter.Checked = false;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";

            txtStudioId.Text = "Auto";
            txtStudioId.Font = new Font(txtStudioId.Font, FontStyle.Italic);

            txtTag.Enabled = true;
            txtTag.ReadOnly = false;
            txtTag.ResetText();

            cmbCity.SelectedIndex= 0;
            cmbDist.SelectedIndex = 0;
            cmbStatus.Enabled = false;

            txtMonthSlots.Enabled = true;
            txtMonthSlots.ReadOnly = false;
            txtMonthSlots.ResetText();

            txtSessionSlots.Enabled = true;
            txtSessionSlots.ReadOnly = false;
            txtSessionSlots.ResetText();

            txtHotline.Enabled = true;
            txtHotline.ReadOnly = false;
            txtHotline.ResetText();

            txtFB.Enabled = true;
            txtFB.ReadOnly = false;
            txtFB.ResetText();

            txtWeb.Enabled = true;
            txtWeb.ReadOnly = false;
            txtWeb.ResetText();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            btn_flag = "edit";

            if (txtStudioId.Text == "")
            {
                XtraMessageBox.Show("Chọn một bản ghi để sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                cmbCity.Enabled = true;
                cmbDist.Enabled = true;
                cmbStatus.Enabled = true;

                txtTag.Enabled = true;
                txtTag.ReadOnly = false;

                txtFB.Enabled = true;
                txtFB.ReadOnly = false;

                txtHotline.Enabled = true;
                txtHotline.ReadOnly = false;

                txtMonthSlots.Enabled = true;
                txtMonthSlots.ReadOnly = false;

                txtSessionSlots.Enabled = true;
                txtSessionSlots.ReadOnly = false;

                txtWeb.Enabled = true;
                txtWeb.ReadOnly = false;

                cmbCity.SelectedItem = grvStudio.GetFocusedRowCellValue("city_name");
                cmbDist.SelectedItem = grvStudio.GetFocusedRowCellValue("dist_name");
                cmbStatus.SelectedValue = grvStudio.GetFocusedRowCellValue("status");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtStudioId.Text == "")
            {
                XtraMessageBox.Show("Chọn một bản ghi để xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa đối tác '" + txtTag.Text + "' ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM studio WHERE studio_id = " + int.Parse(txtStudioId.Text), conn);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (SqlException)
                    {
                        XtraMessageBox.Show("Bản ghi đang được sử dụng, đảm bảo đã xóa hết dữ liệu liên quan.", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Studio.cs (Line: 327 - 340).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        usctr_lists_Studio_Load(sender, e);
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_lists_Studio_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (cmbDist.Text == "" || cmbDist.Text == "--Chọn--")
                {
                    XtraMessageBox.Show("Chọn thông tin quận", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (txtTag.Text == "" || txtMonthSlots.Text == "" || txtSessionSlots.Text == "")
                    {
                        XtraMessageBox.Show("Nhập đủ thông tin: \n - Mã hiệu \n - Giới hạn tháng \n - Giới hạn buổi", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        try
                        {
                            // Khai báo các cột và values cần check duplicate
                            string[] col_names = { "tag" };
                            string[] col_values = { txtTag.Text };

                            conn.Open();
                            if (power.IsNumber(txtHotline.Text.Replace(" ", "")) == true && power.IsNumber(txtMonthSlots.Text.Replace(" ", "")) == true && power.IsNumber(txtSessionSlots.Text.Replace(" ", "")) == true)
                            {
                                //Check trùng
                                if (power.Sql_CheckDuplicate("studio", col_names, col_values) == false)
                                {
                                    cmd = new SqlCommand("a_insert_tb_studio", conn);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add("@tag", SqlDbType.VarChar, 50).Value = txtTag.Text;
                                    cmd.Parameters.Add("@month_slots", SqlDbType.Int).Value = int.Parse(txtMonthSlots.Text.Replace(" ", ""));
                                    cmd.Parameters.Add("@session_slots", SqlDbType.Int).Value = int.Parse(txtSessionSlots.Text.Replace(" ", ""));
                                    cmd.Parameters.Add("@hotline", SqlDbType.VarChar).Value = txtHotline.Text.Replace(" ", "");
                                    cmd.Parameters.Add("@facebook", SqlDbType.VarChar).Value = txtFB.Text;
                                    cmd.Parameters.Add("@website", SqlDbType.VarChar).Value = txtWeb.Text;
                                    cmd.Parameters.Add("@dist_id", SqlDbType.Int).Value = int.Parse(cmbDist.SelectedValue.ToString());
                                    cmd.ExecuteNonQuery();
                                    XtraMessageBox.Show("TẠO THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    ShowFirstInfo();
                                }
                                else
                                {
                                    XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            else
                            {
                                XtraMessageBox.Show("Số Hotline hoặc Số giới hạn sai định dạng số", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Studio.cs (Line: 366 - 397).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;
                            usctr_lists_Studio_Load(sender, e);
                        }
                    }
                }
            }

            if (btn_flag == "edit")
            {
                if (cmbDist.Text == "" || cmbDist.Text == "--Chọn--")
                {
                    XtraMessageBox.Show("Chọn thông tin quận", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (txtTag.Text == "" || txtMonthSlots.Text == "" || txtSessionSlots.Text == "")
                    {
                        XtraMessageBox.Show("Nhập đủ thông tin: \n - Mã hiệu \n - Giới hạn tháng \n - Giới hạn buổi", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        try
                        {
                            // Khai báo các cột và values cần check duplicate
                            //string[] col_names = { "tag" };
                            //string[] col_values = { txtTag.Text };

                            conn.Open();

                            if (power.IsNumber(txtHotline.Text.Replace(" ", "")) == true && power.IsNumber(txtMonthSlots.Text.Replace(" ", "")) == true && power.IsNumber(txtSessionSlots.Text.Replace(" ", "")) == true)
                            {

                                cmd = new SqlCommand("b_update_tb_studio", conn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@tag", SqlDbType.VarChar, 50).Value = txtTag.Text;
                                cmd.Parameters.Add("@month_slots", SqlDbType.Int).Value = int.Parse(txtMonthSlots.Text.Replace(" ", ""));
                                cmd.Parameters.Add("@session_slots", SqlDbType.Int).Value = int.Parse(txtSessionSlots.Text.Replace(" ", ""));
                                cmd.Parameters.Add("@hotline", SqlDbType.VarChar).Value = txtHotline.Text.Replace(" ", "");
                                cmd.Parameters.Add("@facebook", SqlDbType.VarChar).Value = txtFB.Text;
                                cmd.Parameters.Add("@website", SqlDbType.VarChar).Value = txtWeb.Text;
                                cmd.Parameters.Add("@dist_id", SqlDbType.Int).Value = int.Parse(cmbDist.SelectedValue.ToString());
                                cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(txtStudioId.Text);
                                cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = cmbStatus.SelectedValue.ToString();
                                cmd.ExecuteNonQuery();
                                XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ShowFirstInfo();
                            }
                            else
                            {
                                XtraMessageBox.Show("Số Hotline hoặc Số giới hạn sai định dạng", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Studio.cs (Line: 416 - 453).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;
                            usctr_lists_Studio_Load(sender, e);
                        }
                    }
                }
            }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {   
            if (chkFilter.Checked == false)
            {
                XtraMessageBox.Show("Chưa chọn chế độ lọc", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string city_value, dist_value, status_value;
                string studio_id_value, tag_value, hotline_value, month_slots_value, session_slots_value, web_value, fb_value; 

                if (cmbCity.SelectedValue.ToString() == "0")
                {
                    city_value = "";
                }
                else
                {
                    city_value = cmbCity.SelectedValue.ToString();
                }

                if (cmbDist.SelectedValue.ToString() == "0")
                {
                    dist_value = "";
                }
                else
                {
                    dist_value = cmbDist.SelectedValue.ToString();
                }

                if (cmbStatus.SelectedValue.ToString() == "Chọn")
                {
                    status_value = "";
                }
                else
                {
                    status_value = cmbStatus.SelectedValue.ToString();
                }

                studio_id_value = txtStudioId.Text;
                tag_value = txtTag.Text;
                hotline_value = txtHotline.Text;
                month_slots_value = txtMonthSlots.Text;
                session_slots_value = txtSessionSlots.Text;
                web_value = txtWeb.Text;
                fb_value = txtFB.Text;

                try
                {
                    conn.Open();
                    grctrStudio.DataSource = null;
                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter("d_show_filter_studio", conn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add("@studio_id", SqlDbType.NVarChar).Value = studio_id_value;
                    da.SelectCommand.Parameters.Add("@tag", SqlDbType.NVarChar).Value = tag_value;
                    da.SelectCommand.Parameters.Add("@city_id", SqlDbType.NVarChar).Value = city_value;
                    da.SelectCommand.Parameters.Add("@dist_id", SqlDbType.NVarChar).Value = dist_value;
                    da.SelectCommand.Parameters.Add("@status", SqlDbType.NVarChar).Value = status_value;
                    da.SelectCommand.Parameters.Add("@hotline", SqlDbType.NVarChar).Value = hotline_value;
                    da.SelectCommand.Parameters.Add("@month_slots", SqlDbType.NVarChar).Value = month_slots_value;
                    da.SelectCommand.Parameters.Add("@session_slots", SqlDbType.NVarChar, 50).Value = session_slots_value;
                    da.SelectCommand.Parameters.Add("@website", SqlDbType.NVarChar).Value = web_value;
                    da.SelectCommand.Parameters.Add("@facebook", SqlDbType.NVarChar).Value = fb_value;
                    da.Fill(dt);
                    grctrStudio.DataSource = dt;
                    SetCaption();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Studio.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                    lblRowCount.Text = grvStudio.RowCount.ToString();
                }
            }
        }

        private void btnTrans_Click(object sender, EventArgs e)
        {
            if (txtStudioId.Text == "" || power.IsNumber(txtStudioId.Text) == false)
            {
                XtraMessageBox.Show("Chọn một đối tác cụ thể để hiện thông tin chi tiết.", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                studioId = int.Parse(txtStudioId.Text);
                tag = txtTag.Text;
                cityName = cmbCity.Text;
                distName = cmbDist.Text;

                popform_lists_Studio_Translation frm = new popform_lists_Studio_Translation(studioId, tag, cityName, distName);
                frm.ShowDialog();
            }
        }

        private void btnManager_Click(object sender, EventArgs e)
        {
            if (txtStudioId.Text == "" || power.IsNumber(txtStudioId.Text) == false)
            {
                XtraMessageBox.Show("Chọn một đối tác cụ thể để hiện thông tin chi tiết.", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                studioId = int.Parse(txtStudioId.Text);
                tag = txtTag.Text;

                popform_lists_Studio_Manager frm = new popform_lists_Studio_Manager(studioId, tag);
                frm.ShowDialog();
            }
        }

        private void chkFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilter.Checked == true)
            {
                txtStudioId.Enabled = true;
                txtStudioId.ReadOnly = false;
                txtStudioId.Text = "--Tất cả--";

                txtTag.Enabled = true;
                txtTag.ReadOnly = false;
                txtTag.Text = "--Tất cả--";

                txtMonthSlots.Enabled = true;
                txtMonthSlots.ReadOnly = false;
                txtMonthSlots.Text = "--Tất cả--";

                txtSessionSlots.Enabled = true;
                txtSessionSlots.ReadOnly = false;
                txtSessionSlots.Text = "--Tất cả--";

                txtHotline.Enabled = true;
                txtHotline.ReadOnly = false;
                txtHotline.Text = "--Tất cả--";

                txtWeb.Enabled = true;
                txtWeb.ReadOnly = false;
                txtWeb.Text = "--Tất cả--";

                txtFB.Enabled = true;
                txtFB.ReadOnly = false;
                txtFB.Text = "--Tất cả--";

                cmbCity.SelectedIndex = 0;
                cmbDist.SelectedIndex = 0;
                cmbStatus.SelectedIndex = 0;
            }
            else
            {
                usctr_lists_Studio_Load(sender, e);
            }
        }
    }
}
