﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_lists_Bank : UserControl
    {
        // Khai báo biến
        private static usctr_lists_Bank usctr; // User control
        string btn_flag = null; // Biến xác định nút đang sử dụng

        int bank_id;
        string bank_code;

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_lists_Bank UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_lists_Bank();
                }
                return usctr;
            }
        }

        public usctr_lists_Bank()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        // Định dạng text ban đầu
        private void DefaultFormat()
        {
            txtBankId.Font = new Font(txtBankId.Font, FontStyle.Regular);
            txtBankId.Enabled = false;
            txtBankId.ReadOnly = true;
            txtBankCode.Enabled = false;
            txtBankCode.ReadOnly = true;
            txtBankName.Enabled = false;
            txtBankName.ReadOnly = true;
        }

        // Định dạng dữ liệu show ra bảng
        private void SetCaption()
        {
            string[] columnCaptions = { "Mã ngân hàng", "Tên giao dịch", "Tên đầy đủ" };
            power.SetCaption(grvBank, columnCaptions);
        }

        // Show first info
        private void ShowFirstInfo()
        {
            if (grvBank.RowCount == 0)
            {
                txtBankId.ResetText();
                txtBankCode.ResetText();
                txtBankName.ResetText();
            }
            else
            {
                txtBankId.Text = grvBank.GetRowCellValue(0, "bank_id").ToString();
                txtBankCode.Text = grvBank.GetRowCellValue(0, "bank_code").ToString();
                txtBankName.Text = grvBank.GetRowCellValue(0, "bank_name").ToString();
            }
        }

        // Show full data
        private void ShowFullData()
        {
            string tableName = "bank";
            string selectedColumns = "bank_id, bank_code, bank_name";
            string afterWhereStr = "(1=1) ORDER BY bank_id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrBank.DataSource = dt;
            SetCaption();
        }

        private void grvBank_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvBank.RowCount == 0)
            {
                //Nếu grid view ko có dữ liệu thì ko hiện lên textbox để tránh lỗi "Object reference not set to an instance of an object"
                txtBankId.ResetText();
                txtBankCode.ResetText();
                txtBankName.ResetText();
                return;
            }
            else
            {
                txtBankId.Font = new Font(txtBankId.Font, FontStyle.Regular);
                txtBankId.Text = grvBank.GetFocusedRowCellValue("bank_id").ToString();
                txtBankCode.Text = grvBank.GetFocusedRowCellValue("bank_code").ToString();
                txtBankName.Text = grvBank.GetFocusedRowCellValue("bank_name").ToString();
            }

            DefaultFormat();
            btn_flag = null;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";
            txtBankCode.ResetText();
            txtBankName.ResetText();
            txtBankCode.Enabled = true;
            txtBankCode.ReadOnly = false;
            txtBankName.Enabled = true;
            txtBankName.ReadOnly = false;
            txtBankId.Text = "Auto";
            txtBankId.Font = new Font(txtBankId.Font, FontStyle.Italic);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (txtBankCode.Text == "" || txtBankName.Text == "")
            {
                XtraMessageBox.Show("Chọn đối tượng cần sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                btn_flag = "edit";
                txtBankCode.Enabled = true;
                txtBankCode.ReadOnly = false;
                txtBankName.Enabled = true;
                txtBankName.ReadOnly = false;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtBankCode.Text == "" || txtBankName.Text == "")
            {
                XtraMessageBox.Show("Chọn đối tượng cần xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa bản ghi '" + txtBankCode.Text + "' ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM bank WHERE bank_id = " + int.Parse(txtBankId.Text), conn);
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Bank (Line 166 - 169).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btnRefresh_Click(sender, e); // F5
                        btn_flag = null;
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_lists_Bank_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (txtBankName.Text == "" || txtBankCode.Text == "")
                {
                    XtraMessageBox.Show("Chưa nhập đủ thông tin ngân hàng", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        // Khai báo các cột và values cần check duplicate
                        string[] col_names = { "bank_code", "bank_name" };
                        string[] col_values = { txtBankCode.Text, txtBankName.Text };

                        // Check trùng
                        if (power.Sql_CheckDuplicate("bank", col_names, col_values) == false)
                        {
                            conn.Open();
                            cmd = new SqlCommand("a_insert_tb_bank", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@bank_code", SqlDbType.VarChar, 50).Value = txtBankCode.Text;
                            cmd.Parameters.Add("@bank_name", SqlDbType.NVarChar, 50).Value = txtBankName.Text;
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("THÊM THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Bank (Line 196 - 215).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btnRefresh_Click(sender, e); // F5
                        btn_flag = null;
                    }
                }
            }

            if (btn_flag == "edit")
            {
                if (txtBankName.Text == "" || txtBankCode.Text == "")
                {
                    XtraMessageBox.Show("Chưa nhập đủ thông tin ngân hàng", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        // Khai báo các cột và values cần check duplicate
                        string[] col_names = { "bank_code", "bank_name" };
                        string[] col_values = { txtBankCode.Text, txtBankName.Text };

                        // Check trùng
                        if (power.Sql_CheckDuplicate("bank", col_names, col_values) == false)
                        {
                            conn.Open();
                            cmd = new SqlCommand("b_update_tb_bank", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@bank_code", SqlDbType.VarChar, 50).Value = txtBankCode.Text;
                            cmd.Parameters.Add("@bank_name", SqlDbType.NVarChar, 50).Value = txtBankName.Text;
                            cmd.Parameters.Add("@bank_id", SqlDbType.Int).Value = int.Parse(txtBankId.Text);
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Bank (Line 232 - 253).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btnRefresh_Click(sender, e); // F5
                        btn_flag = null;
                    }
                }
            }
        }

        private void usctr_lists_Bank_Load(object sender, EventArgs e)
        {
            btn_flag = null;
            grctrBank.DataSource = null;
            grvBank.FindFilterText = "";
            btnShowBranch.Enabled = true;
            DefaultFormat();
            ShowFullData();
            ShowFirstInfo();
            lblBankCount.Text = grvBank.RowCount.ToString();
        }

        private void btnShowBranch_Click(object sender, EventArgs e)
        {
            bank_id = int.Parse(txtBankId.Text);
            bank_code = txtBankCode.Text;

            popform_lists_Bank_Branch frm = new popform_lists_Bank_Branch(bank_id, bank_code);
            frm.ShowDialog();
        }
    }
}
