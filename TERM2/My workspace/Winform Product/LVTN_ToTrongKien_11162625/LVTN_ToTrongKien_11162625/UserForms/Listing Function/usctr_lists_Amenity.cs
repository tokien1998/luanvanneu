﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using DevExpress.XtraEditors;


namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_lists_Amenity : UserControl
    {
        // Khai báo biến
        private static usctr_lists_Amenity usctr; // User control
        string btn_flag = null; // Biến xác định nút đang sử dụng

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_lists_Amenity UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_lists_Amenity();
                }
                return usctr;
            }
        }
        public usctr_lists_Amenity()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        // Dữ liệu combobox
        private void languageCollection()
        {
            Dictionary<string, string> langeSource = new Dictionary<string, string>();
            langeSource.Add("temp", "--Chọn--");
            langeSource.Add("vi", "Tiếng Việt");
            langeSource.Add("en", "English");

            cmbLang.DataSource = new BindingSource(langeSource, null);
            cmbLang.DisplayMember = "Value";
            cmbLang.ValueMember = "Key";
            cmbLang.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void TransIdCollection()
        {
            if (cmbAmenId.Text != "" || cmbAmenId.Text != "--Chọn--")
            {
                dt = new DataTable();
                string query = "SELECT 0 as id, N'--Chọn--' as id_name UNION ALL SELECT id, CAST(id as NVARCHAR) as id_name FROM amenity_translation WHERE amenity_id = '" + cmbAmenId.SelectedValue.ToString() + "'";
                da = new SqlDataAdapter(query, conn);
                da.Fill(dt);
                cmbTransId.DisplayMember = "id_name";
                cmbTransId.ValueMember = "id";
                cmbTransId.DataSource = dt;
                cmbTransId.DropDownStyle = ComboBoxStyle.DropDownList;
                conn.Close();
            }
        }

        private void AmenIdCollection()
        {
            dt = new DataTable();
            string query = "SELECT 0 as id, N'--Chọn--' as id_name UNION ALL SELECT amenity_id as id, CAST(amenity_id as NVARCHAR) as id_name FROM amenity";
            da = new SqlDataAdapter(query, conn);
            da.Fill(dt);
            cmbAmenId.DisplayMember = "id_name";
            cmbAmenId.ValueMember = "id";
            cmbAmenId.DataSource = dt;
            cmbAmenId.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        // Định dạng text ban đầu
        private void DefaultFormat()
        {
            cmbAmenId.Enabled = false;
            cmbTransId.Enabled = false;
            txtAmenCode.Enabled = false;
            txtAmenCode.ReadOnly = true;
            txtAmenName.Enabled = false;
            txtAmenName.ReadOnly = true;
            txtDesc.Enabled = false;
            txtDesc.ReadOnly = true;
            cmbLang.Enabled = true;
        }

        // Định dạng dữ liệu show ra bảng
        private void SetCaption()
        {
            string[] columnCaptions = { "Mã bản ghi", "Mã tiện ích", "Kí hiệu", "Ngôn ngữ", "Tên tiện ích", "Mô tả" };
            power.SetCaption(gridView1, columnCaptions);
        }

        // Show first info
        private void ShowFirstInfo()
        {
            if (gridView1.RowCount == 0)
            {
                cmbTransId.ResetText();
                cmbAmenId.ResetText();
                txtAmenCode.ResetText();
                txtAmenName.ResetText();
                txtDesc.ResetText();
                cmbLang.ResetText();
                return;
            }
            else
            {
                cmbTransId.SelectedValue = gridView1.GetRowCellValue(0, "id");
                cmbAmenId.SelectedValue = gridView1.GetRowCellValue(0, "amenity_id");
                txtAmenCode.Text = gridView1.GetRowCellValue(0, "code").ToString();
                txtAmenName.Text = gridView1.GetRowCellValue(0, "amenity_name").ToString();
                txtDesc.Text = gridView1.GetRowCellValue(0, "description").ToString();
                cmbLang.SelectedValue = gridView1.GetRowCellValue(0, "language");
            }
        }

        // Show full data
        private void ShowFullData()
        {
            string tableName = @"(SELECT l.id, a.amenity_id, a.code, l.language, l.amenity_name, l.description
                                  FROM amenity a LEFT JOIN amenity_translation l ON (a.amenity_id = l.amenity_id)) a";
            string selectedColumns = "id, amenity_id, code, language, amenity_name, description";
            string afterWhereStr = "(1=1) ORDER BY amenity_id, id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            gridControl1.DataSource = dt;
            SetCaption();
        }

        // Format of radio button Amenity
        private void TickAmenFormat()
        {
            txtAmenCode.Enabled = true;
            txtAmenCode.ReadOnly = false;
            txtAmenName.Enabled = true;
            txtAmenName.ReadOnly = false;
            txtDesc.Enabled = true;
            txtDesc.ReadOnly = false;

            cmbAmenId.Enabled = false;

            cmbTransId.Enabled = false;

            cmbLang.Enabled = true;
            cmbLang.Text = "--Chọn--";
        }

        // Format of checkbox Amenity
        private void TickTransFormat()
        {
            txtAmenCode.Enabled = true;
            txtAmenCode.ReadOnly = false;
            txtAmenName.Enabled = true;
            txtAmenName.ReadOnly = false;
            txtDesc.Enabled = true;
            txtDesc.ReadOnly = false;

            cmbAmenId.Enabled = false;

            cmbTransId.Enabled = false;

            cmbLang.Enabled = true;
            cmbLang.Text = "--Chọn--";
        }

        private void grvAmen_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            // error control
        }

        private void cmbLang_SelectedIndexChanged(object sender, EventArgs e)
        {
            // ...
        }

        private void usctr_lists_Amenity_Load(object sender, EventArgs e)
        {
            btn_flag = null;
            AmenIdCollection();
            languageCollection();
            gridControl1.DataSource = null;
            gridView1.FindFilterText = "";
            ShowFullData();
            ShowFirstInfo();
            DefaultFormat();
            lblRowCount.Text = gridView1.RowCount.ToString();

            radAmen.Checked = true;
            radTrans.Checked = false;

            chkFilter.Checked = false;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";

            if (radTrans.Checked == true && txtAmenCode.Text == "")
            {
                XtraMessageBox.Show("Chọn tiện ích cần thêm bản ghi", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (radAmen.Checked == true)
                {
                    TickAmenFormat();
                    languageCollection();
                    cmbAmenId.SelectedIndex = 0;
                    cmbTransId.SelectedIndex = 0;
                    txtAmenCode.ResetText();
                    txtAmenName.ResetText();
                    txtDesc.ResetText();
                    cmbLang.SelectedIndex = 0;
                }
                else if (radTrans.Checked == true)
                {
                    TickTransFormat();
                    languageCollection();
                    cmbTransId.SelectedIndex = 0;
                    txtAmenName.ResetText();
                    txtDesc.ResetText();
                }
                else
                {
                    return;
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            btn_flag = "edit";

            if (txtAmenCode.Text == "")
            {
                XtraMessageBox.Show("Chọn đối tượng cần sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (radAmen.Checked == true)
                {
                    TickAmenFormat();
                    languageCollection();
                    txtAmenName.ReadOnly = true;
                    txtDesc.ReadOnly = true;
                }
                else if (radTrans.Checked == true)
                {
                    TickTransFormat();
                    languageCollection();
                    cmbLang.SelectedValue = gridView1.GetFocusedRowCellValue("language");
                    cmbLang.Enabled = false;

                }
                else
                {
                    return;
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtAmenCode.Text == "")
            {
                XtraMessageBox.Show("Chọn đối tượng cần xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (radAmen.Checked == true)
                {
                    if (XtraMessageBox.Show("Xóa tiện ích '" + txtAmenCode.Text + "' và tất cả bản ghi liên quan ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        try
                        {
                            conn.Open();
                            cmd = new SqlCommand("DELETE FROM amenity_translation WHERE amenity_id = " + int.Parse(cmbAmenId.SelectedValue.ToString()), conn);
                            cmd.ExecuteNonQuery();

                            cmd = new SqlCommand("DELETE FROM amenity WHERE amenity_id = " + int.Parse(cmbAmenId.SelectedValue.ToString()), conn);
                            cmd.ExecuteNonQuery();

                            XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Amenity.cs (Line: 333 - 344).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;
                            btnRefresh_Click(sender, e);
                        }
                    }
                }
                else if (radTrans.Checked == true)
                {
                    if (XtraMessageBox.Show("Xóa mã bản ghi '" + cmbTransId.SelectedValue.ToString() + " (" + cmbLang.Text + ") của tiện ích " + txtAmenName.Text + "' ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        try
                        {
                            conn.Open();
                            cmd = new SqlCommand("DELETE FROM amenity_translation WHERE id = " + int.Parse(cmbTransId.SelectedValue.ToString()), conn);
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Amenity.cs (Line: 361 - 367).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;
                            btnRefresh_Click(sender, e);
                        }
                    }
                }
                else
                {
                    return;
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_lists_Amenity_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // Editation event
            if (btn_flag == "edit")
            {
                if (radAmen.Checked == true)
                {
                    if (txtAmenCode.Text == "")
                    {
                        XtraMessageBox.Show("Nhập ký hiệu dịch vụ", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        try
                        {
                            // Khai báo các cột và values cần check duplicate
                            string[] col_names = { "code" };
                            string[] col_values = { txtAmenCode.Text };

                            conn.Open();

                            //Check trùng
                            if (power.Sql_CheckDuplicate("amenity", col_names, col_values) == false)
                            {
                                cmd = new SqlCommand("b_update_tb_amenity", conn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@code", SqlDbType.VarChar, 50).Value = txtAmenCode.Text;
                                cmd.Parameters.Add("@amenity_id", SqlDbType.Int).Value = cmbAmenId.SelectedValue.ToString();
                                cmd.ExecuteNonQuery();
                                XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ShowFirstInfo();
                            }
                            else
                            {
                                XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Amenity.cs (Line: 400 - 427).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;
                            btnRefresh_Click(sender, e);
                        }
                    }
                }
                else if (radTrans.Checked == true)
                {
                    if (txtAmenName.Text == "" || cmbLang.Text == "")
                    {
                        XtraMessageBox.Show("Nhập tên tiện ích và ngôn ngữ", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        try
                        {
                            conn.Open();

                            cmd = new SqlCommand("b_update_tb_amenity_translation", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = txtDesc.Text;
                            cmd.Parameters.Add("@id", SqlDbType.Int).Value = cmbTransId.SelectedValue.ToString();
                            cmd.Parameters.Add("@language", SqlDbType.VarChar, 50).Value = cmbLang.SelectedValue.ToString();
                            cmd.Parameters.Add("@amenity_name", SqlDbType.NVarChar, 50).Value = txtAmenName.Text;
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ShowFirstInfo();

                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Amenity.cs (Line: 440 - 452).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;
                            btnRefresh_Click(sender, e);
                        }
                    }
                }
                else
                {
                    return;
                }
            }

            // Creation event
            if (btn_flag == "create")
            {
                if (radAmen.Checked == true)
                {
                    if (txtAmenCode.Text == "")
                    {
                        XtraMessageBox.Show("Nhập ký hiệu dịch vụ", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        try
                        {
                            // Khai báo các cột và values cần check duplicate
                            string[] col_names = { "code" };
                            string[] col_values = { txtAmenCode.Text };

                            conn.Open();

                            //Check trùng
                            if (power.Sql_CheckDuplicate("amenity", col_names, col_values) == false)
                            {
                                cmd = new SqlCommand("a_insert_tb_amenity", conn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@code", SqlDbType.VarChar, 50).Value = txtAmenCode.Text;
                                cmd.Parameters.Add("@language", SqlDbType.VarChar, 50).Value = cmbLang.SelectedValue.ToString();
                                cmd.Parameters.Add("@amenity_name", SqlDbType.NVarChar, 50).Value = txtAmenName.Text;
                                cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = txtDesc.Text;
                                cmd.ExecuteNonQuery();
                                XtraMessageBox.Show("TẠO THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ShowFirstInfo();
                            }
                            else
                            {
                                XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }

                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Amenity.cs (Line: 477 - 505).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;
                            btnRefresh_Click(sender, e);
                        }
                    }
                }
                else if (radTrans.Checked == true)
                {
                    if (txtAmenName.Text == "" || cmbLang.Text == "")
                    {
                        XtraMessageBox.Show("Nhập tên tiện ích và ngôn ngữ", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        try
                        {
                            // Khai báo các cột và values cần check duplicate
                            string[] col_names = { "language", "amenity_id" };
                            string[] col_values = { cmbLang.SelectedValue.ToString(), cmbAmenId.SelectedValue.ToString() };

                            conn.Open();

                            //Check trùng
                            if (power.Sql_CheckDuplicate("amenity_translation", col_names, col_values) == false)
                            {
                                cmd = new SqlCommand("a_insert_tb_amenity_translation", conn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = txtDesc.Text;
                                cmd.Parameters.Add("@amenity_id", SqlDbType.Int).Value = cmbAmenId.SelectedValue.ToString();
                                cmd.Parameters.Add("@language", SqlDbType.VarChar, 50).Value = cmbLang.SelectedValue.ToString();
                                cmd.Parameters.Add("@amenity_name", SqlDbType.NVarChar, 50).Value = txtAmenName.Text;
                                cmd.ExecuteNonQuery();
                                XtraMessageBox.Show("TẠO THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ShowFirstInfo();
                            }
                            else
                            {
                                XtraMessageBox.Show("Ngôn ngữ đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Amenity.cs (Line: 518 - 542).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;
                            btnRefresh_Click(sender, e);
                        }
                    }
                }
                else
                {
                    return;
                }
            }
        }

        private void grctrAmen_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void grvAmen1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {

        }

        private void gridControl1_Load(object sender, EventArgs e)
        {

        }

        private void gridView1_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (gridView1.RowCount == 0)
            {
                cmbAmenId.ResetText();
                cmbTransId.ResetText();
                txtAmenCode.ResetText();
                txtAmenName.ResetText();
                txtDesc.ResetText();
                cmbLang.ResetText();
            }
            else
            {
                cmbAmenId.SelectedValue = gridView1.GetFocusedRowCellValue("amenity_id");
                cmbTransId.SelectedValue = gridView1.GetFocusedRowCellValue("id");
                txtAmenCode.Text = gridView1.GetFocusedRowCellValue("code").ToString();
                txtAmenName.Text = gridView1.GetFocusedRowCellValue("amenity_name").ToString();
                txtDesc.Text = gridView1.GetFocusedRowCellValue("description").ToString();
                cmbLang.SelectedValue = gridView1.GetFocusedRowCellValue("language");
            }
        }

        private void gridView2_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {

        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            if (chkFilter.Checked == false)
            {
               XtraMessageBox.Show("Chưa chọn chế độ lọc", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string id_vl, amenityid_vl, code_vl, lang_vl, amenityname_vl, desc_vl;
                if (cmbTransId.SelectedValue.ToString() == "0" || cmbTransId.SelectedValue == null)
                {
                    id_vl = "";
                }
                else
                {
                    id_vl = cmbTransId.SelectedValue.ToString();
                }

                if (cmbAmenId.SelectedValue.ToString() == "0" || cmbAmenId.SelectedValue == null)
                {
                    amenityid_vl = "";
                }
                else
                {
                    amenityid_vl = cmbAmenId.SelectedValue.ToString();
                }
                
                code_vl = txtAmenCode.Text;
                amenityname_vl = txtAmenName.Text.Replace("'", "''");
                desc_vl = txtDesc.Text.Replace("'","''"); //Đổi dấu ' thành '' để tránh lỗi trong SQL ('' = ' trong SQL)

                if (cmbLang.SelectedValue.ToString() == "temp" || cmbLang.SelectedValue == null)
                {
                    lang_vl = "";
                }
                else
                {
                    lang_vl = cmbLang.SelectedValue.ToString();
                }

                try
                {
                    conn.Open();
                    gridControl1.DataSource = null;
                    dt = new DataTable();
                    da = new SqlDataAdapter("d_show_filter_amenity", conn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add("@id", SqlDbType.NVarChar).Value = id_vl;
                    da.SelectCommand.Parameters.Add("@amenity_id", SqlDbType.NVarChar).Value = amenityid_vl;
                    da.SelectCommand.Parameters.Add("@code", SqlDbType.NVarChar).Value = code_vl;
                    da.SelectCommand.Parameters.Add("@amenity_name", SqlDbType.NVarChar).Value = amenityname_vl;
                    da.SelectCommand.Parameters.Add("@language", SqlDbType.NVarChar).Value = lang_vl;
                    da.SelectCommand.Parameters.Add("@description", SqlDbType.NVarChar).Value = desc_vl;
                    da.Fill(dt);
                    gridControl1.DataSource = dt;
                    SetCaption();

                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_Amenity.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                    lblRowCount.Text = gridView1.RowCount.ToString();
                }

            }
        }

        private void chkFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilter.Checked == true)
            {
                cmbTransId.Text = "--Chọn--";
                cmbTransId.Enabled = true;
                txtAmenCode.Text = "--Tất cả--";
                txtAmenCode.Enabled = true;
                txtAmenCode.ReadOnly = false;
                cmbAmenId.Text = "--Chọn--";
                cmbAmenId.Enabled = true;
                txtAmenName.Text = "--Tất cả--";
                txtAmenName.Enabled = true;
                txtAmenName.ReadOnly = false;
                txtDesc.Text = "--Tất cả--";
                txtDesc.Enabled = true;
                txtDesc.ReadOnly = false;
                cmbLang.Text = "--Chọn--";
                radAmen.Checked = false;
                radTrans.Checked = false;
            }
            else
            {
                usctr_lists_Amenity_Load(sender, e);
            }
        }

        private void cmbTransId_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbAmenId_SelectedIndexChanged(object sender, EventArgs e)
        {
            TransIdCollection();
        }
    }
}
