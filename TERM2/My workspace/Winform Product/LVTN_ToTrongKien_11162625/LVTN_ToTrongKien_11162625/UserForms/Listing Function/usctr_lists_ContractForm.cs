﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using DevExpress.XtraEditors;
using System.IO;
using Word = Microsoft.Office.Interop.Word;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_lists_ContractForm : UserControl
    {
        // Khai báo biến
        private static usctr_lists_ContractForm usctr; // User control
        string btn_flag = null; // Biến xác định nút đang sử dụng

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Biến dùng cho Word
        private readonly string Temp = Application.StartupPath + "/_templates/onaclover_contract_template.docx"; //Đường dẫn đến file hợp đồng mẫu
        string contractFolder = Path.Combine(Application.StartupPath, "_contracts"); // Đường dẫn folder chứa hợp đồng hoàn thiện
        string formFolder = Path.Combine(Application.StartupPath, "_forms"); // Đường dẫn folder chứa biểu mẫu hoàn thiện

        private void btnEdit_Click(object sender, EventArgs e)
        {
            btn_flag = "edit";

            if (lblContractId.Text == "Contract_Id") // default value
            {
                XtraMessageBox.Show("Chọn một mã hợp đồng để sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                lblContractId.Visible = false;
            }
            else
            {
                lblContractId.Visible = true;

                cmbStatus.Enabled = true;

                txtSignedAddress.Enabled = true;
                txtSignedAddress.ReadOnly = false;

                txtContractLink.Enabled = false;
                txtContractLink.ReadOnly = true;

                txtFormLink.Enabled = false;
                txtFormLink.ReadOnly = true;

                dtpSignedDate.Enabled = true;
                dtpValidDate.Enabled = true;
                dtpExpireDate.Enabled = true;

                txtFullname.Enabled = true;
                txtFullname.ReadOnly = false;

                txtRepresentative.Enabled = true;
                txtRepresentative.ReadOnly = false;

                txtAddress.Enabled = true;
                txtAddress.ReadOnly = false;

                txtPhone.Enabled = true;
                txtPhone.ReadOnly = false;

                txtTaxNum.Enabled = true;
                txtTaxNum.ReadOnly = false;

                txtBankAccount.Enabled = true;
                txtBankAccount.ReadOnly = false;

                txtAccountNum.Enabled = true;
                txtAccountNum.ReadOnly = false;
            }
        }

        // Thuộc tính
        public static usctr_lists_ContractForm UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_lists_ContractForm();
                }
                return usctr;
            }
        }

        public usctr_lists_ContractForm()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        // Dữ liệu combobox
        private void CmbStudioID()
        {
            string query = "SELECT 0 as studio_id, N'--Chọn--' as studio_id_str UNION ALL SELECT studio_id, tag as studio_id_str /* CAST(studio_id AS nvarchar) */ FROM studio";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbStudioId.DisplayMember = "studio_id_str";
            cmbStudioId.ValueMember = "studio_id";
            cmbStudioId.DataSource = dt;
            cmbStudioId.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void CmbStatus()
        {
            //conn.Open();
            string list = @"SELECT N'--Chọn--' as status UNION ALL 
                            SELECT N'Hoạt động' as status UNION ALL
                            SELECT N'Vô hiệu hóa' as status";
            dt = new DataTable();
            da = new SqlDataAdapter(list, conn);
            da.Fill(dt);

            cmbStatus.ValueMember = "status";
            cmbStatus.DisplayMember = "status";
            cmbStatus.DataSource = dt;
            cmbStatus.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void CmbBank()
        {
            string query = "SELECT 0 as bank_id, N'--Chọn--' as bank_code UNION ALL SELECT bank_id, bank_code FROM bank";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbBank.DisplayMember = "bank_code";
            cmbBank.ValueMember = "bank_id";
            cmbBank.DataSource = dt;
            cmbBank.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void cmbBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            string bank_id = cmbBank.SelectedValue.ToString();
            string query = "SELECT 0 as branch_id, N'--Chọn--' as branch_name UNION ALL SELECT branch_id, branch_name FROM bank_branch WHERE bank_id = " + bank_id;
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbBankBranch.DisplayMember = "branch_name";
            cmbBankBranch.ValueMember = "branch_id";
            cmbBankBranch.DataSource = dt;
            cmbBankBranch.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void usctr_lists_ContractForm_Load(object sender, EventArgs e)
        {
            btn_flag = null;
            CmbStudioID();
            CmbStatus();
            CmbBank();
            grctrContractForm.DataSource = null;
            //grvContractForm.FindFilterText = "";
            DefaultFormat();
            lblRowCount.Text = grvContractForm.RowCount.ToString();
            lblContractId.Visible = false;
        }

        // Format dữ liệu
        private void DefaultFormat()
        {
            cmbStudioId.SelectedIndex = 0;
            cmbStatus.SelectedIndex = 0;
            cmbStatus.Enabled = true;
            cmbBank.SelectedIndex = 0;
            cmbBankBranch.SelectedIndex = 0;

            txtSignedAddress.Enabled = false;
            txtSignedAddress.ReadOnly = true;
            txtSignedAddress.ResetText();

            txtContractLink.Enabled = false;
            txtContractLink.ReadOnly = true;
            txtContractLink.ResetText();

            txtFormLink.Enabled = false;
            txtFormLink.ReadOnly = true;
            txtFormLink.ResetText();

            dtpSignedDate.Value = DateTime.Today;
            dtpSignedDate.Enabled = false;
            dtpValidDate.Value = DateTime.Today;
            dtpValidDate.Enabled = false;
            dtpExpireDate.Value = DateTime.Today;
            dtpExpireDate.Enabled = false;

            txtFullname.Enabled = false;
            txtFullname.ReadOnly = true;
            txtFullname.ResetText();

            txtRepresentative.Enabled = false;
            txtRepresentative.ReadOnly = true;
            txtRepresentative.ResetText();

            txtAddress.Enabled = false;
            txtAddress.ReadOnly = true;
            txtAddress.ResetText();

            txtPhone.Enabled = false;
            txtPhone.ReadOnly = true;
            txtPhone.ResetText();

            txtTaxNum.Enabled = false;
            txtTaxNum.ReadOnly = true;
            txtTaxNum.ResetText();

            txtBankAccount.Enabled = false;
            txtBankAccount.ReadOnly = true;
            txtBankAccount.ResetText();

            txtAccountNum.Enabled = false;
            txtAccountNum.ReadOnly = true;
            txtAccountNum.ResetText();

            radContract.Checked = false;
            radForm.Checked = false;
        }

        private void SetCaption()
        {
            string[] columnCaptions = { "Mã hợp đồng", "Ngày hiệu lực", "Ngày hết hạn", "Trạng thái", "Ngày ký", "Nơi ký", "Tên tổ chức", "Đại diện", "SĐT", "Địa chỉ", "Mã số thuế", "Tên tài khoản", "Số tài khoản", "Ngân hàng", "Chi nhánh", "Link hợp đồng", "Link biểu mẫu"};
            power.SetCaption(grvContractForm, columnCaptions);
        }

        // Show data on data grid view
        private void ShowFullInfo()
        {
            string studio_id = cmbStudioId.SelectedValue.ToString();
            string tableName = "contract";
            string selectedColumns = "contract_id, valid_date, expire_date, status, signed_date, signed_address, b_full_name, b_representative, b_phone, b_address, b_tax_num, b_bank_account, b_bank_num, b_bank_code, b_bank_branch, contract_link, form_link";
            string afterWhereStr = "studio_id = " + studio_id + " ORDER BY contract_id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrContractForm.DataSource = dt;
            SetCaption();
        }

        // Show the first info
        private void ShowFirstInfo()
        {
            if (grvContractForm.RowCount == 0)
            {
                //cmbStudioId.SelectedIndex = 0;
                //cmbStatus.SelectedIndex = 0;
                //cmbBank.SelectedIndex = 0;
                //cmbBankBranch.SelectedIndex = 0;

                txtSignedAddress.ResetText();
                txtContractLink.ResetText();
                txtFormLink.ResetText();

                dtpSignedDate.Value = DateTime.Today;
                dtpValidDate.Value = DateTime.Today;
                dtpExpireDate.Value = DateTime.Today;

                txtFullname.ResetText();
                txtRepresentative.ResetText();
                txtAddress.ResetText();
                txtPhone.ResetText();
                txtTaxNum.ResetText();
                txtBankAccount.ResetText();
                txtAccountNum.ResetText();

                lblContractId.Visible = false;
                return;
            }
            else
            {
                lblContractId.Visible = true;
                lblContractId.Text = grvContractForm.GetRowCellValue(0, "contract_id").ToString();

                cmbStatus.SelectedValue = grvContractForm.GetRowCellValue(0, "status");
                cmbBank.Text = grvContractForm.GetRowCellValue(0, "b_bank_code").ToString();
                cmbBankBranch.Text = grvContractForm.GetRowCellValue(0, "b_bank_branch").ToString();

                txtSignedAddress.Text = grvContractForm.GetRowCellValue(0, "signed_address").ToString();
                txtContractLink.Text = grvContractForm.GetRowCellValue(0, "contract_link").ToString();
                txtFormLink.Text = grvContractForm.GetRowCellValue(0, "form_link").ToString();
                txtFullname.Text = grvContractForm.GetRowCellValue(0, "b_full_name").ToString();
                txtRepresentative.Text = grvContractForm.GetRowCellValue(0, "b_representative").ToString();
                txtAddress.Text = grvContractForm.GetRowCellValue(0, "b_address").ToString();
                txtPhone.Text = grvContractForm.GetRowCellValue(0, "b_phone").ToString();
                txtTaxNum.Text = grvContractForm.GetRowCellValue(0, "b_tax_num").ToString();
                txtBankAccount.Text = grvContractForm.GetRowCellValue(0, "b_bank_account").ToString();
                txtAccountNum.Text = grvContractForm.GetRowCellValue(0, "b_bank_num").ToString();

                dtpValidDate.Value = DateTime.Parse(grvContractForm.GetRowCellValue(0, "valid_date").ToString());
                dtpExpireDate.Value = DateTime.Parse(grvContractForm.GetRowCellValue(0, "expire_date").ToString());
                dtpSignedDate.Value = DateTime.Parse(grvContractForm.GetRowCellValue(0, "signed_date").ToString());

            }
        }

        private void cmbStudioId_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbStudioId.Text == "--Chọn--")
            {
                lblTag.Visible = false;
            }
            else
            {
                conn.Open();
                // SHow Tag
                string studio_id = cmbStudioId.SelectedValue.ToString();
                string query = "SELECT tag FROM studio WHERE studio_id = " + studio_id;
                string tag;
                cmd = new SqlCommand(query, conn);
                var result = cmd.ExecuteScalar();
                if (result == null)
                {
                    tag = "";
                }
                else
                {
                    tag = result.ToString();
                }
                lblTag.Text = tag;
                lblTag.Visible = true;
                conn.Close();

                // Show data grid view
                ShowFullInfo();
                ShowFirstInfo();

            }
            lblRowCount.Text = grvContractForm.RowCount.ToString();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";

            //cmbStudioId.SelectedIndex = 0;
            //cmbStatus.SelectedIndex = 0;
            //cmbStatus.Enabled = false;
            //cmbBank.SelectedIndex = 0;
            //cmbBankBranch.SelectedIndex = 0;

            txtSignedAddress.Enabled = true;
            txtSignedAddress.ReadOnly = false;
            txtSignedAddress.ResetText();

            txtContractLink.Enabled = false;
            txtContractLink.ReadOnly = true;
            txtContractLink.ResetText();

            txtFormLink.Enabled = false;
            txtFormLink.ReadOnly = true;
            txtFormLink.ResetText();

            dtpSignedDate.Value = DateTime.Today;
            dtpSignedDate.Enabled = true;
            dtpValidDate.Value = DateTime.Today;
            dtpValidDate.Enabled = true;
            dtpExpireDate.Value = DateTime.Today;
            dtpExpireDate.Enabled = true;

            txtFullname.Enabled = true;
            txtFullname.ReadOnly = false;
            txtFullname.ResetText();

            txtRepresentative.Enabled = true;
            txtRepresentative.ReadOnly = false;
            txtRepresentative.ResetText();

            txtAddress.Enabled = true;
            txtAddress.ReadOnly = false;
            txtAddress.ResetText();

            txtPhone.Enabled = true;
            txtPhone.ReadOnly = false;
            txtPhone.ResetText();

            txtTaxNum.Enabled = true;
            txtTaxNum.ReadOnly = false;
            txtTaxNum.ResetText();

            txtBankAccount.Enabled = true;
            txtBankAccount.ReadOnly = false;
            txtBankAccount.ResetText();

            txtAccountNum.Enabled = true;
            txtAccountNum.ReadOnly = false;
            txtAccountNum.ResetText();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lblContractId.Text == "")
            {
                XtraMessageBox.Show("Chọn một bản ghi để xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa hợp đồng '" + lblContractId.Text + "' - " + lblTag.Text + " ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM contract WHERE contract_id = " + int.Parse(lblContractId.Text), conn);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_ContractForm.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        usctr_lists_ContractForm_Load(sender, e);
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_lists_ContractForm_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // Creation Event
            if (btn_flag == "create")
            {
                if (cmbStudioId.Text == "--Chọn--" || txtSignedAddress.Text == "" || (dtpValidDate.Value == dtpExpireDate.Value && dtpExpireDate.Value == dtpSignedDate.Value) || txtFullname.Text == "" || txtRepresentative.Text == "" || txtPhone.Text == "" || txtAddress.Text == "" || txtAccountNum.Text == "" || txtBankAccount.Text == "" || txtTaxNum.Text == "")
                {
                    XtraMessageBox.Show("Nhập đủ thông tin: \n - Các thông tin (*) \n - Kiểm tra giá trị thời gian hợp lệ", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dtpExpireDate.Value <= dtpValidDate.Value)
                {
                    XtraMessageBox.Show("Thời gian hết hạn hợp đồng không phù hợp", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (power.IsNumber(txtPhone.Text.Replace(" ", "")) == false || power.IsNumber(txtAccountNum.Text.Replace(" ", "")) == false || power.IsNumber(txtTaxNum.Text.Replace(" ", "")) == false)
                {
                    XtraMessageBox.Show("Giá trị số nhập sai định dạng", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        // Khai báo các cột và values cần check duplicate
                        string[] col_names = { "valid_date", "expire_date", "studio_id" };
                        string[] col_values = { dtpValidDate.Value.ToString(), dtpExpireDate.Value.ToString(), cmbStudioId.SelectedValue.ToString() };

                        conn.Open();
                        //Check trùng
                        if (power.Sql_CheckDuplicate("contract", col_names, col_values) == false)
                        {
                            cmd = new SqlCommand("a_insert_tb_contract", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudioId.SelectedValue.ToString());
                            cmd.Parameters.Add("@valid_date", SqlDbType.Date).Value = dtpValidDate.Value;
                            cmd.Parameters.Add("@expire_date", SqlDbType.Date).Value = dtpExpireDate.Value;
                            cmd.Parameters.Add("@signed_date", SqlDbType.Date).Value = dtpSignedDate.Value;
                            cmd.Parameters.Add("@signed_address", SqlDbType.NVarChar).Value = txtSignedAddress.Text;
                            cmd.Parameters.Add("@b_full_name", SqlDbType.NVarChar, 50).Value = txtFullname.Text;
                            cmd.Parameters.Add("@b_representative", SqlDbType.NVarChar, 50).Value = txtRepresentative.Text;
                            cmd.Parameters.Add("@b_phone", SqlDbType.VarChar, 13).Value = txtPhone.Text.Replace(" ", "");
                            cmd.Parameters.Add("@b_address", SqlDbType.NVarChar).Value = txtAddress.Text;
                            cmd.Parameters.Add("@b_tax_num", SqlDbType.VarChar, 18).Value = txtTaxNum.Text.Replace(" ", "");
                            cmd.Parameters.Add("@b_bank_account", SqlDbType.NVarChar, 50).Value = txtBankAccount.Text;
                            cmd.Parameters.Add("@b_bank_num", SqlDbType.VarChar, 18).Value = txtAccountNum.Text.Replace(" ", "");
                            cmd.Parameters.Add("@b_bank_code", SqlDbType.NVarChar, 50).Value = cmbBank.Text;
                            cmd.Parameters.Add("@b_bank_branch", SqlDbType.NVarChar, 50).Value = cmbBankBranch.Text;
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("TẠO THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ShowFirstInfo();
                        }
                        else
                        {
                            XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_ContractForm.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;

                        // Show data grid view
                        //grctrContractForm.DataSource = null;
                        ShowFullInfo();
                        ShowFirstInfo();

                        lblRowCount.Text = grvContractForm.RowCount.ToString();
                        lblContractId.Visible = true;
                    }
                }
            }


            // Edit Event
            if (btn_flag == "edit")
            {
                if (cmbStudioId.Text == "--Chọn--" || lblContractId.Text == "Contract_Id" || cmbStatus.Text == "--Chọn--" || txtSignedAddress.Text == "" || (dtpValidDate.Value == dtpExpireDate.Value && dtpExpireDate.Value == dtpSignedDate.Value) || txtFullname.Text == "" || txtRepresentative.Text == "" || txtPhone.Text == "" || txtAddress.Text == "" || txtAccountNum.Text == "" || txtBankAccount.Text == "" || txtTaxNum.Text == "")
                {
                    XtraMessageBox.Show("Nhập đủ thông tin: \n - Các thông tin (*) \n - 3 kiểu thời gian đang cùng giá trị", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dtpExpireDate.Value <= dtpValidDate.Value)
                {
                    XtraMessageBox.Show("Thời gian hết hạn hợp đồng không phù hợp", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (power.IsNumber(txtPhone.Text.Trim()) == false || power.IsNumber(txtAccountNum.Text.Trim()) == false || power.IsNumber(txtTaxNum.Text.Trim()) == false)
                {
                    XtraMessageBox.Show("Giá trị số nhập sai định dạng", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {

                        conn.Open();
                        cmd = new SqlCommand("b_update_tb_contract", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudioId.SelectedValue.ToString());
                        cmd.Parameters.Add("@valid_date", SqlDbType.Date).Value = dtpValidDate.Value;
                        cmd.Parameters.Add("@expire_date", SqlDbType.Date).Value = dtpExpireDate.Value;
                        cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = cmbStatus.SelectedValue.ToString();
                        cmd.Parameters.Add("@contract_id", SqlDbType.Int).Value = lblContractId.Text;
                        cmd.Parameters.Add("@contract_link", SqlDbType.NVarChar).Value = txtContractLink.Text;
                        cmd.Parameters.Add("@form_link", SqlDbType.NVarChar).Value = txtFormLink.Text;
                        cmd.Parameters.Add("@signed_date", SqlDbType.Date).Value = dtpSignedDate.Value;
                        cmd.Parameters.Add("@signed_address", SqlDbType.NVarChar).Value = txtSignedAddress.Text;
                        cmd.Parameters.Add("@b_full_name", SqlDbType.NVarChar, 50).Value = txtFullname.Text;
                        cmd.Parameters.Add("@b_representative", SqlDbType.NVarChar, 50).Value = txtRepresentative.Text;
                        cmd.Parameters.Add("@b_phone", SqlDbType.VarChar, 13).Value = txtPhone.Text.Replace(" ", "");
                        cmd.Parameters.Add("@b_address", SqlDbType.NVarChar).Value = txtAddress.Text;
                        cmd.Parameters.Add("@b_tax_num", SqlDbType.VarChar, 18).Value = txtTaxNum.Text.Replace(" ", "");
                        cmd.Parameters.Add("@b_bank_account", SqlDbType.NVarChar, 50).Value = txtBankAccount.Text;
                        cmd.Parameters.Add("@b_bank_num", SqlDbType.VarChar, 18).Value = txtAccountNum.Text.Replace(" ", "");
                        cmd.Parameters.Add("@b_bank_code", SqlDbType.NVarChar, 50).Value = cmbBank.Text;
                        cmd.Parameters.Add("@b_bank_branch", SqlDbType.NVarChar, 50).Value = cmbBankBranch.Text;
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ShowFirstInfo();
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_ContractForm.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;

                        // Show data grid view
                        //grctrContractForm.DataSource = null;
                        ShowFullInfo();
                        ShowFirstInfo();

                        lblRowCount.Text = grvContractForm.RowCount.ToString();
                        lblContractId.Visible = true;
                    }
                }
            }


            // save a pdf file
            if (btn_flag == "browse")
            {
                // contract
                if (radContract.Checked == true)
                {
                    string fileName = Path.GetFileName(txtContractLink.Text); // Tên file trong đường dẫn
                    string insertPath = Path.Combine("_contracts", fileName); // Đường dẫn lưu vào CSDL (lưu cả folder)

                    try
                    {
                        // Check link tồn tại trong DB
                        conn.Open();

                        cmd = new SqlCommand("SELECT contract_link FROM contract WHERE studio_id = " + cmbStudioId.SelectedValue.ToString() + " AND contract_id = " + lblContractId.Text.ToString(), conn);
                        var result = cmd.ExecuteScalar();
                        if (result.ToString() == "" || result == null) // Chưa có link
                        {
                            // Lưu giá trị vào CSDL
                            cmd = new SqlCommand("b_update_tb_contract", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudioId.SelectedValue.ToString());
                            cmd.Parameters.Add("@valid_date", SqlDbType.Date).Value = dtpValidDate.Value;
                            cmd.Parameters.Add("@expire_date", SqlDbType.Date).Value = dtpExpireDate.Value;
                            cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = cmbStatus.SelectedValue.ToString();
                            cmd.Parameters.Add("@contract_id", SqlDbType.Int).Value = lblContractId.Text;
                            cmd.Parameters.Add("@contract_link", SqlDbType.NVarChar).Value = insertPath; // đường dẫn file vừa chọn
                            cmd.Parameters.Add("@form_link", SqlDbType.NVarChar).Value = txtFormLink.Text;
                            cmd.Parameters.Add("@signed_date", SqlDbType.Date).Value = dtpSignedDate.Value;
                            cmd.Parameters.Add("@signed_address", SqlDbType.NVarChar).Value = txtSignedAddress.Text;
                            cmd.Parameters.Add("@b_full_name", SqlDbType.NVarChar, 50).Value = txtFullname.Text;
                            cmd.Parameters.Add("@b_representative", SqlDbType.NVarChar, 50).Value = txtRepresentative.Text;
                            cmd.Parameters.Add("@b_phone", SqlDbType.VarChar, 13).Value = txtPhone.Text.Replace(" ", "");
                            cmd.Parameters.Add("@b_address", SqlDbType.NVarChar).Value = txtAddress.Text;
                            cmd.Parameters.Add("@b_tax_num", SqlDbType.VarChar, 18).Value = txtTaxNum.Text.Replace(" ", "");
                            cmd.Parameters.Add("@b_bank_account", SqlDbType.NVarChar, 50).Value = txtBankAccount.Text;
                            cmd.Parameters.Add("@b_bank_num", SqlDbType.VarChar, 18).Value = txtAccountNum.Text.Replace(" ", "");
                            cmd.Parameters.Add("@b_bank_code", SqlDbType.NVarChar, 50).Value = cmbBank.Text;
                            cmd.Parameters.Add("@b_bank_branch", SqlDbType.NVarChar, 50).Value = cmbBankBranch.Text;
                            cmd.ExecuteNonQuery();

                            XtraMessageBox.Show("LƯU THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            // Copy file vào folder contracts khi insert thành công
                            File.Copy(txtContractLink.Text, Path.Combine(contractFolder, fileName));

                        }
                        else // Đã có link
                        {
                            if (XtraMessageBox.Show("Xác nhận xóa file cũ và ghi đè lên bản ghi đã tồn tại: '" + result.ToString() + "'  ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                //Xóa file cũ trong folder
                                File.Delete(Path.Combine(Application.StartupPath, result.ToString()));

                                // Lưu giá trị vào CSDL
                                cmd = new SqlCommand("b_update_tb_contract", conn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudioId.SelectedValue.ToString());
                                cmd.Parameters.Add("@valid_date", SqlDbType.Date).Value = dtpValidDate.Value;
                                cmd.Parameters.Add("@expire_date", SqlDbType.Date).Value = dtpExpireDate.Value;
                                cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = cmbStatus.SelectedValue.ToString();
                                cmd.Parameters.Add("@contract_id", SqlDbType.Int).Value = lblContractId.Text;
                                cmd.Parameters.Add("@contract_link", SqlDbType.NVarChar).Value = insertPath; // đường dẫn file vừa chọn
                                cmd.Parameters.Add("@form_link", SqlDbType.NVarChar).Value = txtFormLink.Text;
                                cmd.Parameters.Add("@signed_date", SqlDbType.Date).Value = dtpSignedDate.Value;
                                cmd.Parameters.Add("@signed_address", SqlDbType.NVarChar).Value = txtSignedAddress.Text;
                                cmd.Parameters.Add("@b_full_name", SqlDbType.NVarChar, 50).Value = txtFullname.Text;
                                cmd.Parameters.Add("@b_representative", SqlDbType.NVarChar, 50).Value = txtRepresentative.Text;
                                cmd.Parameters.Add("@b_phone", SqlDbType.VarChar, 13).Value = txtPhone.Text.Replace(" ", "");
                                cmd.Parameters.Add("@b_address", SqlDbType.NVarChar).Value = txtAddress.Text;
                                cmd.Parameters.Add("@b_tax_num", SqlDbType.VarChar, 18).Value = txtTaxNum.Text.Replace(" ", "");
                                cmd.Parameters.Add("@b_bank_account", SqlDbType.NVarChar, 50).Value = txtBankAccount.Text;
                                cmd.Parameters.Add("@b_bank_num", SqlDbType.VarChar, 18).Value = txtAccountNum.Text.Replace(" ", "");
                                cmd.Parameters.Add("@b_bank_code", SqlDbType.NVarChar, 50).Value = cmbBank.Text;
                                cmd.Parameters.Add("@b_bank_branch", SqlDbType.NVarChar, 50).Value = cmbBankBranch.Text;
                                cmd.ExecuteNonQuery();

                                XtraMessageBox.Show("LƯU THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                // Copy file vào folder contracts khi insert thành công
                                File.Copy(txtContractLink.Text, Path.Combine(contractFolder, fileName));

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_ContractForm.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;

                        // Show data grid view
                        //grctrContractForm.DataSource = null;
                        ShowFullInfo();
                        ShowFirstInfo();

                        lblRowCount.Text = grvContractForm.RowCount.ToString();
                        lblContractId.Visible = true;
                    }

                    
                }

                // form
                if (radForm.Checked == true)
                {
                    string fileName = Path.GetFileName(txtFormLink.Text); // Tên file trong đường dẫn
                    string insertPath = Path.Combine("_forms", fileName); // Đường dẫn lưu vào CSDL (lưu cả folder)

                    try
                    {
                        // Check link tồn tại trong DB
                        conn.Open();

                        cmd = new SqlCommand("SELECT form_link FROM contract WHERE studio_id = " + cmbStudioId.SelectedValue.ToString() + " AND contract_id = " + lblContractId.Text.ToString(), conn);
                        var result = cmd.ExecuteScalar();
                        if (result.ToString() == "" || result == null) // Chưa có link
                        {
                            // Lưu giá trị vào CSDL
                            cmd = new SqlCommand("b_update_tb_contract", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudioId.SelectedValue.ToString());
                            cmd.Parameters.Add("@valid_date", SqlDbType.Date).Value = dtpValidDate.Value;
                            cmd.Parameters.Add("@expire_date", SqlDbType.Date).Value = dtpExpireDate.Value;
                            cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = cmbStatus.SelectedValue.ToString();
                            cmd.Parameters.Add("@contract_id", SqlDbType.Int).Value = lblContractId.Text;
                            cmd.Parameters.Add("@contract_link", SqlDbType.NVarChar).Value = txtContractLink.Text;
                            cmd.Parameters.Add("@form_link", SqlDbType.NVarChar).Value = insertPath; // đường dẫn file vừa chọn
                            cmd.Parameters.Add("@signed_date", SqlDbType.Date).Value = dtpSignedDate.Value;
                            cmd.Parameters.Add("@signed_address", SqlDbType.NVarChar).Value = txtSignedAddress.Text;
                            cmd.Parameters.Add("@b_full_name", SqlDbType.NVarChar, 50).Value = txtFullname.Text;
                            cmd.Parameters.Add("@b_representative", SqlDbType.NVarChar, 50).Value = txtRepresentative.Text;
                            cmd.Parameters.Add("@b_phone", SqlDbType.VarChar, 13).Value = txtPhone.Text.Replace(" ", "");
                            cmd.Parameters.Add("@b_address", SqlDbType.NVarChar).Value = txtAddress.Text;
                            cmd.Parameters.Add("@b_tax_num", SqlDbType.VarChar, 18).Value = txtTaxNum.Text.Replace(" ", "");
                            cmd.Parameters.Add("@b_bank_account", SqlDbType.NVarChar, 50).Value = txtBankAccount.Text;
                            cmd.Parameters.Add("@b_bank_num", SqlDbType.VarChar, 18).Value = txtAccountNum.Text.Replace(" ", "");
                            cmd.Parameters.Add("@b_bank_code", SqlDbType.NVarChar, 50).Value = cmbBank.Text;
                            cmd.Parameters.Add("@b_bank_branch", SqlDbType.NVarChar, 50).Value = cmbBankBranch.Text;
                            cmd.ExecuteNonQuery();

                            XtraMessageBox.Show("LƯU THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            // Copy file vào folder forms khi insert thành công
                            File.Copy(txtFormLink.Text, Path.Combine(formFolder, fileName));

                        }
                        else // Đã có link
                        {
                            if (XtraMessageBox.Show("Xác nhận xóa file cũ và ghi đè lên bản ghi đã tồn tại: '" + result.ToString() + "'  ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                //Xóa file cũ trong folder
                                File.Delete(Path.Combine(Application.StartupPath, result.ToString()));

                                // Lưu giá trị vào CSDL
                                cmd = new SqlCommand("b_update_tb_contract", conn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudioId.SelectedValue.ToString());
                                cmd.Parameters.Add("@valid_date", SqlDbType.Date).Value = dtpValidDate.Value;
                                cmd.Parameters.Add("@expire_date", SqlDbType.Date).Value = dtpExpireDate.Value;
                                cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = cmbStatus.SelectedValue.ToString();
                                cmd.Parameters.Add("@contract_id", SqlDbType.Int).Value = lblContractId.Text;
                                cmd.Parameters.Add("@contract_link", SqlDbType.NVarChar).Value = txtContractLink.Text;
                                cmd.Parameters.Add("@form_link", SqlDbType.NVarChar).Value = insertPath; // đường dẫn file vừa chọn
                                cmd.Parameters.Add("@signed_date", SqlDbType.Date).Value = dtpSignedDate.Value;
                                cmd.Parameters.Add("@signed_address", SqlDbType.NVarChar).Value = txtSignedAddress.Text;
                                cmd.Parameters.Add("@b_full_name", SqlDbType.NVarChar, 50).Value = txtFullname.Text;
                                cmd.Parameters.Add("@b_representative", SqlDbType.NVarChar, 50).Value = txtRepresentative.Text;
                                cmd.Parameters.Add("@b_phone", SqlDbType.VarChar, 13).Value = txtPhone.Text.Replace(" ", "");
                                cmd.Parameters.Add("@b_address", SqlDbType.NVarChar).Value = txtAddress.Text;
                                cmd.Parameters.Add("@b_tax_num", SqlDbType.VarChar, 18).Value = txtTaxNum.Text.Replace(" ", "");
                                cmd.Parameters.Add("@b_bank_account", SqlDbType.NVarChar, 50).Value = txtBankAccount.Text;
                                cmd.Parameters.Add("@b_bank_num", SqlDbType.VarChar, 18).Value = txtAccountNum.Text.Replace(" ", "");
                                cmd.Parameters.Add("@b_bank_code", SqlDbType.NVarChar, 50).Value = cmbBank.Text;
                                cmd.Parameters.Add("@b_bank_branch", SqlDbType.NVarChar, 50).Value = cmbBankBranch.Text;
                                cmd.ExecuteNonQuery();

                                XtraMessageBox.Show("LƯU THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                // Copy file vào folder forms khi insert thành công
                                File.Copy(txtFormLink.Text, Path.Combine(formFolder, fileName));

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_ContractForm.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;

                        // Show data grid view
                        //grctrContractForm.DataSource = null;
                        ShowFullInfo();
                        ShowFirstInfo();

                        lblRowCount.Text = grvContractForm.RowCount.ToString();
                        lblContractId.Visible = true;
                    }

                    
                }
            }
        }

        private void grvContractForm_FocusedRowLoaded(object sender, DevExpress.XtraGrid.Views.Base.RowEventArgs e)
        {
            // no code
        }

        private void grvContractForm_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvContractForm.RowCount == 0)
            {
                //Nếu grid view ko có dữ liệu thì ko hiện lên textbox để tránh lỗi "Object reference not set to an instance of an object"
                cmbStudioId.SelectedIndex = 0;
                cmbStatus.SelectedIndex = 0;
                cmbBank.SelectedIndex = 0;
                cmbBankBranch.SelectedIndex = 0;

                txtSignedAddress.ResetText();
                txtContractLink.ResetText();
                txtFormLink.ResetText();

                dtpSignedDate.Value = DateTime.Today;
                dtpValidDate.Value = DateTime.Today;
                dtpExpireDate.Value = DateTime.Today;

                txtFullname.ResetText();
                txtRepresentative.ResetText();
                txtAddress.ResetText();
                txtPhone.ResetText();
                txtTaxNum.ResetText();
                txtBankAccount.ResetText();
                txtAccountNum.ResetText();

                lblContractId.Visible = false;
                return;
            }
            else
            {
                int focused_id = grvContractForm.FocusedRowHandle;
                lblContractId.Visible = true;
                lblContractId.Text = grvContractForm.GetFocusedRowCellValue("contract_id").ToString();

                cmbStatus.SelectedValue = grvContractForm.GetFocusedRowCellValue("status");
                cmbBank.Text = grvContractForm.GetFocusedRowCellValue("b_bank_code").ToString();
                cmbBankBranch.Text = grvContractForm.GetFocusedRowCellValue("b_bank_branch").ToString();

                txtSignedAddress.Text = grvContractForm.GetFocusedRowCellValue("signed_address").ToString();
                txtContractLink.Text = grvContractForm.GetFocusedRowCellValue("contract_link").ToString();
                txtFormLink.Text = grvContractForm.GetFocusedRowCellValue("form_link").ToString();
                txtFullname.Text = grvContractForm.GetFocusedRowCellValue("b_full_name").ToString();
                txtRepresentative.Text = grvContractForm.GetFocusedRowCellValue("b_representative").ToString();
                txtAddress.Text = grvContractForm.GetFocusedRowCellValue("b_address").ToString();
                txtPhone.Text = grvContractForm.GetFocusedRowCellValue("b_phone").ToString();
                txtTaxNum.Text = grvContractForm.GetFocusedRowCellValue("b_tax_num").ToString();
                txtBankAccount.Text = grvContractForm.GetFocusedRowCellValue("b_bank_account").ToString();
                txtAccountNum.Text = grvContractForm.GetFocusedRowCellValue("b_bank_num").ToString();

                dtpValidDate.Value = DateTime.Parse(grvContractForm.GetRowCellValue(focused_id, "valid_date").ToString());
                dtpExpireDate.Value = DateTime.Parse(grvContractForm.GetRowCellValue(focused_id, "expire_date").ToString());
                dtpSignedDate.Value = DateTime.Parse(grvContractForm.GetRowCellValue(focused_id, "signed_date").ToString());
            }

            btn_flag = null;
        }

        private void label40_Click(object sender, EventArgs e)
        {
            // no code
        }

        // Hàm thay giá trị vào biến trong file Word
        private void ReplaceWord(string oldString, string newString, Word.Document wordDocument)
        {
            var range = wordDocument.Content;
            range.Find.ClearFormatting();
            range.Find.Execute(FindText: oldString, ReplaceWith: newString);
        }

        private void btnExportContract_Click(object sender, EventArgs e)
        {
            if (lblContractId.Text == "Contract_Id")
            {
                XtraMessageBox.Show("Chọn một mã hợp đồng để xuất file .docx", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xác nhận xuất file hợp đồng sơ bộ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        // Khai báo biến giá trị truyền vào file Word
                        //2019-03-02
                        var contract_id = lblContractId.Text;
                        var signed_address = txtSignedAddress.Text;
                        var signed_day = dtpSignedDate.Text.Substring(8, 2);
                        var signed_month = dtpSignedDate.Text.Substring(5, 2);
                        var signed_year = dtpSignedDate.Text.Substring(0, 4);
                        var valid_day = dtpValidDate.Text.Substring(8, 2) + "/" + dtpValidDate.Text.Substring(5, 2) + "/" + dtpValidDate.Text.Substring(0, 4);
                        var expire_day = dtpExpireDate.Text.Substring(8, 2) + "/" + dtpExpireDate.Text.Substring(5, 2) + "/" + dtpExpireDate.Text.Substring(0, 4);
                        var b_full_name = txtFullname.Text;
                        var b_representative = txtRepresentative.Text;
                        var b_address = txtAddress.Text;
                        var b_phone = txtPhone.Text;
                        var b_tax_num = txtTaxNum.Text;
                        var b_bank_account = txtBankAccount.Text;
                        var b_account_num = txtAccountNum.Text;
                        var b_bank_code = cmbBank.Text;
                        var b_bank_branch = cmbBankBranch.Text;

                        CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");
                        var wordApp = new Word.Application();
                        var wordDocument = wordApp.Documents.Open(Temp);

                        ReplaceWord("${{contract_id}}", contract_id, wordDocument);
                        ReplaceWord("${{contract_id}}", contract_id, wordDocument);
                        ReplaceWord("${{signed_day}}", signed_day, wordDocument);
                        ReplaceWord("${{signed_month}}", signed_month, wordDocument);
                        ReplaceWord("${{signed_year}}", signed_year, wordDocument);
                        ReplaceWord("${{signed_address}}", signed_address, wordDocument);
                        ReplaceWord("${{b_full_name}}", b_full_name, wordDocument);
                        ReplaceWord("${{b_representative}}", b_representative, wordDocument);
                        ReplaceWord("${{b_address}}", b_address, wordDocument);
                        ReplaceWord("${{b_phone}}", b_phone, wordDocument);
                        ReplaceWord("${{b_tax_num}}", b_tax_num, wordDocument);
                        ReplaceWord("${{b_account_num}}", b_account_num, wordDocument);
                        ReplaceWord("${{b_bank_code}}", b_bank_code, wordDocument);
                        ReplaceWord("${{b_bank_branch}}", b_bank_branch, wordDocument);
                        ReplaceWord("${{valid_date}}", valid_day, wordDocument);
                        ReplaceWord("${{expire_date}}", expire_day, wordDocument);
                        ReplaceWord("${{b_bank_account}}", b_bank_account, wordDocument);
                        ReplaceWord("${{b_account_num}}", b_account_num, wordDocument);
                        ReplaceWord("${{b_bank_code}}", b_bank_code, wordDocument);
                        ReplaceWord("${{b_bank_branch}}", b_bank_branch, wordDocument);
                        ReplaceWord("${{b_representative}}", b_representative, wordDocument);

                        //Đường dẫn chứa file kết quả và lưu files
                        // tên file: HD_tag_contract_id_yyyymmdd: HD_FIT24TT_2020050 (ngày xuất file)
                        string output = "/_templates/exported_files/HD_" + lblTag.Text.Replace(" ", "").Replace("-", "_") + "_" + lblContractId.Text + "_" + DateTime.Now.Year.ToString() + "_" + DateTime.Now.Month.ToString() + "_" + DateTime.Now.Day.ToString() + ".docx";
                        wordDocument.SaveAs2(Application.StartupPath + output);
                        wordApp.Documents.Open(Application.StartupPath + output);

                        XtraMessageBox.Show("TẠO THÀNH CÔNG! \n \n - Vị trí file: \n" + Application.StartupPath + output, "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_ContractForm.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        // none
                    }
                }
                
            }
        }

        private void btnBrowsePDF_Click(object sender, EventArgs e)
        {
            btn_flag = "browse";

            if (radContract.Checked == false && radForm.Checked == false)
            {
                XtraMessageBox.Show("Chọn kiểu tài liệu cần tải lên", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (lblContractId.Text == "Contract_Id")
            {
                XtraMessageBox.Show("Chọn một đối tác cụ thể", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (radContract.Checked == true)
            {
                // Chọn file
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "PDF files (*.pdf)|*.pdf";
                ofd.FilterIndex = 1;
                ofd.RestoreDirectory = true; //Lấy giá trị path mở file
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    txtContractLink.Text = ofd.FileName; // HIển thị đường dẫn lên textbox (CONTRACT)

                    string path = ofd.FileName;

                    //Mở form PDF viewer
                    popform_PDFviewer frm = new popform_PDFviewer(path);
                    frm.ShowDialog();
                }
            }
            else if (radForm.Checked == true)
            {
                // Chọn file
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "PDF files (*.pdf)|*.pdf";
                ofd.FilterIndex = 1;
                ofd.RestoreDirectory = true; //Lấy giá trị path mở file
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    txtFormLink.Text = ofd.FileName; // HIển thị đường dẫn lên textbox (FORM)

                    string path = ofd.FileName;

                    // Mở form PDF Viewer
                    popform_PDFviewer frm = new popform_PDFviewer(path);
                    frm.ShowDialog();
                }
            }
            else
            {
                return;
            }
        }

        private void btnDeletePDF_Click(object sender, EventArgs e)
        {
            if (radContract.Checked == false && radForm.Checked == false)
            {
                XtraMessageBox.Show("Chọn kiểu tài liệu cần xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (radContract.Checked == true)
            {
                if (XtraMessageBox.Show("Xóa tài liệu hợp đồng '" + txtContractLink.Text +  "' ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    // Xóa trong CSDL
                    try
                    {
                        conn.Open();
                        // Lưu giá trị vào CSDL
                        cmd = new SqlCommand("b_update_tb_contract", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudioId.SelectedValue.ToString());
                        cmd.Parameters.Add("@valid_date", SqlDbType.Date).Value = dtpValidDate.Value;
                        cmd.Parameters.Add("@expire_date", SqlDbType.Date).Value = dtpExpireDate.Value;
                        cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = cmbStatus.SelectedValue.ToString();
                        cmd.Parameters.Add("@contract_id", SqlDbType.Int).Value = lblContractId.Text;
                        cmd.Parameters.Add("@contract_link", SqlDbType.NVarChar).Value = ""; // Xóa
                        cmd.Parameters.Add("@form_link", SqlDbType.NVarChar).Value = txtFormLink.Text; 
                        cmd.Parameters.Add("@signed_date", SqlDbType.Date).Value = dtpSignedDate.Value;
                        cmd.Parameters.Add("@signed_address", SqlDbType.NVarChar).Value = txtSignedAddress.Text;
                        cmd.Parameters.Add("@b_full_name", SqlDbType.NVarChar, 50).Value = txtFullname.Text;
                        cmd.Parameters.Add("@b_representative", SqlDbType.NVarChar, 50).Value = txtRepresentative.Text;
                        cmd.Parameters.Add("@b_phone", SqlDbType.VarChar, 13).Value = txtPhone.Text.Replace(" ", "");
                        cmd.Parameters.Add("@b_address", SqlDbType.NVarChar).Value = txtAddress.Text;
                        cmd.Parameters.Add("@b_tax_num", SqlDbType.VarChar, 18).Value = txtTaxNum.Text.Replace(" ", "");
                        cmd.Parameters.Add("@b_bank_account", SqlDbType.NVarChar, 50).Value = txtBankAccount.Text;
                        cmd.Parameters.Add("@b_bank_num", SqlDbType.VarChar, 18).Value = txtAccountNum.Text.Replace(" ", "");
                        cmd.Parameters.Add("@b_bank_code", SqlDbType.NVarChar, 50).Value = cmbBank.Text;
                        cmd.Parameters.Add("@b_bank_branch", SqlDbType.NVarChar, 50).Value = cmbBankBranch.Text;
                        cmd.ExecuteNonQuery();

                        // Xóa trong folder
                        File.Delete(Path.Combine(Application.StartupPath, txtContractLink.Text));

                        XtraMessageBox.Show("Xóa thành công!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_ContractForm.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;

                        // Show data grid view
                        //grctrContractForm.DataSource = null;
                        ShowFullInfo();
                        ShowFirstInfo();

                        lblRowCount.Text = grvContractForm.RowCount.ToString();
                        lblContractId.Visible = true;
                    }

                   
                }
            }
            else if (radForm.Checked == true)
            {
                if (XtraMessageBox.Show("Xóa tài liệu biểu mẫu '" + txtFormLink.Text + "' ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    // Xóa trong CSDL
                    try
                    {
                        conn.Open();
                        // Lưu giá trị vào CSDL
                        cmd = new SqlCommand("b_update_tb_contract", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudioId.SelectedValue.ToString());
                        cmd.Parameters.Add("@valid_date", SqlDbType.Date).Value = dtpValidDate.Value;
                        cmd.Parameters.Add("@expire_date", SqlDbType.Date).Value = dtpExpireDate.Value;
                        cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = cmbStatus.SelectedValue.ToString();
                        cmd.Parameters.Add("@contract_id", SqlDbType.Int).Value = lblContractId.Text;
                        cmd.Parameters.Add("@contract_link", SqlDbType.NVarChar).Value = txtContractLink.Text; 
                        cmd.Parameters.Add("@form_link", SqlDbType.NVarChar).Value = ""; // Xóa
                        cmd.Parameters.Add("@signed_date", SqlDbType.Date).Value = dtpSignedDate.Value;
                        cmd.Parameters.Add("@signed_address", SqlDbType.NVarChar).Value = txtSignedAddress.Text;
                        cmd.Parameters.Add("@b_full_name", SqlDbType.NVarChar, 50).Value = txtFullname.Text;
                        cmd.Parameters.Add("@b_representative", SqlDbType.NVarChar, 50).Value = txtRepresentative.Text;
                        cmd.Parameters.Add("@b_phone", SqlDbType.VarChar, 13).Value = txtPhone.Text.Replace(" ", "");
                        cmd.Parameters.Add("@b_address", SqlDbType.NVarChar).Value = txtAddress.Text;
                        cmd.Parameters.Add("@b_tax_num", SqlDbType.VarChar, 18).Value = txtTaxNum.Text.Replace(" ", "");
                        cmd.Parameters.Add("@b_bank_account", SqlDbType.NVarChar, 50).Value = txtBankAccount.Text;
                        cmd.Parameters.Add("@b_bank_num", SqlDbType.VarChar, 18).Value = txtAccountNum.Text.Replace(" ", "");
                        cmd.Parameters.Add("@b_bank_code", SqlDbType.NVarChar, 50).Value = cmbBank.Text;
                        cmd.Parameters.Add("@b_bank_branch", SqlDbType.NVarChar, 50).Value = cmbBankBranch.Text;
                        cmd.ExecuteNonQuery();

                        // Xóa trong folder
                        File.Delete(Path.Combine(Application.StartupPath, txtFormLink.Text));

                        XtraMessageBox.Show("Xóa thành công!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_lists_ContractForm.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;

                        // Show data grid view
                        //grctrContractForm.DataSource = null;
                        ShowFullInfo();
                        ShowFirstInfo();

                        lblRowCount.Text = grvContractForm.RowCount.ToString();
                        lblContractId.Visible = true;
                    }


                }
            }
            else
            {
                return;
            }
        }

        private void btnContractImage_Click(object sender, EventArgs e)
        {
            //none
        }

        private void btnFormImage_Click(object sender, EventArgs e)
        {
            //none
        }

        private void btnShowFile_Click(object sender, EventArgs e)
        {
            if (lblContractId.Text == "Contract_Id")
            {
                XtraMessageBox.Show("Chọn một bản ghi để xem", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (radContract.Checked == false && radForm.Checked == false)
            {
                XtraMessageBox.Show("Chọn kiểu tài liệu cần xem", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (radContract.Checked == true)
            {
                if (txtContractLink.Text == "")
                {
                    XtraMessageBox.Show("Tài liệu không tồn tại!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string path = Path.Combine(Application.StartupPath, txtContractLink.Text);
                    popform_PDFviewer frm = new popform_PDFviewer(path);
                    frm.ShowDialog();
                }
            }
            else if (radForm.Checked == true)
            {
                if (txtFormLink.Text == "")
                {
                    XtraMessageBox.Show("Tài liệu không tồn tại!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    string path = Path.Combine(Application.StartupPath, txtFormLink.Text);
                    popform_PDFviewer frm = new popform_PDFviewer(path);
                    frm.ShowDialog();
                }
            }
            else
            {
                return;
            }
        }
    }
}
