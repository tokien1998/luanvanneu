﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using DevExpress.XtraEditors;
using System.IO;
using ExcelDataReader;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_tools_ImportReservation : UserControl
    {
        // Khai báo biến
        private static usctr_tools_ImportReservation usctr; // User control

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();
        DataSet ds;

        // Thuộc tính
        public static usctr_tools_ImportReservation UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_tools_ImportReservation();
                }
                return usctr;
            }
        }

        public usctr_tools_ImportReservation()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "All|*.*|Excel Workbook|*.xlsx|Excel Workbook 97-2003|*.xls|CSV (Comma delimited)|*.csv";
            ofd.FilterIndex = 1;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var file = File.Open(ofd.FileName, FileMode.Open, FileAccess.Read);
                    IExcelDataReader reader;
                    if (ofd.FilterIndex == 3)
                    {
                        reader = ExcelReaderFactory.CreateBinaryReader(file);
                    }
                    else if (ofd.FilterIndex == 4)
                    {
                        reader = ExcelReaderFactory.CreateCsvReader(file);
                    }
                    else
                    {
                        reader = ExcelReaderFactory.CreateOpenXmlReader(file);
                    }

                    ds = reader.AsDataSet(new ExcelDataSetConfiguration()
                    {
                        ConfigureDataTable = (_) => new ExcelDataTableConfiguration()
                        {
                            UseHeaderRow = true
                        }
                    }
                    );

                    cmbSheet.Items.Clear();
                    foreach (DataTable dt in ds.Tables)
                    {
                        cmbSheet.Items.Add(dt.TableName);
                    }
                    reader.Close();
                    cmbSheet.Text = "--Chọn trang tính--";
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show("Mở file thất bại: File đang được mở bởi một ứng dụng thứ 3! \n \n \n Lỗi: " + ex.Message, "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void btnImport_Click(object sender, EventArgs e)
        {
            //limit 16 fields
            int error = 0;
            int success = 0;
            string err_info = "";
            if (grvPreview.Columns.Count != 16)
            {
                XtraMessageBox.Show("Nội dung trang tính không khớp số cột với cơ sở dữ liệu hệ thống: \n - Số cột yêu cầu: 16 \n - Số cột trong trang tính: " + grvPreview.Columns.Count.ToString(), "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xác nhận nạp dữ liệu hiện tại vào hệ thống?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    for (int i = 0; i < grvPreview.RowCount; i++)
                    {
                        try
                        {
                            conn.Open();
                            cmd = new SqlCommand("a_insert_tb_reservation_log", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@res_id", SqlDbType.BigInt).Value = int.Parse(grvPreview.GetRowCellValue(i, "res_id").ToString());
                            cmd.Parameters.Add("@city_name", SqlDbType.NVarChar, 50).Value = grvPreview.GetRowCellValue(i, "city_name").ToString();
                            cmd.Parameters.Add("@district_name", SqlDbType.NVarChar, 50).Value = grvPreview.GetRowCellValue(i, "district_name").ToString();
                            cmd.Parameters.Add("@user_located", SqlDbType.NVarChar, 50).Value = grvPreview.GetRowCellValue(i, "user_located").ToString();
                            cmd.Parameters.Add("@time_sch", SqlDbType.VarChar, 14).Value = grvPreview.GetRowCellValue(i, "time_sch").ToString();
                            cmd.Parameters.Add("@dtime_created", SqlDbType.VarChar, 14).Value = grvPreview.GetRowCellValue(i, "dtime_created").ToString();
                            cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = int.Parse(grvPreview.GetRowCellValue(i, "user_id").ToString());
                            cmd.Parameters.Add("@user_name", SqlDbType.NVarChar, 50).Value = grvPreview.GetRowCellValue(i, "user_name").ToString();
                            cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(grvPreview.GetRowCellValue(i, "studio_id").ToString());
                            cmd.Parameters.Add("@studio_name", SqlDbType.NVarChar, 50).Value = grvPreview.GetRowCellValue(i, "studio_name").ToString();
                            cmd.Parameters.Add("@course_id", SqlDbType.Int).Value = int.Parse(grvPreview.GetRowCellValue(i, "course_id").ToString());
                            cmd.Parameters.Add("@course_name", SqlDbType.NVarChar, 50).Value = grvPreview.GetRowCellValue(i, "course_name").ToString();
                            cmd.Parameters.Add("@service_type", SqlDbType.VarChar, 50).Value = grvPreview.GetRowCellValue(i, "service_type").ToString();
                            cmd.Parameters.Add("@category", SqlDbType.NVarChar, 50).Value = grvPreview.GetRowCellValue(i, "category").ToString();
                            cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = grvPreview.GetRowCellValue(i, "status").ToString();
                            cmd.Parameters.Add("@checkin_code", SqlDbType.VarChar, 50).Value = grvPreview.GetRowCellValue(i, "checkin_code").ToString();
                            cmd.ExecuteNonQuery();
                            success += 1;
                           
                        }
                        catch (Exception ex)
                        {
                            err_info = "";
                            //XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_tools_ImportReservation.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            error += 1;
                            //break; // Close the loop
                            err_info = ex.Message;
                        }
                        finally
                        {
                            conn.Close();
                        }
                       
                    }

                    if (err_info == "")
                    {
                        err_info = "Không";
                    }

                    XtraMessageBox.Show("Kết quả nạp dữ liệu: \n - Số bản ghi nạp: " + (success + error) + " \n - Số bản ghi nạp thành công: " + success + " \n - Số bản ghi nạp lỗi: " + error + "\n \n \n Lỗi: " + err_info, "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void usctr_tools_ImportReservation_Load(object sender, EventArgs e)
        {
            grctrPreview.DataSource = null;
            grvPreview.BestFitColumns();
            lblRowCount.Text = grvPreview.RowCount.ToString();
        }

        private void cmbSheet_SelectedIndexChanged(object sender, EventArgs e)
        {
            grctrPreview.DataSource = ds.Tables[cmbSheet.SelectedIndex];
            grvPreview.BestFitColumns();
            lblRowCount.Text = grvPreview.RowCount.ToString();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            grctrPreview.DataSource = null;
            cmbSheet.Items.Clear();
            lblRowCount.Text = grvPreview.RowCount.ToString();
        }
    }
}
