﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_system_Employee : UserControl
    {
        // Khai báo biến
        private static usctr_system_Employee usctr; // User control

        string btn_flag = null;

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_system_Employee UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_system_Employee();
                }
                return usctr;
            }
        }

        public usctr_system_Employee()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void ComboboxStatus()
        {
            cmbStatus.Items.Clear();
            cmbStatus.Items.Add("--Chọn--");
            cmbStatus.Items.Add("Hoạt động");
            cmbStatus.Items.Add("Vô hiệu hóa");
            cmbStatus.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void DefaultFormat()
        {
            //lblEmpId.Text = "";
            cmbStatus.Enabled = false;
            cmbStatus.SelectedItem = "--Chọn--";
            txtEmpName.Enabled = false;
            txtEmpName.ReadOnly = true;
            txtEmail.Enabled = false;
            txtEmail.ReadOnly = true;
            txtPosition.Enabled = false;
            txtPosition.ReadOnly = true;

        }

        private void SetCaption()
        {
            string[] columnCaptions = { "Mã nhân viên", "Tên nhân viên", "Email", "Vị trí", "Trạng thái" };
            power.SetCaption(grvEmp, columnCaptions);
        }

        private void ShowFullInfo()
        {
            string tableName = "employee";
            string selectedColumns = "emp_id, emp_name, emp_email, position, status";
            string afterWhereStr = " emp_name NOT LIKE N'%admin%' and emp_name NOT LIKE N'%IT%' ORDER BY emp_id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrEmp.DataSource = dt;
            SetCaption();
        }

        private void ShowFirstInfo()
        {
            if (grvEmp.RowCount == 0)
            {
                lblEmpId.Text = "";
                cmbStatus.SelectedItem = "--Chọn--";
                txtEmpName.ResetText();
                txtEmail.ResetText();
                txtPosition.ResetText();
            }
            else
            {
                lblEmpId.Text = grvEmp.GetRowCellValue(0, "emp_id").ToString();
                txtEmpName.Text = grvEmp.GetRowCellValue(0, "emp_name").ToString();
                txtEmail.Text = grvEmp.GetRowCellValue(0, "emp_email").ToString();
                txtPosition.Text = grvEmp.GetRowCellValue(0, "position").ToString();
                cmbStatus.SelectedItem = grvEmp.GetRowCellValue(0, "status").ToString();
            }
        }

        private void grvEmp_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvEmp.RowCount == 0)
            {
                lblEmpId.Text = "";
                cmbStatus.SelectedItem = "--Chọn--";
                txtEmpName.ResetText();
                txtEmail.ResetText();
                txtPosition.ResetText();
            }
            else
            {
                lblEmpId.Text = grvEmp.GetFocusedRowCellValue("emp_id").ToString();
                txtEmpName.Text = grvEmp.GetFocusedRowCellValue("emp_name").ToString();
                txtEmail.Text = grvEmp.GetFocusedRowCellValue("emp_email").ToString();
                txtPosition.Text = grvEmp.GetFocusedRowCellValue("position").ToString();
                cmbStatus.SelectedItem = grvEmp.GetFocusedRowCellValue("status").ToString();
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";
            cmbStatus.Enabled = false;
            cmbStatus.SelectedItem = "Hoạt động";
            txtEmpName.Enabled = true;
            txtEmpName.ReadOnly = false;
            txtEmpName.ResetText();
            txtEmail.Enabled = true;
            txtEmail.ReadOnly = false;
            txtEmail.ResetText();
            txtPosition.Enabled = true;
            txtPosition.ReadOnly = false;
            txtPosition.ResetText();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (lblEmpId.Text == "emp_id" || lblEmpId.Text == "")
            {
                XtraMessageBox.Show("Chọn nhân viên muốn sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                btn_flag = "edit";
                cmbStatus.Enabled = true;
                txtEmpName.Enabled = true;
                txtEmpName.ReadOnly = false;
                txtEmail.Enabled = true;
                txtEmail.ReadOnly = false;
                txtPosition.Enabled = true;
                txtPosition.ReadOnly = false;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lblEmpId.Text == "" || lblEmpId.Text == "emp_id")
            {
                XtraMessageBox.Show("Chọn nhân viên muốn xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa nhân viên '" + txtEmpName.Text + "' ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM employee WHERE emp_id = " + int.Parse(lblEmpId.Text), conn);
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_system_Employee (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        usctr_system_Employee_Load(sender, e); // F5
                        btn_flag = null;
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_system_Employee_Load(sender, e);  
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (txtEmpName.Text == "" || txtEmail.Text == "" || txtPosition.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng nhập đủ thông tin của nhân viên", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (txtEmail.Text.Contains("@") == false)
                {
                    XtraMessageBox.Show("Email nhập sai định dạng", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        // Khai báo các cột và values cần check duplicate
                        string[] col_names = { "emp_email" };
                        string[] col_values = { txtEmail.Text };

                        //Check trùng
                        if (power.Sql_CheckDuplicate("employee", col_names, col_values) == false)
                        {
                            conn.Open();
                            cmd = new SqlCommand("a_insert_tb_employee", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@emp_name", SqlDbType.NVarChar, 50).Value = txtEmpName.Text;
                            cmd.Parameters.Add("@emp_email", SqlDbType.NVarChar, 50).Value = txtEmail.Text;
                            cmd.Parameters.Add("@position", SqlDbType.NVarChar, 50).Value = txtPosition.Text;
                            cmd.ExecuteNonQuery();

                            XtraMessageBox.Show("THÊM THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_system_Employee (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        usctr_system_Employee_Load(sender, e);
                        btn_flag = null;
                    }
                }
            }
            
            if (btn_flag == "edit")
            {
                if (txtEmpName.Text == "" || txtEmail.Text == "" || txtPosition.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng nhập đủ thông tin của nhân viên", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (txtEmail.Text.Contains("@") == false)
                {
                    XtraMessageBox.Show("Email nhập sai định dạng", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {

                        conn.Open();
                        cmd = new SqlCommand("b_update_tb_employee", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@emp_name", SqlDbType.NVarChar, 50).Value = txtEmpName.Text;
                        cmd.Parameters.Add("@emp_email", SqlDbType.NVarChar, 50).Value = txtEmail.Text;
                        cmd.Parameters.Add("@position", SqlDbType.NVarChar, 50).Value = txtPosition.Text;
                        cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = cmbStatus.SelectedItem.ToString();
                        cmd.Parameters.Add("@emp_id", SqlDbType.Int).Value = int.Parse(lblEmpId.Text);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_system_Employee (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        usctr_system_Employee_Load(sender, e);
                        btn_flag = null;
                    }
                }
            }
        }

        private void usctr_system_Employee_Load(object sender, EventArgs e)
        {
            ComboboxStatus();
            btn_flag = null;
            grctrEmp.DataSource = null;
            ShowFullInfo();
            DefaultFormat();
            ShowFirstInfo();
            lblRowCount.Text = grvEmp.RowCount.ToString();
        }

        private void btnEmpRole_Click(object sender, EventArgs e)
        {
            //
        }

        private void btnAccount_Click(object sender, EventArgs e)
        {
            int emp_id = int.Parse(lblEmpId.Text);
            string emp_name = txtEmpName.Text;
            popform_system_Account frm = new popform_system_Account(emp_id, emp_name);
            frm.ShowDialog();

        }
    }
}
