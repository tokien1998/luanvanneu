﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_system_EmpRole : UserControl
    {
        // Khai báo biến
        private static usctr_system_EmpRole usctr; // User control
        string btn_flag = null;

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_system_EmpRole UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_system_EmpRole();
                }
                return usctr;
            }
        }

        public usctr_system_EmpRole()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void ComboboxStatus()
        {
            cmbStatus.Items.Clear();
            cmbStatus.Items.Add("--Chọn--");
            cmbStatus.Items.Add("Hoạt động");
            cmbStatus.Items.Add("Vô hiệu hóa");
            cmbStatus.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void ComboboxRole()
        {
            string query = "SELECT 0 as role_id, N'--Chọn--' as role_code UNION ALL SELECT role_id, role_code FROM role ORDER BY role_id";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbRole.DisplayMember = "role_code";
            cmbRole.ValueMember = "role_id";
            cmbRole.DataSource = dt;
            cmbRole.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void ComboboxEmp()
        {
            string query = "SELECT 0 as emp_id, N'--Chọn--' as emp_name UNION ALL SELECT emp_id, emp_name FROM employee WHERE emp_name NOT LIKE N'%admin%' and emp_name NOT LIKE N'%IT%' ORDER BY emp_id";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbEmp.DisplayMember = "emp_name";
            cmbEmp.ValueMember = "emp_id";
            cmbEmp.DataSource = dt;
            cmbEmp.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void DefaultFormat()
        {
            cmbRole.Enabled = false;
            cmbStatus.Enabled = false;
            cmbRole.SelectedIndex = 0;
            cmbStatus.SelectedItem = "--Chọn--";
            lblID.Text = "";
            cmbEmp.Enabled = true;
            txtComment.Enabled = false;
            txtComment.ReadOnly = true;
            
        }

        private void SetCaption()
        {
            string[] columnCaptions = { "Mã bản ghi", "Mã quyền", "Tên quyền", "Mô tả", "Trạng thái" };
            power.SetCaption(grvEmpRole, columnCaptions);
        }

        private void ShowFullInfo()
        {
            string tableName = "(SELECT e.id, e.role_id, r.role_code, r.comment, e.status FROM emp_role e LEFT JOIN role r ON (e.role_id = r.role_id) WHERE e.emp_id = " + int.Parse(cmbEmp.SelectedValue.ToString()) + ") a";
            string selectedColumns = "id, role_id, role_code, comment, status";
            string afterWhereStr = "(1=1) ORDER BY id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrEmpRole.DataSource = dt;
            SetCaption();
        }

        private void ShowFirstInfo()
        {
            if (grvEmpRole.RowCount == 0)
            {
                lblID.Text = "";
                cmbStatus.Enabled = false;
                cmbStatus.SelectedItem = "--Chọn--";
                cmbRole.Enabled = false;
                cmbRole.SelectedIndex = 0;
                cmbEmp.Enabled = true;
                txtComment.ResetText();
            }
            else
            {
                lblID.Text = grvEmpRole.GetRowCellValue(0, "id").ToString();
                cmbRole.SelectedValue = grvEmpRole.GetRowCellValue(0, "role_id").ToString();
                cmbStatus.SelectedItem = grvEmpRole.GetRowCellValue(0, "status");
                txtComment.Text = grvEmpRole.GetRowCellValue(0, "comment").ToString();
            }
        }

        private void grvEmpRole_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvEmpRole.RowCount == 0)
            {
                lblID.Text = "";
                cmbStatus.Enabled = false;
                cmbStatus.SelectedItem = "--Chọn--";
                cmbRole.Enabled = false;
                cmbRole.SelectedIndex = 0;
                cmbEmp.Enabled = true;
                txtComment.ResetText();
            }
            else
            {
                lblID.Text = grvEmpRole.GetFocusedRowCellValue("id").ToString();
                cmbRole.SelectedValue = grvEmpRole.GetFocusedRowCellValue("role_id").ToString();
                cmbStatus.SelectedItem = grvEmpRole.GetFocusedRowCellValue("status");
                txtComment.Text = grvEmpRole.GetFocusedRowCellValue("comment").ToString();
            }
        }

        private void usctr_system_EmpRole_Load(object sender, EventArgs e)
        {
            ComboboxRole();
            ComboboxStatus();
            ComboboxEmp();
            btn_flag = null;
            grctrEmpRole.DataSource = null;
            ShowFullInfo();
            DefaultFormat();
            ShowFirstInfo();
            lblRowCount.Text = grvEmpRole.RowCount.ToString();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (cmbEmp.Text == "--Chọn--" || cmbEmp.Text == "")
            {
                XtraMessageBox.Show("Chọn nhân viên cần phân quyền", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                btn_flag = "create";
                cmbRole.Enabled = true;
                cmbRole.SelectedIndex = 0;
                cmbStatus.Enabled = true;
                cmbStatus.SelectedItem = "--Chọn--";
                cmbEmp.Enabled = false;
                txtComment.Enabled = false;
                txtComment.ReadOnly = true;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (lblID.Text == "" || lblID.Text == "id")
            {
                XtraMessageBox.Show("Chọn bản ghi để chỉnh sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                btn_flag = "edit";
                cmbRole.Enabled = true;
                cmbStatus.Enabled = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lblID.Text == "" || lblID.Text == "id")
            {
                XtraMessageBox.Show("Chọn bản ghi để xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa quyền '" + cmbRole.Text + "' của nhân viên " + cmbEmp.Text + " ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM emp_role WHERE id = " + int.Parse(lblID.Text), conn);
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_system_EmpRole (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        usctr_system_EmpRole_Load(sender, e); // F5
                        btn_flag = null;
                        cmbEmp.Enabled = true;
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_system_EmpRole_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (cmbRole.Text == "")
                {
                    XtraMessageBox.Show("Chọn quyền để ra hạn", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("a_insert_tb_emp_role", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@emp_id", SqlDbType.Int).Value = int.Parse(cmbEmp.SelectedValue.ToString());
                        cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = int.Parse(cmbRole.SelectedValue.ToString());
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("THÊM THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_system_EmpRole (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        ShowFullInfo();
                        //DefaultFormat();
                        btn_flag = null;
                        cmbEmp.Enabled = true;
                    }
                }
            }

            if (btn_flag == "edit")
            {
                if (cmbRole.Text == "")
                {
                    XtraMessageBox.Show("Chọn quyền để ra hạn", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("b_update_tb_emp_role", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@emp_id", SqlDbType.Int).Value = int.Parse(cmbEmp.SelectedValue.ToString());
                        cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = int.Parse(cmbRole.SelectedValue.ToString());
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = int.Parse(lblID.Text);
                        cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = cmbStatus.SelectedItem.ToString();
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_system_EmpRole (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        ShowFullInfo();
                        //DefaultFormat();
                        btn_flag = null;
                        cmbEmp.Enabled = true;
                    }
                }
            }
        }

        private void cmbEmp_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowFullInfo();
            ShowFirstInfo();
        }

        private void cmbRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "SELECT comment FROM role WHERE role_id = " + int.Parse(cmbRole.SelectedValue.ToString());
            conn.Open();
            cmd = new SqlCommand(query, conn);
            var result = cmd.ExecuteScalar();
            if (result == null)
            {
                txtComment.Text = "";
            }
            else
            {
                txtComment.Text = result.ToString();
            }
            conn.Close();
        }
    }
}
