﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_system_Role : UserControl
    {
        // Khai báo biến
        private static usctr_system_Role usctr; // User control
        string btn_flag = null;

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_system_Role UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_system_Role();
                }
                return usctr;
            }
        }

        public usctr_system_Role()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void DefaultFormat()
        {
            //lblEmpId.Text = "";
            txtRoleCode.Enabled = false;
            txtRoleCode.ReadOnly = true;
            txtComment.Enabled = false;
            txtComment.ReadOnly = true;

        }

        private void SetCaption()
        {
            string[] columnCaptions = { "Mã quyền", "Tên quyền", "Mô tả" };
            power.SetCaption(grvRole, columnCaptions);
        }

        private void ShowFullInfo()
        {
            string tableName = "role";
            string selectedColumns = "role_id, role_code, comment";
            string afterWhereStr = "(1=1) ORDER BY role_id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrRole.DataSource = dt;
            SetCaption();
        }

        private void ShowFirstInfo()
        {
            if (grvRole.RowCount == 0)
            {
                lblRoleId.Text = "";
                txtComment.ResetText();
                txtRoleCode.ResetText();
            }
            else
            {
                lblRoleId.Text = grvRole.GetRowCellValue(0, "role_id").ToString();
                txtRoleCode.Text = grvRole.GetRowCellValue(0, "role_code").ToString();
                txtComment.Text = grvRole.GetRowCellValue(0, "comment").ToString();
            }
        }

        private void grvRole_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvRole.RowCount == 0)
            {
                lblRoleId.Text = "";
                txtComment.ResetText();
                txtRoleCode.ResetText();
            }
            else
            {
                lblRoleId.Text = grvRole.GetFocusedRowCellValue("role_id").ToString();
                txtRoleCode.Text = grvRole.GetFocusedRowCellValue("role_code").ToString();
                txtComment.Text = grvRole.GetFocusedRowCellValue("comment").ToString();
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";
            txtComment.Enabled = true;
            txtComment.ReadOnly = false;
            txtComment.ResetText();
            txtRoleCode.Enabled = true;
            txtRoleCode.ReadOnly = false;
            txtRoleCode.ResetText();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (lblRoleId.Text == "" || lblRoleId.Text == "role_id")
            {
                XtraMessageBox.Show("Chọn quyền muốn sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                btn_flag = "edit";
                txtComment.Enabled = true;
                txtComment.ReadOnly = false;
                txtRoleCode.Enabled = true;
                txtRoleCode.ReadOnly = false;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lblRoleId.Text == "" || lblRoleId.Text == "role_id")
            {
                XtraMessageBox.Show("Chọn quyền muốn xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa quyền '" + txtRoleCode.Text + "' ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM role WHERE role_id = " + int.Parse(lblRoleId.Text), conn);
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_system_Role (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        usctr_system_Role_Load(sender, e); // F5
                        btn_flag = null;
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_system_Role_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (txtRoleCode.Text == "" || txtComment.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng nhập đủ thông tin quyền truy cập", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        // Khai báo các cột và values cần check duplicate
                        string[] col_names = { "role_code" };
                        string[] col_values = { txtRoleCode.Text };

                        //Check trùng
                        if (power.Sql_CheckDuplicate("role", col_names, col_values) == false)
                        {
                            conn.Open();
                            cmd = new SqlCommand("a_insert_tb_role", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@role_code", SqlDbType.NVarChar, 50).Value = txtRoleCode.Text;
                            cmd.Parameters.Add("@comment", SqlDbType.NVarChar).Value = txtComment.Text;
                            cmd.ExecuteNonQuery();

                            XtraMessageBox.Show("THÊM THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_system_Role (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        usctr_system_Role_Load(sender, e);
                        btn_flag = null;
                    }
                }
            }

            if (btn_flag == "edit")
            {
                if (txtRoleCode.Text == "" || txtComment.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng nhập đủ thông tin quyền truy cập", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {

                        conn.Open();
                        cmd = new SqlCommand("b_update_tb_role", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@role_code", SqlDbType.NVarChar, 50).Value = txtRoleCode.Text;
                        cmd.Parameters.Add("@comment", SqlDbType.NVarChar).Value = txtComment.Text;
                        cmd.Parameters.Add("@role_id", SqlDbType.Int).Value = int.Parse(lblRoleId.Text);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_system_Role (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        usctr_system_Role_Load(sender, e);
                        btn_flag = null;
                    }
                }
            }
        }

        private void usctr_system_Role_Load(object sender, EventArgs e)
        {
            btn_flag = null;
            grctrRole.DataSource = null;
            ShowFullInfo();
            DefaultFormat();
            ShowFirstInfo();
            lblRowCount.Text = grvRole.RowCount.ToString();
        }
    }
}
