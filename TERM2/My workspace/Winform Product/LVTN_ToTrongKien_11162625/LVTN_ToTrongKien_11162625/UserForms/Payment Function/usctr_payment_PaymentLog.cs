﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_payment_PaymentLog : UserControl
    {
        // Khai báo biến
        private static usctr_payment_PaymentLog usctr; // User control

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_payment_PaymentLog UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_payment_PaymentLog();
                }
                return usctr;
            }
        }

        public usctr_payment_PaymentLog()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void ComboboxYearMonth()
        {
            cmbYear.Items.Clear();
            cmbYear.Items.Add("--Chọn--");
            cmbYear.Items.Add("2020");
            cmbYear.Items.Add("2019");
            cmbYear.Items.Add("2018");
            cmbYear.Items.Add("2017");
            cmbYear.DropDownStyle = ComboBoxStyle.DropDownList;

            cmbMonth.Items.Clear();
            cmbMonth.Items.Add("--Chọn--");
            cmbMonth.Items.Add("1");
            cmbMonth.Items.Add("2");
            cmbMonth.Items.Add("3");
            cmbMonth.Items.Add("4");
            cmbMonth.Items.Add("5");
            cmbMonth.Items.Add("6");
            cmbMonth.Items.Add("7");
            cmbMonth.Items.Add("8");
            cmbMonth.Items.Add("9");
            cmbMonth.Items.Add("10");
            cmbMonth.Items.Add("11");
            cmbMonth.Items.Add("12");
            cmbMonth.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void ComboboxStatus()
        {
            cmbStatus.Items.Clear();
            cmbStatus.Items.Add("--Chọn--");
            cmbStatus.Items.Add("Chờ xử lý");
            cmbStatus.Items.Add("Còn nợ");
            cmbStatus.Items.Add("Hoàn thành");
            cmbStatus.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void ComboboxStudio()
        {
            string query = "SELECT 0 as studio_id, N'--Chọn--' as tag UNION ALL select studio_id, tag FROM studio ORDER BY studio_id";
            dt = new DataTable();
            da = new SqlDataAdapter(query, conn);
            da.Fill(dt);
            cmbStudio.DisplayMember = "tag";
            cmbStudio.ValueMember = "studio_id";
            cmbStudio.DataSource = dt;
            cmbStudio.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void DefaultFormat()
        {
            lblBankAccount.Text = "";
            lblBankCode.Text = "";
            lblBankName.Text = "";
            lblBankNum.Text = "";
            lblBooking.Text = "";
            lblBranch.Text = "";
            lblCancelled.Text = "";
            lblFinished.Text = "";
            lblID.Text = "";
            lblRealBooking.Text = "";
            lblServiceType.Text = "";
            lblStudioId.Text = "";
            lblTag.Text = "";
            lblTransferredCount.Text = "";
            txtDebt.Enabled = false;
            txtDebt.ReadOnly = true;
            txtRevenue.Enabled = false;
            txtRevenue.ReadOnly = true;
            txtTotalTrans.Enabled = false;
            txtTotalTrans.ReadOnly = true;
            cmbMonth.Enabled = false;
            cmbYear.Enabled = false;
            cmbStatus.Enabled = false;
            cmbStudio.Enabled = false;
            grvPaymentLog.Columns["revenue"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            grvPaymentLog.Columns["revenue"].DisplayFormat.FormatString = "{0:N0}";
            grvPaymentLog.Columns["debt"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            grvPaymentLog.Columns["debt"].DisplayFormat.FormatString = "{0:N0}";

        }

        private void SetCaption()
        {
            string[] columnCaptions = {"Mã bản ghi", "Năm", "Tháng", "Mã đối tác", "Tên đối tác", "Mã hiệu thanh toán", "Ngân hàng", "Ký hiệu", "Chi nhánh", "Chủ tài khoản", "Số tài khoản", "Kiểu loại DV", "Đặt chỗ", "Đặt chỗ thực", "Hủy", "Hoàn thành", "Doanh thu", "Còn nợ", "Trạng thái","Số lần chuyển khoản"};
            power.SetCaption(grvPaymentLog, columnCaptions);
        }

        private void ShowFullInfo()
        {
            try
            {
                conn.Open();
                grctrPaymentLog.DataSource = null;
                dt = new DataTable();
                da = new SqlDataAdapter("d_show_full_info_payment_log", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.Fill(dt);
                grctrPaymentLog.DataSource = dt;
                SetCaption();
            }
            catch (Exception ex)
            {
                XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_payment_PaymentLog.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                conn.Close();
                lblRowCount.Text = grvPaymentLog.RowCount.ToString();
            }
        }

        private void ShowFirstInfo()
        {
            if (grvPaymentLog.RowCount == 0)
            {
                lblBankAccount.Text = "";
                lblBankCode.Text = "";
                lblBankName.Text = "";
                lblBankNum.Text = "";
                lblBooking.Text = "";
                lblBranch.Text = "";
                lblCancelled.Text = "";
                lblFinished.Text = "";
                lblID.Text = "";
                lblRealBooking.Text = "";
                lblServiceType.Text = "";
                lblStudioId.Text = "";
                lblTag.Text = "";
                lblTransferredCount.Text = "";
                txtDebt.ResetText();
                txtRevenue.ResetText();
                txtTotalTrans.ResetText();
                cmbMonth.SelectedItem = "--Chọn--";
                cmbYear.SelectedItem = "--Chọn--";
                cmbStatus.SelectedItem = "--Chọn--";
                cmbStudio.SelectedItem = "--Chọn--";
                return;
            }
            else
            {
                lblID.Text = grvPaymentLog.GetRowCellValue(0, "id").ToString();
                cmbYear.SelectedItem = grvPaymentLog.GetRowCellValue(0, "year").ToString();
                cmbMonth.SelectedItem = grvPaymentLog.GetRowCellValue(0, "month").ToString();
                cmbStatus.SelectedItem = grvPaymentLog.GetRowCellValue(0, "status").ToString();
                lblBankName.Text = grvPaymentLog.GetRowCellValue(0, "bank_name").ToString();
                lblBankCode.Text = grvPaymentLog.GetRowCellValue(0, "bank_code").ToString();
                lblBranch.Text = grvPaymentLog.GetRowCellValue(0, "branch_name").ToString();
                lblBankAccount.Text = grvPaymentLog.GetRowCellValue(0, "bank_account").ToString();
                lblBankNum.Text = grvPaymentLog.GetRowCellValue(0, "bank_num").ToString();
                lblServiceType.Text = grvPaymentLog.GetRowCellValue(0, "service_type").ToString();
                lblBooking.Text = grvPaymentLog.GetRowCellValue(0, "booking").ToString();
                lblRealBooking.Text = grvPaymentLog.GetRowCellValue(0, "real_booking").ToString();
                lblCancelled.Text = grvPaymentLog.GetRowCellValue(0, "cancelled").ToString();
                lblFinished.Text = grvPaymentLog.GetRowCellValue(0, "finished").ToString();
                lblTransferredCount.Text = grvPaymentLog.GetRowCellValue(0, "transferred_count").ToString();
                txtRevenue.Text = string.Format("{0:N0}", double.Parse(grvPaymentLog.GetRowCellValue(0, "revenue").ToString()));
                txtDebt.Text = string.Format("{0:N0}", double.Parse(grvPaymentLog.GetRowCellValue(0, "debt").ToString()));
                txtTotalTrans.Text = string.Format("{0:N0}", (double.Parse(grvPaymentLog.GetRowCellValue(0, "revenue").ToString()) - double.Parse(grvPaymentLog.GetRowCellValue(0, "debt").ToString())));
                lblStudioId.Text = grvPaymentLog.GetRowCellValue(0, "studio_id").ToString();
                cmbStudio.SelectedValue = grvPaymentLog.GetRowCellValue(0, "studio_id");
                lblTag.Text = grvPaymentLog.GetRowCellValue(0, "tag").ToString();

            }
        }

        private void grvPaymentLog_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvPaymentLog.RowCount == 0)
            {
                lblBankAccount.Text = "";
                lblBankCode.Text = "";
                lblBankName.Text = "";
                lblBankNum.Text = "";
                lblBooking.Text = "";
                lblBranch.Text = "";
                lblCancelled.Text = "";
                lblFinished.Text = "";
                lblID.Text = "";
                lblRealBooking.Text = "";
                lblServiceType.Text = "";
                lblStudioId.Text = "";
                lblTag.Text = "";
                lblTransferredCount.Text = "";
                txtDebt.ResetText();
                txtRevenue.ResetText();
                txtTotalTrans.ResetText();
                cmbMonth.SelectedItem = "--Chọn--";
                cmbYear.SelectedItem = "--Chọn--";
                cmbStatus.SelectedItem = "--Chọn--";
                cmbStudio.SelectedItem = "--Chọn--";
                return;
            }
            else
            {
                lblID.Text = grvPaymentLog.GetFocusedRowCellValue("id").ToString();
                cmbYear.SelectedItem = grvPaymentLog.GetFocusedRowCellValue("year").ToString();
                cmbMonth.SelectedItem = grvPaymentLog.GetFocusedRowCellValue("month").ToString();
                cmbStatus.SelectedItem = grvPaymentLog.GetFocusedRowCellValue("status").ToString();
                lblBankName.Text = grvPaymentLog.GetFocusedRowCellValue("bank_name").ToString();
                lblBankCode.Text = grvPaymentLog.GetFocusedRowCellValue("bank_code").ToString();
                lblBranch.Text = grvPaymentLog.GetFocusedRowCellValue("branch_name").ToString();
                lblBankAccount.Text = grvPaymentLog.GetFocusedRowCellValue("bank_account").ToString();
                lblBankNum.Text = grvPaymentLog.GetFocusedRowCellValue("bank_num").ToString();
                lblServiceType.Text = grvPaymentLog.GetFocusedRowCellValue("service_type").ToString();
                lblBooking.Text = grvPaymentLog.GetFocusedRowCellValue("booking").ToString();
                lblRealBooking.Text = grvPaymentLog.GetFocusedRowCellValue("real_booking").ToString();
                lblCancelled.Text = grvPaymentLog.GetFocusedRowCellValue("cancelled").ToString();
                lblFinished.Text = grvPaymentLog.GetFocusedRowCellValue("finished").ToString();
                lblTransferredCount.Text = grvPaymentLog.GetFocusedRowCellValue("transferred_count").ToString();
                txtRevenue.Text = string.Format("{0:N0}", double.Parse(grvPaymentLog.GetFocusedRowCellValue("revenue").ToString()));
                txtDebt.Text = string.Format("{0:N0}", double.Parse(grvPaymentLog.GetFocusedRowCellValue("debt").ToString()));
                txtTotalTrans.Text = string.Format("{0:N0}", (double.Parse(grvPaymentLog.GetFocusedRowCellValue("revenue").ToString()) - double.Parse(grvPaymentLog.GetFocusedRowCellValue("debt").ToString())));
                lblStudioId.Text = grvPaymentLog.GetFocusedRowCellValue("studio_id").ToString();
                cmbStudio.SelectedValue = grvPaymentLog.GetFocusedRowCellValue("studio_id");
                lblTag.Text = grvPaymentLog.GetFocusedRowCellValue("tag").ToString();

            }
        }

        private void usctr_payment_PaymentLog_Load(object sender, EventArgs e)
        {
            ComboboxStatus();
            ComboboxStudio();
            ComboboxYearMonth();
            grctrPaymentLog.DataSource = null;
            ShowFullInfo();
            ShowFirstInfo();
            DefaultFormat();
            lblRowCount.Text = grvPaymentLog.RowCount.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lblID.Text == "" || lblID.Text == "ID")
            {
                XtraMessageBox.Show("Chưa chọn đối tác cần xóa tài khoản!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa bản thanh toán " + lblTag.Text + "?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM payment_log WHERE id = " + int.Parse(lblID.Text), conn);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("Xóa thành công!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_payment_PaymentLog.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        ShowFullInfo();
                        ShowFirstInfo();
                        lblRowCount.Text = grvPaymentLog.RowCount.ToString();
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_payment_PaymentLog_Load(sender, e);
        }

        private void btnPaid_Click(object sender, EventArgs e)
        {
            string payment_log_id = lblID.Text;
            string tag = lblTag.Text;
            decimal debt = decimal.Parse(txtDebt.Text);

            popform_inputNumber frm = new popform_inputNumber(tag, payment_log_id, debt);
            frm.ShowDialog();

        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            if (chkFilter.Checked == false)
            {
                XtraMessageBox.Show("Chưa chọn chế độ lọc", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string year, month, stt, studio;
                if (cmbYear.SelectedItem.ToString() == "--Chọn--")
                {
                    year = "";
                }
                else
                {
                    year = cmbYear.SelectedItem.ToString();
                }

                if (cmbMonth.SelectedItem.ToString() == "--Chọn--")
                {
                    month = "";
                }
                else
                {
                    month = cmbMonth.SelectedItem.ToString();
                }

                if (cmbStatus.SelectedItem.ToString() == "--Chọn--")
                {
                    stt = "";
                }
                else
                {
                    stt = cmbStatus.SelectedItem.ToString();
                }

                if (cmbStudio.SelectedValue.ToString() == "0")
                {
                    studio = "";
                }
                else
                {
                    studio = cmbStudio.SelectedValue.ToString();
                }

                try
                {
                    conn.Open();
                    grctrPaymentLog.DataSource = null;
                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter("d_show_filter_payment_log", conn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add("@year", SqlDbType.NVarChar).Value = year;
                    da.SelectCommand.Parameters.Add("@month", SqlDbType.NVarChar).Value = month;
                    da.SelectCommand.Parameters.Add("@status", SqlDbType.NVarChar).Value = stt;
                    da.SelectCommand.Parameters.Add("@studio", SqlDbType.NVarChar).Value = studio;
                    da.Fill(dt);
                    grctrPaymentLog.DataSource = dt;
                    SetCaption();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_payment_PaymentLog.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                    lblRowCount.Text = grvPaymentLog.RowCount.ToString();
                }
            }
        }

        private void chkFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilter.Checked == true)
            {
                cmbYear.Enabled = true;
                cmbYear.SelectedItem = "--Chọn--";
                cmbMonth.Enabled = true;
                cmbMonth.SelectedItem = "--Chọn--";
                cmbStudio.Enabled = true;
                cmbStudio.SelectedText = "--Chọn--";
                cmbStatus.Enabled = true;
                cmbStatus.SelectedItem = "--Chọn--";
            }
            else
            {
                cmbYear.Enabled = false;
                cmbMonth.Enabled = false;
                cmbStudio.Enabled = false;
                cmbStatus.Enabled = false;
            }
        }

        private void btnTransaction_Click(object sender, EventArgs e)
        {
            int payment_log_id = int.Parse(lblID.Text);
            string tag = lblTag.Text;
            string studio = cmbStudio.Text;
            popform_payment_PaymentTransaction frm = new popform_payment_PaymentTransaction(payment_log_id, tag, studio);
            frm.ShowDialog();

        }

        private void cmbStudio_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblStudioId.Text = cmbStudio.SelectedValue.ToString();
        }
    }
}
