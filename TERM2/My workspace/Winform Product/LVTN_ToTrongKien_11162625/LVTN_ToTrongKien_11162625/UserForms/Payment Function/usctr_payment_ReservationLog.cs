﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_payment_ReservationLog : UserControl
    {
        // Khai báo biến
        private static usctr_payment_ReservationLog usctr; // User control

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_payment_ReservationLog UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_payment_ReservationLog();
                }
                return usctr;
            }
        }

        public usctr_payment_ReservationLog()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void ComboboxCity()
        {
            string query = "SELECT N'--Chọn--' as city_name UNION ALL SELECT DISTINCT city_name FROM reservation_log";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbCity.DisplayMember = "city_name";
            cmbCity.ValueMember = "city_name";
            cmbCity.DataSource = dt;
            cmbCity.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void cmbCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "SELECT N'--Chọn--' as district_name UNION ALL SELECT DISTINCT district_name FROM reservation_log WHERE city_name = N'" + cmbCity.SelectedValue.ToString() + "'";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbDistrict.DisplayMember = "district_name";
            cmbDistrict.ValueMember = "district_name";
            cmbDistrict.DataSource = dt;
            cmbDistrict.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void ComboboxStatus()
        {
            string query = "SELECT N'--Chọn--' as label UNION ALL SELECT N'Hoàn thành' as label UNION ALL SELECT N'Hủy' as label";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbStatus.DisplayMember = "label";
            cmbStatus.ValueMember = "label";
            cmbStatus.DataSource = dt;
            cmbStatus.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void cmbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
        }

        private void ComboboxServiceType()
        {
            string query = "SELECT N'--Chọn--' as value, N'--Chọn--' as service UNION ALL SELECT 'F' as value, N'Luyện tập' as service UNION ALL SELECT 'B' as value, N'Làm đẹp' as service";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbServiceType.DisplayMember = "service";
            cmbServiceType.ValueMember = "value";
            cmbServiceType.DataSource = dt;
            cmbServiceType.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void cmbServiceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "SELECT N'--Chọn--' as value, N'--Chọn--' as category UNION ALL (SELECT DISTINCT c.code as value, t.category_name as category FROM category c INNER JOIN category_translation t ON (c.category_id = t.category_id AND t.language = N'vi') WHERE c.service_type = '" + cmbServiceType.SelectedValue.ToString() + "')";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbCategory.DisplayMember = "category";
            cmbCategory.ValueMember = "value";
            cmbCategory.DataSource = dt;
            cmbCategory.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void ComboboxIsTransferred()
        {
            string query = "SELECT N'--Chọn--' as label UNION ALL SELECT N'Đã tính toán' UNION ALL SELECT N'Chưa tính toán'";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbIsTransferred.DisplayMember = "label";
            cmbIsTransferred.ValueMember = "label";
            cmbIsTransferred.DataSource = dt;
            cmbIsTransferred.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void cmbIsTransferred_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
        }

        private void ComboboxStudio()
        {
            string query = "SELECT N'--Chọn--' as studio UNION ALL SELECT DISTINCT tag FROM studio ORDER BY 1";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbStudio.DisplayMember = "studio";
            cmbStudio.ValueMember = "studio";
            cmbStudio.DataSource = dt;
            cmbStudio.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void cmbStudio_SelectedIndexChanged(object sender, EventArgs e)
        {
            string query = "SELECT N'--Chọn--' as course_name UNION ALL SELECT DISTINCT course_name FROM reservation_log WHERE studio_name = N'" + cmbStudio.SelectedValue.ToString() + "'";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbCourse.DisplayMember = "course_name";
            cmbCourse.ValueMember = "course_name";
            cmbCourse.DataSource = dt;
            cmbCourse.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void ComboboxYearMonth()
        {
            cmbYear.Items.Clear();
            cmbYear.Items.Add("--Chọn--");
            cmbYear.Items.Add("2020");
            cmbYear.Items.Add("2019");
            cmbYear.Items.Add("2018");
            cmbYear.Items.Add("2017");

            cmbMonth.Items.Clear();
            cmbMonth.Items.Add("--Chọn--");
            cmbMonth.Items.Add("01");
            cmbMonth.Items.Add("02");
            cmbMonth.Items.Add("03");
            cmbMonth.Items.Add("04");
            cmbMonth.Items.Add("05");
            cmbMonth.Items.Add("06");
            cmbMonth.Items.Add("07");
            cmbMonth.Items.Add("08");
            cmbMonth.Items.Add("09");
            cmbMonth.Items.Add("10");
            cmbMonth.Items.Add("11");
            cmbMonth.Items.Add("12");
        }

        private void DefaultFormat()
        {
            radRecord.Checked = true;
            radTime.Checked = false;
            chkFilter.Checked = false;
            cmbCity.Enabled = false;
            cmbDistrict.Enabled = false;
            cmbCategory.Enabled = false;
            cmbCourse.Enabled = false;
            cmbIsTransferred.Enabled = false;
            cmbMonth.Enabled = false;
            cmbYear.Enabled = false;
            cmbStudio.Enabled = false;
            cmbStatus.Enabled = false;
            cmbServiceType.Enabled = false;
            grvReservationLog.Columns["import_at"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            grvReservationLog.Columns["import_at"].DisplayFormat.FormatString = "yyyy-MM-dd";
            grvReservationLog.Columns["transferred_at"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            grvReservationLog.Columns["transferred_at"].DisplayFormat.FormatString = "yyyy-MM-dd HH:MM:ss";
            dtpFrom.Enabled = false;
            dtpTo.Enabled = false;
        }

        private void SetCaption()
        {
            string[] columnCaptions = { "Mã lượt", "Thành phố", "Quận", "Vị trí khách hàng", "Thời gian dịch vụ", "Thời gian tạo lượt dịch vụ", "Mã KH", "Tên khách hàng", "Mã đối tác", "Tên đối tác", "Mã dịch vụ", "Tên dịch vụ", "Kiểu loại dịch vụ", "Loại hình dịch vụ", "Trạng thái", "Mã checkin", "Thời gian lưu CSDL", "Trạng thái tính toán", "Thời gian xử lý" };
            power.SetCaption(grvReservationLog, columnCaptions);
        }

        private void ShowFullInfo()
        {
            string tableName = "reservation_log";
            string selectedColumns = "res_id, city_name, district_name, user_located, CONCAT(substring(time_sch,1,4),'-',substring(time_sch,5,2),'-',substring(time_sch,7,2)) as time_sch, CONCAT(substring(dtime_created,1,4),'-',substring(dtime_created,5,2),'-',substring(dtime_created,7,2),' ',substring(dtime_created,9,2),':',substring(dtime_created,11,2),':',substring(dtime_created,13,2)) as dtime_created, user_id, user_name, studio_id, studio_name, course_id, course_name, service_type, category, status, checkin_code, import_at, transfer_tag, transferred_at";
            string afterWhereStr = " (1=1) ORDER BY res_id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrReservationLog.DataSource = dt;
            SetCaption();
        }

        private void ShowFirstInfo()
        {
            if (grvReservationLog.RowCount == 0)
            {
                cmbCategory.SelectedValue = "--Chọn--";
                cmbCity.SelectedValue = "--Chọn--";
                cmbStudio.SelectedValue = "--Chọn--";
                cmbCourse.SelectedValue = "--Chọn--";
                cmbDistrict.SelectedValue = "--Chọn--";
                cmbIsTransferred.SelectedValue = "--Chọn--";
                cmbMonth.SelectedItem = "--Chọn--";
                cmbServiceType.SelectedValue = "--Chọn--";
                cmbStatus.SelectedValue = "--Chọn--";
                cmbYear.SelectedItem = "--Chọn--";
                lblResId.Text = "";
                return;
            }
            else
            {
                lblResId.Text = grvReservationLog.GetRowCellValue(0, "res_id").ToString();
                cmbCity.SelectedValue = grvReservationLog.GetRowCellValue(0, "city_name");
                cmbDistrict.SelectedValue = grvReservationLog.GetRowCellValue(0, "district_name");
                cmbCategory.SelectedValue = grvReservationLog.GetRowCellValue(0, "category");
                cmbCourse.SelectedValue = grvReservationLog.GetRowCellValue(0, "course_name");
                cmbIsTransferred.SelectedValue = grvReservationLog.GetRowCellValue(0, "transfer_tag");
                cmbMonth.SelectedItem = grvReservationLog.GetRowCellValue(0, "time_sch").ToString().Substring(5, 2);
                cmbYear.SelectedItem = grvReservationLog.GetRowCellValue(0, "time_sch").ToString().Substring(0, 4);
                cmbServiceType.SelectedValue = grvReservationLog.GetRowCellValue(0, "service_type");
                cmbStatus.SelectedValue = grvReservationLog.GetRowCellValue(0, "status");
                cmbStudio.SelectedValue = grvReservationLog.GetRowCellValue(0, "studio_name");
                
            }
        }

        private void grvReservationLog_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvReservationLog.RowCount == 0)
            {
                cmbCategory.SelectedValue = "--Chọn--";
                cmbCity.SelectedValue = "--Chọn--";
                cmbStudio.SelectedValue = "--Chọn--";
                cmbCourse.SelectedValue = "--Chọn--";
                cmbDistrict.SelectedValue = "--Chọn--";
                cmbIsTransferred.SelectedValue = "--Chọn--";
                cmbMonth.SelectedItem = "--Chọn--";
                cmbServiceType.SelectedValue = "--Chọn--";
                cmbStatus.SelectedValue = "--Chọn--";
                cmbYear.SelectedItem = "--Chọn--";
                lblResId.Text = "";
                return;
            }
            else
            {
                //int focused_id = grvReservationLog.FocusedRowHandle;
                lblResId.Text = grvReservationLog.GetFocusedRowCellValue("res_id").ToString();
                cmbCity.SelectedValue = grvReservationLog.GetFocusedRowCellValue("city_name");
                cmbDistrict.SelectedValue = grvReservationLog.GetFocusedRowCellValue("district_name");
                cmbCategory.SelectedValue = grvReservationLog.GetFocusedRowCellValue("category");

                cmbIsTransferred.SelectedValue = grvReservationLog.GetFocusedRowCellValue("transfer_tag");
                cmbMonth.SelectedItem = grvReservationLog.GetFocusedRowCellValue("time_sch").ToString().Substring(5, 2);
                cmbYear.SelectedItem = grvReservationLog.GetFocusedRowCellValue("time_sch").ToString().Substring(0, 4);
                cmbServiceType.SelectedValue = grvReservationLog.GetFocusedRowCellValue("service_type");
                cmbStatus.SelectedValue = grvReservationLog.GetFocusedRowCellValue("status");
                cmbStudio.SelectedValue = grvReservationLog.GetFocusedRowCellValue("studio_name");
                cmbCourse.SelectedValue = grvReservationLog.GetFocusedRowCellValue("course_name");
            }
        }

        private void usctr_payment_ReservationLog_Load(object sender, EventArgs e)
        {
            ComboboxCity();
            ComboboxIsTransferred();
            ComboboxServiceType();
            ComboboxStudio();
            ComboboxYearMonth();
            ComboboxStatus();
            grctrReservationLog.DataSource = null;
            ShowFullInfo();
            ShowFirstInfo();
            DefaultFormat();
            lblRowCount.Text = grvReservationLog.RowCount.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (radRecord.Checked == true)
            {
                if (XtraMessageBox.Show("Xóa lượt sử dụng mã " + lblResId.Text + "  ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM reservation_log WHERE res_id = " + int.Parse(lblResId.Text), conn);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("Xóa thành công!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_payment_ReservationLog.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        ShowFullInfo();
                        ShowFirstInfo();
                        lblRowCount.Text = grvReservationLog.RowCount.ToString();
                    }
                }
            }

            if (radTime.Checked == true)
            {
                if (XtraMessageBox.Show("Xác nhận xóa tất cả dữ liệu sử dụng từ " + dtpFrom.Text + " đến " + dtpTo.Text + " ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM reservation_log WHERE substring(time_sch,1,8) BETWEEN '" + dtpFrom.Text.Replace("-", "") + "' AND '" + dtpTo.Text.Replace("-","") + "'",conn);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("Xóa thành công!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_payment_ReservationLog.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        ShowFullInfo();
                        ShowFirstInfo();
                        lblRowCount.Text = grvReservationLog.RowCount.ToString();
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_payment_ReservationLog_Load(sender, e);
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            if (chkFilter.Checked == false)
            {
                XtraMessageBox.Show("Chưa chọn chế độ lọc", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string city, district, status, service_type, category, transfer_tag, studio, course, year, month;
                if (cmbCity.SelectedValue.ToString() == "--Chọn--")
                {
                    city = "";
                }
                else
                {
                    city = cmbCity.SelectedValue.ToString();
                }

                if (cmbDistrict.SelectedValue.ToString() == "--Chọn--")
                {
                    district = "";
                }
                else
                {
                    district = cmbDistrict.SelectedValue.ToString();
                }

                if (cmbStatus.SelectedValue.ToString() == "--Chọn--")
                {
                    status = "";
                }
                else
                {
                    status = cmbStatus.SelectedValue.ToString();
                }

                if (cmbServiceType.SelectedValue.ToString() == "--Chọn--")
                {
                    service_type = "";
                }
                else
                {
                    service_type = cmbServiceType.SelectedValue.ToString();
                }

                if (cmbCategory.SelectedValue.ToString() == "--Chọn--")
                {
                    category = "";
                }
                else
                {
                    category = cmbCategory.SelectedValue.ToString();
                }

                if (cmbIsTransferred.SelectedValue.ToString() == "--Chọn--")
                {
                    transfer_tag = "";
                }
                else
                {
                    transfer_tag = cmbIsTransferred.SelectedValue.ToString();
                }

                if (cmbStudio.SelectedValue.ToString() == "--Chọn--")
                {
                    studio = "";
                }
                else
                {
                    studio = cmbStudio.SelectedValue.ToString();
                }

                if (cmbCourse.SelectedValue.ToString() == "--Chọn--")
                {
                    course = "";
                }
                else
                {
                    course = cmbCourse.SelectedValue.ToString();
                }

                if (cmbYear.SelectedItem.ToString() == "--Chọn--")
                {
                    year = "";
                }
                else
                {
                    year = cmbYear.SelectedItem.ToString();
                }

                if (cmbMonth.SelectedItem.ToString() == "--Chọn--")
                {
                    month = "";
                }
                else
                {
                    month = cmbMonth.SelectedItem.ToString();
                }

                try
                {
                    conn.Open();
                    grctrReservationLog.DataSource = null;
                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter("d_show_filter_reservation_log", conn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add("@city", SqlDbType.NVarChar).Value = city;
                    da.SelectCommand.Parameters.Add("@district", SqlDbType.NVarChar).Value = district;
                    da.SelectCommand.Parameters.Add("@status", SqlDbType.NVarChar).Value = status;
                    da.SelectCommand.Parameters.Add("@service_type", SqlDbType.NVarChar).Value = service_type;
                    da.SelectCommand.Parameters.Add("@category", SqlDbType.NVarChar).Value = category;
                    da.SelectCommand.Parameters.Add("@transfer_tag", SqlDbType.NVarChar).Value = transfer_tag;
                    da.SelectCommand.Parameters.Add("@studio", SqlDbType.NVarChar).Value = studio;
                    da.SelectCommand.Parameters.Add("@course", SqlDbType.NVarChar, 50).Value = course;
                    da.SelectCommand.Parameters.Add("@year", SqlDbType.NVarChar).Value = year;
                    da.SelectCommand.Parameters.Add("@month", SqlDbType.NVarChar).Value = month;
                    da.Fill(dt);
                    grctrReservationLog.DataSource = dt;
                    SetCaption();

                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_payment_ReservationLog.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                    lblRowCount.Text = grvReservationLog.RowCount.ToString();
                }
            }
        }

        private void btnCalculateRev_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Xác nhận đưa dữ liệu vào tính toán chi phí?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    conn.Open();

                    cmd = new SqlCommand("a_insert_tb_payment_log", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    int result = cmd.ExecuteNonQuery();

                    SqlCommand cmd0 = new SqlCommand("SELECT COUNT(*) FROM (SELECT DISTINCT CAST(substring(time_sch,1,4) as INT) year, CAST(substring(time_sch,5,2) as INT) month, studio_id FROM reservation_log) a", conn);
                    var inserted_rows = cmd0.ExecuteScalar();

                    result = result - int.Parse(inserted_rows.ToString()); //Không tính các affected rows của lệnh UPDATE trong stored procedues

                    if (result == 0)
                    {
                        XtraMessageBox.Show("Tính toán thất bại: các bản ghi đã được sử dụng!", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        XtraMessageBox.Show("Tính toán hoàn tất! \n \n \n Số bản ghi xử lý: " + result, "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_payment_ReservationLog.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                    usctr_payment_ReservationLog_Load(sender, e);
                }
            }
        }

        private void chkFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilter.Checked == true)
            {
                cmbCity.Enabled = true;
                cmbDistrict.Enabled = true;
                cmbCategory.Enabled = true;
                cmbCourse.Enabled = true;
                cmbIsTransferred.Enabled = true;
                cmbYear.Enabled = true;
                cmbMonth.Enabled = true;
                cmbStudio.Enabled = true;
                cmbStatus.Enabled = true;
                cmbServiceType.Enabled = true;
                lblResId.Text = "";

                cmbCity.SelectedValue = "--Chọn--";
                cmbDistrict.SelectedValue = "--Chọn--";
                cmbCategory.SelectedValue = "--Chọn--";
                cmbCourse.SelectedValue = "--Chọn--";
                cmbIsTransferred.SelectedValue = "--Chọn--";
                cmbYear.SelectedItem = "--Chọn--";
                cmbMonth.SelectedItem = "--Chọn--";
                cmbStudio.SelectedValue = "--Chọn--";
                cmbStatus.SelectedValue = "--Chọn--";
                cmbServiceType.SelectedValue = "--Chọn--";
            }
            else
            {
                ShowFirstInfo();
                DefaultFormat();
            }
        }

        private void radTime_CheckedChanged(object sender, EventArgs e)
        {
            if (radTime.Checked == true)
            {
                dtpFrom.Enabled = true;
                dtpTo.Enabled = true;
            }
            else
            {
                dtpFrom.Enabled = false;
                dtpTo.Enabled = false;
            }
        }
    }
}
