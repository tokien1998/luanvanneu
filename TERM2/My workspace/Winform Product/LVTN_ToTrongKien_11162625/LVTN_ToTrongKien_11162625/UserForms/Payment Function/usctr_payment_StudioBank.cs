﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_payment_StudioBank : UserControl
    {
        // Khai báo biến
        private static usctr_payment_StudioBank usctr; // User control
        string btn_flag = null; // Biến xác định nút đang sử dụng

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_payment_StudioBank UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_payment_StudioBank();
                }
                return usctr;
            }
        }

        public usctr_payment_StudioBank()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void ComboboxStudio()
        {
            string query = "SELECT 0 as studio_id, N'--Chọn--' as tag UNION ALL SELECT studio_id, tag FROM studio ORDER BY studio_id";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbStudio.DisplayMember = "tag";
            cmbStudio.ValueMember = "studio_id";
            cmbStudio.DataSource = dt;
            cmbStudio.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void ComboboxStatus()
        {
            string query = "SELECT N'--Chọn--' as status UNION ALL SELECT N'Hoạt động' UNION ALL SELECT N'Vô hiệu hóa'";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbStatus.DisplayMember = "status";
            cmbStatus.ValueMember = "status";
            cmbStatus.DataSource = dt;
            cmbStatus.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void ComboboxBank()
        {
            string query = "SELECT 0 as bank_id, N'--Chọn--' as bank_code UNION ALL SELECT bank_id, bank_code FROM bank ORDER BY bank_id";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbBank.DisplayMember = "bank_code";
            cmbBank.ValueMember = "bank_id";
            cmbBank.DataSource = dt;
            cmbBank.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void cmbBank_SelectedIndexChanged(object sender, EventArgs e)
        {
            int bank_id = int.Parse(cmbBank.SelectedValue.ToString());
            string query = "SELECT 0 as branch_id, N'--Chọn--' as branch_name UNION ALL SELECT branch_id, branch_name FROM bank_branch WHERE bank_id = " + bank_id + " ORDER BY branch_id";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbBranch.DisplayMember = "branch_name";
            cmbBranch.ValueMember = "branch_id";
            cmbBranch.DataSource = dt;
            cmbBranch.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void DefaultFormat()
        {
            cmbStudio.Enabled = true;
            //cmbStudio.SelectedIndex = 0;
            cmbStatus.Enabled = false;
            cmbStatus.Text = "--Chọn--";
            cmbBank.Enabled = false;
            cmbBank.Text = "--Chọn--";
            cmbBranch.Enabled = false;
            cmbBranch.Text = "--Chọn--";
            txtAccountNum.Enabled = false;
            txtAccountNum.ReadOnly = true;
            txtBankAccount.Enabled = false;
            txtBankAccount.ReadOnly = true;
        }


        private void SetCaption()
        {
            string[] columnCaptions = { "Mã tài khoản", "Chủ tài khoản", "Số tài khoản", "Ngân hàng", "Chi nhánh", "Trạng thái"};
            power.SetCaption(grvStudioBank, columnCaptions);
        }

        private void ShowFullInfo()
        {
            string studio_id = cmbStudio.SelectedValue.ToString();

            string tableName = @"(SELECT sb.id, sb.bank_account, sb.bank_num, b.bank_code, bb.branch_name, sb.status
                                    FROM studio_bank sb 
                                        LEFT JOIN studio s ON (sb.studio_id = s.studio_id)
                                        LEFT JOIN bank_branch bb ON (sb.branch_id = bb.branch_id)
                                        LEFT JOIN bank b ON (bb.bank_id = b.bank_id)
                                    WHERE s.studio_id = " + studio_id + ") a";
            string selectedColumns = "id, bank_account, bank_num, bank_code, branch_name, status";
            string afterWhereStr = " (1=1) ORDER BY id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrStudioBank.DataSource = dt;
            SetCaption();
        }

        private void ShowFirstInfo()
        {
            if (grvStudioBank.RowCount == 0)
            {
                cmbStatus.Text = "--Chọn--";
                cmbBank.Text = "--Chọn--";
                cmbBranch.Text = "--Chọn--";
                txtAccountNum.ResetText();
                txtBankAccount.ResetText();
                return;
            }
            else
            {
                lblID.Text = grvStudioBank.GetRowCellValue(0, "id").ToString();
                cmbStatus.SelectedValue = grvStudioBank.GetRowCellValue(0, "status");
                cmbBank.Text = grvStudioBank.GetRowCellValue(0, "bank_code").ToString();
                cmbBranch.Text = grvStudioBank.GetRowCellValue(0, "branch_name").ToString();
                txtAccountNum.Text = grvStudioBank.GetRowCellValue(0, "bank_num").ToString();
                txtBankAccount.Text = grvStudioBank.GetRowCellValue(0, "bank_account").ToString();
            }
        }

        private void grvStudioBank_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvStudioBank.RowCount == 0)
            {
                cmbStatus.SelectedIndex = 0;
                cmbBank.SelectedIndex = 0;
                cmbBranch.SelectedIndex = 0;
                txtAccountNum.ResetText();
                txtBankAccount.ResetText();
                return;
            }
            else
            {
                lblID.Text = grvStudioBank.GetFocusedRowCellValue("id").ToString();
                cmbStatus.SelectedValue = grvStudioBank.GetFocusedRowCellValue("status");
                cmbBank.Text = grvStudioBank.GetFocusedRowCellValue("bank_code").ToString();
                cmbBranch.Text = grvStudioBank.GetFocusedRowCellValue("branch_name").ToString();
                txtAccountNum.Text = grvStudioBank.GetFocusedRowCellValue("bank_num").ToString();
                txtBankAccount.Text = grvStudioBank.GetFocusedRowCellValue("bank_account").ToString();
            }
        }

        private void usctr_payment_StudioBank_Load(object sender, EventArgs e)
        {
            lblID.Text = "";
            btn_flag = null;
            ComboboxStudio();
            ComboboxBank();
            ComboboxStatus();
            grctrStudioBank.DataSource = null;
            ShowFullInfo();
            DefaultFormat();
            ShowFirstInfo();
            lblRowCount.Text = grvStudioBank.RowCount.ToString();

        }

        private void cmbStudio_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowFullInfo();
            ShowFirstInfo();
            //DefaultFormat();
            lblRowCount.Text = grvStudioBank.RowCount.ToString();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";
            if (cmbStudio.Text == "" || cmbStudio.Text == "--Chọn--")
            {
                XtraMessageBox.Show("Chưa chọn đối tác cần tạo!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                //cmbStudio.Enabled = true;
                //cmbStudio.SelectedIndex = 0;
                cmbStatus.Enabled = false;
                cmbStatus.SelectedIndex = 1;
                cmbBank.Enabled = true;
                cmbBank.SelectedIndex = 0;
                cmbBranch.Enabled = true;
                cmbBranch.SelectedIndex = 0;
                txtAccountNum.Enabled = true;
                txtAccountNum.ReadOnly = false;
                txtAccountNum.ResetText();
                txtBankAccount.Enabled = true;
                txtBankAccount.ReadOnly = false;
                txtBankAccount.ResetText();

            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            btn_flag = "edit";
            if (lblID.Text == "" || lblID.Text == "id")
            {
                XtraMessageBox.Show("Chưa chọn đối tác cần sửa!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                cmbStatus.Enabled = true;
                cmbBank.Enabled = true;
                cmbBranch.Enabled = true;
                txtAccountNum.Enabled = true;
                txtAccountNum.ReadOnly = false;
                txtBankAccount.Enabled = true;
                txtBankAccount.ReadOnly = false;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            btn_flag = "edit";
            if (lblID.Text == "" || lblID.Text == "id")
            {
                XtraMessageBox.Show("Chưa chọn đối tác cần xóa tài khoản!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa lịch tài khoản " + txtBankAccount.Text + " - " + txtAccountNum.Text + "  ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM studio_bank WHERE id = " + int.Parse(lblID.Text), conn);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("Xóa thành công!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_payment_StudioBank.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        ShowFullInfo();
                        ShowFirstInfo();
                        lblRowCount.Text = grvStudioBank.RowCount.ToString();
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_payment_StudioBank_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (cmbBank.Text == "--Chọn--" || cmbBank.Text == "" || cmbBranch.Text == "" || cmbBranch.Text == "--Chọn--" || txtAccountNum.Text == "" || txtBankAccount.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng nhập đủ thông tin!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (power.IsNumber(txtAccountNum.Text.Replace(" ", "")) == false)
                {
                    XtraMessageBox.Show("Số tài khoản nhập sai định dạng!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtAccountNum.ResetText();
                }
                else
                {
                    try
                    {
                        // Khai báo các cột và values cần check duplicate
                        string[] col_names = { "branch_id", "bank_account", "bank_num" };
                        string[] col_values = { cmbBranch.SelectedValue.ToString(), txtBankAccount.Text, txtAccountNum.Text.Replace(" ", "") };

                        conn.Open();

                        //Check trùng
                        if (power.Sql_CheckDuplicate("studio_bank", col_names, col_values) == false)
                        {
                            cmd = new SqlCommand("a_insert_tb_studio_bank", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudio.SelectedValue.ToString());
                            cmd.Parameters.Add("@branch_id", SqlDbType.Int).Value = int.Parse(cmbBranch.SelectedValue.ToString());
                            cmd.Parameters.Add("@bank_account", SqlDbType.NVarChar, 50).Value = txtBankAccount.Text;
                            cmd.Parameters.Add("@bank_num", SqlDbType.VarChar, 50).Value = txtAccountNum.Text.Replace(" ", "");
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("TẠO THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ShowFirstInfo();
                        }
                        else
                        {
                            XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_payment_StudioBank.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        ShowFullInfo();
                        DefaultFormat();
                        ShowFirstInfo();
                        lblRowCount.Text = grvStudioBank.RowCount.ToString();
                    }
                }
            }

            if (btn_flag == "edit")
            {
                if (cmbBank.Text == "--Chọn--" || cmbBank.Text == "" || cmbBranch.Text == "" || cmbBranch.Text == "--Chọn--" || txtAccountNum.Text == "" || txtBankAccount.Text == "" || cmbStatus.Text == "--Chọn--" || cmbStatus.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng nhập đủ thông tin!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (power.IsNumber(txtAccountNum.Text.Replace(" ", "")) == false)
                {
                    XtraMessageBox.Show("Số tài khoản nhập sai định dạng!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtAccountNum.ResetText();
                }
                else
                {
                    try
                    {

                        conn.Open();

                        //Check trùng
                        cmd = new SqlCommand("b_update_tb_studio_bank", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = int.Parse(lblID.Text);
                        cmd.Parameters.Add("@branch_id", SqlDbType.Int).Value = int.Parse(cmbBranch.SelectedValue.ToString());
                        cmd.Parameters.Add("@bank_account", SqlDbType.NVarChar, 50).Value = txtBankAccount.Text;
                        cmd.Parameters.Add("@bank_num", SqlDbType.VarChar, 50).Value = txtAccountNum.Text.Replace(" ", "");
                        cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = cmbStatus.SelectedValue.ToString();
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ShowFirstInfo();

                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_payment_StudioBank.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        ShowFullInfo();
                        DefaultFormat();
                        ShowFirstInfo();
                        lblRowCount.Text = grvStudioBank.RowCount.ToString();
                    }
                }
            }
        }
    }
}
