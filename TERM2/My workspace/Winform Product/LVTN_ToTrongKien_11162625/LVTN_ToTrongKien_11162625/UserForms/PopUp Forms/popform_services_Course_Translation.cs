﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.Configuration;

namespace LVTN_ToTrongKien_11162625
{
    public partial class popform_services_Course_Translation : DevExpress.XtraEditors.XtraForm
    {
        string btn_flag = null;

        int course_id;
        string tag;
        string courseTag;
        int month_slots;
        int session_slots;

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        public popform_services_Course_Translation(int course_id, string tag, string courseTag, int month_slots, int session_slots)
        {
            InitializeComponent();
            this.course_id = course_id;
            this.tag = tag;
            this.courseTag = courseTag;
            this.month_slots = month_slots;
            this.session_slots = session_slots;
            conn = new SqlConnection(str);
        }

        private void popform_services_Course_Translation_Load(object sender, EventArgs e)
        {
            btn_flag = null;
            ResetAllText();
            AboutCourse();
            FillSpecificCombobox();
            DefaultFormat();
        }


        // ResetText
        private void ResetAllText()
        {
            txtFullname.ResetText();
            txtDesc.ResetText();
            txtNoteCus.ResetText();
            txtRules.ResetText();
        }

        // Hiển thị thông tin từ form course truyền sang
        private void AboutCourse()
        {
            lblCourseId.Text = course_id.ToString();
            lblStudioTag.Text = tag.ToString();
            lblTag.Text = courseTag.ToString();
        }

        // Fill default data into combobox
        private void FillDefaultCombobox()
        {
            cmbLang.DataSource = null;

            Dictionary<string, string> langeSource = new Dictionary<string, string>();
            langeSource.Add("temp", "--Chọn--");
            langeSource.Add("vi", "Tiếng Việt");
            langeSource.Add("en", "English");

            cmbLang.DataSource = new BindingSource(langeSource, null);
            cmbLang.DisplayMember = "Value";
            cmbLang.ValueMember = "Key";
            cmbLang.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        // Fill conditional data of studio into combobox
        private void FillSpecificCombobox()
        {
            cmbLang.DataSource = null;

            conn.Open();
            string query = @"SELECT 'temp' as code, N'--Chọn--' as language UNION ALL SELECT DISTINCT language as code, CASE WHEN language = 'vi' THEN N'Tiếng Việt' WHEN language = 'en' THEN N'English' ELSE NULL END as language FROM course_translation WHERE course_id = " + course_id;
            dt = new DataTable();
            da = new SqlDataAdapter(query, conn);
            da.Fill(dt);
            cmbLang.DisplayMember = "language";
            cmbLang.ValueMember = "code";
            cmbLang.DataSource = dt;
            cmbLang.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        //Default Format
        private void DefaultFormat()
        {
            txtFullname.Enabled = false;
            txtFullname.ReadOnly = true;
            txtDesc.Enabled = false;
            txtDesc.ReadOnly = true;
            txtNoteCus.Enabled = false;
            txtNoteCus.ReadOnly = true;
            txtRules.Enabled = false;
            txtRules.ReadOnly = true;
            lblTransId.Visible = false;
            lblAcceptanceLevel.Visible = false;
            lblApplyFor.Visible = false;
            cmbLang.Enabled = true;
        }

        //Open editing function for creation
        private void OpenEditCreation()
        {
            txtFullname.Enabled = true;
            txtFullname.ReadOnly = false;
            txtFullname.ResetText();
            txtDesc.Enabled = true;
            txtDesc.ReadOnly = false;
            txtDesc.ResetText();
            txtNoteCus.Enabled = true;
            txtNoteCus.ReadOnly = false;
            txtNoteCus.ResetText();
            txtRules.Enabled = true;
            txtRules.ReadOnly = false;
            txtRules.ResetText();
            cmbLang.SelectedIndex = 0;
            cmbLang.Enabled = true;
        }

        //Open editing function for editing
        private void OpenEdit()
        {
            txtFullname.Enabled = true;
            txtFullname.ReadOnly = false;
            txtDesc.Enabled = true;
            txtDesc.ReadOnly = false;
            txtNoteCus.Enabled = true;
            txtNoteCus.ReadOnly = false;
            txtRules.Enabled = true;
            txtRules.ReadOnly = false;
            cmbLang.Enabled = false;
            lblTransId.Visible = true;
            lblAcceptanceLevel.Visible = true;
            lblApplyFor.Visible = true;
        }

        private void cmbLang_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (btn_flag == null && (cmbLang.Text == "Tiếng Việt" || cmbLang.Text == "English"))
            {
                lblTransId.Visible = true;
                lblAcceptanceLevel.Visible = true;
                lblApplyFor.Visible = true;
                string lang = cmbLang.SelectedValue.ToString();
                string script = "SELECT id, full_name, description, rules, note_cus, acceptance_level, apply_for FROM course_translation WHERE language = N'" + lang + "' AND course_id = " + course_id;
                dt = new DataTable();
                da = new SqlDataAdapter(script, conn);
                da.Fill(dt);
                lblTransId.Text = dt.Rows[0]["id"].ToString();
                txtFullname.Text = dt.Rows[0]["full_name"].ToString();
                txtDesc.Text = dt.Rows[0]["description"].ToString();
                txtRules.Text = dt.Rows[0]["rules"].ToString();
                txtNoteCus.Text = dt.Rows[0]["note_cus"].ToString();
                lblAcceptanceLevel.Text = dt.Rows[0]["acceptance_level"].ToString();
                lblApplyFor.Text = dt.Rows[0]["apply_for"].ToString();
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";
            FillDefaultCombobox();
            OpenEditCreation();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (txtFullname.Text == "")
            {
                XtraMessageBox.Show("Chọn một bản ghi để sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                btn_flag = "edit";
                OpenEdit();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (cmbLang.SelectedIndex == 0)
            {
                XtraMessageBox.Show("Chọn một bản ghi để xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                try
                {
                    if (XtraMessageBox.Show("Xóa bản ghi '" + lblTransId.Text + " - " + cmbLang.Text + "' của " + courseTag + " ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        conn.Open();

                        cmd = new SqlCommand("DELETE FROM course_translation WHERE course_id = " + course_id + " AND language = N'" + cmbLang.SelectedValue.ToString() + "'", conn);
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_services_Course_Translation (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                    popform_services_Course_Translation_Load(sender, e);
                    btn_flag = null;
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            popform_services_Course_Translation_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (txtFullname.Text == "" || txtRules.Text == "")
                {
                    XtraMessageBox.Show("Điển đủ thông tin sau: \n - Tên đầy đủ \n - Nội quy", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (cmbLang.Text == "--Chọn--")
                {
                    XtraMessageBox.Show("Chọn ngôn ngữ cho bản ghi", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        // Khai báo các cột và values cần check duplicate
                        string[] col_names = { "language", "course_id" };
                        string[] col_values = { cmbLang.SelectedValue.ToString(), course_id.ToString() };

                        if (power.Sql_CheckDuplicate("course_translation", col_names, col_values) == false)
                        {
                            conn.Open();

                            cmd = new SqlCommand("a_insert_tb_course_translation", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@course_id", SqlDbType.Int).Value = course_id;
                            cmd.Parameters.Add("@language", SqlDbType.NVarChar, 50).Value = cmbLang.SelectedValue.ToString();
                            cmd.Parameters.Add("@full_name", SqlDbType.NVarChar, 50).Value = txtFullname.Text;
                            cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = txtDesc.Text;
                            cmd.Parameters.Add("@rules", SqlDbType.NVarChar).Value = txtRules.Text;
                            cmd.Parameters.Add("@note_cus", SqlDbType.NVarChar).Value = txtNoteCus.Text;
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("THÊM THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }

                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_lists_Studio_Translation (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        popform_services_Course_Translation_Load(sender, e);
                        btn_flag = null;
                    }
                }
            }



            if (btn_flag == "edit")
            {
                if (txtFullname.Text == "" || txtRules.Text == "")
                {
                    XtraMessageBox.Show("Điển đủ thông tin sau: \n - Tên đầy đủ \n - Nội quy", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        conn.Open();

                        cmd = new SqlCommand("b_update_tb_course_translation", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = int.Parse(lblTransId.Text);
                        cmd.Parameters.Add("@language", SqlDbType.NVarChar, 50).Value = cmbLang.SelectedValue.ToString();
                        cmd.Parameters.Add("@full_name", SqlDbType.NVarChar, 50).Value = txtFullname.Text;
                        cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = txtDesc.Text;
                        cmd.Parameters.Add("@rules", SqlDbType.NVarChar).Value = txtRules.Text;
                        cmd.Parameters.Add("@note_cus", SqlDbType.NVarChar).Value = txtNoteCus.Text;

                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_lists_Studio_Translation (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        popform_services_Course_Translation_Load(sender, e);
                        btn_flag = null;
                    }
                }
            }
        }
    }
}