﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.Configuration;

namespace LVTN_ToTrongKien_11162625
{
    public partial class popform_payment_PaymentTransaction : DevExpress.XtraEditors.XtraForm
    {
        // Khai báo biến

        int payment_log_id;
        string tag, studio;

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        public popform_payment_PaymentTransaction(int payment_log_id, string tag, string studio)
        {
            InitializeComponent();
            this.payment_log_id = payment_log_id;
            this.tag = tag;
            this.studio = studio;
            conn = new SqlConnection(str);
        }

        private void popform_payment_PaymentTransaction_Load(object sender, EventArgs e)
        {
            grctrTransaction.DataSource = null;
            ShowFullInfo();
            ShowFirstInfo();
            DefaultFormat();
            lblRowCount.Text = grvTransaction.RowCount.ToString();
            lblPaymentLogId.Text = payment_log_id.ToString();
            lblStudio.Text = studio;
            lblPaymentTag.Text = tag;
        }

        private void DefaultFormat()
        {
            //lblID.Text = "";
            txtAmount.Enabled = false;
            txtAmount.ReadOnly = true;
            dtpTransferredAt.Enabled = false;
            dtpTransferredAt.Value = DateTime.Now;
            dtpFrom.Enabled = false;
            dtpFrom.Value = DateTime.Today;
            dtpTo.Enabled = false;
            dtpFrom.Value = DateTime.Today;
            chkFilter.Checked = false;
            grvTransaction.Columns["amount"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            grvTransaction.Columns["amount"].DisplayFormat.FormatString = "{0:N0}";
            grvTransaction.Columns["transferred_at"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            grvTransaction.Columns["transferred_at"].DisplayFormat.FormatString = "dd/MM/yyyy HH:MM:ss";
            lblPaymentLogId.Text = payment_log_id.ToString();
            lblStudio.Text = studio;
            lblPaymentTag.Text = tag;
        }

        private void SetCaption()
        {
            string[] columnCaptions = {"Mã giao dịch", "Mã thanh toán", "Lượng tiền giao dịch", "Thời điểm giao dịch"};
            power.SetCaption(grvTransaction, columnCaptions);
        }

        private void ShowFullInfo()
        {
            string tableName = "payment_transaction";
            string selectedColumns = "id, payment_log_id, amount, transferred_at";
            string afterWhereStr = " payment_log_id = " + payment_log_id + " ORDER BY id";

            dt = new DataTable();
            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrTransaction.DataSource = dt;
            SetCaption();
        }

        private void grvTransaction_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvTransaction.RowCount == 0)
            {
                //lblID.Text = "";
                txtAmount.ResetText();
                dtpTransferredAt.Enabled = false;
                dtpTransferredAt.Value = DateTime.Now;
                return;
            }
            else
            {
                lblID.Text = grvTransaction.GetFocusedRowCellValue("id").ToString();
                txtAmount.Text = string.Format("{0:N0}", double.Parse(grvTransaction.GetFocusedRowCellValue("amount").ToString()));
                dtpTransferredAt.Value = DateTime.Parse(grvTransaction.GetFocusedRowCellValue("transferred_at").ToString());
                lblPaymentTag.Text = tag;
            }
        }

        private void chkFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilter.Checked == true)
            {
                dtpFrom.Enabled = true;
                dtpFrom.Value = DateTime.Today;
                dtpTo.Enabled = true;
                dtpTo.Value = DateTime.Today;
            }
            else
            {
                dtpFrom.Enabled = false;
                dtpTo.Enabled = false;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            popform_payment_PaymentTransaction_Load(sender, e);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lblID.Text == "" || lblID.Text == "ID")
            {
                XtraMessageBox.Show("Chưa chọn đối tác cần xóa!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa lịch sử giao dịch mã: " + lblID.Text + "?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM payment_transaction WHERE id = " + int.Parse(lblID.Text), conn);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("Xóa thành công!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_payment_PaymentTransaction.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        ShowFullInfo();
                        ShowFirstInfo();
                        lblRowCount.Text = grvTransaction.RowCount.ToString();
                    }
                }
            }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            if (chkFilter.Checked == false)
            {
                XtraMessageBox.Show("Chưa chọn chế độ lọc!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string tableName = "payment_transaction";
                string selectedColumns = "id, payment_log_id, amount, transferred_at";
                string afterWhereStr = " payment_log_id = " + payment_log_id + " AND CAST(transferred_at AS DATE) BETWEEN '" + dtpFrom.Text + "' AND '" + dtpTo.Text + "' ORDER BY id";

                dt = new DataTable();
                dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
                grctrTransaction.DataSource = dt;
                SetCaption();
            }
        }

        private void ShowFirstInfo()
        {
            if (grvTransaction.RowCount == 0)
            {
                //lblID.Text = "";
                txtAmount.ResetText();
                dtpTransferredAt.Enabled = false;
                dtpTransferredAt.Value = DateTime.Now;
                return;
            }
            else
            {
                lblID.Text = grvTransaction.GetRowCellValue(0, "id").ToString();
                txtAmount.Text = string.Format("{0:N0}", double.Parse(grvTransaction.GetRowCellValue(0, "amount").ToString()));
                dtpTransferredAt.Value = DateTime.Parse(grvTransaction.GetRowCellValue(0, "transferred_at").ToString());
            }
        }
    }
}