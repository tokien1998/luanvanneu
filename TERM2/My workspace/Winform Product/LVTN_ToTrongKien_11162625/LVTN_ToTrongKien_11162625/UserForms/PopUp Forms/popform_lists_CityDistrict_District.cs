﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.Configuration;

namespace LVTN_ToTrongKien_11162625
{
    public partial class popform_lists_CityDistrict_District : DevExpress.XtraEditors.XtraForm
    {
        // Khai báo biến
        string btn_flag = null; // Biến xác định nút đang sử dụng

        int city_id;
        string city_name;

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        public popform_lists_CityDistrict_District(int city_id, string city_name)
        {
            InitializeComponent();
            this.city_id = city_id;
            this.city_name = city_name;
            conn = new SqlConnection(str);
        }

        private void popform_lists_CityDistrict_District_Load(object sender, EventArgs e)
        {
            cmbCity.Text = city_name;
            btn_flag = null;
            grctrDistrict.DataSource = null;
            grvDistrict.FindFilterText = "";
            DefaultFormat();
            ShowFullData();
            ShowFirstInfo();
            lblDistCount.Text = grvDistrict.RowCount.ToString();
        }

        // Định dạng textbox ban đầu
        private void DefaultFormat()
        {
            txtDistId.ReadOnly = true;
            txtDistId.Enabled = false;
            txtDistId.Font = new Font(txtDistId.Font, FontStyle.Regular);
            txtDistName.ReadOnly = true;
            txtDistName.Enabled = false;

            cmbCity.Enabled = false;
        }

        // Show full data on datagridview
        private void ShowFullData()
        {
            string tableName = "district";
            string selectedColumns = "dist_id, dist_name";
            string afterWhereStr = "city_id = " + city_id + " ORDER BY dist_id";
            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrDistrict.DataSource = dt;
            SetCaption();
        }

        // Set captions
        private void SetCaption()
        {
            string[] columnCaptions = { "Mã quận", "Tên quận" };
            power.SetCaption(grvDistrict, columnCaptions);
        }

        // Show 1st record on textbox
        private void ShowFirstInfo()
        {
            if (grvDistrict.RowCount == 0)
            {
                txtDistId.ResetText();
                txtDistName.ResetText();
                return;
            }
            else
            {
                txtDistId.Text = grvDistrict.GetRowCellValue(0, "dist_id").ToString();
                txtDistName.Text = grvDistrict.GetRowCellValue(0, "dist_name").ToString();
            }
        }

        private void grvDistrict_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvDistrict.RowCount == 0)
            {
                //Nếu grid view ko có dữ liệu thì ko hiện lên textbox để tránh lỗi "Object reference not set to an instance of an object"
                txtDistId.ResetText();
                txtDistName.ResetText();
                return;
            }
            else
            {
                txtDistId.Font = new Font(txtDistId.Font, FontStyle.Regular);
                txtDistId.Text = grvDistrict.GetFocusedRowCellValue("dist_id").ToString();
                txtDistName.Text = grvDistrict.GetFocusedRowCellValue("dist_name").ToString();
            }

            DefaultFormat();
            btn_flag = null;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";
            txtDistName.ResetText();
            txtDistName.Enabled = true;
            txtDistName.ReadOnly = false;
            txtDistId.Text = "Auto";
            txtDistId.Font = new Font(txtDistId.Font, FontStyle.Italic);
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (txtDistName.Text == "")
            {
                XtraMessageBox.Show("Chọn đối tượng cần sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                btn_flag = "edit";
                txtDistName.ReadOnly = false;
                txtDistName.Enabled = true;
                cmbCity.DataSource = null;
                cmbCity.Enabled = true;

                // Đổ dữ liệu vào combobox
                try
                {
                    da = new SqlDataAdapter();
                    dt = new DataTable();

                    conn.Open();
                    da = new SqlDataAdapter("SELECT city_id, city_name FROM city ORDER BY city_name", conn);
                    da.Fill(dt);
                    cmbCity.DataSource = dt;
                    cmbCity.ValueMember = dt.Columns["city_id"].ToString();
                    cmbCity.DisplayMember = dt.Columns["city_name"].ToString();
                    cmbCity.DropDownStyle = ComboBoxStyle.DropDownList;
                    cmbCity.Text = city_name;
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_lists_CityDistrict_District.cs (Line: 148 - 158).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtDistName.Text == "")
            {
                XtraMessageBox.Show("Chọn đối tượng cần xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa bản ghi '" + txtDistName.Text + "' ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM district WHERE dist_id = " + int.Parse(txtDistId.Text), conn);
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (SqlException)
                    {
                        XtraMessageBox.Show("Dữ liệu bản ghi đang được sử dụng, vui lòng xóa các dữ liệu liên quan", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_lists_Bank_Branch.cs (Line: 182 - 185).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btnRefresh_Click(sender, e); // F5
                        btn_flag = null;
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            popform_lists_CityDistrict_District_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (txtDistName.Text == "")
                {
                    XtraMessageBox.Show("Nhập tên quận cần thêm", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        // Khai báo các cột và values cần check duplicate
                        string[] col_names = { "dist_name", "city_id" };
                        string[] col_values = { txtDistName.Text, city_id.ToString() };

                        // Check trùng
                        if (power.Sql_CheckDuplicate("district", col_names, col_values) == false)
                        {
                            conn.Open();
                            cmd = new SqlCommand("a_insert_tb_district", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@dist_name", SqlDbType.NVarChar, 50).Value = txtDistName.Text;
                            cmd.Parameters.Add("@city_id", SqlDbType.Int).Value = city_id;
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("THÊM THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_lists_CityDistrict_District.cs (Line: 218 - 236).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btnRefresh_Click(sender, e); // F5
                        btn_flag = null;
                    }
                }
            }

            if (btn_flag == "edit")
            {
                if (txtDistName.Text == "")
                {
                    XtraMessageBox.Show("Nhập thông tin quận cần sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        // Khai báo các cột và values cần check duplicate
                        string[] col_names = { "dist_name", "city_id" };
                        string[] col_values = { txtDistName.Text, cmbCity.SelectedValue.ToString() };

                        // Check trùng
                        if (power.Sql_CheckDuplicate("district", col_names, col_values) == false)
                        {
                            conn.Open();
                            cmd = new SqlCommand("b_update_tb_district", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@dist_name", SqlDbType.NVarChar, 50).Value = txtDistName.Text;
                            cmd.Parameters.Add("@city_id", SqlDbType.Int).Value = int.Parse(cmbCity.SelectedValue.ToString());
                            cmd.Parameters.Add("@dist_id", SqlDbType.Int).Value = int.Parse(txtDistId.Text);
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_lists_CityDistrict_District.cs (Line: 260 - 280).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btnRefresh_Click(sender, e); // F5
                        btn_flag = null;
                    }
                }
            }
        }

        private void cmbCity_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}