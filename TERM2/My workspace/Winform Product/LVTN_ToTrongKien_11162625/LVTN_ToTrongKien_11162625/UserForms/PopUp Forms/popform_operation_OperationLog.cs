﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.Configuration;

namespace LVTN_ToTrongKien_11162625
{
    public partial class popform_operation_OperationLog : DevExpress.XtraEditors.XtraForm
    {
        string btn_flag = null;

        int partner_id;
        string partner_name;

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        public popform_operation_OperationLog(int partner_id, string partner_name)
        {
            InitializeComponent();
            this.partner_id = partner_id;
            this.partner_name = partner_name;
            conn = new SqlConnection(str);
        }

        private void lblID_Click(object sender, EventArgs e)
        {
            //
        }

        private void popform_operation_OperationLog_Load(object sender, EventArgs e)
        {
            lblPartnerId.Text = partner_id.ToString();
            lblPartnerName.Text = partner_name;
            ComboboxEmployee();
            ShowFullInfo();
            ShowFirstInfo();
            DefaultFormat();
            lblRowCount.Text = grvOpeLog.RowCount.ToString();
            btn_flag = null;
        }

        private void ComboboxEmployee()
        {
            string query = "SELECT 0 as emp_id, N'--Chọn--' as emp_name UNION ALL SELECT emp_id, emp_name FROM employee ORDER BY emp_id";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbEmp.DisplayMember = "emp_name";
            cmbEmp.ValueMember = "emp_id";
            cmbEmp.DataSource = dt;
            cmbEmp.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void DefaultFormat()
        {
            txtOpeTitle.Enabled = false;
            txtOpeTitle.ReadOnly = true;
            txtDetail.Enabled = false;
            txtDetail.ReadOnly = true;
            txtNote.Enabled = false;
            txtNote.ReadOnly = true;
            cmbEmp.Enabled = false;
            dtpOpeDate.Enabled = false;
            dtpOpeDate.Value = DateTime.Today;
            grvOpeLog.Columns["ope_date"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            grvOpeLog.Columns["ope_date"].DisplayFormat.FormatString = "dd/MM/yyyy";

        }

        private void SetCaption()
        {
            string[] columnCaptions = { "Mã bản ghi", "Tiêu đề", "Nội dung vận hành", "Ghi chú", "Ngày vận hành", "Nhân viên phụ trách" };
            power.SetCaption(grvOpeLog, columnCaptions);
        }


        private void ShowFullInfo()
        {
            string tableName = " (SELECT o.id, o.ope_title, o.detail, o.note, o.ope_date, e.emp_name, o.partner_id FROM operation_log o LEFT JOIN employee e ON (o.emp_id = e.emp_id)) a";
            string selectedColumns = " id, ope_title, detail, note, ope_date, emp_name ";
            string afterWhereStr = " partner_id = " + int.Parse(lblPartnerId.Text) + " ORDER BY id"; 

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrOpeLog.DataSource = dt;
            SetCaption();
        }

        private void ShowFirstInfo()
        {
            if (grvOpeLog.RowCount == 0)
            {
                lblID.Text = "";
                txtOpeTitle.ResetText();
                txtDetail.ResetText();
                txtNote.ResetText();
                cmbEmp.Text = "--Chọn--";
                dtpOpeDate.Value = DateTime.Today;
                return;
            }
            else
            {
                lblID.Text = grvOpeLog.GetRowCellValue(0, "id").ToString();
                txtOpeTitle.Text = grvOpeLog.GetRowCellValue(0, "ope_title").ToString();
                txtDetail.Text = grvOpeLog.GetRowCellValue(0, "detail").ToString();
                txtNote.Text = grvOpeLog.GetRowCellValue(0, "note").ToString();
                cmbEmp.Text = grvOpeLog.GetRowCellValue(0, "emp_name").ToString(); //Text if error
                dtpOpeDate.Value = DateTime.Parse(grvOpeLog.GetRowCellValue(0, "ope_date").ToString());
            }
        }

        private void grvOpeLog_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvOpeLog.RowCount == 0)
            {
                lblID.Text = "";
                txtOpeTitle.ResetText();
                txtDetail.ResetText();
                txtNote.ResetText();
                cmbEmp.Text = "--Chọn--";
                dtpOpeDate.Value = DateTime.Today;
                return;
            }
            else
            {
                int focused_id = grvOpeLog.FocusedRowHandle;
                lblID.Text = grvOpeLog.GetFocusedRowCellValue("id").ToString();
                txtOpeTitle.Text = grvOpeLog.GetFocusedRowCellValue("ope_title").ToString();
                txtDetail.Text = grvOpeLog.GetFocusedRowCellValue("detail").ToString();
                txtNote.Text = grvOpeLog.GetFocusedRowCellValue("note").ToString();
                cmbEmp.Text = grvOpeLog.GetFocusedRowCellValue("emp_name").ToString(); //Text if error
                dtpOpeDate.Value = DateTime.Parse(grvOpeLog.GetRowCellValue(focused_id, "ope_date").ToString());
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";
            lblID.Text = "Auto";
            txtOpeTitle.ResetText();
            txtOpeTitle.Enabled = true;
            txtOpeTitle.ReadOnly = false;
            txtNote.ResetText();
            txtNote.Enabled = true;
            txtNote.ReadOnly = false;
            txtDetail.ResetText();
            txtDetail.Enabled = true;
            txtDetail.ReadOnly = false;
            cmbEmp.Enabled = true;
            cmbEmp.SelectedIndex = 0;
            dtpOpeDate.Enabled = true;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            btn_flag = "edit";
            if (lblID.Text == "" || lblID.Text == "ID")
            {
                XtraMessageBox.Show("Chọn một bản ghi để chỉnh sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                txtOpeTitle.Enabled = true;
                txtOpeTitle.ReadOnly = false;
                txtNote.Enabled = true;
                txtNote.ReadOnly = false;
                txtDetail.Enabled = true;
                txtDetail.ReadOnly = false;
                cmbEmp.Enabled = true;
                dtpOpeDate.Enabled = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lblID.Text == "" || lblID.Text == "ID")
            {
                XtraMessageBox.Show("Chọn một bản ghi để xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa bản ghi: '" + lblID.Text + "' ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM operation_log WHERE id = " + int.Parse(lblID.Text), conn);
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_operation_OperationLog.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        popform_operation_OperationLog_Load(sender, e);
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            popform_operation_OperationLog_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (cmbEmp.Text == "--Chọn--" || cmbEmp.Text == "" || txtOpeTitle.Text == "" || txtDetail.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng điền đủ thông tin", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("a_insert_tb_operation_log", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@emp_id", SqlDbType.Int).Value = int.Parse(cmbEmp.SelectedValue.ToString());
                        cmd.Parameters.Add("@partner_id", SqlDbType.Int).Value = partner_id;
                        cmd.Parameters.Add("@ope_title", SqlDbType.NVarChar, 50).Value = txtOpeTitle.Text;
                        cmd.Parameters.Add("@detail", SqlDbType.NVarChar).Value = txtDetail.Text;
                        cmd.Parameters.Add("@note", SqlDbType.NVarChar).Value = txtNote.Text;
                        cmd.Parameters.Add("@ope_date", SqlDbType.Date).Value = dtpOpeDate.Value;
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("TẠO THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_operation_OperationLog.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        popform_operation_OperationLog_Load(sender, e);
                    }
                }
            }

            if (btn_flag == "edit")
            {
                if (cmbEmp.Text == "--Chọn--" || cmbEmp.Text == "" || txtOpeTitle.Text == "" || txtDetail.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng điền đủ thông tin", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("b_update_tb_operation_log", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@emp_id", SqlDbType.Int).Value = int.Parse(cmbEmp.SelectedValue.ToString());
                        cmd.Parameters.Add("@ope_title", SqlDbType.NVarChar, 50).Value = txtOpeTitle.Text;
                        cmd.Parameters.Add("@detail", SqlDbType.NVarChar).Value = txtDetail.Text;
                        cmd.Parameters.Add("@note", SqlDbType.NVarChar).Value = txtNote.Text;
                        cmd.Parameters.Add("@ope_date", SqlDbType.Date).Value = dtpOpeDate.Value;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = int.Parse(lblID.Text);
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_operation_OperationLog.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        popform_operation_OperationLog_Load(sender, e);
                    }
                }
            }
        }
    }
}