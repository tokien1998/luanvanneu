﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class popform_PictureViewer : DevExpress.XtraEditors.XtraForm
    {
        string path;

        public popform_PictureViewer(string path)
        {
            InitializeComponent();
            this.path = path;
        }

        private void popform_PictureViewer_Load(object sender, EventArgs e)
        {
            PictureReader.Image = Image.FromFile(path);
            PictureReader.SizeMode = PictureBoxSizeMode.StretchImage;
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            PrintDialog pdl = new PrintDialog();
            PrintDocument dcm = new PrintDocument();
            dcm.PrintPage += Dcm_PrintPage;
            pdl.Document = dcm;
            if (pdl.ShowDialog() == DialogResult.OK)
            {
                dcm.Print();
            }
        }

        private void Dcm_PrintPage(object sender, PrintPageEventArgs e)
        {
            Bitmap bm = new Bitmap(PictureReader.Width, PictureReader.Height);
            PictureReader.DrawToBitmap(bm, new Rectangle(0, 0, PictureReader.Width, PictureReader.Height));
            e.Graphics.DrawImage(bm, 0, 0);
            bm.Dispose();
        }
    }
}