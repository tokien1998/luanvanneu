﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.Configuration;

namespace LVTN_ToTrongKien_11162625
{
    public partial class popform_lists_Bank_Branch : DevExpress.XtraEditors.XtraForm
    {
        // Khai báo biến
        string btn_flag = null; // Biến xác định nút đang sử dụng

        int bank_id;
        string bank_code;

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();
        public popform_lists_Bank_Branch(int bank_id, string bank_code)
        {
            InitializeComponent();
            this.bank_id = bank_id;
            this.bank_code = bank_code;
            conn = new SqlConnection(str);
        }

        private void simpleButton4_Click(object sender, EventArgs e) // BtnRefresh
        {
            popform_lists_Bank_Branch_Load(sender, e);
        }

        private void popform_lists_Bank_Branch_Load(object sender, EventArgs e)
        {
            btn_flag = null;
            grvBranch.FindFilterText = "";
            grctrBranch.DataSource = null;
            FillComboBoxCity();
            ShowFullData();
            DefaultFormat();
            ShowFirstInfo();
            lblBranchCount.Text = grvBranch.RowCount.ToString();
            chkFilter.Checked = false;
        }

        // Định dạng textbox ban đầu
        private void DefaultFormat()
        {
            cmbCity.Enabled = false;
            cmbDistrict.Enabled = false;
            txtBranchId.Enabled = false;
            txtBranchId.ReadOnly = true;
            txtBranchId.ResetText();
            txtBranchId.Font = new Font(txtBranchId.Font, FontStyle.Regular);
            txtBranchName.Enabled = false;
            txtBranchName.ReadOnly = true;
            txtBranchName.ResetText();
            txtAddress.Enabled = false;
            txtAddress.ReadOnly = true;
            txtAddress.ResetText();
        }

        // Show full data on datagridview
        private void ShowFullData()
        {
            string tableName = @"(SELECT r.branch_id, r.branch_name, r.address, d.dist_name, c.city_name
                                FROM bank b INNER JOIN bank_branch r ON (b.bank_id = r.bank_id)
                                            INNER JOIN district d ON (r.dist_id = d.dist_id)
                                            INNER JOIN city c ON (c.city_id = d.city_id)
                                WHERE b.bank_id = " + bank_id + ") a";
            string selectedColumns = "branch_id, branch_name, address, city_name, dist_name";
            string afterWhereStr = "(1=1) ORDER BY branch_id";

            dt = new DataTable();
            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrBranch.DataSource = dt;
            SetCaption();
        }

        // Set captions
        private void SetCaption()
        {
            string[] columnCaptions = { "Mã chi nhánh", "Tên chi nhánh", "Địa chỉ", "Thành phố", "Quận" };
            power.SetCaption(grvBranch, columnCaptions);
        }

        // Show 1st record on textbox
        private void ShowFirstInfo()
        {
            if (grvBranch.RowCount == 0)
            {
                txtAddress.ResetText();
                txtBranchId.ResetText();
                txtBranchName.ResetText();
                cmbCity.SelectedIndex = 0;
                cmbDistrict.SelectedIndex = 0;
                return;
            }
            else
            {
                txtAddress.Text = grvBranch.GetRowCellValue(0, "address").ToString();
                txtBranchId.Text = grvBranch.GetRowCellValue(0, "branch_id").ToString();
                txtBranchName.Text = grvBranch.GetRowCellValue(0, "branch_name").ToString();
                cmbCity.Text = grvBranch.GetRowCellValue(0, "city_name").ToString();
                cmbDistrict.Text = grvBranch.GetRowCellValue(0, "dist_name").ToString();
            }
        }

        private void FillComboBoxCity()
        {
            string query = "SELECT 0 as city_id, N'--Chọn--' as city_name UNION ALL SELECT city_id, city_name FROM city";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbCity.DisplayMember = "city_name";
            cmbCity.ValueMember = "city_id";
            cmbCity.DataSource = dt;
            cmbCity.DropDownStyle = ComboBoxStyle.DropDownList;
            cmbDistrict.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";

            txtAddress.ResetText();
            txtAddress.Enabled = true;
            txtAddress.ReadOnly = false;
            txtBranchName.ResetText();
            txtBranchName.Enabled = true;
            txtBranchName.ReadOnly = false;
            txtBranchId.Text = "Auto";
            txtBranchId.Enabled = false;
            txtBranchId.ReadOnly = true;
            txtBranchId.Font = new Font(txtBranchId.Font, FontStyle.Italic);
            cmbCity.SelectedIndex = 0;
            cmbCity.Enabled = true;
            cmbDistrict.SelectedIndex = 0;
            cmbDistrict.Enabled = true;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            btn_flag = "edit";

            if (txtBranchName.Text == "")
            {
                XtraMessageBox.Show("Chọn một bản ghi để chỉnh sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                txtAddress.Enabled = true;
                txtAddress.ReadOnly = false;
                txtBranchName.Enabled = true;
                txtBranchName.ReadOnly = false;
                txtBranchId.Enabled = false;
                txtBranchId.ReadOnly = true;
                txtBranchId.Font = new Font(txtBranchId.Font, FontStyle.Regular);
                cmbCity.Enabled = true;
                cmbDistrict.Enabled = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtBranchName.Text == "")
            {
                XtraMessageBox.Show("Chọn một bản ghi để xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa chi nhánh '" + txtBranchName.Text + "' ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();

                        cmd = new SqlCommand("DELETE FROM bank_branch WHERE branch_id = " + txtBranchId.Text, conn);
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_lists_Bank_Branch.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        popform_lists_Bank_Branch_Load(sender, e);
                    }
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (txtBranchName.Text == "" || cmbCity.SelectedIndex == 0 || cmbDistrict.SelectedIndex == 0)
                {
                    XtraMessageBox.Show("Nhập đủ thông tin: \n - Tên chi nhánh \n - Thành phố \n - Quận", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        // Khai báo các cột và values cần check duplicate
                        string[] col_names = { "branch_name", "city_name", "dist_name" };
                        string[] col_values = { txtBranchName.Text, cmbCity.Text, cmbDistrict.Text };

                        conn.Open();

                        string tableName = @"(SELECT r.branch_id, r.branch_name, r.address, d.dist_name, c.city_name
                                FROM bank b INNER JOIN bank_branch r ON (b.bank_id = r.bank_id)
                                            INNER JOIN district d ON (r.dist_id = d.dist_id)
                                            INNER JOIN city c ON (c.city_id = d.city_id)
                                WHERE b.bank_id = " + bank_id + ") a";

                        //Check trùng
                        if (power.Sql_CheckDuplicate(tableName, col_names, col_values) == false)
                        {
                            cmd = new SqlCommand("a_insert_tb_bank_branch", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@branch_name", SqlDbType.NVarChar, 50).Value = txtBranchName.Text;
                            cmd.Parameters.Add("@address", SqlDbType.NVarChar).Value = txtAddress.Text;
                            cmd.Parameters.Add("@dist_id", SqlDbType.Int).Value = cmbDistrict.SelectedValue.ToString();
                            cmd.Parameters.Add("@bank_id", SqlDbType.Int).Value = bank_id;
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("THÊM THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ShowFirstInfo();
                        }
                        else
                        {
                            XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_lists_Bank_Branch.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        popform_lists_Bank_Branch_Load(sender, e);
                    }
                }
            }

            ///

            if (btn_flag == "edit")
            {
                if (txtBranchName.Text == "" || cmbCity.SelectedIndex == 0 || cmbDistrict.SelectedIndex == 0)
                {
                    XtraMessageBox.Show("Nhập đủ thông tin: \n - Tên chi nhánh \n - Thành phố \n - Quận", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("b_update_tb_bank_branch", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@branch_name", SqlDbType.NVarChar, 50).Value = txtBranchName.Text;
                        cmd.Parameters.Add("@address", SqlDbType.NVarChar).Value = txtAddress.Text;
                        cmd.Parameters.Add("@dist_id", SqlDbType.Int).Value = cmbDistrict.SelectedValue.ToString();
                        cmd.Parameters.Add("@branch_id", SqlDbType.Int).Value = txtBranchId.Text;
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        ShowFirstInfo();
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_lists_Bank_Branch.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        popform_lists_Bank_Branch_Load(sender, e);
                    }
                }
            }
        }

        private void grvBranch_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvBranch.RowCount == 0)
            {
                txtAddress.ResetText();
                txtBranchId.ResetText();
                txtBranchName.ResetText();
                cmbCity.SelectedIndex = 0;
                cmbDistrict.SelectedIndex = 0;
                return;
            }
            else
            {
                txtAddress.Text = grvBranch.GetFocusedRowCellValue("address").ToString();
                txtBranchId.Text = grvBranch.GetFocusedRowCellValue("branch_id").ToString();
                txtBranchName.Text = grvBranch.GetFocusedRowCellValue("branch_name").ToString();
                cmbCity.Text = grvBranch.GetFocusedRowCellValue("city_name").ToString();
                cmbDistrict.Text = grvBranch.GetFocusedRowCellValue("dist_name").ToString();
            }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            if (chkFilter.Checked == false)
            {
                XtraMessageBox.Show("Cần chọn chức năng Lọc", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {

                string conCity, conDist, conAdd, conBranchName, conBranchId;

                if (cmbCity.Text == "--Chọn--" || cmbCity.SelectedIndex == 0)
                {
                    conCity = " (1=1) ";
                }
                else
                {
                    conCity = " c.city_id = " + cmbCity.SelectedValue.ToString();
                }

                if (cmbDistrict.Text == "--Chọn--" || cmbDistrict.SelectedIndex == 0)
                {
                    conDist = " (1=1) ";
                }
                else
                {
                    conDist = " d.dist_id = " + cmbDistrict.SelectedValue.ToString();
                }

                if (txtAddress.Text == "")
                {
                    conAdd = " (1=1) ";
                }
                else
                {
                    conAdd = " r.address LIKE N'%" + txtAddress.Text + "%' ";
                }

                if (txtBranchName.Text == "")
                {
                    conBranchName = " (1=1) ";
                }
                else
                {
                    conBranchName = "r.branch_name LIKE N'%" + txtBranchName.Text + "%' ";
                }

                if (txtBranchId.Text == "")
                {
                    conBranchId = " (1=1) ";
                }
                else
                {
                    conBranchId = "r.branch_id = " + txtBranchId.Text;
                }

                string tableName = @"(SELECT r.branch_id, r.branch_name, r.address, d.dist_name, c.city_name
                                FROM bank b INNER JOIN bank_branch r ON (b.bank_id = r.bank_id)
                                            INNER JOIN district d ON (r.dist_id = d.dist_id)
                                            INNER JOIN city c ON (c.city_id = d.city_id)
                                WHERE b.bank_id = " + bank_id +
                                        " AND " + conCity + " AND " + conDist + " AND " + conAdd + " AND " + conBranchId + " AND " + conBranchName + " ) a";
                string selectedColumns = "branch_id, branch_name, address, city_name, dist_name";
                string afterWhereStr = "(1=1) ORDER BY branch_id";

                dt = new DataTable();
                dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
                grctrBranch.DataSource = dt;
                SetCaption();
                ShowFirstInfo();
            }
        }

        private void cmbCity_SelectedIndexChanged(object sender, EventArgs e)
        {
            string city_id = cmbCity.SelectedValue.ToString();
            string query = "SELECT 0 as dist_id, N'--Chọn--' as dist_name UNION ALL SELECT dist_id, dist_name FROM district WHERE city_id = " + city_id;
            dt = new DataTable();
            da = new SqlDataAdapter(query, conn);
            da.Fill(dt);
            cmbDistrict.DisplayMember = "dist_name";
            cmbDistrict.ValueMember = "dist_id";
            cmbDistrict.DataSource = dt;
            conn.Close();
        }

        private void chkFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilter.Checked == true)
            {
                cmbCity.SelectedIndex = 0;
                cmbCity.Enabled = true;
                cmbDistrict.SelectedIndex = 0;
                cmbDistrict.Enabled = true;
                txtAddress.ResetText();
                txtAddress.Enabled = true;
                txtAddress.ReadOnly = false;
                txtBranchName.ResetText();
                txtBranchName.Enabled = true;
                txtBranchName.ReadOnly = false;
                txtBranchId.ResetText();
                txtBranchId.Enabled = true;
                txtBranchId.ReadOnly = false;
            }
        }

        private void cmbDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}