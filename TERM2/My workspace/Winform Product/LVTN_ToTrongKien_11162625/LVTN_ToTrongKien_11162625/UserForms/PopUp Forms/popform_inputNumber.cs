﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class popform_inputNumber : DevExpress.XtraEditors.XtraForm
    {
        string tag, payment_log_id;
        decimal debt;

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        private void popform_inputNumber_Load(object sender, EventArgs e)
        {
            lblTag.Text = tag;
            dtpDateTime.Value = DateTime.Now;
            lblDebt.Text = string.Format("{0:N0}", double.Parse(debt.ToString()));
            txtTotalMoney.ResetText();
        }

        private void btnPaid_Click(object sender, EventArgs e)
        {
            if (power.IsNumber(txtTotalMoney.Text.Replace(" ", "").Replace(",", "")) == false)
            {
                XtraMessageBox.Show("Số tiền nhập sai định dạng!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (txtTotalMoney.Text == "")
            {
                XtraMessageBox.Show("Vui lòng nhập số tiền!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                try
                {
                    conn.Open();
                    cmd = new SqlCommand("[a_insert_tb_payment_transaction]", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@payment_log_id", SqlDbType.BigInt).Value = int.Parse(payment_log_id);
                    cmd.Parameters.Add("@amount", SqlDbType.Decimal).Value = decimal.Parse(txtTotalMoney.Text.Replace(" ", "").Replace(",", ""));
                    cmd.Parameters.Add("@transferred_at", SqlDbType.DateTime).Value = dtpDateTime.Value;
                    cmd.ExecuteNonQuery();

                
                    XtraMessageBox.Show("Thanh toán thành công!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    this.Close();

                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_inputNumber.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                }
            }
        }

        private void txtTotalMoney_TextChanged(object sender, EventArgs e)
        {
            
        }

        public popform_inputNumber(string tag, string payment_log_id, decimal debt)
        {
            InitializeComponent();
            this.tag = tag;
            this.payment_log_id = payment_log_id;
            this.debt = debt;
            conn = new SqlConnection(str);
        }
    }
}