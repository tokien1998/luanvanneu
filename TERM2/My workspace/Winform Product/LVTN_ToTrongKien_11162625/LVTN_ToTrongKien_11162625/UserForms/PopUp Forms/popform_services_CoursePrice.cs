﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.Configuration;

namespace LVTN_ToTrongKien_11162625
{
    public partial class popform_services_CoursePrice : DevExpress.XtraEditors.XtraForm
    {
        string btn_flag = null;

        int course_id;
        string tag;
        string courseTag;

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        public popform_services_CoursePrice(int course_id, string tag, string courseTag)
        {
            InitializeComponent();
            this.course_id = course_id;
            this.tag = tag;
            this.courseTag = courseTag;
            conn = new SqlConnection(str);
        }

        private void ComboboxStatus()
        {
            string query = "SELECT N'--Chọn--' as stt UNION SELECT N'Hoạt động' as stt UNION SELECT N'Vô hiệu hóa' as stt";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbStatus.DisplayMember = "stt";
            cmbStatus.ValueMember = "stt";
            cmbStatus.DataSource = dt;
            cmbStatus.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void AboutCourse()
        {
            lblTag.Text = tag;
            lblCourseId.Text = course_id.ToString();
            lblCourseTag.Text = courseTag;
        }
        private string NumtoText_VND(long number)
        {
            string output = power.NumberToString(number) + " đồng.";
            if (output.Length > 1)
                return char.ToUpper(output[0]) + output.Substring(1); // Viết hoa chữ cái đầu
            return output.ToUpper();
        }

        private void txtUnitPrice_TextChanged(object sender, EventArgs e)
        {
            lblNumText.Visible = true;
            try
            {
                double value = double.Parse(txtUnitPrice.Text.Replace(" ", "").Replace(",","")); // loại ký tự trống và ký tự phẩy
                long number_vnd = Convert.ToInt64(value);
                lblNumText.Text = NumtoText_VND(number_vnd);
            }
            catch
            {
                lblNumText.Text = "Số được nhập sai định dạng!";
            }
        }

        private void DefaultFormat()
        {
            AboutCourse();
            lblID.Visible = false;
            lblNumText.Visible = false;
            txtUnitPrice.Enabled = false;
            txtUnitPrice.ReadOnly = true;
            cmbStatus.Enabled = false;
            grvCoursePrice.Columns["created_at"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            grvCoursePrice.Columns["created_at"].DisplayFormat.FormatString = "dd/MM/yyyy hh:mm:ss";
            grvCoursePrice.Columns["updated_at"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            grvCoursePrice.Columns["updated_at"].DisplayFormat.FormatString = "dd/MM/yyyy hh:mm:ss";
            grvCoursePrice.Columns["unit_price"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            grvCoursePrice.Columns["unit_price"].DisplayFormat.FormatString = "N";
        }

        private void SetCaption()
        {
            string[] columnCaptions = { "Mã bản ghi", "Đơn giá", "Trạng thái", "Thời gian tạo", "Thời gian cập nhật gần nhất" };
            power.SetCaption(grvCoursePrice, columnCaptions);
        }

        private void ShowFullInfo()
        {
            string tableName = "course_price";
            string selectedColumns = "id, unit_price, status, created_at, updated_at";
            string afterWhereStr = "course_id = " + course_id + " ORDER BY course_id"; // Những giá cũ đã cài đặt

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrCoursePrice.DataSource = dt;
            SetCaption();
        }

        private void CurrentPrice()
        {
            lblNumText.Visible = true;
            lblID.Visible = true;
            conn.Open();
            da = new SqlDataAdapter("SELECT id, unit_price, status FROM course_price WHERE status = N'Hoạt động' AND course_id = " + course_id, conn);
            dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count.ToString() == "0")
            {
                txtUnitPrice.Text = "Dịch vụ chưa cập nhật giá!";
                lblID.ResetText();
                lblNumText.Text = "";
            }
            else
            {
                txtUnitPrice.Text = double.Parse(dt.Rows[0]["unit_price"].ToString()).ToString();
                cmbStatus.SelectedValue = dt.Rows[0]["status"];
                lblID.Text = dt.Rows[0]["id"].ToString();
            }
            conn.Close();
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";
            txtUnitPrice.ResetText();
            txtUnitPrice.Enabled = true;
            txtUnitPrice.ReadOnly = false;
            lblNumText.Text = "";
            cmbStatus.Enabled = false;

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            btn_flag = "edit";
            txtUnitPrice.Enabled = true;
            txtUnitPrice.ReadOnly = false;
            cmbStatus.Enabled = true;

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Xác nhận hủy bỏ giá hiện tại? \n Lưu ý: Đơn giá sẽ được vô hiệu hóa và ghi vào lịch sử.", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                try
                {
                    conn.Open();
                    cmd = new SqlCommand("b_update_tb_course_price", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = int.Parse(lblID.Text);
                    cmd.Parameters.Add("@unit_price", SqlDbType.Decimal).Value = decimal.Parse(txtUnitPrice.Text.Replace(" ", "").Replace(",", ""));
                    cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = "Vô hiệu hóa";
                    cmd.ExecuteNonQuery();

                    XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_services_CoursesPrice.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                    btn_flag = null;
                    popform_services_CoursePrice_Load(sender, e);
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            popform_services_CoursePrice_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                try
                {
                    conn.Open();
                    cmd = new SqlCommand("a_insert_tb_course_price", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@course_id", SqlDbType.Int).Value = course_id;
                    cmd.Parameters.Add("@unit_price", SqlDbType.Decimal).Value = decimal.Parse(txtUnitPrice.Text.Replace(" ", "").Replace(",", ""));
                    cmd.ExecuteNonQuery();
                    XtraMessageBox.Show("TẠO THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_services_Courses.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                    btn_flag = null;
                    popform_services_CoursePrice_Load(sender, e);
                }
            }

            if (btn_flag == "edit")
            {
                try
                {
                    conn.Open();
                    cmd = new SqlCommand("b_update_tb_course_price", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = int.Parse(lblID.Text);
                    cmd.Parameters.Add("@unit_price", SqlDbType.Decimal).Value = decimal.Parse(txtUnitPrice.Text.Replace(" ", "").Replace(",", ""));
                    cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = cmbStatus.SelectedValue.ToString();
                    cmd.ExecuteNonQuery();
                    XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_services_Courses.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                    btn_flag = null;
                    popform_services_CoursePrice_Load(sender, e);
                }
            }
        }

        private void popform_services_CoursePrice_Load(object sender, EventArgs e)
        {
            btn_flag = null;
            ComboboxStatus();
            AboutCourse();
            ShowFullInfo();
            DefaultFormat();
            CurrentPrice();
            lblRowCount.Text = grvCoursePrice.RowCount.ToString();
        }

        private void grvCoursePrice_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            lblID.Text = grvCoursePrice.GetFocusedRowCellValue("id").ToString();
            txtUnitPrice.Text = grvCoursePrice.GetFocusedRowCellValue("unit_price").ToString();
            cmbStatus.SelectedValue = grvCoursePrice.GetFocusedRowCellValue("status");
        }
    }
}