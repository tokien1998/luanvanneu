﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.Configuration;

namespace LVTN_ToTrongKien_11162625
{
    public partial class popform_system_Account : DevExpress.XtraEditors.XtraForm
    {
        // Khai báo biến
        string btn_flag = null; // Biến xác định nút đang sử dụng

        int emp_id;
        string emp_name;

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        public popform_system_Account(int emp_id,string emp_name)
        {
            InitializeComponent();
            this.emp_id = emp_id;
            this.emp_name = emp_name;
            conn = new SqlConnection(str);
        }

        private void DefaultFormat()
        {
            lblAccountId.Text = "";
            txtPassword.Enabled = false;
            txtPassword.ReadOnly = true;
            txtUserName.Enabled = false;
            txtUserName.ReadOnly = true;
            txtPassword.UseSystemPasswordChar = true;
        }

        private void ShowRecord()
        {
            string query = "SELECT account_id, username, password FROM account WHERE emp_id = " + emp_id;
            conn.Open();
            dt = new DataTable();
            da = new SqlDataAdapter(query, conn);
            da.Fill(dt);
            if (dt.Rows.Count == 0)
            {
                lblAccountId.Text = "Nhân viên chưa có tài khoản";
                txtUserName.Text = "";
                txtPassword.Text = "";
            }
            else
            {
                lblAccountId.Text = dt.Rows[0]["account_id"].ToString();
                txtUserName.Text = dt.Rows[0]["username"].ToString();
                txtPassword.Text = dt.Rows[0]["password"].ToString();
            }
            conn.Close();
        }

        private void grvAccount_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            //
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (lblAccountId.Text == "" || lblAccountId.Text == "account_id" || lblAccountId.Text == "Nhân viên chưa có tài khoản")
            {
                btn_flag = "create";
                txtUserName.Enabled = true;
                txtUserName.ReadOnly = false;
                txtUserName.ResetText();
                txtPassword.Enabled = true;
                txtPassword.ReadOnly = false;
                txtPassword.ResetText();
            }
            else
            {
                XtraMessageBox.Show("Nhân viên đã có tài khoản!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (lblAccountId.Text == "" || lblAccountId.Text == "account_id")
            {
                XtraMessageBox.Show("Nhân viên chưa có tài khoản!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                btn_flag = "edit";
                txtUserName.Enabled = false;
                txtUserName.ReadOnly = true;
                txtPassword.Enabled = true;
                txtPassword.ReadOnly = false;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lblAccountId.Text == "" || lblAccountId.Text == "account_id" || lblAccountId.Text == "Nhân viên chưa có tài khoản")
            {
                XtraMessageBox.Show("Vui lòng chọn tài khoản để xóa!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa tài khoản '" + lblAccountId.Text + "' của nhân viên " + emp_name + " ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM account WHERE account_id = " + int.Parse(lblAccountId.Text), conn);
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_system_Account (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        popform_system_Account_Load(sender, e); // F5
                        btn_flag = null;
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            popform_system_Account_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (txtUserName.Text == "" || txtPassword.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng nhập đủ thông tin tài khoản", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {

                        conn.Open();
                        cmd = new SqlCommand("a_insert_tb_account", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@username", SqlDbType.VarChar, 50).Value = txtUserName.Text;
                        cmd.Parameters.Add("@password", SqlDbType.VarChar, 50).Value = txtPassword.Text;
                        cmd.Parameters.Add("@emp_id", SqlDbType.Int).Value = int.Parse(lblEmpId.Text);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("THÊM THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_system_Account (Line x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        popform_system_Account_Load(sender, e);
                        btn_flag = null;
                    }
                }
            }

            if (btn_flag == "edit")
            {
                if (txtUserName.Text == "" || txtPassword.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng nhập đủ thông tin của nhân viên", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {

                        conn.Open();
                        cmd = new SqlCommand("b_update_tb_account", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@password", SqlDbType.VarChar, 50).Value = txtPassword.Text;
                        cmd.Parameters.Add("@account_id", SqlDbType.Int).Value = int.Parse(lblAccountId.Text);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_system_Account (Line 1 - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        popform_system_Account_Load(sender, e);
                        btn_flag = null;
                    }
                }
            }
        }

        private void popform_system_Account_Load(object sender, EventArgs e)
        {
            btn_flag = null;
            DefaultFormat();
            ShowRecord();
            chkShow.Checked = false;
            lblEmpId.Text = emp_id.ToString();
            lblEmpName.Text = emp_name;
        }

        private void chkShow_CheckedChanged(object sender, EventArgs e)
        {
            if (chkShow.Checked == true)
            {
                txtPassword.UseSystemPasswordChar = false;
            }
            else
            {
                txtPassword.UseSystemPasswordChar = true;
            }
        }
    }
}