﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.Configuration;

namespace LVTN_ToTrongKien_11162625
{
    public partial class popform_lists_Studio_Manager : DevExpress.XtraEditors.XtraForm
    {
        // Khai báo biến
        string btn_flag = null; // Biến xác định nút đang sử dụng

        int studioId;
        string tag;

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        public popform_lists_Studio_Manager(int studioId, string tag)
        {
            InitializeComponent();
            this.studioId = studioId;
            this.tag = tag;
            conn = new SqlConnection(str);
        }

        private void StatusCombobox()
        {
            cmbStatus.DataSource = null;

            Dictionary<string, string> sttSource = new Dictionary<string, string>();
            sttSource.Add("Chọn", "--Chọn--");
            sttSource.Add("Hoạt động", "Hoạt động");
            sttSource.Add("Vô hiệu hóa", "Vô hiệu hóa");

            cmbStatus.DataSource = new BindingSource(sttSource, null);
            cmbStatus.DisplayMember = "Value";
            cmbStatus.ValueMember = "Key";
            cmbStatus.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void ShowFullData()
        {
            string tableName = "studio_manager";
            string selectedColumns = "id, manager_name, phone, email, role, status";
            string afterWhereStr = "studio_id = " + studioId + " ORDER BY id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrManager.DataSource = dt;
            SetCaption();
        }

        private void SetCaption()
        {
            string[] columnCaptions = { "Mã quản lý", "Tên quản lý", "Số điện thoại", "Email", "Vai trò", "Trạng thái" };
            power.SetCaption(grvManager, columnCaptions);
        }

        private void DefaultFormat()
        {
            lblStudioId.Text = studioId.ToString();
            lblTag.Text = tag;
            txtManagerName.Enabled = false;
            txtManagerName.ReadOnly = true;
            txtManagerName.ResetText();
            txtManagerId.Font = new Font(txtManagerId.Font, FontStyle.Regular);
            txtManagerId.Enabled = false;
            txtManagerId.ReadOnly = true;
            txtManagerId.ResetText();
            txtEmail.Enabled = false;
            txtEmail.ReadOnly = true;
            txtEmail.ResetText();
            txtPhone.Enabled = false;
            txtPhone.ReadOnly = true;
            txtPhone.ResetText();
            txtRole.Enabled = false;
            txtRole.ReadOnly = true;
            txtRole.ResetText();
            StatusCombobox();
            cmbStatus.SelectedIndex = 0;
            cmbStatus.Enabled = false;
        }

        private void ShowFirstInfo()
        {
            if (grvManager.RowCount == 0)
            {
                txtEmail.ResetText();
                cmbStatus.SelectedIndex = 0;
                txtManagerName.ResetText();
                txtPhone.ResetText();
                txtRole.ResetText();
                txtManagerId.ResetText();
                return;
            }
            else
            {
                txtManagerId.Text = grvManager.GetRowCellValue(0, "id").ToString();
                txtManagerName.Text = grvManager.GetRowCellValue(0, "manager_name").ToString();
                txtPhone.Text = grvManager.GetRowCellValue(0, "phone").ToString();
                txtEmail.Text = grvManager.GetRowCellValue(0, "email").ToString();
                txtRole.Text = grvManager.GetRowCellValue(0, "role").ToString();
                cmbStatus.SelectedValue = grvManager.GetRowCellValue(0, "status");
            }
        }

        private void grvManager_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvManager.RowCount == 0)
            {
                //Nếu grid view ko có dữ liệu thì ko hiện lên textbox để tránh lỗi "Object reference not set to an instance of an object"
                txtEmail.ResetText();
                txtManagerId.ResetText();
                txtManagerName.ResetText();
                txtPhone.ResetText();
                txtRole.ResetText();
                cmbStatus.SelectedIndex = 0;
                return;
            }
            else
            {
                txtManagerId.Font = new Font(txtManagerId.Font, FontStyle.Regular);
                txtEmail.Text = grvManager.GetFocusedRowCellValue("email").ToString();
                txtManagerId.Text = grvManager.GetFocusedRowCellValue("id").ToString();
                txtManagerName.Text = grvManager.GetFocusedRowCellValue("manager_name").ToString();
                txtPhone.Text = grvManager.GetFocusedRowCellValue("phone").ToString();
                txtRole.Text = grvManager.GetFocusedRowCellValue("role").ToString();
                cmbStatus.SelectedValue = grvManager.GetFocusedRowCellValue("status");
            }

            //DefaultFormat();
            btn_flag = null;
        }

        private void popform_lists_Studio_Manager_Load(object sender, EventArgs e)
        {
            btn_flag = null;
            DefaultFormat();
            ShowFullData();
            SetCaption();
            ShowFirstInfo();
            grvManager.FindFilterText = "";
            lblRowCount.Text = grvManager.RowCount.ToString();
        }

        private void CreateFormat()
        {
            txtManagerName.Enabled = true;
            txtManagerName.ReadOnly = false;
            txtManagerName.ResetText();
            txtManagerId.Font = new Font(txtManagerId.Font, FontStyle.Italic);
            txtManagerId.Enabled = false;
            txtManagerId.ReadOnly = true;
            txtManagerId.Text = "Auto";
            txtEmail.Enabled = true;
            txtEmail.ReadOnly = false;
            txtEmail.ResetText();
            txtPhone.Enabled = true;
            txtPhone.ReadOnly = false;
            txtPhone.ResetText();
            txtRole.Enabled = true;
            txtRole.ReadOnly = false;
            txtRole.ResetText();
            cmbStatus.SelectedIndex = 0;
            cmbStatus.Enabled = false;
        }

        private void EditFormat()
        {
            txtManagerName.Enabled = true;
            txtManagerName.ReadOnly = false;
            txtManagerId.Font = new Font(txtManagerId.Font, FontStyle.Regular);
            txtEmail.Enabled = true;
            txtEmail.ReadOnly = false;
            txtPhone.Enabled = true;
            txtPhone.ReadOnly = false;
            txtRole.Enabled = true;
            txtRole.ReadOnly = false;
            cmbStatus.Enabled = true;
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";
            CreateFormat();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            btn_flag = "edit";
            if (txtManagerId.Text == "")
            {
                XtraMessageBox.Show("Chọn một bản ghi để sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                EditFormat();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (txtManagerId.Text == "")
            {
                XtraMessageBox.Show("Chọn một bản ghi để sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa quản lý '" + txtManagerName.Text + "' ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM studio_manager WHERE id = " + int.Parse(txtManagerId.Text), conn);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_lists_Studio_Manager.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        popform_lists_Studio_Manager_Load(sender, e);
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            popform_lists_Studio_Manager_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (txtManagerName.Text == "" || txtPhone.Text == "")
                {
                    XtraMessageBox.Show("Nhập đủ thông tin cần thiết: \n - Tên quản lý \n - Số điện thoại", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (power.IsNumber(txtPhone.Text.Replace(" ", "")) == false)
                {
                    XtraMessageBox.Show("Số điện thoại nhập không đúng định dạng", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (txtEmail.Text.Contains("@") == false)
                {
                    XtraMessageBox.Show("Email nhập không đúng định dạng", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        // Khai báo các cột và values cần check duplicate
                        string[] col_names = { "manager_name", "phone", "email" };
                        string[] col_values = { txtManagerName.Text, txtPhone.Text, txtEmail.Text };

                        conn.Open();
                        if (power.IsNumber(txtPhone.Text.Replace(" ", "")) == true && txtEmail.Text.Contains("@") == true)
                        {
                            //Check trùng
                            if (power.Sql_CheckDuplicate("studio_manager", col_names, col_values) == false)
                            {
                                cmd = new SqlCommand("a_insert_tb_studio_manager", conn);
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = studioId;
                                cmd.Parameters.Add("@manager_name", SqlDbType.NVarChar, 50).Value = txtManagerName.Text;
                                cmd.Parameters.Add("@phone", SqlDbType.VarChar, 13).Value = txtPhone.Text.Replace(" ", "");
                                cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = txtEmail.Text.Replace(" ", "");
                                cmd.Parameters.Add("@role", SqlDbType.NVarChar, 50).Value = txtRole.Text;
                                cmd.ExecuteNonQuery();
                                XtraMessageBox.Show("TẠO THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                ShowFirstInfo();
                            }
                            else
                            {
                                XtraMessageBox.Show("Bản ghi đã tồn tại", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            XtraMessageBox.Show("Số điện thoại hoặc Email nhập sai định dạng", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_lists_Studio_Manager.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        popform_lists_Studio_Manager_Load(sender, e);
                    }
                }
            }


            if (btn_flag == "edit")
            {
                if (txtManagerName.Text == "" || txtPhone.Text == "" || cmbStatus.SelectedIndex == 0)
                {
                    XtraMessageBox.Show("Nhập đủ thông tin cần thiết: \n - Tên quản lý \n - Số điện thoại \n - Trạng thái hoạt động", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (power.IsNumber(txtPhone.Text.Replace(" ", "")) == false)
                {
                    XtraMessageBox.Show("Số điện thoại nhập không đúng định dạng", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (txtEmail.Text.Contains("@") == false)
                {
                    XtraMessageBox.Show("Email nhập không đúng định dạng", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        conn.Open();
                        if (power.IsNumber(txtPhone.Text.Replace(" ", "")) == true && txtEmail.Text.Contains("@") == true)
                        {
                            cmd = new SqlCommand("b_update_tb_studio_manager", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@id", SqlDbType.Int).Value = int.Parse(txtManagerId.Text);
                            cmd.Parameters.Add("@manager_name", SqlDbType.NVarChar, 50).Value = txtManagerName.Text;
                            cmd.Parameters.Add("@phone", SqlDbType.VarChar, 13).Value = txtPhone.Text.Replace(" ", "");
                            cmd.Parameters.Add("@email", SqlDbType.VarChar, 50).Value = txtEmail.Text.Replace(" ", "");
                            cmd.Parameters.Add("@role", SqlDbType.NVarChar, 50).Value = txtRole.Text;
                            cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = cmbStatus.SelectedValue.ToString();
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("CẬP NHẬT THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ShowFirstInfo();
                        }
                        else
                        {
                            XtraMessageBox.Show("Số điện thoại hoặc Email nhập sai định dạng", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: popform_lists_Studio_Manager.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        popform_lists_Studio_Manager_Load(sender, e);
                    }
                }
            }
        }
    }
}