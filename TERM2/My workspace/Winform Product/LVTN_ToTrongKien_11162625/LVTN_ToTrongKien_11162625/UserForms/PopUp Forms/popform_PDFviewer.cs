﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class popform_PDFviewer : DevExpress.XtraEditors.XtraForm
    {
        string path;

        public popform_PDFviewer(string path)
        {
            InitializeComponent();
            this.path = path;
        }

        private void popform_PDFviewer_Load(object sender, EventArgs e)
        {
            PDFReader.src = path; //HIển thị file từ đường dẫn truyền vào
        }
    }
}