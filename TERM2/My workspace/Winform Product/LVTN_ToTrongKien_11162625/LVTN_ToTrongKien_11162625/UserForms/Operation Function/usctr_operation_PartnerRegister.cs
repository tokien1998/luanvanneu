﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_operation_PartnerRegister : UserControl
    {
        // Khai báo biến
        private static usctr_operation_PartnerRegister usctr; // User control
        string btn_flag = null; // Biến xác định nút đang sử dụng

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_operation_PartnerRegister UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_operation_PartnerRegister();
                }
                return usctr;
            }
        }

        public usctr_operation_PartnerRegister()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void DefaultFormat()
        {
            txtAddress.Enabled = false;
            txtAddress.ReadOnly = true;
            txtManagerName.Enabled = false;
            txtManagerName.ReadOnly = true;
            txtPartnerName.Enabled = false;
            txtPartnerName.ReadOnly = true;
            txtPhone.Enabled = false;
            txtPhone.ReadOnly = true;
            dtpRegisterDate.Enabled = false;
            grvPartnerRegister.Columns["register_date"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            grvPartnerRegister.Columns["register_date"].DisplayFormat.FormatString = "dd/MM/yyyy";
        }

        private void SetCaption()
        {
            string[] columnCaptions = {"Mã yêu cầu", "Tên tổ chức", "Tên quản lý", "Địa chỉ", "Số điện thoại", "Ngày yêu cầu"};
            power.SetCaption(grvPartnerRegister, columnCaptions);
        }

        // Show dữ liệu
        private void ShowFullInfo()
        {
            string tableName = "partner_register";
            string selectedColumns = "partner_id, partner_name, manager_name, address, phone, register_date";
            string afterWhereStr = " (1=1) ORDER BY partner_id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrPartnerRegister.DataSource = dt;
            SetCaption();
        }

        private void ShowFirstInfo()
        {
            if (grvPartnerRegister.RowCount == 0)
            {
                lblPartnerId.Text = "";
                txtAddress.ResetText();
                txtManagerName.ResetText();
                txtPartnerName.ResetText();
                txtPhone.ResetText();
                dtpRegisterDate.Value = DateTime.Today;
                return;
            }
            else
            {
                lblPartnerId.Text = grvPartnerRegister.GetRowCellValue(0, "partner_id").ToString();
                txtAddress.Text = grvPartnerRegister.GetRowCellValue(0, "address").ToString();
                txtManagerName.Text = grvPartnerRegister.GetRowCellValue(0, "manager_name").ToString();
                txtPartnerName.Text = grvPartnerRegister.GetRowCellValue(0, "partner_name").ToString();
                txtPhone.Text = grvPartnerRegister.GetRowCellValue(0, "phone").ToString();
                dtpRegisterDate.Value = DateTime.Parse(grvPartnerRegister.GetRowCellValue(0, "register_date").ToString());

            }
        }

        private void usctr_operation_PartnerRegister_Load(object sender, EventArgs e)
        {
            btn_flag = null;
            grctrPartnerRegister.DataSource = null;
            ShowFullInfo();
            ShowFirstInfo();
            DefaultFormat();
            lblRowCount.Text = grvPartnerRegister.RowCount.ToString();
        }

        private void grvPartnerRegister_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvPartnerRegister.RowCount == 0)
            {
                //Nếu grid view ko có dữ liệu thì ko hiện lên textbox để tránh lỗi "Object reference not set to an instance of an object"
                lblPartnerId.Text = "";
                txtAddress.ResetText();
                txtManagerName.ResetText();
                txtPartnerName.ResetText();
                txtPhone.ResetText();
                dtpRegisterDate.Value = DateTime.Today;
                return;
            }
            else
            {
                int focused_id = grvPartnerRegister.FocusedRowHandle;
                lblPartnerId.Text = grvPartnerRegister.GetFocusedRowCellValue("partner_id").ToString();
                txtAddress.Text = grvPartnerRegister.GetFocusedRowCellValue("address").ToString();
                txtManagerName.Text = grvPartnerRegister.GetFocusedRowCellValue("manager_name").ToString();
                txtPartnerName.Text = grvPartnerRegister.GetFocusedRowCellValue("partner_name").ToString();
                txtPhone.Text = grvPartnerRegister.GetFocusedRowCellValue("phone").ToString();
                dtpRegisterDate.Value = DateTime.Parse(grvPartnerRegister.GetRowCellValue(focused_id, "register_date").ToString());

                btn_flag = null;
            }

        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";
            lblPartnerId.Text = "Auto";
            txtAddress.ResetText();
            txtAddress.Enabled = true;
            txtAddress.ReadOnly = false;
            txtManagerName.ResetText();
            txtManagerName.Enabled = true;
            txtManagerName.ReadOnly = false;
            txtPartnerName.ResetText();
            txtPartnerName.Enabled = true;
            txtPartnerName.ReadOnly = false;
            txtPhone.ResetText();
            txtPhone.Enabled = true;
            txtPhone.ReadOnly = false;
            dtpRegisterDate.Enabled = true;
            dtpRegisterDate.Value = DateTime.Today;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            btn_flag = "edit";
            if (lblPartnerId.Text == "" || lblPartnerId.Text == "partner_id")
            {
                XtraMessageBox.Show("Chọn một bản ghi để chỉnh sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                txtAddress.Enabled = true;
                txtAddress.ReadOnly = false;
                txtManagerName.Enabled = true;
                txtManagerName.ReadOnly = false;
                txtPartnerName.Enabled = true;
                txtPartnerName.ReadOnly = false;
                txtPhone.Enabled = true;
                txtPhone.ReadOnly = false;
                dtpRegisterDate.Enabled = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lblPartnerId.Text == "" || lblPartnerId.Text == "partner_id")
            {
                XtraMessageBox.Show("Chọn một mã yêu cầu để xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa mã yêu cầu: " + lblPartnerId.Text + " ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM partner_register WHERE partner_id = " + int.Parse(lblPartnerId.Text), conn);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_PartnerRegister.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        usctr_operation_PartnerRegister_Load(sender, e);
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_operation_PartnerRegister_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (txtAddress.Text == "" || txtPartnerName.Text == "" || txtPhone.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng điền đủ thông tin", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (power.IsNumber(txtPhone.Text.Replace(" ", "")) == false)
                {
                    XtraMessageBox.Show("Giá trị số nhập sai định dạng", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        conn.Open();

                        cmd = new SqlCommand("a_insert_tb_partner_register", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@partner_name", SqlDbType.NVarChar, 50).Value = txtPartnerName.Text;
                        cmd.Parameters.Add("@address", SqlDbType.NVarChar).Value = txtAddress.Text;
                        cmd.Parameters.Add("@phone", SqlDbType.VarChar, 50).Value = txtPhone.Text.Replace(" ", "");
                        cmd.Parameters.Add("@manager_name", SqlDbType.NVarChar, 50).Value = txtManagerName.Text;
                        cmd.Parameters.Add("@register_date", SqlDbType.Date).Value = dtpRegisterDate.Value;
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("TẠO THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_PartnerRegister.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;

                        usctr_operation_PartnerRegister_Load(sender, e);

                        lblRowCount.Text = grvPartnerRegister.RowCount.ToString();
                    }
                }
            }


            if (btn_flag == "edit")
            {
                if (txtAddress.Text == "" || txtPartnerName.Text == "" || txtPhone.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng điền đủ thông tin", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (power.IsNumber(txtPhone.Text.Replace(" ", "")) == false)
                {
                    XtraMessageBox.Show("Giá trị số nhập sai định dạng", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        conn.Open();

                        cmd = new SqlCommand("b_update_tb_partner_register", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@partner_name", SqlDbType.NVarChar, 50).Value = txtPartnerName.Text;
                        cmd.Parameters.Add("@address", SqlDbType.NVarChar).Value = txtAddress.Text;
                        cmd.Parameters.Add("@phone", SqlDbType.VarChar, 50).Value = txtPhone.Text.Replace(" ", "");
                        cmd.Parameters.Add("@manager_name", SqlDbType.NVarChar, 50).Value = txtManagerName.Text;
                        cmd.Parameters.Add("@register_date", SqlDbType.Date).Value = dtpRegisterDate.Value;
                        cmd.Parameters.Add("@partner_id", SqlDbType.Int).Value = int.Parse(lblPartnerId.Text);
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_PartnerRegister.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;

                        usctr_operation_PartnerRegister_Load(sender, e);

                        lblRowCount.Text = grvPartnerRegister.RowCount.ToString();
                    }
                }
            }
        }

        private void btnOpeLog_Click(object sender, EventArgs e)
        {
            if (lblPartnerId.Text == "" || lblPartnerId.Text == "partner_id")
            {
                XtraMessageBox.Show("Không tìm thấy mã yêu cầu", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                int partner_id = int.Parse(lblPartnerId.Text);
                string partner_name = txtPartnerName.Text;
                popform_operation_OperationLog frm = new popform_operation_OperationLog(partner_id, partner_name);
                frm.ShowDialog();
            }
        }
    }
}
