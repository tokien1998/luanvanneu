﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using DevExpress.XtraEditors;
using System.IO;
using System.Drawing.Printing;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_operation_CourseSchedule : UserControl
    {
        // Khai báo biến
        private static usctr_operation_CourseSchedule usctr; // User control
        string btn_flag = null; // Biến xác định nút đang sử dụng

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Biến dùng cho Pictures
        string pictureMainFolder = Path.Combine(Application.StartupPath, "_pictures/Course_Schedules"); // Đường dẫn folder chứa file ảnh (chưa có folder của từng đối tác)

        // Thuộc tính
        public static usctr_operation_CourseSchedule UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_operation_CourseSchedule();
                }
                return usctr;
            }
        }

        public usctr_operation_CourseSchedule()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        //ComboBox
        private void ComboboxStudio()
        {
            string query = "SELECT 0 as studio_id, N'--Chọn--' as tag UNION ALL SELECT studio_id, tag FROM studio ORDER BY studio_id";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbStudio.DisplayMember = "tag";
            cmbStudio.ValueMember = "studio_id";
            cmbStudio.DataSource = dt;
            cmbStudio.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void ComboboxScheduleType()
        {
            string query = "SELECT N'--Chọn--' as type UNION ALL SELECT N'Lịch cố định' UNION ALL SELECT N'Lịch tuần' UNION ALL SELECT N'Lịch tháng' UNION ALL SELECT N'Lịch tùy chọn'";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbScheduleType.DisplayMember = "type";
            cmbScheduleType.ValueMember = "type";
            cmbScheduleType.DataSource = dt;
            cmbScheduleType.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void cmbStudio_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowFullInfo();
            lblRowCount.Text = grvCourseSchedule.RowCount.ToString();
            ShowFirstInfo();
            conn.Close();
        }

        private void DefaultFormat()
        {
            cmbScheduleType.Enabled = false;
            cmbScheduleType.SelectedIndex = 0;
            cmbStudio.Enabled = true;
            cmbStudio.SelectedIndex = 0;
            dtpExpireDate.Value = DateTime.Today;
            dtpExpireDate.Enabled = false;
            dtpValidDate.Value = DateTime.Today;
            dtpValidDate.Enabled = false;
            txtLink.Enabled = false;
            txtLink.ReadOnly = true;
            btnBrowseImage.Enabled = false;
            grvCourseSchedule.Columns["valid_date"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            grvCourseSchedule.Columns["valid_date"].DisplayFormat.FormatString = "dd/MM/yyyy";
            grvCourseSchedule.Columns["expire_date"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            grvCourseSchedule.Columns["expire_date"].DisplayFormat.FormatString = "dd/MM/yyyy";
        }

        private void SetCaption()
        {
            string[] columnCaptions = {"Mã lịch", "Đối tác", "Loại lịch", "Ngày hiệu lực", "Ngày hết hạn", "Đường dẫn hình ảnh"};
            power.SetCaption(grvCourseSchedule, columnCaptions);
        }

        private void ShowFullInfo()
        {
            string studio_id = cmbStudio.SelectedValue.ToString();

            string tableName = @"(SELECT cs.schedule_id, s.tag as studio_tag, cs.schedule_type, cs.valid_date, cs.expire_date, cs.link
                                    FROM course_schedule cs LEFT JOIN studio s ON (cs.studio_id = s.studio_id)
                                    WHERE s.studio_id = " + studio_id + ") a";
            string selectedColumns = "schedule_id, studio_tag, schedule_type, valid_date, expire_date, link";
            string afterWhereStr = " (1=1) ORDER BY schedule_id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrCourseSchedule.DataSource = dt;
            SetCaption();
        }

        private void ShowFirstInfo()
        {
            if (grvCourseSchedule.RowCount == 0)
            {
                //cmbStudio.SelectedIndex = 0;
                cmbScheduleType.SelectedIndex = 0;

                dtpValidDate.Value = DateTime.Today;
                dtpExpireDate.Value = DateTime.Today;

                txtLink.ResetText();

                lblScheduleId.Text = "";
                return;
            }
            else
            {
                lblScheduleId.Text = grvCourseSchedule.GetRowCellValue(0, "schedule_id").ToString();

                cmbScheduleType.SelectedValue = grvCourseSchedule.GetRowCellValue(0, "schedule_type");
                cmbStudio.Text = grvCourseSchedule.GetRowCellValue(0, "studio_tag").ToString();

                txtLink.Text = grvCourseSchedule.GetRowCellValue(0, "link").ToString();

                dtpValidDate.Value = DateTime.Parse(grvCourseSchedule.GetRowCellValue(0, "valid_date").ToString());
                dtpExpireDate.Value = DateTime.Parse(grvCourseSchedule.GetRowCellValue(0, "expire_date").ToString());

            }
        }

        private void usctr_operation_CourseSchedule_Load(object sender, EventArgs e)
        {
            lblScheduleId.Text = "";
            btn_flag = null;
            ComboboxScheduleType();
            ComboboxStudio();
            grctrCourseSchedule.DataSource = null;
            ShowFullInfo();
            ShowFirstInfo();
            DefaultFormat();
            lblRowCount.Text = grvCourseSchedule.RowCount.ToString();
        }

        private void grvCourseSchedule_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvCourseSchedule.RowCount == 0)
            {
                cmbStudio.SelectedIndex = 0;
                cmbScheduleType.SelectedIndex = 0;

                dtpValidDate.Value = DateTime.Today;
                dtpExpireDate.Value = DateTime.Today;

                txtLink.ResetText();

                lblScheduleId.Visible = false;
                return;
            }
            else
            {
                int focused_id = grvCourseSchedule.FocusedRowHandle;
                lblScheduleId.Visible = true;
                lblScheduleId.Text = grvCourseSchedule.GetFocusedRowCellValue("schedule_id").ToString();

                cmbScheduleType.SelectedValue = grvCourseSchedule.GetFocusedRowCellValue("schedule_type");
                cmbStudio.Text = grvCourseSchedule.GetFocusedRowCellValue("studio_tag").ToString();

                txtLink.Text = grvCourseSchedule.GetFocusedRowCellValue("link").ToString();

                dtpValidDate.Value = DateTime.Parse(grvCourseSchedule.GetRowCellValue(focused_id, "valid_date").ToString());
                dtpExpireDate.Value = DateTime.Parse(grvCourseSchedule.GetRowCellValue(focused_id, "expire_date").ToString());

            }
            btn_flag = null;
        }

        private void cmbCourse_SelectedIndexChanged(object sender, EventArgs e)
        {
            //
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (cmbStudio.Text == "" || cmbStudio.Text == "--Chọn--")
            {
                XtraMessageBox.Show("Chọn đối tác cần tạo lịch", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                btn_flag = "create";
                btnBrowseImage.Enabled = true;
                cmbScheduleType.Enabled = true;
                cmbScheduleType.SelectedIndex = 0;
                dtpExpireDate.Enabled = true;
                dtpExpireDate.Value = DateTime.Today;
                dtpValidDate.Enabled = true;
                dtpValidDate.Value = DateTime.Today;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (cmbStudio.Text == "" || cmbStudio.Text == "--Chọn--")
            {
                XtraMessageBox.Show("Chọn đối tác cần sửa lịch", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                btn_flag = "edit";
                cmbScheduleType.Enabled = true;
                dtpExpireDate.Enabled = true;
                dtpValidDate.Enabled = true;
                cmbStudio.Enabled = false;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lblScheduleId.Text == "" || lblScheduleId.Text == "schedule_id")
            {
                XtraMessageBox.Show("Chọn lịch dịch vụ cần xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa lịch dịch vụ '" + txtLink.Text + "' ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM course_schedule WHERE schedule_id = " + int.Parse(lblScheduleId.Text), conn);
                        cmd.ExecuteNonQuery();

                        // Xóa trong folder
                        File.Delete(Path.Combine(Application.StartupPath, txtLink.Text));

                        XtraMessageBox.Show("Xóa thành công!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_CourseSchedule.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        ShowFullInfo();
                        ShowFirstInfo();
                        lblRowCount.Text = grvCourseSchedule.RowCount.ToString();
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_operation_CourseSchedule_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (cmbStudio.Text == "" || cmbStudio.Text == "--Chọn--" || cmbScheduleType.Text == "" || cmbScheduleType.Text == "--Chọn--" || txtLink.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng nhập đủ thông tin!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dtpExpireDate.Value < dtpValidDate.Value)
                {
                    XtraMessageBox.Show("Thời gian lịch không hợp lệ!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    //Tạo folder của đối tác

                    //Tên file
                    string fileName = Path.GetFileName(txtLink.Text);

                    // Tạo đường dẫn tới thư mục của đối tác
                    string documentPath = Path.Combine(pictureMainFolder, cmbStudio.Text);

                    //Đường dẫn đầy đủ có tên file
                    string fullPath = Path.Combine(documentPath, fileName);

                    //Đường dẫn để lưu vào CSDL
                    string insertPath = Path.Combine("_pictures/Course_Schedules", cmbStudio.Text, fileName);

                    try
                    {
                        conn.Open();

                        // Kiểm trả đường dẫn trên đã tồn tại chưa
                        if (Directory.Exists(documentPath) == false)
                        {
                            Directory.CreateDirectory(documentPath); //Tạo thư mục Documents
                        }

                        // insert into database
                        cmd = new SqlCommand("a_insert_tb_course_schedule", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudio.SelectedValue.ToString());
                        cmd.Parameters.Add("@schedule_type", SqlDbType.NVarChar).Value = cmbScheduleType.SelectedValue.ToString();
                        cmd.Parameters.Add("@valid_date", SqlDbType.Date).Value = dtpValidDate.Value;
                        cmd.Parameters.Add("@expire_date", SqlDbType.Date).Value = dtpExpireDate.Value;
                        cmd.Parameters.Add("@link", SqlDbType.NVarChar).Value = insertPath;
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("LƯU THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        File.Copy(txtLink.Text, fullPath); //Copy file
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_CourseSchedule.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;

                        ShowFullInfo();
                        ShowFirstInfo();

                        lblRowCount.Text = grvCourseSchedule.RowCount.ToString();
                    }

                }
            }

            if (btn_flag == "edit")
            {
                if (lblScheduleId.Text == "" || lblScheduleId.Text == "schedule_id")
                {
                    XtraMessageBox.Show("Chọn lịch dịch vụ cần sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (cmbStudio.Text == "" || cmbStudio.Text == "--Chọn--" || cmbScheduleType.Text == "" || cmbScheduleType.Text == "--Chọn--" || txtLink.Text == "")
                    {
                        XtraMessageBox.Show("Vui lòng nhập đủ thông tin!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if (dtpExpireDate.Value < dtpValidDate.Value)
                    {
                        XtraMessageBox.Show("Thời gian lịch không hợp lệ!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        try
                        {

                            conn.Open();
                            cmd = new SqlCommand("b_update_tb_course_schedule", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@schedule_type", SqlDbType.NVarChar).Value = cmbScheduleType.SelectedValue.ToString();
                            cmd.Parameters.Add("@valid_date", SqlDbType.Date).Value = dtpValidDate.Value;
                            cmd.Parameters.Add("@expire_date", SqlDbType.Date).Value = dtpExpireDate.Value;
                            cmd.Parameters.Add("@schedule_id", SqlDbType.BigInt).Value = int.Parse(lblScheduleId.Text);
                            cmd.ExecuteNonQuery();
                            XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            ShowFirstInfo();
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_CourseSchedule.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;

                            // Show data grid view
                            ShowFullInfo();
                            ShowFirstInfo();

                            lblRowCount.Text = grvCourseSchedule.RowCount.ToString();
                        }
                    }
                }
            }
        }

        private void btnShowFile_Click(object sender, EventArgs e)
        {
            if (txtLink.Text == "")
            {
                XtraMessageBox.Show("Tài liệu không tồn tại!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string path = Path.Combine(Application.StartupPath, txtLink.Text);
                popform_PictureViewer frm = new popform_PictureViewer(path);
                frm.ShowDialog();
            }
        }

        private void btnBrowseImage_Click(object sender, EventArgs e)
        {
            // Chọn file
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Pictures files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png)|*.jpg; *.jpeg; *.jpe; *.jfif; *.png|JPG files (*.jpg)|*.jpg|JPEG files (*.jpeg)|*.jpeg|JFIF files (*.jfif)|*.jfif|PNG files (*.png)|*.png";
            ofd.FilterIndex = 1;
            ofd.RestoreDirectory = true; //Lấy giá trị path mở file
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtLink.Text = ofd.FileName; // HIển thị đường dẫn lên textbox (CONTRACT)

                string path = ofd.FileName;

                //Mở form PDF viewer
                popform_PictureViewer frm = new popform_PictureViewer(path);
                frm.ShowDialog();
            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            //
        }

        private void Pdoc_PrintPage(object sender, PrintPageEventArgs e)
        {
            //
        }
    }
}
