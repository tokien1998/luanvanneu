﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using System.Data.SqlClient;
using System.Configuration;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_operation_Issues : UserControl
    {
        // Khai báo biến
        private static usctr_operation_Issues usctr; // User control
        string btn_flag = null; // Biến xác định nút đang sử dụng

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Thuộc tính
        public static usctr_operation_Issues UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_operation_Issues();
                }
                return usctr;
            }
        }

        public usctr_operation_Issues()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void label25_Click(object sender, EventArgs e)
        {
            //
        }

        private void ComboboxStudio()
        {
            string query = "SELECT 0 as studio_id, N'--Chọn--' as tag UNION ALL SELECT studio_id, tag FROM studio ORDER BY studio_id";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbStudio.DisplayMember = "tag";
            cmbStudio.ValueMember = "studio_id";
            cmbStudio.DataSource = dt;
            cmbStudio.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void ComboboxStatus()
        {
            string query = "SELECT N'--Chọn--' as status UNION ALL SELECT N'Đang chờ' UNION ALL SELECT N'Đang xử lý' UNION ALL SELECT N'Đã xử lý' UNION ALL SELECT N'Hủy bỏ'";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbStatus.DisplayMember = "status";
            cmbStatus.ValueMember = "status";
            cmbStatus.DataSource = dt;
            cmbStatus.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void ComboboxType()
        {
            string query = "SELECT N'--Chọn--' as type UNION ALL SELECT N'Dịch vụ' UNION ALL SELECT N'Hoạt động' UNION ALL SELECT N'Hợp tác' UNION ALL SELECT N'Khác'";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbIssueType.DisplayMember = "type";
            cmbIssueType.ValueMember = "type";
            cmbIssueType.DataSource = dt;
            cmbIssueType.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void ComboboxPriority()
        {
            string query = "SELECT N'--Chọn--' as priority UNION ALL SELECT N'Ưu tiên cao' UNION ALL SELECT N'Ưu tiên' UNION ALL SELECT N'Không ưu tiên'";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbPriority.DisplayMember = "priority";
            cmbPriority.ValueMember = "priority";
            cmbPriority.DataSource = dt;
            cmbPriority.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void ComboboxEmployee()
        {
            string query = "SELECT 0 as emp_id, N'--Chọn--' as emp_name UNION ALL SELECT emp_id, emp_name FROM employee ORDER BY emp_id";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbEmp.DisplayMember = "emp_name";
            cmbEmp.ValueMember = "emp_id";
            cmbEmp.DataSource = dt;
            cmbEmp.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void DefaultFormat()
        {
            cmbStudio.Enabled = false;
            cmbStatus.Enabled = false;
            cmbPriority.Enabled = false;
            cmbIssueType.Enabled = false;
            cmbEmp.Enabled = false;
            txtTitle.Enabled = false;
            txtTitle.ReadOnly = true;
            txtDesc.Enabled = false;
            txtDesc.ReadOnly = true;
            txtReceivedBy.Enabled = false;
            txtReceivedBy.ReadOnly = true;
            dtpIssueDate.Value = DateTime.Today;
            grvIssue.Columns["issue_date"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            grvIssue.Columns["issue_date"].DisplayFormat.FormatString = "dd/MM/yyyy";
            chkFilter.Checked = false;

            dtpDateFrom.Enabled = false;
            dtpDateFrom.Value = DateTime.Today;
            dtpDateTo.Enabled = false;
            dtpDateTo.Value = DateTime.Today;
        }

        private void SetCaption()
        {
            string[] columnCaptions = { "Mã bản ghi", "Đối tác", "Tiêu đề", "Mô tả", "Loại vấn đề", "Mức độ ưu tiên", "Trạng thái", "Ngày tiếp nhận", "Đơn vị tiếp nhận", "Phụ trách" };
            power.SetCaption(grvIssue, columnCaptions);
        }

        private void ShowFullInfo()
        {
            string tableName = @"(SELECT i.id, s.tag, i.title, i.description, i.issue_type, i.priority, i.status, i.issue_date, i.received_by, e.emp_name
                                    FROM issues i 
                                    LEFT JOIN studio s ON (i.studio_id = s.studio_id)
                                    LEFT JOIN employee e ON (i.assignee_id = e.emp_id) ) a";
            string selectedColumns = "id, tag, title, description, issue_type, priority, status, issue_date, received_by, emp_name";
            string afterWhereStr = " (1=1) ORDER BY id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrIssue.DataSource = dt;
            SetCaption();
        }

        private void ShowFirstInfo()
        {
            if (grvIssue.RowCount == 0)
            {
                lblID.Text = "";
                cmbEmp.SelectedIndex = 0;
                cmbIssueType.SelectedIndex = 0;
                cmbPriority.SelectedIndex = 0;
                cmbStatus.SelectedIndex = 0;
                cmbStudio.SelectedIndex = 0;
                txtTitle.ResetText();
                txtDesc.ResetText();
                txtReceivedBy.ResetText();
                dtpIssueDate.Value = DateTime.Today;
                return;
            }
            else
            {
                lblID.Text = grvIssue.GetRowCellValue(0, "id").ToString();
                cmbEmp.Text = grvIssue.GetRowCellValue(0, "emp_name").ToString();
                cmbIssueType.SelectedValue = grvIssue.GetRowCellValue(0, "issue_type").ToString();
                cmbPriority.SelectedValue = grvIssue.GetRowCellValue(0, "priority").ToString();
                cmbStatus.SelectedValue = grvIssue.GetRowCellValue(0, "status").ToString();
                cmbStudio.Text = grvIssue.GetRowCellValue(0, "tag").ToString();
                txtTitle.Text = grvIssue.GetRowCellValue(0, "title").ToString();
                txtDesc.Text = grvIssue.GetRowCellValue(0, "description").ToString();
                txtReceivedBy.Text = grvIssue.GetRowCellValue(0, "received_by").ToString();
                dtpIssueDate.Value = DateTime.Parse(grvIssue.GetRowCellValue(0, "issue_date").ToString());

            }
        }

        private void usctr_operation_Issues_Load(object sender, EventArgs e)
        {
            btn_flag = null;
            ComboboxEmployee();
            ComboboxPriority();
            ComboboxStatus();
            ComboboxStudio();
            ComboboxType();
            grctrIssue.DataSource = null;
            ShowFullInfo();
            ShowFirstInfo();
            DefaultFormat();
            lblRowCount.Text = grvIssue.RowCount.ToString();

        }

        private void grvIssue_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvIssue.RowCount == 0)
            {
                lblID.Text = "";
                cmbEmp.SelectedIndex = 0;
                cmbIssueType.SelectedIndex = 0;
                cmbPriority.SelectedIndex = 0;
                cmbStatus.SelectedIndex = 0;
                cmbStudio.SelectedIndex = 0;
                txtTitle.ResetText();
                txtDesc.ResetText();
                txtReceivedBy.ResetText();
                dtpIssueDate.Value = DateTime.Today;
                return;
            }
            else
            {
                int focused_id = grvIssue.FocusedRowHandle;
                lblID.Text = grvIssue.GetFocusedRowCellValue("id").ToString();
                cmbEmp.Text = grvIssue.GetFocusedRowCellValue("emp_name").ToString();
                cmbIssueType.SelectedValue = grvIssue.GetFocusedRowCellValue("issue_type").ToString();
                cmbPriority.SelectedValue = grvIssue.GetFocusedRowCellValue("priority").ToString();
                cmbStatus.SelectedValue = grvIssue.GetFocusedRowCellValue("status").ToString();
                cmbStudio.Text = grvIssue.GetFocusedRowCellValue("tag").ToString();
                txtTitle.Text = grvIssue.GetFocusedRowCellValue("title").ToString();
                txtDesc.Text = grvIssue.GetFocusedRowCellValue("description").ToString();
                txtReceivedBy.Text = grvIssue.GetFocusedRowCellValue("received_by").ToString();
                dtpIssueDate.Value = DateTime.Parse(grvIssue.GetRowCellValue(focused_id, "issue_date").ToString());
                btn_flag = null;
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            btn_flag = "create";
            chkFilter.Checked = false;
            lblID.Text = "Auto";
            cmbStudio.SelectedIndex = 0;
            cmbStudio.Enabled = true;
            cmbStatus.SelectedIndex = 1; // Đang chờ
            cmbStatus.Enabled = false;
            cmbPriority.SelectedIndex = 0;
            cmbPriority.Enabled = true;
            cmbIssueType.SelectedIndex = 0;
            cmbIssueType.Enabled = true;
            cmbEmp.SelectedIndex = 0;
            cmbEmp.Enabled = false;
            txtTitle.ResetText();
            txtTitle.Enabled = true;
            txtTitle.ReadOnly = false;
            txtDesc.ResetText();
            txtDesc.Enabled = true;
            txtDesc.ReadOnly = false;
            txtReceivedBy.ResetText();
            txtReceivedBy.Enabled = true;
            txtReceivedBy.ReadOnly = false;
            dtpIssueDate.Enabled = true;
            dtpIssueDate.Value = DateTime.Today;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            //chkFilter.Checked = false;
            btn_flag = "edit";
            if (lblID.Text == "" || lblID.Text == "ID")
            {
                XtraMessageBox.Show("Chọn một bản ghi để chỉnh sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                cmbStudio.Enabled = true;
                cmbStatus.Enabled = true;
                cmbPriority.Enabled = true;
                cmbIssueType.Enabled = true;
                cmbEmp.Enabled = true;
                txtTitle.Enabled = true;
                txtTitle.ReadOnly = false;
                txtDesc.Enabled = true;
                txtDesc.ReadOnly = false;
                txtReceivedBy.Enabled = true;
                txtReceivedBy.ReadOnly = false;
                dtpIssueDate.Enabled = true;
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_operation_Issues_Load(sender, e);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lblID.Text == "" || lblID.Text == "ID")
            {
                XtraMessageBox.Show("Chọn một bản ghi để xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa bản ghi: " + lblID.Text + " ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM issue WHERE id = " + int.Parse(lblID.Text), conn);
                        cmd.ExecuteNonQuery();

                        XtraMessageBox.Show("XÓA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_Issues.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        usctr_operation_Issues_Load(sender, e);
                    }
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (cmbStudio.Text == "" || cmbStudio.Text == "--Chọn--" || txtTitle.Text == "" || txtDesc.Text == "" || txtReceivedBy.Text == "" || cmbIssueType.Text == "" || cmbIssueType.Text == "--Chọn--" || cmbPriority.Text == "" || cmbPriority.Text == "--Chọn--")
                {
                    XtraMessageBox.Show("Vui lòng điền đủ thông tin", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        conn.Open();

                        cmd = new SqlCommand("a_insert_tb_issues", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudio.SelectedValue.ToString());
                        cmd.Parameters.Add("@title", SqlDbType.NVarChar, 50).Value = txtTitle.Text;
                        cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = txtDesc.Text;
                        cmd.Parameters.Add("@issue_date", SqlDbType.Date).Value = dtpIssueDate.Value;
                        cmd.Parameters.Add("@issue_type", SqlDbType.NVarChar, 50).Value = cmbIssueType.SelectedValue.ToString();
                        cmd.Parameters.Add("@priority", SqlDbType.NVarChar, 50).Value = cmbPriority.SelectedValue.ToString();
                        cmd.Parameters.Add("@received_by", SqlDbType.NVarChar, 50).Value = txtReceivedBy.Text;
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("TẠO THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_Issues.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;

                        usctr_operation_Issues_Load(sender, e);

                        lblRowCount.Text = grvIssue.RowCount.ToString();
                    }
                }
            }



            if (btn_flag == "edit")
            {
                if (cmbStudio.Text == "" || cmbStudio.Text == "--Chọn--" || txtTitle.Text == "" || txtDesc.Text == "" || txtReceivedBy.Text == "" || cmbIssueType.Text == "" || cmbIssueType.Text == "--Chọn--" || cmbPriority.Text == "" || cmbPriority.Text == "--Chọn--" || cmbStatus.Text == "" || cmbStatus.Text == "--Chọn--" || cmbEmp.Text == "" || cmbEmp.Text == "--Chọn--")
                {
                    XtraMessageBox.Show("Vui lòng điền đủ thông tin", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    try
                    {
                        conn.Open();

                        cmd = new SqlCommand("b_update_tb_issues", conn);
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudio.SelectedValue.ToString());
                        cmd.Parameters.Add("@title", SqlDbType.NVarChar, 50).Value = txtTitle.Text;
                        cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = txtDesc.Text;
                        cmd.Parameters.Add("@issue_date", SqlDbType.Date).Value = dtpIssueDate.Value;
                        cmd.Parameters.Add("@issue_type", SqlDbType.NVarChar, 50).Value = cmbIssueType.SelectedValue.ToString();
                        cmd.Parameters.Add("@priority", SqlDbType.NVarChar, 50).Value = cmbPriority.SelectedValue.ToString();
                        cmd.Parameters.Add("@received_by", SqlDbType.NVarChar, 50).Value = txtReceivedBy.Text;
                        cmd.Parameters.Add("@id", SqlDbType.BigInt).Value = int.Parse(lblID.Text);
                        cmd.Parameters.Add("@status", SqlDbType.NVarChar, 50).Value = cmbStatus.SelectedValue.ToString();
                        cmd.Parameters.Add("@assignee_id", SqlDbType.Int).Value = int.Parse(cmbEmp.SelectedValue.ToString());
                        cmd.ExecuteNonQuery();
                        XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_Issues.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;

                        usctr_operation_Issues_Load(sender, e);

                        lblRowCount.Text = grvIssue.RowCount.ToString();
                    }
                }
            }
        }

        private void chkFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (chkFilter.Checked == true)
            {
                cmbStudio.SelectedIndex = 0;
                cmbStudio.Enabled = true;
                cmbStatus.SelectedIndex = 0;
                cmbStatus.Enabled = true;
                cmbIssueType.SelectedIndex = 0;
                cmbIssueType.Enabled = true;
                cmbPriority.SelectedIndex = 0;
                cmbPriority.Enabled = true;
                cmbEmp.SelectedIndex = 0;
                cmbEmp.Enabled = true;
                txtTitle.Text = "--Tất cả--";
                txtTitle.Enabled = true;
                txtTitle.ReadOnly = false;
                txtDesc.Text = "--Tất cả--";
                txtDesc.Enabled = true;
                txtDesc.ReadOnly = false;
                txtReceivedBy.Text = "--Tất cả--";
                txtReceivedBy.Enabled = true;
                txtReceivedBy.ReadOnly = false;
                dtpIssueDate.Enabled = false;
                dtpDateFrom.Enabled = true;
                dtpDateTo.Enabled = true;
            }
            else
            {
                usctr_operation_Issues_Load(sender, e);
            }
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            if (chkFilter.Checked == false)
            {
                XtraMessageBox.Show("Chưa chọn chế độ lọc", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string status_vl, title_vl, type_vl, priority_vl, desc_vl, received_vl, dateFrom, dateTo, studioId_vl, emp_vl;

                if (cmbStudio.Text == "--Chọn--" || cmbStudio.Text == "")
                {
                    studioId_vl = "--Chọn--";
                }
                else
                {
                    studioId_vl = cmbStudio.SelectedValue.ToString();
                }

                if (cmbStatus.Text == "--Chọn--" || cmbStatus.Text == "")
                {
                    status_vl = "--Chọn--";
                }
                else
                {
                    status_vl = cmbStatus.SelectedValue.ToString();
                }

                if (cmbEmp.Text == "--Chọn--" || cmbEmp.Text == "")
                {
                    emp_vl = "--Chọn--";
                }
                else
                {
                    emp_vl = cmbEmp.SelectedValue.ToString();
                }

                if (cmbPriority.Text == "--Chọn--" || cmbPriority.Text == "")
                {
                    priority_vl = "--Chọn--";
                }
                else
                {
                    priority_vl = cmbPriority.SelectedValue.ToString();
                }

                if (cmbIssueType.Text == "--Chọn--" || cmbIssueType.Text == "")
                {
                    type_vl = "--Chọn--";
                }
                else
                {
                    type_vl = cmbIssueType.SelectedValue.ToString();
                }
                dateFrom = dtpDateFrom.Value.ToString();
                dateTo = dtpDateTo.Value.ToString();
                title_vl = txtTitle.Text;
                desc_vl = txtDesc.Text;
                received_vl = txtReceivedBy.Text;


                try
                {
                    conn.Open();
                    grctrIssue.DataSource = null;
                    dt = new DataTable();
                    da = new SqlDataAdapter("d_show_filter_issues", conn);
                    da.SelectCommand.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand.Parameters.Add("@studio_id", SqlDbType.NVarChar).Value = studioId_vl;
                    da.SelectCommand.Parameters.Add("@status", SqlDbType.NVarChar).Value = status_vl;
                    da.SelectCommand.Parameters.Add("@title", SqlDbType.NVarChar).Value = title_vl;
                    da.SelectCommand.Parameters.Add("@type", SqlDbType.NVarChar).Value = type_vl;
                    da.SelectCommand.Parameters.Add("@priority", SqlDbType.NVarChar).Value = priority_vl;
                    da.SelectCommand.Parameters.Add("@description", SqlDbType.NVarChar).Value = desc_vl;
                    da.SelectCommand.Parameters.Add("@received", SqlDbType.NVarChar).Value = received_vl;
                    da.SelectCommand.Parameters.Add("@assignee", SqlDbType.NVarChar).Value = emp_vl;
                    da.SelectCommand.Parameters.Add("@dateFrom", SqlDbType.NVarChar).Value = dateFrom;
                    da.SelectCommand.Parameters.Add("@dateTo", SqlDbType.NVarChar).Value = dateTo;
                    da.Fill(dt);
                    grctrIssue.DataSource = dt;
                    SetCaption();
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_Issues.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                    lblRowCount.Text = grvIssue.RowCount.ToString();
                }
            }
        }
    }
}
