﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;
using DevExpress.XtraEditors;
using System.IO;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_operation_StudioOffDate : UserControl
    {
        // Khai báo biến
        private static usctr_operation_StudioOffDate usctr; // User control
        string btn_flag = null; // Biến xác định nút đang sử dụng

        // Biến data
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Khai báo class đa năng
        PowerQuery power = new PowerQuery();

        SqlDataAdapter da = new SqlDataAdapter();
        SqlCommand cmd = new SqlCommand();
        DataTable dt = new DataTable();

        // Biến dùng cho Pictures
        string pictureMainFolder = Path.Combine(Application.StartupPath, "_pictures/Off_Dates"); // Đường dẫn folder chứa file ảnh (chưa có folder của từng đối tác)

        // Thuộc tính
        public static usctr_operation_StudioOffDate UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_operation_StudioOffDate();
                }
                return usctr;
            }
        }

        public usctr_operation_StudioOffDate()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        private void ComboboxStudio()
        {
            string query = "SELECT 0 as studio_id, N'--Chọn--' as tag UNION ALL SELECT studio_id, tag FROM studio ORDER BY studio_id";
            da = new SqlDataAdapter(query, conn);
            dt = new DataTable();
            da.Fill(dt);
            cmbStudio.DisplayMember = "tag";
            cmbStudio.ValueMember = "studio_id";
            cmbStudio.DataSource = dt;
            cmbStudio.DropDownStyle = ComboBoxStyle.DropDownList;
            conn.Close();
        }

        private void cmbStudio_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowFullInfo();
            lblRowCount.Text = grvStudioOffDate.RowCount.ToString();
            ShowFirstInfo();
            conn.Close();
        }

        private void DefaultFormat()
        {
            cmbStudio.Enabled = true;
            //cmbStudio.SelectedIndex = 0;
            dtpStartDate.Value = DateTime.Today;
            dtpStartDate.Enabled = false;
            dtpEndDate.Value = DateTime.Today;
            dtpEndDate.Enabled = false;
            txtLink.Enabled = false;
            txtLink.ReadOnly = true;
            txtReason.Enabled = false;
            txtReason.ReadOnly = true;
            btnBrowseImage.Enabled = false;
            grvStudioOffDate.Columns["start_date"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            grvStudioOffDate.Columns["start_date"].DisplayFormat.FormatString = "dd/MM/yyyy";
            grvStudioOffDate.Columns["end_date"].DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            grvStudioOffDate.Columns["end_date"].DisplayFormat.FormatString = "dd/MM/yyyy";
            btnRemoveImage.Enabled = false;
        }

        private void SetCaption()
        {
            string[] columnCaptions = {"Mã lịch nghỉ", "Đối tác", "Ngày bắt đầu", "Ngày kết thúc", "Lý do", "Đường dẫn ảnh" };
            power.SetCaption(grvStudioOffDate, columnCaptions);
        }

        private void ShowFullInfo()
        {
            string studio_id = cmbStudio.SelectedValue.ToString();

            string tableName = @"(SELECT o.id, s.tag as studio_tag, o.start_date, o.end_date, o.reason, o.link 
                                    FROM studio_off_date o LEFT JOIN studio s ON (o.studio_id = s.studio_id)
                                    WHERE s.studio_id = " + studio_id + ") a";
            string selectedColumns = "id, studio_tag, start_date, end_date, reason, link";
            string afterWhereStr = " (1=1) ORDER BY id";

            dt = power.ShowFullData(tableName, selectedColumns, afterWhereStr);
            grctrStudioOffDate.DataSource = dt;
            SetCaption();
        }

        private void ShowFirstInfo()
        {
            if (grvStudioOffDate.RowCount == 0)
            {
                dtpStartDate.Value = DateTime.Today;
                dtpEndDate.Value = DateTime.Today;

                txtLink.ResetText();
                txtReason.ResetText();

                lblID.Text = "";
                return;
            }
            else
            {
                lblID.Text = grvStudioOffDate.GetRowCellValue(0, "id").ToString();

                cmbStudio.Text = grvStudioOffDate.GetRowCellValue(0, "studio_tag").ToString();

                txtLink.Text = grvStudioOffDate.GetRowCellValue(0, "link").ToString();
                txtReason.Text = grvStudioOffDate.GetRowCellValue(0, "reason").ToString();

                dtpStartDate.Value = DateTime.Parse(grvStudioOffDate.GetRowCellValue(0, "start_date").ToString());
                dtpEndDate.Value = DateTime.Parse(grvStudioOffDate.GetRowCellValue(0, "end_date").ToString());

            }
        }

        private void usctr_operation_StudioOffDate_Load(object sender, EventArgs e)
        {
            btn_flag = null;
            ComboboxStudio();
            grctrStudioOffDate.DataSource = null;
            ShowFullInfo();
            ShowFirstInfo();
            DefaultFormat();
            lblRowCount.Text = grvStudioOffDate.RowCount.ToString();
            //lblID.Text = "";
        }

        private void grvStudioOffDate_FocusedRowChanged(object sender, DevExpress.XtraGrid.Views.Base.FocusedRowChangedEventArgs e)
        {
            if (grvStudioOffDate.RowCount == 0)
            {
                dtpStartDate.Value = DateTime.Today;
                dtpEndDate.Value = DateTime.Today;

                txtLink.ResetText();
                txtReason.ResetText();

                lblID.Text = "";
                return;
            }
            else
            {
                int focused_id = grvStudioOffDate.FocusedRowHandle;
                lblID.Text = grvStudioOffDate.GetFocusedRowCellValue("id").ToString();

                cmbStudio.Text = grvStudioOffDate.GetFocusedRowCellValue("studio_tag").ToString();

                txtLink.Text = grvStudioOffDate.GetFocusedRowCellValue("link").ToString();
                txtReason.Text = grvStudioOffDate.GetFocusedRowCellValue("reason").ToString();

                dtpStartDate.Value = DateTime.Parse(grvStudioOffDate.GetRowCellValue(focused_id, "start_date").ToString());
                dtpEndDate.Value = DateTime.Parse(grvStudioOffDate.GetRowCellValue(focused_id, "end_date").ToString());

                if (grvStudioOffDate.GetFocusedRowCellValue("link").ToString() != "")
                {
                    btnRemoveImage.Enabled = true;
                }
                else
                {
                    btnRemoveImage.Enabled = false;
                }

            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            if (cmbStudio.Text == "" || cmbStudio.Text == "--Chọn--")
            {
                XtraMessageBox.Show("Chọn đối tác cần tạo lịch nghỉ", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                btn_flag = "create";
                txtReason.Enabled = true;
                txtReason.ReadOnly = false;
                txtReason.ResetText();
                btnBrowseImage.Enabled = true;
                dtpStartDate.Enabled = true;
                dtpStartDate.Value = DateTime.Today;
                dtpEndDate.Enabled = true;
                dtpEndDate.Value = DateTime.Today;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            if (cmbStudio.Text == "" || cmbStudio.Text == "--Chọn--")
            {
                XtraMessageBox.Show("Chọn đối tác cần sửa lịch nghỉ", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                btn_flag = "edit";
                dtpStartDate.Enabled = true;
                dtpEndDate.Enabled = true;
                cmbStudio.Enabled = false;
                txtReason.Enabled = true;
                txtReason.ReadOnly = false;
                btnBrowseImage.Enabled = true;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lblID.Text == "" || lblID.Text == "id")
            {
                XtraMessageBox.Show("Chọn lịch dịch nghỉ cần xóa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (XtraMessageBox.Show("Xóa lịch nghỉ'" + lblID.Text + "' của " + cmbStudio.Text + " ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    try
                    {
                        conn.Open();
                        cmd = new SqlCommand("DELETE FROM studio_off_date WHERE id = " + int.Parse(lblID.Text), conn);
                        cmd.ExecuteNonQuery();

                        // Xóa trong folder
                        File.Delete(Path.Combine(Application.StartupPath, txtLink.Text));

                        XtraMessageBox.Show("Xóa thành công!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    catch (Exception ex)
                    {
                        XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_StudioOffDate.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    finally
                    {
                        conn.Close();
                        btn_flag = null;
                        ShowFullInfo();
                        ShowFirstInfo();
                        lblRowCount.Text = grvStudioOffDate.RowCount.ToString();
                    }
                }
            }
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            usctr_operation_StudioOffDate_Load(sender, e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (btn_flag == "create")
            {
                if (cmbStudio.Text == "" || cmbStudio.Text == "--Chọn--" || txtReason.Text == "")
                {
                    XtraMessageBox.Show("Vui lòng nhập đủ thông tin!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (dtpEndDate.Value < dtpStartDate.Value)
                {
                    XtraMessageBox.Show("Thời gian lịch không hợp lệ!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (txtLink.Text != "")
                    {
                        //Tạo folder của đối tác

                        //Tên file
                        string fileName = Path.GetFileName(txtLink.Text);

                        // Tạo đường dẫn tới thư mục của đối tác
                        string documentPath = Path.Combine(pictureMainFolder, cmbStudio.Text);

                        //Đường dẫn đầy đủ có tên file
                        string fullPath = Path.Combine(documentPath, fileName);

                        //Đường dẫn để lưu vào CSDL
                        string insertPath = Path.Combine("_pictures/Off_Dates", cmbStudio.Text, fileName);

                        try
                        {
                            conn.Open();

                            // Kiểm trả đường dẫn trên đã tồn tại chưa
                            if (Directory.Exists(documentPath) == false)
                            {
                                Directory.CreateDirectory(documentPath); //Tạo thư mục Documents
                            }

                            // insert into database
                            cmd = new SqlCommand("a_insert_tb_studio_off_date", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudio.SelectedValue.ToString());
                            cmd.Parameters.Add("@start_date", SqlDbType.Date).Value = dtpStartDate.Value;
                            cmd.Parameters.Add("@end_date", SqlDbType.Date).Value = dtpEndDate.Value;
                            cmd.Parameters.Add("@reason", SqlDbType.NVarChar).Value = txtReason.Text;
                            cmd.Parameters.Add("@link", SqlDbType.NVarChar).Value = insertPath;
                            cmd.ExecuteNonQuery();

                            XtraMessageBox.Show("LƯU THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            File.Copy(txtLink.Text, fullPath); //Copy file
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_CourseSchedule.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;

                            ShowFullInfo();
                            DefaultFormat();
                            ShowFirstInfo();

                            lblRowCount.Text = grvStudioOffDate.RowCount.ToString();
                        }
                    }
                    else
                    {
                        try
                        {
                            conn.Open();

                            // insert into database
                            cmd = new SqlCommand("a_insert_tb_studio_off_date", conn);
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.Add("@studio_id", SqlDbType.Int).Value = int.Parse(cmbStudio.SelectedValue.ToString());
                            cmd.Parameters.Add("@start_date", SqlDbType.Date).Value = dtpStartDate.Value;
                            cmd.Parameters.Add("@end_date", SqlDbType.Date).Value = dtpEndDate.Value;
                            cmd.Parameters.Add("@reason", SqlDbType.NVarChar).Value = txtReason.Text;
                            cmd.Parameters.Add("@link", SqlDbType.NVarChar).Value = txtLink.Text;
                            cmd.ExecuteNonQuery();

                            XtraMessageBox.Show("LƯU THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        catch (Exception ex)
                        {
                            XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_CourseSchedule.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        finally
                        {
                            conn.Close();
                            btn_flag = null;

                            ShowFullInfo();
                            DefaultFormat();
                            ShowFirstInfo();

                            lblRowCount.Text = grvStudioOffDate.RowCount.ToString();
                        }
                    }
                }
            }

            if (btn_flag == "edit")
            {
                if (lblID.Text == "" || lblID.Text == "id")
                {
                    XtraMessageBox.Show("Chọn lịch nghỉ cần sửa", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (cmbStudio.Text == "" || cmbStudio.Text == "--Chọn--" || txtReason.Text == "")
                    {
                        XtraMessageBox.Show("Vui lòng nhập đủ thông tin!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else if (dtpEndDate.Value < dtpStartDate.Value)
                    {
                        XtraMessageBox.Show("Thời gian lịch không hợp lệ!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        conn.Open();
                        cmd = new SqlCommand("SELECT link FROM studio_off_date WHERE id = " + int.Parse(lblID.Text), conn);
                        var result = cmd.ExecuteScalar();
                        if (result.ToString() == "" || result == null) // Chưa có link
                        {
                            if (txtLink.Text == "")
                            {
                                try
                                {

                                    // insert into database
                                    cmd = new SqlCommand("b_update_tb_studio_off_date", conn);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = int.Parse(lblID.Text);
                                    cmd.Parameters.Add("@start_date", SqlDbType.Date).Value = dtpStartDate.Value;
                                    cmd.Parameters.Add("@end_date", SqlDbType.Date).Value = dtpEndDate.Value;
                                    cmd.Parameters.Add("@reason", SqlDbType.NVarChar).Value = txtReason.Text;
                                    cmd.Parameters.Add("@link", SqlDbType.NVarChar).Value = txtLink.Text;
                                    cmd.ExecuteNonQuery();

                                    XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                catch (Exception ex)
                                {
                                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_CourseSchedule.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                finally
                                {
                                    conn.Close();
                                    btn_flag = null;

                                    ShowFullInfo();
                                    DefaultFormat();
                                    ShowFirstInfo();

                                    lblRowCount.Text = grvStudioOffDate.RowCount.ToString();
                                }
                            }
                            else
                            {
                                //Tạo folder của đối tác

                                //Tên file
                                string fileName = Path.GetFileName(txtLink.Text);

                                // Tạo đường dẫn tới thư mục của đối tác
                                string documentPath = Path.Combine(pictureMainFolder, cmbStudio.Text);

                                //Đường dẫn đầy đủ có tên file
                                string fullPath = Path.Combine(documentPath, fileName);

                                //Đường dẫn để lưu vào CSDL
                                string insertPath = Path.Combine("_pictures/Off_Dates", cmbStudio.Text, fileName);

                                try
                                {

                                    // Kiểm trả đường dẫn trên đã tồn tại chưa
                                    if (Directory.Exists(documentPath) == false)
                                    {
                                        Directory.CreateDirectory(documentPath); //Tạo thư mục Documents
                                    }

                                    // insert into database
                                    cmd = new SqlCommand("b_update_tb_studio_off_date", conn);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = int.Parse(lblID.Text);
                                    cmd.Parameters.Add("@start_date", SqlDbType.Date).Value = dtpStartDate.Value;
                                    cmd.Parameters.Add("@end_date", SqlDbType.Date).Value = dtpEndDate.Value;
                                    cmd.Parameters.Add("@reason", SqlDbType.NVarChar).Value = txtReason.Text;
                                    cmd.Parameters.Add("@link", SqlDbType.NVarChar).Value = insertPath;
                                    cmd.ExecuteNonQuery();

                                    XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    File.Copy(txtLink.Text, fullPath); //Copy file
                                }
                                catch (Exception ex)
                                {
                                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_CourseSchedule.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                finally
                                {
                                    conn.Close();
                                    btn_flag = null;

                                    ShowFullInfo();
                                    DefaultFormat();
                                    ShowFirstInfo();

                                    lblRowCount.Text = grvStudioOffDate.RowCount.ToString();
                                }
                            }
                        }
                        else // Đã có link
                        {
                            if (XtraMessageBox.Show("Xác nhận xóa file cũ và ghi đè lên bản ghi đã tồn tại: '" + result.ToString() + "'  ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                //Xóa file cũ trong folder
                                File.Delete(Path.Combine(Application.StartupPath, result.ToString()));

                                // Lưu giá trị vào CSDL

                                //Tạo folder của đối tác

                                //Tên file
                                string fileName = Path.GetFileName(txtLink.Text);

                                // Tạo đường dẫn tới thư mục của đối tác
                                string documentPath = Path.Combine(pictureMainFolder, cmbStudio.Text);

                                //Đường dẫn đầy đủ có tên file
                                string fullPath = Path.Combine(documentPath, fileName);

                                //Đường dẫn để lưu vào CSDL
                                string insertPath = Path.Combine("_pictures/Off_Dates", cmbStudio.Text, fileName);

                                try
                                {

                                    // Kiểm trả đường dẫn trên đã tồn tại chưa
                                    if (Directory.Exists(documentPath) == false)
                                    {
                                        Directory.CreateDirectory(documentPath); //Tạo thư mục Documents
                                    }

                                    // insert into database
                                    cmd = new SqlCommand("b_update_tb_studio_off_date", conn);
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = int.Parse(lblID.Text);
                                    cmd.Parameters.Add("@start_date", SqlDbType.Date).Value = dtpStartDate.Value;
                                    cmd.Parameters.Add("@end_date", SqlDbType.Date).Value = dtpEndDate.Value;
                                    cmd.Parameters.Add("@reason", SqlDbType.NVarChar).Value = txtReason.Text;
                                    cmd.Parameters.Add("@link", SqlDbType.NVarChar).Value = insertPath;
                                    cmd.ExecuteNonQuery();

                                    XtraMessageBox.Show("SỬA THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                    File.Copy(txtLink.Text, fullPath); //Copy file
                                }
                                catch (Exception ex)
                                {
                                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_CourseSchedule.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                finally
                                {
                                    conn.Close();
                                    btn_flag = null;

                                    ShowFullInfo();
                                    DefaultFormat();
                                    ShowFirstInfo();

                                    lblRowCount.Text = grvStudioOffDate.RowCount.ToString();
                                }
                            }
                        }
                    }
                }
            }
        }

        private void btnShowFile_Click(object sender, EventArgs e)
        {
            if (txtLink.Text == "")
            {
                XtraMessageBox.Show("Tài liệu không tồn tại!", "CẢNH BÁO", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                string path = Path.Combine(Application.StartupPath, txtLink.Text);
                popform_PictureViewer frm = new popform_PictureViewer(path);
                frm.ShowDialog();
            }
        }

        private void btnBrowseImage_Click(object sender, EventArgs e)
        {
            // Chọn file
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Pictures files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png)|*.jpg; *.jpeg; *.jpe; *.jfif; *.png|JPG files (*.jpg)|*.jpg|JPEG files (*.jpeg)|*.jpeg|JFIF files (*.jfif)|*.jfif|PNG files (*.png)|*.png";
            ofd.FilterIndex = 1;
            ofd.RestoreDirectory = true; //Lấy giá trị path mở file
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtLink.Text = ofd.FileName; // HIển thị đường dẫn lên textbox (CONTRACT)

                string path = ofd.FileName;

                //Mở form PDF viewer
                popform_PictureViewer frm = new popform_PictureViewer(path);
                frm.ShowDialog();
            }
        }

        private void btnRemoveImage_Click(object sender, EventArgs e)
        {
            if (XtraMessageBox.Show("Xác nhận xóa file: '" + Path.GetFileName(txtLink.Text) + "'  ?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                //Xóa file cũ trong folder
                File.Delete(Path.Combine(Application.StartupPath, txtLink.Text));

                //Xóa CSDL
                try
                {
                    conn.Open();

                    // insert into database
                    cmd = new SqlCommand("b_update_tb_studio_off_date", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = int.Parse(lblID.Text);
                    cmd.Parameters.Add("@start_date", SqlDbType.Date).Value = dtpStartDate.Value;
                    cmd.Parameters.Add("@end_date", SqlDbType.Date).Value = dtpEndDate.Value;
                    cmd.Parameters.Add("@reason", SqlDbType.NVarChar).Value = txtReason.Text;
                    cmd.Parameters.Add("@link", SqlDbType.NVarChar).Value = "";
                    cmd.ExecuteNonQuery();

                    XtraMessageBox.Show("XÓA ẢNH THÀNH CÔNG!", "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                catch (Exception ex)
                {
                    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: usctr_operation_CourseSchedule.cs (Line: x - x).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    conn.Close();
                    btn_flag = null;

                    ShowFullInfo();
                    DefaultFormat();
                    ShowFirstInfo();

                    lblRowCount.Text = grvStudioOffDate.RowCount.ToString();
                }
            }
        }

    }
}
