﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraEditors;

namespace LVTN_ToTrongKien_11162625
{
    public partial class PreSplashForm : DevExpress.XtraEditors.XtraForm
    {
        int i = 0;

        public PreSplashForm()
        {
            InitializeComponent();
        }

        private void PreSplashForm_Load(object sender, EventArgs e)
        {
            timer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (i >= 41)
            {
                timer.Stop();
                SplashForm frm = new SplashForm();
                frm.Show();
                this.Hide();
            }

            i++;
        }
    }
}