﻿using DevExpress.XtraEditors;
using System;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;

namespace LVTN_ToTrongKien_11162625
{
    public partial class frmMain : DevExpress.XtraBars.FluentDesignSystem.FluentDesignForm
    {
        // Khai báo biến
        string role_code, emp_name;

        // Biến xác định hiệu suất hệ thống
        PerformanceCounter cpuCounter = new PerformanceCounter("Processor", "% Processor Time", "_Total");
        PerformanceCounter ramCounter = new PerformanceCounter("Memory", "Available MBytes");

        DialogResult result = DialogResult.No;
        private bool flag_show = true; // flag đánh dấu việc hiện MessageBox - mặc định hiện
        private bool isLogout = false; // Xác định hành vi Logout hay Direct Closing - mặc định Direct Closing

        public frmMain(string role_code, string emp_name)
        {
            InitializeComponent();
            this.role_code = role_code;
            this.emp_name = emp_name;
        }


        private void timer_Tick(object sender, EventArgs e)
        {
            // ...
        }

        // Sự kiện thoát chương trình (X - close)
        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Nếu logout thì kết thúc phương thức bằng yes - show (false)
            if (isLogout == true)
            {
                flag_show = false;
                result = DialogResult.Yes;
            }

            //Trường hợp tắt trực tiếp ứng dụng
            if (flag_show == true)
            {
                result = XtraMessageBox.Show("Bạn chắc chắn muốn thoát không?", " XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                flag_show = false;
            }

            if (result == DialogResult.No)
            {
                e.Cancel = true;
                flag_show = true;
            }
            else
            {
                return;
            }
        }

        private void frmMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            // Kiểm tra loại trường hợp tắt form
            if (isLogout == true)
            {
                return; // Ko làm gì để show form login
            }
            else
            {
                Application.Exit();
            }
        }

        // Nút đăng xuất
        private void ace_system_LogOut_Click(object sender, EventArgs e)
        {
            result = XtraMessageBox.Show("Bạn chắc chắn muốn đăng xuất không?", "XÁC NHẬN", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.Yes)
            {
                isLogout = true;
                this.Close(); // frmLogin show dialog ra frmMain nên chỉ cần close
            }
            else
            {
                return;
            }
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            // Hiển thị thời gian thực + user đang đăng nhập
            timerMain.Start();
            barToday.Caption = " | Ngày: " + DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString();
            barUser.Caption = " | Tài khoản: " + emp_name;

            // Phân quyền
            Set_Roles();
        }

        // Hiện các user forms
        private void ace_lists_CityDistrict_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_lists_CityDistrict.UserControl))
            {
                panelMain.Controls.Add(usctr_lists_CityDistrict.UserControl);
                usctr_lists_CityDistrict.UserControl.Dock = DockStyle.Fill;
                usctr_lists_CityDistrict.UserControl.BringToFront();
            }
            else
            {
                usctr_lists_CityDistrict.UserControl.BringToFront();
            }
        }

        private void timer_Tick_1(object sender, EventArgs e) //SỰ kiện timerMain_Tick
        {
            // Hiển thị thời gian thực
            barTime.Caption = " | Thời gian: " + DateTime.Now.ToLongTimeString();
            //barCPU.Caption = "Hiệu suất CPU: " + cpuCounter.NextValue() + "% |";
            //barMemory.Caption = "Dung lượng RAM: " +ramCounter.NextValue() + "MB |";
            lblCPU.Text = Math.Round(cpuCounter.NextValue(), 2) + " %";
            lblRAM.Text = Math.Round(ramCounter.NextValue(), 2) +  " MB";
        }

        private void ace_lists_Bank_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_lists_Bank.UserControl))
            {
                panelMain.Controls.Add(usctr_lists_Bank.UserControl);
                usctr_lists_Bank.UserControl.Dock = DockStyle.Fill;
                usctr_lists_Bank.UserControl.BringToFront();
            }
            else
            {
                usctr_lists_Bank.UserControl.BringToFront();
            }
        }

        private void ace_lists_Category_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_lists_Category.UserControl))
            {
                panelMain.Controls.Add(usctr_lists_Category.UserControl);
                usctr_lists_Category.UserControl.Dock = DockStyle.Fill;
                usctr_lists_Category.UserControl.BringToFront();
            }
            else
            {
                usctr_lists_Category.UserControl.BringToFront();
            }
        }

        private void ace_lists_Amenity_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_lists_Amenity.UserControl))
            {
                panelMain.Controls.Add(usctr_lists_Amenity.UserControl);
                usctr_lists_Amenity.UserControl.Dock = DockStyle.Fill;
                usctr_lists_Amenity.UserControl.BringToFront();
            }
            else
            {
                usctr_lists_Amenity.UserControl.BringToFront();
            }
        }

        private void ace_lists_Studio_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_lists_Studio.UserControl))
            {
                panelMain.Controls.Add(usctr_lists_Studio.UserControl);
                usctr_lists_Studio.UserControl.Dock = DockStyle.Fill;
                usctr_lists_Studio.UserControl.BringToFront();
            }
            else
            {
                usctr_lists_Studio.UserControl.BringToFront();
            }
        }

        private void ace_lists_ContractForm_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_lists_ContractForm.UserControl))
            {
                panelMain.Controls.Add(usctr_lists_ContractForm.UserControl);
                usctr_lists_ContractForm.UserControl.Dock = DockStyle.Fill;
                usctr_lists_ContractForm.UserControl.BringToFront();
            }
            else
            {
                usctr_lists_ContractForm.UserControl.BringToFront();
            }
        }

        private void ace_services_Courses_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_services_Courses.UserControl))
            {
                panelMain.Controls.Add(usctr_services_Courses.UserControl);
                usctr_services_Courses.UserControl.Dock = DockStyle.Fill;
                usctr_services_Courses.UserControl.BringToFront();
            }
            else
            {
                usctr_services_Courses.UserControl.BringToFront();
            }
        }

        private void ace_operation_PartnerRegister_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_operation_PartnerRegister.UserControl))
            {
                panelMain.Controls.Add(usctr_operation_PartnerRegister.UserControl);
                usctr_operation_PartnerRegister.UserControl.Dock = DockStyle.Fill;
                usctr_operation_PartnerRegister.UserControl.BringToFront();
            }
            else
            {
                usctr_operation_PartnerRegister.UserControl.BringToFront();
            }
        }

        private void ace_operation_Issues_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_operation_Issues.UserControl))
            {
                panelMain.Controls.Add(usctr_operation_Issues.UserControl);
                usctr_operation_Issues.UserControl.Dock = DockStyle.Fill;
                usctr_operation_Issues.UserControl.BringToFront();
            }
            else
            {
                usctr_operation_Issues.UserControl.BringToFront();
            }
        }

        private void ace_services_Schedule_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_operation_CourseSchedule.UserControl))
            {
                panelMain.Controls.Add(usctr_operation_CourseSchedule.UserControl);
                usctr_operation_CourseSchedule.UserControl.Dock = DockStyle.Fill;
                usctr_operation_CourseSchedule.UserControl.BringToFront();
            }
            else
            {
                usctr_operation_CourseSchedule.UserControl.BringToFront();
            }
        }

        private void ace_services_OffDate_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_operation_StudioOffDate.UserControl))
            {
                panelMain.Controls.Add(usctr_operation_StudioOffDate.UserControl);
                usctr_operation_StudioOffDate.UserControl.Dock = DockStyle.Fill;
                usctr_operation_StudioOffDate.UserControl.BringToFront();
            }
            else
            {
                usctr_operation_StudioOffDate.UserControl.BringToFront();
            }
        }

        private void ace_payment_StudioBank_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_payment_StudioBank.UserControl))
            {
                panelMain.Controls.Add(usctr_payment_StudioBank.UserControl);
                usctr_payment_StudioBank.UserControl.Dock = DockStyle.Fill;
                usctr_payment_StudioBank.UserControl.BringToFront();
            }
            else
            {
                usctr_payment_StudioBank.UserControl.BringToFront();
            }
        }

        private void ace_tools_ImportReservation_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_tools_ImportReservation.UserControl))
            {
                panelMain.Controls.Add(usctr_tools_ImportReservation.UserControl);
                usctr_tools_ImportReservation.UserControl.Dock = DockStyle.Fill;
                usctr_tools_ImportReservation.UserControl.BringToFront();
            }
            else
            {
                usctr_tools_ImportReservation.UserControl.BringToFront();
            }
        }

        private void ace_payment_ReservationLog_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_payment_ReservationLog.UserControl))
            {
                panelMain.Controls.Add(usctr_payment_ReservationLog.UserControl);
                usctr_payment_ReservationLog.UserControl.Dock = DockStyle.Fill;
                usctr_payment_ReservationLog.UserControl.BringToFront();
            }
            else
            {
                usctr_payment_ReservationLog.UserControl.BringToFront();
            }
        }

        private void ace_payment_PaymentLog_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_payment_PaymentLog.UserControl))
            {
                panelMain.Controls.Add(usctr_payment_PaymentLog.UserControl);
                usctr_payment_PaymentLog.UserControl.Dock = DockStyle.Fill;
                usctr_payment_PaymentLog.UserControl.BringToFront();
            }
            else
            {
                usctr_payment_PaymentLog.UserControl.BringToFront();
            }
        }

        private void ace_system_employees_List_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_system_Employee.UserControl))
            {
                panelMain.Controls.Add(usctr_system_Employee.UserControl);
                usctr_system_Employee.UserControl.Dock = DockStyle.Fill;
                usctr_system_Employee.UserControl.BringToFront();
            }
            else
            {
                usctr_system_Employee.UserControl.BringToFront();
            }
        }

        private void ace_system_employees_CreateRole_Click(object sender, EventArgs e)
        {
            if (!panelMain.Controls.Contains(usctr_system_Role.UserControl))
            {
                panelMain.Controls.Add(usctr_system_Role.UserControl);
                usctr_system_Role.UserControl.Dock = DockStyle.Fill;
                usctr_system_Role.UserControl.BringToFront();
            }
            else
            {
                usctr_system_Role.UserControl.BringToFront();
            }

        }

        private void ace_system_employees_AccRole_Click(object sender, EventArgs e)
        {
            //usctr_system_EmpRole
            if (!panelMain.Controls.Contains(usctr_system_EmpRole.UserControl))
            {
                panelMain.Controls.Add(usctr_system_EmpRole.UserControl);
                usctr_system_EmpRole.UserControl.Dock = DockStyle.Fill;
                usctr_system_EmpRole.UserControl.BringToFront();
            }
            else
            {
                usctr_system_EmpRole.UserControl.BringToFront();
            }

        }

        private void ace_system_About_Click(object sender, EventArgs e)
        {
            //usctr_About
            if (!panelMain.Controls.Contains(usctr_About.UserControl))
            {
                panelMain.Controls.Add(usctr_About.UserControl);
                usctr_About.UserControl.Dock = DockStyle.Fill;
                usctr_About.UserControl.BringToFront();
            }
            else
            {
                usctr_About.UserControl.BringToFront();
            }
        }

        private void ace_tools_CreateReport_Click(object sender, EventArgs e)
        {
            //usctr_tools_CreateReport
            if (!panelMain.Controls.Contains(usctr_tools_CreateReport.UserControl))
            {
                panelMain.Controls.Add(usctr_tools_CreateReport.UserControl);
                usctr_tools_CreateReport.UserControl.Dock = DockStyle.Fill;
                usctr_tools_CreateReport.UserControl.BringToFront();
            }
            else
            {
                usctr_tools_CreateReport.UserControl.BringToFront();
            }

        }

        // Phân quyền người dùng
        private void Set_Roles()
        {
            if (role_code == "MANAGER")
            {
                ace_system_employees_CreateRole.Visible = false;
            }

            else if (role_code == "FIN-EMP")
            {
                ace_system_Employees.Visible = false;
            }

            else if (role_code == "EMP")
            {
                ace_system_Employees.Visible = false;
                ace_Payment.Visible = false;
            }

            else
            {
                return;
            }
        }
    }
}
