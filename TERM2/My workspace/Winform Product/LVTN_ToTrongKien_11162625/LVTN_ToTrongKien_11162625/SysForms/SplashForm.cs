﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace LVTN_ToTrongKien_11162625
{
    public partial class SplashForm : DevExpress.XtraEditors.XtraForm
    {
        public SplashForm()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            panel_main.Width += 2;
            if (panel_main.Width >= 606)
            {
                timer1.Stop();
                frmLogin frm = new frmLogin();
                frm.Show();
                this.Close();
            }
        }

        private void SplashForm_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void SplashForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            // ...
        }
    }
}