﻿using DevExpress.XtraEditors;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace LVTN_ToTrongKien_11162625
{
    public partial class frmLogin : DevExpress.XtraEditors.XtraForm
    {
        // Khai báo biến kết nối CSDL
        private SqlConnection conn;
        string str = ConfigurationManager.ConnectionStrings["onaclover"].ConnectionString;

        // Biến số lần đăng nhập sai tối đa
        int turns = 3;

        // Biến public
        public int emp_id;
        public string username, emp_name, emp_email, emp_position, role_code, role_comment;

        public frmLogin()
        {
            InitializeComponent();
            conn = new SqlConnection(str);
        }

        // Căn giữa ô đăng nhập
        private void frmLogin_Resize(object sender, EventArgs e)
        {
            int x = (this.Width - panLoginArea.Width) / 2;
            int y = (this.Height - panLoginArea.Height) / 2;
            panLoginArea.Location = new Point(x, y);
        }

        // Ô nhập liệu mặc định
        private void Default_TextBox()
        {
            txtUser.Text = " Nhập tài khoản";
            txtUser.Font = new Font(txtUser.Font, FontStyle.Italic);
            txtUser.ForeColor = Color.DarkGray;

            txtPass.Text = " Nhập mật khẩu";
            txtPass.Font = new Font(txtPass.Font, FontStyle.Italic);
            txtPass.ForeColor = Color.DarkGray;
        }

        // Hiệu ứng trên textbox khi click + nhập
        private void txtUser_Enter(object sender, EventArgs e)
        {
            if (txtUser.Text == " Nhập tài khoản")
            {
                txtUser.ResetText();
                txtUser.Font = new Font(txtUser.Font, FontStyle.Regular);
                txtUser.ForeColor = Color.White;
            }
        }

        private void txtUser_Leave(object sender, EventArgs e)
        {
            if (txtUser.Text == string.Empty)
            {
                txtUser.Font = new Font(txtUser.Font, FontStyle.Italic);
                txtUser.ForeColor = Color.DarkGray;
                txtUser.Text = " Nhập tài khoản";
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            //...
        }

        private void frmLogin_FormClosed_1(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void frmLogin_FormClosing_1(object sender, FormClosingEventArgs e)
        {

        }

        // Hiệu ứng trên textbox khi click + nhập
        private void txtPass_Enter(object sender, EventArgs e)
        {
            if (txtPass.Text == " Nhập mật khẩu")
            {
                txtPass.ResetText();
                txtPass.Font = new Font(txtPass.Font, FontStyle.Regular);
                txtPass.UseSystemPasswordChar = true;
                txtPass.ForeColor = Color.White;
            }
        }

        private void txtPass_Leave(object sender, EventArgs e)
        {
            if (txtPass.Text == string.Empty)
            {
                txtPass.Font = new Font(txtPass.Font, FontStyle.Italic);
                txtPass.ForeColor = Color.DarkGray;
                txtPass.UseSystemPasswordChar = false;
                txtPass.Text = " Nhập mật khẩu";
            }
        }

        // Chức năng ẩn/hiện mật khẩu
        private void picShow_MouseDown(object sender, MouseEventArgs e)
        {
            txtPass.UseSystemPasswordChar = false;
        }

        private void picShow_MouseUp(object sender, MouseEventArgs e)
        {
            txtPass.UseSystemPasswordChar = true;
        }

        // Kiểm tra đăng nhập
        private void btnLogin_Click(object sender, EventArgs e)
        {
            // Kiểm tra nhập liệu
            if (txtUser.Text == " Nhập tài khoản" || txtUser.Text == string.Empty || txtPass.Text == " Nhập mật khẩu" || txtPass.Text == string.Empty)
            {
                XtraMessageBox.Show("Vui lòng nhập tài khoản và mật khẩu!", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Default_TextBox();
                return;
            }

            conn.Close();

            //Kiểm tra tài khoản và số lần đăng nhập sai
            //try
            //{
                conn.Open();

                //Kiểm tra số lần đăng nhập sai
                if (turns == 0)
                {
                    XtraMessageBox.Show("Đăng nhập sai quá 3 lần! \nHãy khởi động lại chương trình!", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }
                else
                {
                    // Chạy thủ tục thực hiện kiểm tra tài khoản
                    SqlCommand cmd = new SqlCommand("d_show_login_info", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@username", SqlDbType.VarChar, 50).Value = txtUser.Text;
                    cmd.Parameters.Add("@password", SqlDbType.VarChar, 50).Value = txtPass.Text;
                    SqlDataReader dr = cmd.ExecuteReader();

                    if (dr.HasRows) // Có kết quả - Đăng nhập đúng
                    {
                        // Đọc dữ liệu trả về
                        dr.Read();

                        //Thứ tự select: a.username, e.emp_id, e.emp_name, e.emp_email, e.position, r.role_code
                        username = dr[0].ToString();
                        emp_id = int.Parse(dr[1].ToString());
                        emp_name = dr[2].ToString();
                        emp_email = dr[3].ToString();
                        emp_position = dr[4].ToString();
                        role_code = dr[5].ToString();
                        role_comment = dr[6].ToString();

                        // Hiển thị thông báo
                        XtraMessageBox.Show("THÔNG TIN TÀI KHOẢN (" + username + ") \n \n - Tên nhân viên: " + emp_name + " \n - Mã nhân viên: " + emp_id + " \n - Email: " + emp_email + " \n - Vị trí: " + emp_position + " \n - Quyền truy cập: " + role_code + " - " + role_comment, "THÔNG BÁO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        // Hiện Main Menu (CT chỉ hiện 1 form và ko tạo thêm form trùng mới khi quay ngược hành vi)
                        frmMain frm = new frmMain(role_code, emp_name);
                        this.Hide();
                        frm.ShowDialog();
                        this.Show();
                    }
                    else
                    {
                        XtraMessageBox.Show("Sai tài khoản, mật khẩu hoặc tài khoản chưa được cấp quyền truy cập! \n \n Bạn còn " + (turns - 1).ToString() + " lượt đăng nhập sai!", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Default_TextBox();
                        turns -= 1;
                        return;
                    }
                }
//            }
            //catch (Exception ex)
            //{
            //    XtraMessageBox.Show(ex.Message + "\n \n \nLocation: frmLogin.cs (Line: 138 - 190).", "LỖI", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
            //finally
            //{
            //    conn.Close();
            //}
        }
    }
}
