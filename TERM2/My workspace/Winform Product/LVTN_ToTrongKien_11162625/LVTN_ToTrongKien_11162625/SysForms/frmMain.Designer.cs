﻿namespace LVTN_ToTrongKien_11162625
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.accordionControl1 = new DevExpress.XtraBars.Navigation.AccordionControl();
            this.ace_Lists = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_lists_CityDistrict = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_lists_Bank = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_lists_Category = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_lists_Amenity = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_lists_Studio = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_services_Courses = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_lists_ContractForm = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_Operation = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_operation_PartnerRegister = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_operation_Issues = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_services_Schedule = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_services_OffDate = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_Payment = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_payment_StudioBank = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_payment_PaymentLog = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_payment_ReservationLog = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_Tools = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_tools_ImportReservation = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_tools_CreateReport = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_System = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_system_Employees = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_system_employees_List = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_system_employees_CreateRole = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_system_employees_AccRole = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_system_About = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.ace_system_LogOut = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            this.fluentDesignFormControl1 = new DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl();
            this.barTimeIsNow = new DevExpress.XtraBars.BarStaticItem();
            this.barUser = new DevExpress.XtraBars.BarStaticItem();
            this.barToday = new DevExpress.XtraBars.BarStaticItem();
            this.barTime = new DevExpress.XtraBars.BarStaticItem();
            this.barCPU = new DevExpress.XtraBars.BarStaticItem();
            this.panelMain = new DevExpress.XtraEditors.PanelControl();
            this.timerMain = new System.Windows.Forms.Timer(this.components);
            this.panelSystemUsage = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lblRAM = new System.Windows.Forms.Label();
            this.lblCPU = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ace_services_CoursePrice = new DevExpress.XtraBars.Navigation.AccordionControlElement();
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentDesignFormControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelMain)).BeginInit();
            this.panelSystemUsage.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // accordionControl1
            // 
            this.accordionControl1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("accordionControl1.BackgroundImage")));
            this.accordionControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.accordionControl1.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.ace_Lists,
            this.ace_Operation,
            this.ace_Payment,
            this.ace_Tools,
            this.ace_System});
            this.accordionControl1.Location = new System.Drawing.Point(0, 26);
            this.accordionControl1.Name = "accordionControl1";
            this.accordionControl1.ScrollBarMode = DevExpress.XtraBars.Navigation.ScrollBarMode.Fluent;
            this.accordionControl1.ShowFilterControl = DevExpress.XtraBars.Navigation.ShowFilterControl.Always;
            this.accordionControl1.Size = new System.Drawing.Size(250, 734);
            this.accordionControl1.TabIndex = 1;
            this.accordionControl1.ViewType = DevExpress.XtraBars.Navigation.AccordionControlViewType.HamburgerMenu;
            // 
            // ace_Lists
            // 
            this.ace_Lists.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.ace_lists_CityDistrict,
            this.ace_lists_Bank,
            this.ace_lists_Category,
            this.ace_lists_Amenity,
            this.ace_lists_Studio,
            this.ace_services_Courses,
            this.ace_lists_ContractForm});
            this.ace_Lists.Expanded = true;
            this.ace_Lists.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ace_Lists.ImageOptions.Image")));
            this.ace_Lists.Name = "ace_Lists";
            this.ace_Lists.Text = "DANH MỤC";
            // 
            // ace_lists_CityDistrict
            // 
            this.ace_lists_CityDistrict.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_lists_CityDistrict.ImageOptions.SvgImage")));
            this.ace_lists_CityDistrict.Name = "ace_lists_CityDistrict";
            this.ace_lists_CityDistrict.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_lists_CityDistrict.Text = "Thành phố - Quận";
            this.ace_lists_CityDistrict.Click += new System.EventHandler(this.ace_lists_CityDistrict_Click);
            // 
            // ace_lists_Bank
            // 
            this.ace_lists_Bank.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_lists_Bank.ImageOptions.SvgImage")));
            this.ace_lists_Bank.Name = "ace_lists_Bank";
            this.ace_lists_Bank.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_lists_Bank.Text = "Ngân hàng";
            this.ace_lists_Bank.Click += new System.EventHandler(this.ace_lists_Bank_Click);
            // 
            // ace_lists_Category
            // 
            this.ace_lists_Category.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_lists_Category.ImageOptions.SvgImage")));
            this.ace_lists_Category.Name = "ace_lists_Category";
            this.ace_lists_Category.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_lists_Category.Text = "Loại hình dịch vụ";
            this.ace_lists_Category.Click += new System.EventHandler(this.ace_lists_Category_Click);
            // 
            // ace_lists_Amenity
            // 
            this.ace_lists_Amenity.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_lists_Amenity.ImageOptions.SvgImage")));
            this.ace_lists_Amenity.Name = "ace_lists_Amenity";
            this.ace_lists_Amenity.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_lists_Amenity.Text = "Tiện ích";
            this.ace_lists_Amenity.Click += new System.EventHandler(this.ace_lists_Amenity_Click);
            // 
            // ace_lists_Studio
            // 
            this.ace_lists_Studio.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_lists_Studio.ImageOptions.SvgImage")));
            this.ace_lists_Studio.Name = "ace_lists_Studio";
            this.ace_lists_Studio.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_lists_Studio.Text = "Đối tác";
            this.ace_lists_Studio.Click += new System.EventHandler(this.ace_lists_Studio_Click);
            // 
            // ace_services_Courses
            // 
            this.ace_services_Courses.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_services_Courses.ImageOptions.SvgImage")));
            this.ace_services_Courses.Name = "ace_services_Courses";
            this.ace_services_Courses.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_services_Courses.Text = "Thông tin dịch vụ";
            this.ace_services_Courses.Click += new System.EventHandler(this.ace_services_Courses_Click);
            // 
            // ace_lists_ContractForm
            // 
            this.ace_lists_ContractForm.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_lists_ContractForm.ImageOptions.SvgImage")));
            this.ace_lists_ContractForm.Name = "ace_lists_ContractForm";
            this.ace_lists_ContractForm.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_lists_ContractForm.Text = "Hợp đồng - Biểu mẫu";
            this.ace_lists_ContractForm.Click += new System.EventHandler(this.ace_lists_ContractForm_Click);
            // 
            // ace_Operation
            // 
            this.ace_Operation.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.ace_operation_PartnerRegister,
            this.ace_operation_Issues,
            this.ace_services_Schedule,
            this.ace_services_OffDate});
            this.ace_Operation.Expanded = true;
            this.ace_Operation.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ace_Operation.ImageOptions.Image")));
            this.ace_Operation.Name = "ace_Operation";
            this.ace_Operation.Text = "VẬN HÀNH";
            // 
            // ace_operation_PartnerRegister
            // 
            this.ace_operation_PartnerRegister.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_operation_PartnerRegister.ImageOptions.SvgImage")));
            this.ace_operation_PartnerRegister.Name = "ace_operation_PartnerRegister";
            this.ace_operation_PartnerRegister.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_operation_PartnerRegister.Text = "Danh sách đăng ký hợp tác";
            this.ace_operation_PartnerRegister.Click += new System.EventHandler(this.ace_operation_PartnerRegister_Click);
            // 
            // ace_operation_Issues
            // 
            this.ace_operation_Issues.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_operation_Issues.ImageOptions.SvgImage")));
            this.ace_operation_Issues.Name = "ace_operation_Issues";
            this.ace_operation_Issues.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_operation_Issues.Text = "Vấn đề phát sinh";
            this.ace_operation_Issues.Click += new System.EventHandler(this.ace_operation_Issues_Click);
            // 
            // ace_services_Schedule
            // 
            this.ace_services_Schedule.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_services_Schedule.ImageOptions.SvgImage")));
            this.ace_services_Schedule.Name = "ace_services_Schedule";
            this.ace_services_Schedule.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_services_Schedule.Text = "Thời gian biểu dịch vụ";
            this.ace_services_Schedule.Click += new System.EventHandler(this.ace_services_Schedule_Click);
            // 
            // ace_services_OffDate
            // 
            this.ace_services_OffDate.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_services_OffDate.ImageOptions.SvgImage")));
            this.ace_services_OffDate.Name = "ace_services_OffDate";
            this.ace_services_OffDate.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_services_OffDate.Text = "Lịch nghỉ đối tác";
            this.ace_services_OffDate.Click += new System.EventHandler(this.ace_services_OffDate_Click);
            // 
            // ace_Payment
            // 
            this.ace_Payment.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.ace_payment_StudioBank,
            this.ace_payment_PaymentLog,
            this.ace_payment_ReservationLog});
            this.ace_Payment.Expanded = true;
            this.ace_Payment.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ace_Payment.ImageOptions.Image")));
            this.ace_Payment.Name = "ace_Payment";
            this.ace_Payment.Text = "THANH TOÁN CHI PHÍ";
            // 
            // ace_payment_StudioBank
            // 
            this.ace_payment_StudioBank.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_payment_StudioBank.ImageOptions.SvgImage")));
            this.ace_payment_StudioBank.Name = "ace_payment_StudioBank";
            this.ace_payment_StudioBank.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_payment_StudioBank.Text = "Thông tin thanh toán";
            this.ace_payment_StudioBank.Click += new System.EventHandler(this.ace_payment_StudioBank_Click);
            // 
            // ace_payment_PaymentLog
            // 
            this.ace_payment_PaymentLog.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_payment_PaymentLog.ImageOptions.SvgImage")));
            this.ace_payment_PaymentLog.Name = "ace_payment_PaymentLog";
            this.ace_payment_PaymentLog.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_payment_PaymentLog.Text = "Quản lý thanh toán";
            this.ace_payment_PaymentLog.Click += new System.EventHandler(this.ace_payment_PaymentLog_Click);
            // 
            // ace_payment_ReservationLog
            // 
            this.ace_payment_ReservationLog.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_payment_ReservationLog.ImageOptions.SvgImage")));
            this.ace_payment_ReservationLog.Name = "ace_payment_ReservationLog";
            this.ace_payment_ReservationLog.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_payment_ReservationLog.Text = "Lịch sử sử dụng dịch vụ";
            this.ace_payment_ReservationLog.Click += new System.EventHandler(this.ace_payment_ReservationLog_Click);
            // 
            // ace_Tools
            // 
            this.ace_Tools.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.ace_tools_ImportReservation,
            this.ace_tools_CreateReport});
            this.ace_Tools.Expanded = true;
            this.ace_Tools.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ace_Tools.ImageOptions.Image")));
            this.ace_Tools.Name = "ace_Tools";
            this.ace_Tools.Text = "CÔNG CỤ";
            // 
            // ace_tools_ImportReservation
            // 
            this.ace_tools_ImportReservation.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_tools_ImportReservation.ImageOptions.SvgImage")));
            this.ace_tools_ImportReservation.Name = "ace_tools_ImportReservation";
            this.ace_tools_ImportReservation.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_tools_ImportReservation.Text = "Nạp dữ liệu dùng dịch vụ";
            this.ace_tools_ImportReservation.Click += new System.EventHandler(this.ace_tools_ImportReservation_Click);
            // 
            // ace_tools_CreateReport
            // 
            this.ace_tools_CreateReport.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_tools_CreateReport.ImageOptions.SvgImage")));
            this.ace_tools_CreateReport.Name = "ace_tools_CreateReport";
            this.ace_tools_CreateReport.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_tools_CreateReport.Text = "Tạo báo cáo";
            this.ace_tools_CreateReport.Click += new System.EventHandler(this.ace_tools_CreateReport_Click);
            // 
            // ace_System
            // 
            this.ace_System.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.ace_system_Employees,
            this.ace_system_About,
            this.ace_system_LogOut});
            this.ace_System.Expanded = true;
            this.ace_System.HeaderTemplate.AddRange(new DevExpress.XtraBars.Navigation.HeaderElementInfo[] {
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.HeaderControl, DevExpress.XtraBars.Navigation.HeaderElementAlignment.Left),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.Image),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.Text),
            new DevExpress.XtraBars.Navigation.HeaderElementInfo(DevExpress.XtraBars.Navigation.HeaderElementType.ContextButtons)});
            this.ace_System.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ace_System.ImageOptions.Image")));
            this.ace_System.Name = "ace_System";
            this.ace_System.Text = "HỆ THỐNG";
            // 
            // ace_system_Employees
            // 
            this.ace_system_Employees.Elements.AddRange(new DevExpress.XtraBars.Navigation.AccordionControlElement[] {
            this.ace_system_employees_List,
            this.ace_system_employees_CreateRole,
            this.ace_system_employees_AccRole});
            this.ace_system_Employees.Expanded = true;
            this.ace_system_Employees.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_system_Employees.ImageOptions.SvgImage")));
            this.ace_system_Employees.Name = "ace_system_Employees";
            this.ace_system_Employees.Text = "Nhân viên";
            // 
            // ace_system_employees_List
            // 
            this.ace_system_employees_List.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_system_employees_List.ImageOptions.SvgImage")));
            this.ace_system_employees_List.Name = "ace_system_employees_List";
            this.ace_system_employees_List.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_system_employees_List.Text = "Danh sách nhân viên";
            this.ace_system_employees_List.Click += new System.EventHandler(this.ace_system_employees_List_Click);
            // 
            // ace_system_employees_CreateRole
            // 
            this.ace_system_employees_CreateRole.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_system_employees_CreateRole.ImageOptions.SvgImage")));
            this.ace_system_employees_CreateRole.Name = "ace_system_employees_CreateRole";
            this.ace_system_employees_CreateRole.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_system_employees_CreateRole.Text = "Tạo quyền truy cập";
            this.ace_system_employees_CreateRole.Click += new System.EventHandler(this.ace_system_employees_CreateRole_Click);
            // 
            // ace_system_employees_AccRole
            // 
            this.ace_system_employees_AccRole.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_system_employees_AccRole.ImageOptions.SvgImage")));
            this.ace_system_employees_AccRole.Name = "ace_system_employees_AccRole";
            this.ace_system_employees_AccRole.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_system_employees_AccRole.Text = "Phân quyền người dùng";
            this.ace_system_employees_AccRole.Click += new System.EventHandler(this.ace_system_employees_AccRole_Click);
            // 
            // ace_system_About
            // 
            this.ace_system_About.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("ace_system_About.ImageOptions.SvgImage")));
            this.ace_system_About.Name = "ace_system_About";
            this.ace_system_About.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_system_About.Text = "Giới thiệu phần mềm";
            this.ace_system_About.Click += new System.EventHandler(this.ace_system_About_Click);
            // 
            // ace_system_LogOut
            // 
            this.ace_system_LogOut.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("ace_system_LogOut.ImageOptions.Image")));
            this.ace_system_LogOut.Name = "ace_system_LogOut";
            this.ace_system_LogOut.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_system_LogOut.Text = "Đăng xuất";
            this.ace_system_LogOut.Click += new System.EventHandler(this.ace_system_LogOut_Click);
            // 
            // fluentDesignFormControl1
            // 
            this.fluentDesignFormControl1.FluentDesignForm = this;
            this.fluentDesignFormControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barTimeIsNow,
            this.barUser,
            this.barToday,
            this.barTime,
            this.barCPU});
            this.fluentDesignFormControl1.Location = new System.Drawing.Point(0, 0);
            this.fluentDesignFormControl1.Name = "fluentDesignFormControl1";
            this.fluentDesignFormControl1.Size = new System.Drawing.Size(1143, 26);
            this.fluentDesignFormControl1.TabIndex = 2;
            this.fluentDesignFormControl1.TabStop = false;
            this.fluentDesignFormControl1.TitleItemLinks.Add(this.barUser);
            this.fluentDesignFormControl1.TitleItemLinks.Add(this.barToday);
            this.fluentDesignFormControl1.TitleItemLinks.Add(this.barTime);
            // 
            // barTimeIsNow
            // 
            this.barTimeIsNow.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barTimeIsNow.Caption = "barStaticItem3";
            this.barTimeIsNow.Id = 2;
            this.barTimeIsNow.Name = "barTimeIsNow";
            // 
            // barUser
            // 
            this.barUser.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barUser.Caption = "barUser";
            this.barUser.Id = 1;
            this.barUser.Name = "barUser";
            // 
            // barToday
            // 
            this.barToday.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barToday.Caption = "barToday";
            this.barToday.Id = 2;
            this.barToday.Name = "barToday";
            // 
            // barTime
            // 
            this.barTime.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barTime.Caption = "barTime";
            this.barTime.Id = 3;
            this.barTime.Name = "barTime";
            // 
            // barCPU
            // 
            this.barCPU.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right;
            this.barCPU.Caption = "barCPU";
            this.barCPU.Id = 1;
            this.barCPU.Name = "barCPU";
            // 
            // panelMain
            // 
            this.panelMain.ContentImage = ((System.Drawing.Image)(resources.GetObject("panelMain.ContentImage")));
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(250, 26);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(893, 734);
            this.panelMain.TabIndex = 3;
            // 
            // timerMain
            // 
            this.timerMain.Tick += new System.EventHandler(this.timer_Tick_1);
            // 
            // panelSystemUsage
            // 
            this.panelSystemUsage.Controls.Add(this.panel1);
            this.panelSystemUsage.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSystemUsage.Location = new System.Drawing.Point(250, 727);
            this.panelSystemUsage.Name = "panelSystemUsage";
            this.panelSystemUsage.Size = new System.Drawing.Size(893, 33);
            this.panelSystemUsage.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.lblRAM);
            this.panel1.Controls.Add(this.lblCPU);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Location = new System.Drawing.Point(388, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(502, 27);
            this.panel1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Hiệu suất CPU:";
            // 
            // lblRAM
            // 
            this.lblRAM.AutoSize = true;
            this.lblRAM.Location = new System.Drawing.Point(384, 6);
            this.lblRAM.Name = "lblRAM";
            this.lblRAM.Size = new System.Drawing.Size(115, 13);
            this.lblRAM.TabIndex = 3;
            this.lblRAM.Text = "RAM available memory";
            // 
            // lblCPU
            // 
            this.lblCPU.AutoSize = true;
            this.lblCPU.Location = new System.Drawing.Point(102, 6);
            this.lblCPU.Name = "lblCPU";
            this.lblCPU.Size = new System.Drawing.Size(59, 13);
            this.lblCPU.TabIndex = 1;
            this.lblCPU.Text = "CPU usage";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(224, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Bộ nhớ RAM khả dụng:";
            // 
            // ace_services_CoursePrice
            // 
            this.ace_services_CoursePrice.Name = "ace_services_CoursePrice";
            this.ace_services_CoursePrice.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item;
            this.ace_services_CoursePrice.Text = "Quản lý giá dịch vụ";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 760);
            this.Controls.Add(this.panelSystemUsage);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.accordionControl1);
            this.Controls.Add(this.fluentDesignFormControl1);
            this.DoubleBuffered = true;
            this.FluentDesignFormControl = this.fluentDesignFormControl1;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.NavigationControl = this.accordionControl1;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PHẦN MỀM QUẢN LÝ DỊCH VỤ VÀ CHI PHÍ TRẢ ĐÔI TÁC";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.accordionControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fluentDesignFormControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelMain)).EndInit();
            this.panelSystemUsage.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraBars.Navigation.AccordionControl accordionControl1;
        private DevExpress.XtraBars.FluentDesignSystem.FluentDesignFormControl fluentDesignFormControl1;
        private DevExpress.XtraBars.BarStaticItem barTimeIsNow;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_Lists;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_lists_CityDistrict;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_lists_Bank;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_lists_Category;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_lists_Amenity;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_Payment;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_Tools;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_tools_ImportReservation;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_tools_CreateReport;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_System;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_system_Employees;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_system_employees_List;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_system_About;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_system_LogOut;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_lists_Studio;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_lists_ContractForm;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_services_Schedule;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_services_OffDate;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_Operation;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_operation_Issues;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator1;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator2;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator3;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_services_Courses;
        private DevExpress.XtraBars.Navigation.AccordionControlSeparator accordionControlSeparator4;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_payment_StudioBank;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_payment_PaymentLog;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_system_employees_CreateRole;
        private DevExpress.XtraEditors.PanelControl panelMain;
        private System.Windows.Forms.Timer timerMain;
        private DevExpress.XtraBars.BarStaticItem barUser;
        private DevExpress.XtraBars.BarStaticItem barToday;
        private DevExpress.XtraBars.BarStaticItem barTime;
        private DevExpress.XtraBars.BarStaticItem barCPU;
        private System.Windows.Forms.Panel panelSystemUsage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblRAM;
        private System.Windows.Forms.Label lblCPU;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_operation_PartnerRegister;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_services_CoursePrice;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_payment_ReservationLog;
        private DevExpress.XtraBars.Navigation.AccordionControlElement ace_system_employees_AccRole;
    }
}