﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LVTN_ToTrongKien_11162625.Report;
using LVTN_ToTrongKien_11162625.UserForms.Report_Function;

namespace LVTN_ToTrongKien_11162625
{
    public partial class usctr_tools_CreateReport : UserControl
    {
        // Khai báo biến
        private static usctr_tools_CreateReport usctr; // User control

        // Thuộc tính
        public static usctr_tools_CreateReport UserControl
        {
            get
            {
                if (usctr == null)
                {
                    usctr = new usctr_tools_CreateReport();
                }
                return usctr;
            }
        }

        public usctr_tools_CreateReport()
        {
            InitializeComponent();
        }

        private void usctr_tools_CreateReport_Load(object sender, EventArgs e)
        {
            ComboboxReportGroup();
            cmbReportGroup.SelectedIndex = 0;
            cmbReportTitle.SelectedIndex = 0;
        }

        private void ComboboxReportGroup()
        {
            cmbReportGroup.Items.Clear();
            cmbReportGroup.Items.Add("--Chọn--");
            cmbReportGroup.Items.Add("Danh mục");
            cmbReportGroup.Items.Add("Vận hành");
            cmbReportGroup.Items.Add("Thanh toán chi phí");
            cmbReportGroup.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void cmbReportGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbReportGroup.SelectedItem.ToString() == "Danh mục")
            {
                cmbReportTitle.Items.Clear();
                cmbReportTitle.Items.Add("--Chọn--");
                cmbReportTitle.Items.Add("Thông tin chi tiết"); //
                cmbReportTitle.Items.Add("Loại DV và tiện ích"); //
                cmbReportTitle.DropDownStyle = ComboBoxStyle.DropDownList;
                cmbReportTitle.SelectedIndex = 0;
            }
            else if (cmbReportGroup.SelectedItem.ToString() == "Vận hành")
            {
                cmbReportTitle.Items.Clear();
                cmbReportTitle.Items.Add("--Chọn--");
                cmbReportTitle.Items.Add("Yêu cầu hợp tác"); //
                cmbReportTitle.Items.Add("Thông tin phát sinh"); //
                cmbReportTitle.DropDownStyle = ComboBoxStyle.DropDownList;
                cmbReportTitle.SelectedIndex = 0;
            }
            else if (cmbReportGroup.SelectedItem.ToString() == "Thanh toán chi phí")
            {
                cmbReportTitle.Items.Clear();
                cmbReportTitle.Items.Add("--Chọn--");
                cmbReportTitle.Items.Add("Báo cáo chi phí tháng"); //
                cmbReportTitle.DropDownStyle = ComboBoxStyle.DropDownList;
                cmbReportTitle.SelectedIndex = 0;
            }
            else
            {
                cmbReportTitle.Items.Clear();
                cmbReportTitle.Items.Add("--Chọn--");
                cmbReportTitle.SelectedIndex = 0;
            }
        }

        private void cmbReportTitle_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Danh mục
            if (cmbReportTitle.SelectedItem.ToString() == "Thông tin chi tiết")
            {
                Show_usctr_report_StudioInfo();
            }
            
            if (cmbReportTitle.SelectedItem.ToString() == "Loại DV và tiện ích")
            {
                Show_usctr_report_CategoryAmenity();
            }

            // Vận hành
            if (cmbReportTitle.SelectedItem.ToString() == "Yêu cầu hợp tác")
            {
                Show_usctr_report_PartnerRegister();
            }

            if (cmbReportTitle.SelectedItem.ToString() == "Thông tin phát sinh")
            {
                Show_usctr_report_Issues();
            }

            // Thanh toán
            if (cmbReportTitle.SelectedItem.ToString() == "Báo cáo chi phí tháng")
            {
                Show_usctr_report_MonthlyRev();
            }

        }

        // Các hàm show user control của từng báo cáo
        private void Show_usctr_report_StudioInfo()
        {
            if (!panelMainReport.Controls.Contains(usctr_report_StudioInfo.UserControl))
            {
                panelMainReport.Controls.Add(usctr_report_StudioInfo.UserControl);
                usctr_report_StudioInfo.UserControl.Dock = DockStyle.Fill;
                usctr_report_StudioInfo.UserControl.BringToFront();
            }
            else
            {
                usctr_report_StudioInfo.UserControl.BringToFront();
            }
        }
        
        private void Show_usctr_report_CategoryAmenity()
        {
            if (!panelMainReport.Controls.Contains(usctr_report_CategoryAmenity.UserControl))
            {
                panelMainReport.Controls.Add(usctr_report_CategoryAmenity.UserControl);
                usctr_report_CategoryAmenity.UserControl.Dock = DockStyle.Fill;
                usctr_report_CategoryAmenity.UserControl.BringToFront();
            }
            else
            {
                usctr_report_CategoryAmenity.UserControl.BringToFront();
            }
        }

        //usctr_report_PartnerRegister
        private void Show_usctr_report_PartnerRegister()
        {
            if (!panelMainReport.Controls.Contains(usctr_report_PartnerRegister.UserControl))
            {
                panelMainReport.Controls.Add(usctr_report_PartnerRegister.UserControl);
                usctr_report_PartnerRegister.UserControl.Dock = DockStyle.Fill;
                usctr_report_PartnerRegister.UserControl.BringToFront();
            }
            else
            {
                usctr_report_PartnerRegister.UserControl.BringToFront();
            }
        }

        //usctr_report_Issues
        private void Show_usctr_report_Issues()
        {
            if (!panelMainReport.Controls.Contains(usctr_report_Issues.UserControl))
            {
                panelMainReport.Controls.Add(usctr_report_Issues.UserControl);
                usctr_report_Issues.UserControl.Dock = DockStyle.Fill;
                usctr_report_Issues.UserControl.BringToFront();
            }
            else
            {
                usctr_report_Issues.UserControl.BringToFront();
            }
        }

        //usctr_report_MonthlyRev
        private void Show_usctr_report_MonthlyRev()
        {
            if (!panelMainReport.Controls.Contains(usctr_report_MonthlyRev.UserControl))
            {
                panelMainReport.Controls.Add(usctr_report_MonthlyRev.UserControl);
                usctr_report_MonthlyRev.UserControl.Dock = DockStyle.Fill;
                usctr_report_MonthlyRev.UserControl.BringToFront();
            }
            else
            {
                usctr_report_MonthlyRev.UserControl.BringToFront();
            }
        }
    }
}
